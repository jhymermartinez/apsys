function updateBackground() {
	screenWidth = $(window).width();
	screenHeight = $(window).height();
	var bg = jQuery("#bg");

	// Proporcion horizontal/vertical. En este caso la imagen es cuadrada
	ratio = 1;

	if (screenWidth / screenHeight > ratio) {
		$(bg).height("auto");
		$(bg).width("100%");
	} else {
		$(bg).width("auto");
		$(bg).height("100%");
	}

	// Si a la imagen le sobra anchura, la centramos a mano
	if ($(bg).width() > 0) {
		$(bg).css('left', (screenWidth - $(bg).width()) / 2);
	}
}
$(document).ready(function() {
	// Actualizamos el fondo al cargar la pagina
	updateBackground();
	$(window).bind("resize", function() {
		// Y tambien cada vez que se redimensione el navegador
		updateBackground();
	});
});


PrimeFaces.locales['es'] = {
        closeText: 'Cerrar',
        prevText: 'Anterior',
        nextText: 'Siguiente',
        monthNames: ['Enero','Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
        dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
        dayNamesShort: ['Dom','Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
        weekHeader: 'Semana',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        timeOnlyTitle: 'Sólo hora',
        timeText: 'Tiempo',
        hourText: 'Hora',
        minuteText: 'Minuto',
        secondText: 'Segundo',
        currentText: 'Fecha actual',
        ampm: false,
        month: 'Mes',
        week: 'Semana',
        day: 'Día',
        allDayText : 'Todo el día'
};

function clean(element) {
	element.value="";
}

function extLegend() {
    this.cfg.legend= {
        show: true,
        location: 'e',
        placement: 'outsideGrid'
    };
}


var disabledDays = ['5-5-2014', '6-5-2014'];
function disableAllTheseDays(date) {
  var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
  for (i = 0; i < disabledDays.length; i++) {
      if($.inArray((m+1) + '-' + d + '-' + y,disabledDays) != -1) {
          return [false];
      }
  }
  return [true];
}

