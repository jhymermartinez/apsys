package ec.edu.unl.DAO;

import java.util.List;

import ec.edu.unl.entity.StudentCareer;
import ec.edu.unl.utilities.academic.StudentCareerUtil;


public interface StudentCareerDAO {

    public void saveStudentCareer(StudentCareer studentCareer);

    public void updateStudentCareer(StudentCareer studentCareer);
    
    public void saveOrUpdateStudentCareer(StudentCareer studentCareer);
    
    public List<StudentCareerUtil> returnListStudentCareers(long idOffer,long idStudent);
}
