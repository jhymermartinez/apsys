package ec.edu.unl.DAO;

import java.util.List;

import ec.edu.unl.entity.PersonalFactor;
import ec.edu.unl.entity._PersonalFactor;
import ec.edu.unl.entity.__PersonalFactor;


public interface PersonalFactorDAO {

	public void savePersonalFactor(PersonalFactor personalFactor);
	public void savePersonalFactor(_PersonalFactor personalFactor);
	public void savePersonalFactor(__PersonalFactor personalFactor);
	
	public void updatePersonalFactor(PersonalFactor personalFactor);
	public void updatePersonalFactor(_PersonalFactor personalFactor);
	public void updatePersonalFactor(__PersonalFactor personalFactor);
	
	public void updateAllNewPersonalFactor(long offerId, long careerId,
			long moduleId, long matterId, long StudentId,PersonalFactor pf,_PersonalFactor _pf,__PersonalFactor __pf);

	public void deletePersonalFactor(PersonalFactor personalFactor);
	public void deletePersonalFactor(_PersonalFactor personalFactor);
	public void deletePersonalFactor(__PersonalFactor personalFactor);

	public void generateDefaultPersonalFactor(long offerId, long careerId,
			long moduleId, long matterId, long StudentId);

	public void generateDefaultPersonalFactor(long offerId, long careerId,
			 long StudentId);

	public void generateDefaultPersonalFactor(long StudentId);

	public boolean existPersonalFactorForThisStudentCareer(long offerId, long careerId,long StudentId);
	
	public PersonalFactor returnPersonalFactor(long offerId, long careerId,
			long moduleId, long matterId, long StudentId);
	public _PersonalFactor returnPersonalFactor(long offerId, long careerId,long StudentId);
	public __PersonalFactor returnPersonalFactor(long StudentId);

}
