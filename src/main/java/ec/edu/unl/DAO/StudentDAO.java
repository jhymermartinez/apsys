package ec.edu.unl.DAO;

import java.util.List;
import java.util.Set;

import ec.edu.unl.entity.Career;
import ec.edu.unl.entity.Evaluation;
import ec.edu.unl.entity.Matter;
import ec.edu.unl.entity.Module;
import ec.edu.unl.entity.Student;
import ec.edu.unl.utilities.academic.TeacherUtil;

public interface StudentDAO {

	public Student returnStudentByIdentificactionCard(String identification_card);

	public Student returnStudentById(long idStudent);

	public void saveStudent(Student student);
        
        public void saveOrUpdateStudent(Student student);

	public boolean existStudent(Student student);
        public boolean existStudent(String identification_card);


	public boolean existStudentModule(String identification_card, Module module);

	public boolean existStudentCareer(String identification_card, Career career);

	public boolean existStudentMatter(String identification_card, Matter matter);

	public void updateStudent(Student student);

	public List<Student> returnAllStudents();

	

	public List<Student> returnStudentPararllelModule(String module,
			Character parallel);

	public long returnStudentId(String identification_card);
	
	public TeacherUtil returnTeacherByMatter(long idOffer, long idCareer, long idModule,
			long idMatter,  long idStudent);
	
}
