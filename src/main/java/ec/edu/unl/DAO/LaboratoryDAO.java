package ec.edu.unl.DAO;

import java.util.List;

import ec.edu.unl.entity.Laboratory;
import ec.edu.unl.utilities.laboratory.LaboratoryUtil;

public interface LaboratoryDAO {
	public void saveLaboratory(Laboratory laboratory);
	
	public void updateLaboratory(Laboratory laboratory);
	
	public void deleteLaboratory(Laboratory laboratory);

	public List<LaboratoryUtil> returnListStudentLaboratories(long offerSelect,
			long careerSelect, long moduleSelect, long idMatter, long idStudent);
}
