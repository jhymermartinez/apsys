package ec.edu.unl.DAO;

import java.util.List;

import ec.edu.unl.entity.Lesson;
import ec.edu.unl.utilities.lesson.LessonUtil;

public interface LessonDAO {
	
	public void saveLesson(Lesson lesson);
	
	public void updateLesson(Lesson lesson);

	public void deleteLesson(Lesson lesson);

	public List<LessonUtil> returnListStudentLessons(long offerSelect,
			long careerSelect, long moduleSelect, long idMatter, long idStudent);
}
