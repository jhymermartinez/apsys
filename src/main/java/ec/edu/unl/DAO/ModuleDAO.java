package ec.edu.unl.DAO;

import java.util.List;

import ec.edu.unl.entity.Module;

public interface ModuleDAO {

    public void saveModule(Module module);

    public void updateModule(Module module);

    public List<Module> findAllModules();
    
    public Module returnModuleById(long id);
    
    public boolean existModule(Integer number,String parallel);

    public long returnModuleId(Integer moduleNumber, String parallel);

}
