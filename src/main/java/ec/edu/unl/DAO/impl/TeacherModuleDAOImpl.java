package ec.edu.unl.DAO.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.TeacherModuleDAO;
import ec.edu.unl.entity.TeacherModule;

/**
 * 
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public class TeacherModuleDAOImpl implements TeacherModuleDAO {

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Override
	@Transactional
	public void saveTeacherModule(TeacherModule teacherModule) {
		hibernateTemplate.save(teacherModule);
	}

	@Override
	@Transactional
	public void updateTeacherModule(TeacherModule teacherModule) {
		hibernateTemplate.update(teacherModule);
	}

	@Override
	@Transactional
	public void saveOrUpdateTeacherModule(TeacherModule teacherModule) {
		hibernateTemplate.saveOrUpdate(teacherModule);
		
	}

	@Override
	@Transactional
	public boolean existRelationTeacherModule(String teacherIC,
			Integer moduleNumber, String parallel) {
		
		 	boolean resul;

	        Session session = hibernateTemplate.getSessionFactory()
	                .getCurrentSession();

	        Query hql = session
	                .createQuery("FROM Teacher t JOIN t.teacherModules mod WHERE t.identification_card=? AND mod.module.number=? AND mod.module.parallel=? ");

	        hql.setString(0, teacherIC);
	        hql.setInteger(1, moduleNumber);
	        hql.setString(2, parallel);
	        
	        resul = hql.uniqueResult() != null;
	        
	        return resul;
		
	}

}
