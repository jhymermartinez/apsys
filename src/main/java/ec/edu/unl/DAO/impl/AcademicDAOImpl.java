package ec.edu.unl.DAO.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.AcademicDAO;
import ec.edu.unl.DAO.CareerDAO;
import ec.edu.unl.DAO.MatterDAO;
import ec.edu.unl.DAO.MatterModuleDAO;
import ec.edu.unl.DAO.MatterOfferDAO;
import ec.edu.unl.DAO.ModuleDAO;
import ec.edu.unl.DAO.OfferDAO;
import ec.edu.unl.DAO.ReferenceDAO;
import ec.edu.unl.DAO.StudentCareerDAO;
import ec.edu.unl.DAO.StudentDAO;
import ec.edu.unl.DAO.StudentMatterDAO;
import ec.edu.unl.DAO.StudentModuleDAO;
import ec.edu.unl.DAO.TeacherCareerDAO;
import ec.edu.unl.DAO.TeacherDAO;
import ec.edu.unl.DAO.TeacherMatterDAO;
import ec.edu.unl.DAO.TeacherModuleDAO;
import ec.edu.unl.entity.Career;
import ec.edu.unl.entity.Evaluation;
import ec.edu.unl.entity.Matter;
import ec.edu.unl.entity.MatterModule;
import ec.edu.unl.entity.MatterOffer;
import ec.edu.unl.entity.Module;
import ec.edu.unl.entity.Offer;
import ec.edu.unl.entity.Reference;
import ec.edu.unl.entity.Student;
import ec.edu.unl.entity.StudentCareer;
import ec.edu.unl.entity.StudentMatter;
import ec.edu.unl.entity.StudentModule;
import ec.edu.unl.entity.Teacher;
import ec.edu.unl.entity.TeacherCareer;
import ec.edu.unl.entity.TeacherMatter;
import ec.edu.unl.entity.TeacherModule;
import ec.edu.unl.operations.Conections;
import ec.edu.unl.operations.Operations;
import ec.edu.unl.utilities.academic.ModuleStudentUtil;
import ec.edu.unl.utilities.academic.StudentMatterUtil;
import ec.edu.unl.utilities.academic.StudentQualificationUtil;
import ec.edu.unl.utilities.academic.StudentSpecificUtil;
import ec.edu.unl.utilities.academic.TeacherCareerUtil;
import ec.edu.unl.utilities.academic.TeacherModuleMatterUtil;
import ec.edu.unl.utilities.academic.TeacherModuleMatterTempUtil;
import ec.edu.unl.utilities.academic.TeacherModuleUtil;
import ec.edu.unl.utilities.academic.TeacherOfferUtil;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public class AcademicDAOImpl implements AcademicDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Autowired
    @Qualifier("student")
    Student student;

    @Autowired
    @Qualifier("operationsImpl")
    Operations operations;

    @Autowired
    @Qualifier("conectionImpl")
    Conections conectionWS;

    @Autowired
    @Qualifier("student")
    Student studentForSave;

    @Autowired
    @Qualifier("matter")
    Matter matterForSave;

    @Autowired
    @Qualifier("teacher")
    Teacher teacherForSave;

    
    @Autowired
    @Qualifier("career")
    Career careerForSave;

    @Autowired
    @Qualifier("module")
    Module moduleForSave;

    @Autowired
    @Qualifier("offer")
    Offer offerForSave;
    
    @Autowired
    @Qualifier("reference")
    Reference referenceForSave;

    // //// DAOS /////////
    @Autowired
    @Qualifier("studentDAOImpl")
    StudentDAO studentDAO;

    @Autowired
    @Qualifier("matterDAOImpl")
    MatterDAO matterDAO;

    @Autowired
    @Qualifier("moduleDAOImpl")
    ModuleDAO moduleDAO;

    @Autowired
    @Qualifier("referenceDAOImpl")
    ReferenceDAO referenceDAO;
    
  
    
    
    @Autowired
    @Qualifier("careerDAOImpl")
    CareerDAO careerDAO;

    @Autowired
    @Qualifier("offerDAOImpl")
    OfferDAO offerDAO;

    @Autowired
    @Qualifier("teacherDAOImpl")
    TeacherDAO teacherDAO;

    @Autowired
    @Qualifier("matterModuleDAOImpl")
    MatterModuleDAO matterModuleDAO;

    @Autowired
    @Qualifier("matterOfferDAOImpl")
    MatterOfferDAO matterOfferDAO;

    @Autowired
    @Qualifier("studentMatterDAOImpl")
    StudentMatterDAO studentMatterDAO;

    @Autowired
    @Qualifier("studentModuleDAOImpl")
    StudentModuleDAO studentModuleDAO;

    @Autowired
    @Qualifier("studentCareerDAOImpl")
    StudentCareerDAO studentCareerDAO;

    @Autowired
    @Qualifier("teacherMatterDAOImpl")
    TeacherMatterDAO teacherMatterDAO;

    @Autowired
    @Qualifier("teacherModuleDAOImpl")
    TeacherModuleDAO teacherModuleDAO;

    @Autowired
    @Qualifier("teacherCareerDAOImpl")
    TeacherCareerDAO teacherCareerDAO;

    // //////////////
    @Autowired
    @Qualifier("studentCareer")
    StudentCareer studentCareerForSave;

    @Autowired
    @Qualifier("studentMatter")
    StudentMatter studentMatterForSave;

    @Autowired
    @Qualifier("studentModule")
    StudentModule studentModuleForSave;

    @Autowired
    @Qualifier("teacherMatter")
    TeacherMatter teacherMatterForSave;

    @Autowired
    @Qualifier("teacherModule")
    TeacherModule teacherModuleForSave;

    @Autowired
    @Qualifier("teacherCareer")
    TeacherCareer teacherCareerForSave;

    @Autowired
    @Qualifier("matterModule")
    MatterModule matterModuleForSave;
    
    @Autowired
    @Qualifier("matterOffer")
    MatterOffer matterOfferForSave;
    
  
	@Override
	@Transactional
	public List<TeacherOfferUtil> returnListTeacherOffer(
			String identification_card) {
		List<TeacherOfferUtil> listTO = null;
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		
	    Query hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.TeacherOfferUtil("
	                		+ "t.identification_card,offe.id,offe.description) "
	                        + "FROM Teacher t, Offer offe "
	                        + "WHERE t.identification_card=? ");
	        
	     hql.setString(0, identification_card);
	        
	     listTO = hql.list();
	     
	     
		return listTO;
	}

	@Override
	@Transactional
	public List<TeacherCareerUtil> returnListTeacherCareer(
			String identification_card) {
		List<TeacherCareerUtil> listTC = null;
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		
	    Query hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.TeacherCareerUtil("
	                		+ "t.identification_card,car.career.id,car.career.name) "
	                        + "FROM Teacher t "
	                        + "JOIN t.teacherCareers car "
	                        + "WHERE t.identification_card=? ");
	        
	     hql.setString(0, identification_card);
	        
	     listTC = hql.list();
		return listTC;
	}

	@Override
	@Transactional
	public List<TeacherModuleUtil> returnListTeacherModule(
			String identification_card) {
		List<TeacherModuleUtil> listTM = null;
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		
	    Query hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.TeacherModuleUtil("
	                		+ "t.identification_card,mod.module.id,mod.module.number,mod.module.parallel) "
	                        + "FROM Teacher t "
	                        + "JOIN t.teacherModules mod "
	                        + "WHERE t.identification_card=? ");
	        
	     hql.setString(0, identification_card);
	        
	     listTM = hql.list();
		
	     return listTM;
	}
    

    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<TeacherModuleMatterUtil> returnTeacherModuleMatter(
            String identificationCard,long idOffer,long idCareer,long idModule) {
        long idTeacher = teacherDAO.returnTeacherId(identificationCard);
        
    	Teacher teacher = (Teacher) hibernateTemplate.get(Teacher.class, idTeacher);
    	
    	List<TeacherModuleMatterUtil> listNew = new ArrayList<TeacherModuleMatterUtil>();
        List<TeacherModuleMatterTempUtil> list1 = null;
//        List<TeacherModuleMatterTempUtil> list2 = null;
//        List<TeacherModuleMatterTempUtil> list3 = null;
//        List<TeacherModuleMatterTempUtil> list4 = null;
        
        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

//        Query hql = session
//                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.TeacherModuleMatterTempUtil(t.identification_card,car.Career.id,per.period.id,mod.module.id,mat.matter.id) "
//                        + "FROM Teacher t JOIN t.teacherMatters mat JOIN mat.matter.matterPeriods per JOIN mat.matter.matterModules mod JOIN t.teacherCareers car "
//                        + "WHERE t.identification_card=?");
         
      Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.TeacherModuleMatterTempUtil(t.teacher.identification_card,car.career.id,offe.offer.id,mod.module.id,mat.id) "
                        + "FROM Matter mat "
                        + "JOIN mat.teacherMatters t "
                        + "JOIN mat.matterModules mod " 
                        + "JOIN mat.matterOffers offe "
                        + "JOIN t.teacher.teacherCareers car "
                        + "WHERE t.teacher.identification_card=? "
                        + "AND offe.offer.id=? "
                        + "AND car.career.id=? "
                        + "AND mod.module.id=? ");
  
        
      /* Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.TeacherModuleMatterTempUtil(t.identification_card,car.career.id,offe.offer.id,mod.module.id,mat.matter.id) "
                        + "FROM Teacher t "
                        + "JOIN t.teacherCareers car " 
                        + "JOIN t.teacherMatters mat "
                        + "JOIN mat.matter.matterOffers offe "
                        + "JOIN t.teacherModules mod "
                        + "WHERE t.identification_card=? "
                        + "AND offe.offer.id=? "
                        + "AND car.career.id=? "
                        + "AND mod.module.id=? ");*/
        
        hql.setString(0, identificationCard);
        hql.setLong(1, idOffer);
        hql.setLong(2, idCareer);
        hql.setLong(3, idModule);
        
        list1 = hql.list();

        
        
//        Query hql = session
//                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.TeacherModuleMatterTempUtil(t.identification_card,offe.id) "
//                        + "FROM Teacher t, Offer offe "
//                        + "WHERE t.identification_card=?");      
//        hql.setString(0, identificationCard);  
//        list1 = hql.list();
//        
//        for(TeacherModuleMatterTempUtil tmmt:list1){
//        	
//        }
        
        
        
        
        
        
                 
        /**
         * procesando lista para obtener descripcion de oferta
         */
        for (TeacherModuleMatterTempUtil data : list1) {
            
        	String careerName = careerDAO.returnCareerById(data.getIdCareer()).getName();
        	String decriptionOffer = offerDAO.returnOfferById(data.getIdOffer()).getDescription();
            String matterName = matterDAO.returnMatterById(data.getIdMatter()).getName();
            Module moduleNew = moduleDAO.returnModuleById(data.getIdModule());
        	
            TeacherModuleMatterUtil tmm = new TeacherModuleMatterUtil();
            
            tmm.setTeacherIC(data.getTeacherIC());
            tmm.setIdCareer(data.getIdCareer());
            tmm.setIdOffer(data.getIdOffer());
            tmm.setIdModule(data.getIdModule());
            tmm.setIdMatter(data.getIdMatter());
            
            tmm.setAcademicOffer(decriptionOffer);
            tmm.setCareerName(careerName);
            tmm.setMatterName(matterName);
            tmm.setModuleNumber(moduleNew.getNumber());
            tmm.setParallel(moduleNew.getParallel());
            
            listNew.add(tmm);
        }

        return listNew;
    }

	@Override
	@Transactional
	public List<StudentMatterUtil> returnListStudentMatter(long offerSelect,
			long careerSelect, long moduleSelect, long idStudent) {
		
		List<StudentMatterUtil> listSM = null;
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		
	    Query hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.StudentMatterUtil("
	                		+ "mat.matter.id,mat.matter.name) "
	                        + "FROM Student stud  "
	                        + "JOIN stud.studentMatters mat "
	                        + "JOIN stud.studentCareers car "
	                        + "JOIN stud.studentModules mod "
	                        + "JOIN mat.matter.matterOffers offe "
	                        + "WHERE stud.id=? "
	                        + "AND car.career.id=? "
	                        + "AND mod.module.id=? "
	                        + "AND offe.offer.id=? ");
	        
	    hql.setLong(0, idStudent);
	    hql.setLong(1, careerSelect);
	    hql.setLong(2, moduleSelect);
	    hql.setLong(3, offerSelect);

	        
	     listSM = hql.list();
		return listSM;
	}

	
    
    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<ModuleStudentUtil> returnModulesStudents(String identificationCard) {

        List<ModuleStudentUtil> list = null;

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();
        Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ModuleStudentUtil(mod.number,mod.parallel) "
                        + "FROM Module mod JOIN mod.studentModules stud WHERE stud.student.identification_card=?");
        hql.setString(0, identificationCard);
        list = hql.list();

        return list;
    }

   

    @Override
    @Transactional
    public boolean existDataTeacher(String identificationCard, Integer moduleNumber, Character parallel, String matterName) {
        boolean resul = false;

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("FROM Teacher t JOIN t.teacherMatters mat JOIN t.teacherModules mod WHERE t.identification_card=? AND mod.module.number=? AND mod.module.parallel=? AND mat.matter.name=?");

        hql.setString(0, identificationCard);
        hql.setInteger(1, moduleNumber);
        hql.setCharacter(2, parallel);
        hql.setString(3, matterName);

        resul = hql.uniqueResult() != null;
        return resul;
    }

    @Override
    @Transactional
    public void relationSaveModuleTeacher(Module module, Teacher teacher) {
 
        teacherModuleForSave.setModulesId(module.getId());
        teacherModuleForSave.setTeachersId(teacher.getId());
        teacherModuleDAO.saveTeacherModule(teacherModuleForSave);

    }

    @Override
    @Transactional
    public void relationSaveModuleTeacher(Module module, long teacherId) {
    	Teacher teacher = (Teacher) hibernateTemplate.get(Teacher.class,teacherId);
        teacherModuleForSave.setModulesId(module.getId());
        teacherModuleForSave.setTeachersId(teacher.getId());
        teacherModuleDAO.saveTeacherModule(teacherModuleForSave);

    }

    @Override
    @Transactional
    public void relationUpdateModuleTeacher(long idModulo,
            long idTeacher) {
        Module module = (Module) hibernateTemplate.get(Module.class, idModulo);
        Teacher teacher = (Teacher) hibernateTemplate.get(Teacher.class, idTeacher);
        teacherModuleForSave.setModulesId(module.getId());
        teacherModuleForSave.setTeachersId(teacher.getId());
        teacherModuleDAO.saveOrUpdateTeacherModule(teacherModuleForSave);

    }

    @Override
    @Transactional
    public void relationUpdateModuleTeacher(long idModulo,
            Teacher teacher) {
        Module module = (Module) hibernateTemplate.get(Module.class, idModulo);
        teacherModuleForSave.setModulesId(module.getId());
        teacherModuleForSave.setTeachersId(teacher.getId());
        teacherModuleDAO.saveOrUpdateTeacherModule(teacherModuleForSave);

    }

    @Override
    @Transactional
    public void relationUpdateMatterTeacher(long idMatter, long idTeacher) {

        Matter matter = (Matter) hibernateTemplate.get(Matter.class, idMatter);
        Teacher teacher = (Teacher) hibernateTemplate.get(Teacher.class, idTeacher);
        teacherMatterForSave.setMattersId(matter.getId());
        teacherMatterForSave.setTeachersId(teacher.getId());
        teacherMatterDAO.saveOrUpdateTeacherMatter(teacherMatterForSave);

    }

    @Override
    @Transactional
    public void relationUpdateMatterTeacher(long idMatter,
            Teacher teacherForSave) {

        Matter matter = (Matter) hibernateTemplate.get(Matter.class, idMatter);
        teacherMatterForSave.setMattersId(matter.getId());
        teacherMatterForSave.setTeachersId(teacherForSave.getId());
        teacherMatterDAO.saveOrUpdateTeacherMatter(teacherMatterForSave);

    }

    @Override
    @Transactional
    public void relationSaveMatterTeacher(Matter matterForSave, long idTeacher) {
        teacherMatterForSave.setMattersId(matterForSave.getId());
        teacherMatterForSave.setTeachersId(idTeacher);
        teacherMatterDAO.saveTeacherMatter(teacherMatterForSave);

    }

    @Override
    @Transactional
    public void relationSaveMatterTeacher(Matter matterForSave, Teacher teacher) {

        teacherMatterForSave.setMattersId(matterForSave.getId());
        teacherMatterForSave.setTeachersId(teacher.getId());
        teacherMatterDAO.saveTeacherMatter(teacherMatterForSave);
    }

    @Override
    @Transactional
    public void relationSaveMatterModule(Matter matterForSave, long idModulo) {
        Module module = (Module) hibernateTemplate.get(Module.class, idModulo);
        matterModuleForSave.setMattersId(matterForSave.getId());
        matterModuleForSave.setModulesId(module.getId());
        matterModuleDAO.saveMatterModule(matterModuleForSave);

    }

    @Override
    @Transactional
    public void relationSaveMatterModule(Matter matter, Module module) {
        matterModuleForSave.setMattersId(matter.getId());
        matterModuleForSave.setModulesId(module.getId());
        matterModuleDAO.saveMatterModule(matterModuleForSave);
    }

    @Override
    @Transactional
    public void relationUpdateMatterModule(long idMatter, long idModulo) {
        Matter matter = (Matter) hibernateTemplate.get(Matter.class, idMatter);
        Module module = (Module) hibernateTemplate.get(Module.class, idModulo);
        matterModuleForSave.setMattersId(matter.getId());
        matterModuleForSave.setModulesId(module.getId());
        matterModuleDAO.saveOrUpdateMatterModule(matterModuleForSave);

    }

    @Override
    @Transactional
    public void relationUpdateMatterModule(long idMatter, Module module) {
        Matter matter = (Matter) hibernateTemplate.get(Matter.class, idMatter);
        matterModuleForSave.setMattersId(matter.getId());
        matterModuleForSave.setModulesId(module.getId());
        matterModuleDAO.saveOrUpdateMatterModule(matterModuleForSave);

    }

    @Override
    @Transactional
    public void relationUpdateCareerTeacher(long idCareer, long idTeacher) {
        Career career = (Career) hibernateTemplate.get(Career.class, idCareer);
        Teacher teacher = (Teacher) hibernateTemplate.get(Teacher.class, idTeacher);
        teacherCareerForSave.setCareersId(career.getId());
        teacherCareerForSave.setTeachersId(teacher.getId());
        teacherCareerDAO.saveOrUpdateTeacherCareer(teacherCareerForSave);

    }

    @Override
    @Transactional
    public void relationUpdateCareerTeacher(long idCareer, Teacher teacher) {
        Career career = (Career) hibernateTemplate.get(Career.class, idCareer);
        teacherCareerForSave.setCareersId(career.getId());
        teacherCareerForSave.setTeachersId(teacher.getId());
        teacherCareerDAO.saveOrUpdateTeacherCareer(teacherCareerForSave);
    }

    @Override
    @Transactional
    public void relationSaveCareerTeacher(Career career, long idTeacher) {
        Teacher teacher = (Teacher) hibernateTemplate.get(Teacher.class, idTeacher);
        teacherCareerForSave.setCareersId(career.getId());
        teacherCareerForSave.setTeachersId(teacher.getId());
        teacherCareerDAO.saveTeacherCareer(teacherCareerForSave);
    }

    @Override
    @Transactional
    public void relationSaveCareerTeacher(Career career, Teacher teacher) {
        teacherCareerForSave.setCareersId(career.getId());
        teacherCareerForSave.setTeachersId(teacher.getId());
        teacherCareerDAO.saveTeacherCareer(teacherCareerForSave);
    }

	@Override
	@Transactional
	public void relationSaveMatterOffer(long idMatter, Offer offerForSave) {
		
		 Matter matter = (Matter) hibernateTemplate.get(Matter.class, idMatter);
		matterOfferForSave.setMattersId(matter.getId());
		matterOfferForSave.setOffersId(offerForSave.getId());
		matterOfferDAO.saveMatterOffer(matterOfferForSave);
		
	}
	

	@Override
	@Transactional
	public void relationSaveMatterOffer(Matter matter, Offer offer) {
		matterOfferForSave.setMattersId(matter.getId());
		matterOfferForSave.setOffersId(offer.getId());
		matterOfferDAO.saveMatterOffer(matterOfferForSave);
	}

	@Override
	@Transactional
	public void relationUpdateMatterOffer(long idMatter, long idOffer) {
		Matter matter = (Matter) hibernateTemplate.get(Matter.class, idMatter);
		Offer offer = (Offer) hibernateTemplate.get(Offer.class, idOffer);
		matterOfferForSave.setMattersId(matter.getId());
		matterOfferForSave.setOffersId(offer.getId());
		matterOfferDAO.saveOrUpdateMatterOffer(matterOfferForSave);
	}

	@Override
	@Transactional
	public void relationUpdateMatterOffer(Matter matter, long idOffer) {
		Offer offer = (Offer) hibernateTemplate.get(Offer.class, idOffer);
		matterOfferForSave.setMattersId(matter.getId());
		matterOfferForSave.setOffersId(offer.getId());
		matterOfferDAO.saveOrUpdateMatterOffer(matterOfferForSave);
		
	}

	///////// ESTUDIANTE - CARRERA ////////
	
	@Override
	@Transactional
	public void relationSaveOrUpdateCareerStudent(long idCareer, long idStudent) {
		Student student = (Student) hibernateTemplate.get(Student.class, idStudent);
		Career career = (Career) hibernateTemplate.get(Career.class, idCareer);
		studentCareerForSave.setStudentsId(student.getId());
		studentCareerForSave.setCareersId(career.getId());
		studentCareerDAO.saveOrUpdateStudentCareer(studentCareerForSave);
	}

	@Override
	@Transactional
	public void relationSaveOrUpdateCareerStudent(long idCareer, Student student) {
		Career career = (Career) hibernateTemplate.get(Career.class, idCareer);
		studentCareerForSave.setStudentsId(student.getId());
		studentCareerForSave.setCareersId(career.getId());
		studentCareerDAO.saveOrUpdateStudentCareer(studentCareerForSave);
		
	}

	@Override
	@Transactional
	public void relationSaveOrUpdateCareerStudent(Career career, long idStudent) {
		Student student = (Student) hibernateTemplate.get(Student.class, idStudent);
		studentCareerForSave.setStudentsId(student.getId());
		studentCareerForSave.setCareersId(career.getId());
		studentCareerDAO.saveOrUpdateStudentCareer(studentCareerForSave);
		
	}

	@Override
	@Transactional
	public void relationSaveOrUpdateCareerStudent(Career career, Student student) {
		studentCareerForSave.setStudentsId(student.getId());
		studentCareerForSave.setCareersId(career.getId());
		studentCareerDAO.saveOrUpdateStudentCareer(studentCareerForSave);
	}

	
	////////////// ESTUDIANTE - MODULO /////////////
	
	@Override
	@Transactional
	public void relationSaveOrUpdateModuleStudent(long idModule, long idStudent) {
		Module module = (Module) hibernateTemplate.get(Module.class, idModule);
		Student student = (Student) hibernateTemplate.get(Student.class, idStudent);
		studentModuleForSave.setModulesId(module.getId());
		studentModuleForSave.setStudentsId(student.getId());
		studentModuleDAO.saveOrUpdateStudentModule(studentModuleForSave);
	}

	@Override
	@Transactional
	public void relationSaveOrUpdateModuleStudent(long idModule, Student student) {
		Module module = (Module) hibernateTemplate.get(Module.class, idModule);
		
		studentModuleForSave.setModulesId(module.getId());
		studentModuleForSave.setStudentsId(student.getId());
		studentModuleDAO.saveOrUpdateStudentModule(studentModuleForSave);
		
	}

	@Override
	@Transactional
	public void relationSaveOrUpdateModuleStudent(Module module, long idStudent) {
		Student student = (Student) hibernateTemplate.get(Student.class, idStudent);
		studentModuleForSave.setModulesId(module.getId());
		studentModuleForSave.setStudentsId(student.getId());
		studentModuleDAO.saveOrUpdateStudentModule(studentModuleForSave);
		
	}

	@Override
	@Transactional
	public void relationSaveOrUpdateModuleStudent(Module module, Student student) {
		studentModuleForSave.setModulesId(module.getId());
		studentModuleForSave.setStudentsId(student.getId());
		studentModuleDAO.saveOrUpdateStudentModule(studentModuleForSave);
		
	}
	
	/////////// ESTUDIANTE - MATERIA ////////
	
	
	@Override
	@Transactional
	public void relationSaveOrUpdateMatterStudent(long idMatter, long idStudent) {
		Matter matter = (Matter) hibernateTemplate.get(Matter.class, idMatter);
		Student student = (Student) hibernateTemplate.get(Student.class, idStudent);
		studentMatterForSave.setMattersId(matter.getId());
		studentMatterForSave.setStudentsId(student.getId());
		studentMatterDAO.saveOrUpdateStudentMatter(studentMatterForSave);
		
		
	}

	@Override
	@Transactional
	public void relationSaveOrUpdateMatterStudent(long idMatter, Student student) {
		Matter matter = (Matter) hibernateTemplate.get(Matter.class, idMatter);
		studentMatterForSave.setMattersId(matter.getId());
		studentMatterForSave.setStudentsId(student.getId());
		studentMatterDAO.saveOrUpdateStudentMatter(studentMatterForSave);		
	}

	@Override
	@Transactional
	public void relationSaveOrUpdateMatterStudent(Matter matter, long idStudent) {
		Student student = (Student) hibernateTemplate.get(Student.class, idStudent);
		studentMatterForSave.setMattersId(matter.getId());
		studentMatterForSave.setStudentsId(student.getId());
		studentMatterDAO.saveOrUpdateStudentMatter(studentMatterForSave);
	}

	@Override
	@Transactional
	public void relationSaveOrUpdateMatterStudent(Matter matter, Student student) {
		studentMatterForSave.setMattersId(matter.getId());
		studentMatterForSave.setStudentsId(student.getId());
		studentMatterDAO.saveOrUpdateStudentMatter(studentMatterForSave);
	}




	////////////////
	

	





}
