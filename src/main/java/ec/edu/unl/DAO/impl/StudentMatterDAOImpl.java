package ec.edu.unl.DAO.impl;

import ec.edu.unl.DAO.StudentMatterDAO;
import ec.edu.unl.entity.StudentMatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public class StudentMatterDAOImpl implements StudentMatterDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Override
    @Transactional
    public void saveStudentMatter(StudentMatter studentMatter) {
        hibernateTemplate.save(studentMatter);
    }

    @Override
    @Transactional
    public void updateStudentMatter(StudentMatter studentMatter) {
    	hibernateTemplate.update(studentMatter);
    }

    @Override
    @Transactional
    public void saveOrUpdateStudentMatter(StudentMatter studentMatter) {
        hibernateTemplate.saveOrUpdate(studentMatter);
    }

}
