package ec.edu.unl.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.EvaluationDAO;
import ec.edu.unl.entity.Evaluation;
import ec.edu.unl.utilities.academic.StudentSpecificUtil;
import ec.edu.unl.utilities.evaluation.EvaluationUtil;

public class EvaluationDAOImpl implements EvaluationDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;
	
	@Override
	@Transactional
	public void saveEvaluation(Evaluation evaluation) {
		hibernateTemplate.save(evaluation);
		
	}

	@Override
	@Transactional
	public void updateEvaluation(Evaluation evaluation) {
		hibernateTemplate.update(evaluation);
		
	}

	@Override
	@Transactional
	public void deleteEvaluation(Evaluation evaluation) {
		hibernateTemplate.delete(evaluation);
	}

	@Override
	@Transactional
	public List<EvaluationUtil> returnListStudentEvaluations(long idOffer,
			long idCareer, long idModule, long idMatter, long idStudent) {
		
		List<EvaluationUtil> listFinal = new ArrayList<EvaluationUtil>();

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.utilities.evaluation.EvaluationUtil(eval.id,eval.num_evaluation,eval.qualification,eval.annotation,eval.date_evaluation,ref.general_reference) "
                		+ "FROM Student stud "
                		+ "JOIN stud.evaluations eval "
                		+ "JOIN eval.matter mat "
                		+ "JOIN eval.reference ref "
                		+ "JOIN stud.studentModules mod "
                		+ "JOIN stud.studentCareers car "
                		+ "JOIN mat.matterOffers offe "
                		+ "WHERE offe.offer.id = ? "
                		+ "AND car.career.id = ? "
                		+ "AND mod.module.id = ? "
                		+ "AND mat.id = ? "
                		+ "AND stud.id = ?");
         
        hql.setLong(0, idOffer);
        hql.setLong(1, idCareer);
        hql.setLong(2, idModule);
        hql.setLong(3, idMatter);
        hql.setLong(4, idStudent);
        
        listFinal = hql.list();
		
		return listFinal;
	}

}
