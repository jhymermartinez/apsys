package ec.edu.unl.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.ClassWorkDAO;
import ec.edu.unl.entity.ClassWork;
import ec.edu.unl.utilities.classWork.ClassWorkUtil;


public class ClassWorkDAOImpl implements ClassWorkDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;
    
	@Override
	@Transactional
	public void saveClassWork(ClassWork classWork) {
	
		hibernateTemplate.save(classWork);
	}

	@Override
	@Transactional
	public void updateClassWork(ClassWork classWork) {
		hibernateTemplate.update(classWork);
		
	}

	@Override
	@Transactional
	public void deleteClassWork(ClassWork classWork) {
		hibernateTemplate.delete(classWork);
		
	}

	@Override
	@Transactional
	public List<ClassWorkUtil> returnListStudentClassWorks(long idOffer,
			long idCareer, long idModule, long idMatter, long idStudent) {
		
		List<ClassWorkUtil> listFinal = new ArrayList<ClassWorkUtil>();

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.utilities.classWork.ClassWorkUtil(cwork.id,cwork.num_classWork,cwork.qualification,cwork.annotation,cwork.date_classwork,ref.general_reference) "
                		+ "FROM Student stud "
                		+ "JOIN stud.classWorks cwork "
                		+ "JOIN cwork.matter mat "
                		+ "JOIN cwork.reference ref "
                		+ "JOIN stud.studentModules mod "
                		+ "JOIN stud.studentCareers car "
                		+ "JOIN mat.matterOffers offe "
                		+ "WHERE offe.offer.id = ? "
                		+ "AND car.career.id = ? "
                		+ "AND mod.module.id = ? "
                		+ "AND mat.id = ? "
                		+ "AND stud.id = ?");
         
        hql.setLong(0, idOffer);
        hql.setLong(1, idCareer);
        hql.setLong(2, idModule);
        hql.setLong(3, idMatter);
        hql.setLong(4, idStudent);
        
        listFinal = hql.list();
		
		return listFinal;
		
		
		
	}

}
