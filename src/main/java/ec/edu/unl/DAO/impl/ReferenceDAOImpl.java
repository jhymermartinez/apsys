package ec.edu.unl.DAO.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.ReferenceDAO;
import ec.edu.unl.entity.Reference;

public class ReferenceDAOImpl implements ReferenceDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;
    
	@Override
	@Transactional
	public Reference returnReferenceById(long idReference) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public void saveReference(Reference reference) {
		hibernateTemplate.save(reference);
	}

	@Override
	@Transactional
	public void saveOrUpdateReference(Reference reference) {
		hibernateTemplate.saveOrUpdate(reference);	
	}

	@Override
	@Transactional
	public void updateReference(Reference reference) {
		hibernateTemplate.update(reference);
		
	}

	@Override
	@Transactional
	public void deleteReference(Reference reference) {
		hibernateTemplate.delete(reference);
		
	}

}
