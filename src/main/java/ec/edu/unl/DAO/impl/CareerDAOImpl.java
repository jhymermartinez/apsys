package ec.edu.unl.DAO.impl;

import ec.edu.unl.DAO.CareerDAO;
import ec.edu.unl.entity.Career;
import org.hibernate.Query;
import org.hibernate.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public class CareerDAOImpl implements CareerDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Override
    @Transactional
    public void saveCareer(Career career) {
        hibernateTemplate.save(career);
    }

    @Override
    @Transactional
    public void updateCareer(Career career) {
        hibernateTemplate.update(career);
    }

    @Override
    @Transactional
    public boolean existCareer(String name, Long code_career) {
        boolean resul;

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("FROM Career car WHERE car.name=? AND car.code_career=? ");

        hql.setString(0, name);
        hql.setLong(1, code_career);
        resul = hql.uniqueResult() != null;
        return resul;

    }

    @Override
    @Transactional
    public Career returnCareerById(long idCareer) {
        Career career = (Career) hibernateTemplate.get(Career.class, idCareer);
        return career;
    }

    @Override
    @Transactional
    public long returnCareerId(String careerName, long codeCareer) {
    
        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();
        Query hql = session
                .createQuery("FROM Career car WHERE car.name=? AND car.code_career=? ");
        hql.setString(0, careerName);
        hql.setLong(1, codeCareer);
        Career career = (Career) hql.uniqueResult();
        return career.getId();
    }

}
