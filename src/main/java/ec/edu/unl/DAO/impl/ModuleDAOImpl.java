package ec.edu.unl.DAO.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.ModuleDAO;
import ec.edu.unl.entity.Module;

public class ModuleDAOImpl implements ModuleDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Override
    @Transactional
    public void saveModule(Module module) {
        hibernateTemplate.save(module);
    }

    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<Module> findAllModules() {
        List<Module> listModules = null;
        String hql = "FROM Module";

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();
        listModules = session.createQuery(hql).list();

        return listModules;

    }

    @Override
    @Transactional
    public void updateModule(Module module) {
        hibernateTemplate.update(module);
    }

    @Override
    @Transactional
    public Module returnModuleById(long id) {
        Module module = (Module) hibernateTemplate.get(Module.class, id);
        return module;
    }

    @Override
    @Transactional
    public boolean existModule(Integer number, String parallel) {

        boolean resul;
        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("FROM Module mod WHERE mod.number=? AND mod.parallel=?");

        hql.setInteger(0, number);
        hql.setString(1, parallel);
        resul = hql.uniqueResult() != null;
        return resul;

    }

    @Override
    @Transactional
    public long returnModuleId(Integer moduleNumber, String parallel) {
    Module moduleNew = null;

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("FROM Module mod WHERE mod.number=? AND mod.parallel=?");

        hql.setInteger(0, moduleNumber);
        hql.setString(1, parallel);

        moduleNew = (Module) hql.uniqueResult();

        return moduleNew.getId();
    }

}
