package ec.edu.unl.DAO.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.StudentCareerDAO;
import ec.edu.unl.entity.Offer;
import ec.edu.unl.entity.StudentCareer;
import ec.edu.unl.utilities.academic.StudentCareerUtil;

/**
 * 
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public class StudentCareerDAOImpl implements StudentCareerDAO {

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Override
	@Transactional
	public void saveStudentCareer(StudentCareer studentCareer) {
		hibernateTemplate.save(studentCareer);
	}

	@Override
	@Transactional
	public void updateStudentCareer(StudentCareer studentCareer) {
		hibernateTemplate.update(studentCareer);
	}

        @Override
	@Transactional
	public void saveOrUpdateStudentCareer(StudentCareer studentCareer) {
		hibernateTemplate.saveOrUpdate(studentCareer);
	}

		@Override
		@Transactional
		public List<StudentCareerUtil> returnListStudentCareers(long idOffer,long idStudent) {
			
			List<StudentCareerUtil> listFinal = null;
			
			Session session = hibernateTemplate.getSessionFactory()
	                .getCurrentSession();

	        Query hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.StudentCareerUtil(car.career.id,car.career.name) "
	                		+"FROM Matter mat "
	                		+"JOIN mat.matterOffers offe "
	                		+"JOIN mat.studentMatters stud "
	                		+"JOIN stud.student.studentCareers car "
	                		+"WHERE stud.student.id=? "
	                		+"AND offe.offer.id=? ");
	        
	        hql.setLong(0, idStudent);
	        hql.setLong(1, idOffer);
	        
	        listFinal = hql.list();
	        
			return listFinal;
		}
        
    
        
}
