package ec.edu.unl.DAO.impl;

import java.util.Date;
import java.util.List;

import ec.edu.unl.DAO.OfferDAO;
import ec.edu.unl.entity.Offer;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public class OfferDAOImpl implements OfferDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Override
    @Transactional
    public void saveOffer(Offer offer) {
        hibernateTemplate.save(offer);
    }

    @Override
    @Transactional
    public void updateOffer(Offer offer) {

        hibernateTemplate.update(offer);

    }

    @Override
    @Transactional
    public boolean existOffer(String description,Date start,Date end) {

        boolean resul;

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("FROM Offer offe "
                		+ "WHERE offe.description=? "
                		+ "AND offe.startDate=? "
                		+ "AND offe.endDate=? ");

        hql.setString(0, description);
        hql.setDate(1, start);
        hql.setDate(2, end);
        
        resul = hql.uniqueResult() != null;
        return resul;

    }

    @Override
    @Transactional
    public Offer returnOfferById(long idOffer) {

        Offer offer = (Offer) hibernateTemplate.get(Offer.class, idOffer);
        return offer;

    }

	@Override
	@Transactional
	public long returnOfferId(String descriptionOffer,Date start,Date end) {
        

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("FROM Offer offe "
                		+ "WHERE offe.description=? "
                		+ "AND offe.startDate=? "
                		+ "AND offe.endDate=? ");

        hql.setString(0, descriptionOffer);
        hql.setDate(1, start);
        hql.setDate(2, end);
        
        Offer resul = (Offer) hql.uniqueResult();
        return resul.getId();
	}

	@Override
	@Transactional
	public Date returnStartDateOfferById(long offerSelect) {
		Offer offer = (Offer) hibernateTemplate.get(Offer.class, offerSelect);
		
		return offer.getStartDate();
	}

	@Override
	@Transactional
	public Date returnEndDateOfferById(long offerSelect) {
		Offer offer = (Offer) hibernateTemplate.get(Offer.class, offerSelect);
		return offer.getEndDate();
	}

	@Override
	@Transactional
	public List<Offer> returnAllOffers() {
		List<Offer> listFinal = null;
		
		Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("FROM Offer");
        
        listFinal = hql.list();
        
		return listFinal;
	}

}
