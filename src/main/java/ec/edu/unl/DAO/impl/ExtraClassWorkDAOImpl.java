package ec.edu.unl.DAO.impl;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.ExtraClassWorkDAO;
import ec.edu.unl.entity.ExtraClassWork;
import ec.edu.unl.utilities.extraClassWork.ExtraClassWorkUtil;

public class ExtraClassWorkDAOImpl implements ExtraClassWorkDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;
    
	@Override
	@Transactional
	public void saveExtraClassWork(ExtraClassWork extraClassWork) {
		hibernateTemplate.save(extraClassWork);
		
	}

	@Override
	@Transactional
	public void updateExtraClassWork(ExtraClassWork extraClassWork) {
		hibernateTemplate.update(extraClassWork);
		
	}

	@Override
	@Transactional
	public void deleteExtraClassWork(ExtraClassWork extraClassWork) {
		hibernateTemplate.delete(extraClassWork);
	}

	@Override
	@Transactional
	public List<ExtraClassWorkUtil> returnListStudentExtraClassWorks(
			long idOffer, long idCareer, long idModule,
			long idMatter, long idStudent) {
		
		List<ExtraClassWorkUtil> listFinal = new ArrayList<ExtraClassWorkUtil>();

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.utilities.extraClassWork.ExtraClassWorkUtil(ecw.id,ecw.num_extraClassWork,ecw.qualification,ecw.annotation,ecw.date_extraclasswork,ref.general_reference) "
                		+ "FROM Student stud "
                		+ "JOIN stud.extraClassWorks ecw "
                		+ "JOIN ecw.matter mat "
                		+ "JOIN ecw.reference ref "
                		+ "JOIN stud.studentModules mod "
                		+ "JOIN stud.studentCareers car "
                		+ "JOIN mat.matterOffers offe "
                		+ "WHERE offe.offer.id = ? "
                		+ "AND car.career.id = ? "
                		+ "AND mod.module.id = ? "
                		+ "AND mat.id = ? "
                		+ "AND stud.id = ?");
         
        hql.setLong(0, idOffer);
        hql.setLong(1, idCareer);
        hql.setLong(2, idModule);
        hql.setLong(3, idMatter);
        hql.setLong(4, idStudent);
        
        listFinal = hql.list();
		
		return listFinal;
	}

}
