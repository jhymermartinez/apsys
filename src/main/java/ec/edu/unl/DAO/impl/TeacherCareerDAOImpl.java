/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.unl.DAO.impl;

import ec.edu.unl.DAO.TeacherCareerDAO;
import ec.edu.unl.entity.TeacherCareer;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public class TeacherCareerDAOImpl implements TeacherCareerDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Override
    @Transactional
    public void saveTeacherCareer(TeacherCareer teacherCareer) {
        hibernateTemplate.save(teacherCareer);
    }

    @Override
    @Transactional
    public void updateTeacherCareer(TeacherCareer teacherCareer) {
        hibernateTemplate.update(teacherCareer);

    }

    @Override
    @Transactional
    public void saveOrUpdateTeacherCareer(TeacherCareer teacherCareer) {
        hibernateTemplate.saveOrUpdate(teacherCareer);
    }

	@Override
	@Transactional
	public boolean existRelationTeacherCareer(String teacherIC,
			long codeCareer, String careerName) {
		
		boolean resul;

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("FROM Teacher t JOIN t.teacherCareers car WHERE t.identification_card=? AND car.career.name=?  AND car.career.code_career=?");

        hql.setString(0, teacherIC);
        hql.setString(1, careerName);
        hql.setLong(2, codeCareer);
      
        resul = hql.uniqueResult() != null;
        
        return resul;
	}

}
