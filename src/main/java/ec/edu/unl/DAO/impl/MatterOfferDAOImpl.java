package ec.edu.unl.DAO.impl;

import java.util.Date;

import ec.edu.unl.DAO.MatterOfferDAO;
import ec.edu.unl.entity.MatterOffer;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public class MatterOfferDAOImpl implements MatterOfferDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Override
    @Transactional
    public void saveMatterOffer(MatterOffer matterOffer) {

        hibernateTemplate.save(matterOffer);

    }

    @Override
    @Transactional
    public void updateMatterOffer(MatterOffer matterOffer) {
        hibernateTemplate.update(matterOffer);

    }

    @Override
    @Transactional
    public void saveOrUpdateMatterOffer(MatterOffer matterOffer) {
        hibernateTemplate.saveOrUpdate(matterOffer);

    }

	@Override
	@Transactional
	public boolean existRelationMatterOffer(String matter,
			Double numHoursMatter, String descriptionOffer, Date start,Date end) {
		boolean resul;

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("FROM Matter mat "
                		+ "JOIN mat.matterOffers offe "
                		+ "WHERE mat.name=? "
                		+ "AND mat.num_hours=?  "
                		+ "AND offe.offer.description=? "
                		+ "AND offe.offer.startDate=? "
                		+ "AND offe.offer.endDate=? ");

        hql.setString(0, matter);
        hql.setDouble(1, numHoursMatter);
        hql.setString(2, descriptionOffer);
        hql.setDate(3, start);
        hql.setDate(4, end);
        
        resul = hql.uniqueResult() != null;
        
        return resul;
	}

}
