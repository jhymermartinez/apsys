package ec.edu.unl.DAO.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.MatterDAO;
import ec.edu.unl.entity.Matter;

public class MatterDAOImpl implements MatterDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Override
    @Transactional
    public void saveMatter(Matter matter) {
        hibernateTemplate.save(matter);
    }

    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<Matter> findAllMatters() {
        List<Matter> listMatters = null;
        String hql = "FROM Matter";

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();
        listMatters = session.createQuery(hql).list();

        return listMatters;

    }

    @Override
    @Transactional
    public void updateMatter(Matter matter) {
        hibernateTemplate.update(matter);
    }

    @Override
    @Transactional
    public boolean existMatter(String matter, Double numHoursMatter) {
        boolean resul;

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("FROM Matter mat WHERE mat.name=? AND mat.num_hours=? ");

        hql.setString(0, matter);
        hql.setDouble(1, numHoursMatter);
        resul = hql.uniqueResult() != null;
        return resul;
    }

    @Override
    @Transactional
    public Matter returnMatterById(long idMatter) {
        Matter matter = (Matter) hibernateTemplate.get(Matter.class, idMatter);
        return matter;
    }

    @Override
    @Transactional
    public long returnMatterId(String matter, Double numHoursMatter) {
     Matter matterNew = null;

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.entity.Matter(mat.id, mat.name, mat.num_hours) FROM Matter mat WHERE mat.name=? AND mat.num_hours=? ");
        hql.setString(0, matter);
        hql.setDouble(1, numHoursMatter);

        matterNew = (Matter) hql.uniqueResult();

        return matterNew.getId();
    
    }


}
