/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ec.edu.unl.DAO.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.TeacherMatterDAO;
import ec.edu.unl.entity.TeacherMatter;

/**
 * 
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public class TeacherMatterDAOImpl implements TeacherMatterDAO {

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Override
	@Transactional
	public void saveTeacherMatter(TeacherMatter teacherMatter) {
		hibernateTemplate.save(teacherMatter);
	}

	@Override
	@Transactional
	public void updateTeacherMatter(TeacherMatter teacherMatter) {
		hibernateTemplate.update(teacherMatter);
	}

	@Override
	@Transactional
	public void saveOrUpdateTeacherMatter(TeacherMatter teacherMatterForSave) {
		hibernateTemplate.saveOrUpdate(teacherMatterForSave);
		
	}

	@Override
	@Transactional
	public boolean existRelationTeacherMatter(String teacherIC, String matter,
			Double numHoursMatter) {
		boolean resul;

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("FROM Teacher t JOIN t.teacherMatters mat WHERE t.identification_card=? AND mat.matter.name=? AND mat.matter.num_hours=? ");

        hql.setString(0, teacherIC);
        hql.setString(1, matter);
        hql.setDouble(2, numHoursMatter);
       
        
        resul = hql.uniqueResult() != null;
        
        return resul;
	}

}
