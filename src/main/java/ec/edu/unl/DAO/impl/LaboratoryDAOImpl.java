package ec.edu.unl.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.LaboratoryDAO;
import ec.edu.unl.entity.Laboratory;
import ec.edu.unl.utilities.laboratory.LaboratoryUtil;

public class LaboratoryDAOImpl implements LaboratoryDAO{

    @Autowired
    HibernateTemplate hibernateTemplate;
    
	@Override
	@Transactional
	public void saveLaboratory(Laboratory laboratory) {
		hibernateTemplate.save(laboratory);
		
	}

	@Override
	@Transactional
	public void updateLaboratory(Laboratory laboratory) {
		hibernateTemplate.update(laboratory);
		
	}

	@Override
	@Transactional
	public void deleteLaboratory(Laboratory laboratory) {
		hibernateTemplate.delete(laboratory);
		
	}

	@Override
	@Transactional
	public List<LaboratoryUtil> returnListStudentLaboratories(long idOffer,
			long idCareer, long idModule, long idMatter, long idStudent) {
		
		List<LaboratoryUtil> listFinal = new ArrayList<LaboratoryUtil>();

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.utilities.laboratory.LaboratoryUtil(lab.id,lab.num_laboratory,lab.qualification,lab.annotation,lab.date_laboratory,ref.general_reference) "
                		+ "FROM Student stud "
                		+ "JOIN stud.laboratories lab "
                		+ "JOIN lab.matter mat "
                		+ "JOIN lab.reference ref "
                		+ "JOIN stud.studentModules mod "
                		+ "JOIN stud.studentCareers car "
                		+ "JOIN mat.matterOffers offe "
                		+ "WHERE offe.offer.id = ? "
                		+ "AND car.career.id = ? "
                		+ "AND mod.module.id = ? "
                		+ "AND mat.id = ? "
                		+ "AND stud.id = ?");
         
        hql.setLong(0, idOffer);
        hql.setLong(1, idCareer);
        hql.setLong(2, idModule);
        hql.setLong(3, idMatter);
        hql.setLong(4, idStudent);
        
        listFinal = hql.list();
		
		return listFinal;
	}

}
