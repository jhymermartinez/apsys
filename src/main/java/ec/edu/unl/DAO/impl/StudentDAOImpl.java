package ec.edu.unl.DAO.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.AcademicDAO;
import ec.edu.unl.DAO.StudentDAO;
import ec.edu.unl.entity.Career;
import ec.edu.unl.entity.Evaluation;
import ec.edu.unl.entity.Matter;
import ec.edu.unl.entity.Module;
import ec.edu.unl.entity.Student;
import ec.edu.unl.entity.Teacher;
import ec.edu.unl.utilities.academic.ParameterEditUtil;
import ec.edu.unl.utilities.academic.TeacherUtil;

public class StudentDAOImpl implements StudentDAO,Serializable {

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Override
    @SuppressWarnings("unchecked")
    @Transactional
    public boolean existStudentModule(String identification_card, Module module) {

        boolean result;
        Query hql = null;
        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();
        hql = session.createQuery("FROM Student s JOIN s.studentModules mod WHERE s.identification_card=? AND  mod.module.number=? AND  mod.module.parallel=?");

        hql.setString(0, identification_card);
        hql.setInteger(1, module.getNumber());
        hql.setString(2, module.getParallel());

        result = hql.uniqueResult() != null;

        return result;
    }

    @Override
    @Transactional
    public boolean existStudentCareer(String identification_card, Career career) {

        boolean result;
        Query hql = null;
        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();
        hql = session.createQuery("FROM Student s JOIN s.student_careers car "
                + "WHERE s.identification_card=? AND  car.career.code_career=?");

        hql.setString(0, identification_card);
        hql.setLong(1, career.getCode_career());

        result = hql.uniqueResult() != null;

        return result;
    }

    @Override
    @Transactional
    public boolean existStudentMatter(String identification_card, Matter matter) {
        boolean result = false;
        Query hql = null;
        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();
        hql = session.createQuery("FROM Student s JOIN s.studentMatters mat "
                + "WHERE s.identification_card=? AND  mat.matter.name=?");

        hql.setString(0, identification_card);
        hql.setString(1, matter.getName());

        result = hql.uniqueResult() != null;

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<Student> returnAllStudents() {

        List<Student> students = new ArrayList<Student>();

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();
        String hql = "FROM Student";
        students = session.createQuery(hql).list();

        return students;
    }

    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<Student> returnStudentPararllelModule(String module,
            Character parallel) {

        // List<Student> students = new ArrayList<Student>();
        //
        // Session session = hibernateTemplate.getSessionFactory()
        // .getCurrentSession();
        // String hql =
        // "SELECT NEW ec.edu.unl.utilities.academic.StudentEvaluationMatter(st.identificationCard,e.matter.name,e.numEvaluation,e.qualification,e.annotation) FROM Evaluation e JOIN e.student st JOIN st.studentModules mod WHERE mod.module.name='"
        // + module + "' AND mod.module.parallel='" + parallel + "'";
        //
        // list = hql.list();
        return null;

    }


////////////////////////////    
    @Override
    @Transactional
    public Student returnStudentByIdentificactionCard(String identification_card) {
        Student student = null;
        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();
        String hql = "FROM Student s WHERE s.identification_card='"
                + identification_card + "'";

        student = (Student) session.createQuery(hql).uniqueResult();
        return student;
    }

    @Override
    @Transactional
    public Student returnStudentById(long idStudent) {
        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();
        Student student = (Student) session.get(Student.class, idStudent);

        return student;
    }

    @Override
    @Transactional
    public void updateStudent(Student student) {
        hibernateTemplate.update(student);

    }

    @Override
    @Transactional
    public void saveStudent(Student student) {
        hibernateTemplate.save(student);
    }

    @Override
    @Transactional
    public boolean existStudent(Student student) {
        boolean resul;
        Student studentNew = returnStudentByIdentificactionCard(student
                .getIdentification_card());
        resul = studentNew != null;
        return resul;
    }

    @Override
    @Transactional
    public boolean existStudent(String identification_card) {
        boolean resul;
        Student studentNew = returnStudentByIdentificactionCard(identification_card);
        resul = studentNew != null;
        return resul;
    }

    @Override
    @Transactional
    public void saveOrUpdateStudent(Student student) {
        hibernateTemplate.saveOrUpdate(student);

    }

	@Override
	@Transactional
	public long returnStudentId(String identification_card) {
		Student studentNew = returnStudentByIdentificactionCard(identification_card);
		return studentNew.getId();
	}

	@Override
	@Transactional
	public TeacherUtil returnTeacherByMatter(long idOffer, long idCareer,
			long idModule, long idMatter, long idStudent) {

		TeacherUtil teacher = null;
		
        Session session = hibernateTemplate.getSessionFactory().getCurrentSession();  
        
        
        Query hql = null; 
        
       
        hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.TeacherUtil(t.id,t.identification_card ) "
                    		+ "FROM Teacher t "
                    		+ "JOIN t.teacherMatters mat "
                    		+ "JOIN mat.matter.studentMatters stud "
                    		+ "JOIN mat.matter.matterOffers offe "
                    		+ "JOIN t.teacherModules mod "
                    		+ "JOIN t.teacherCareers car "
                    		+ "WHERE offe.offer.id= ?  "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.matter.id=? "
                    		+ "AND stud.student.id=? ");
	
        hql.setLong(0, idOffer);
        hql.setLong(1, idCareer);
        hql.setLong(2, idModule);
        hql.setLong(3, idMatter);
        hql.setLong(4, idStudent);
       
        
        teacher = (TeacherUtil) hql.uniqueResult();
        
        return teacher;
	}

}
