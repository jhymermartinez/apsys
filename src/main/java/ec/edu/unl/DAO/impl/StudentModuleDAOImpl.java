package ec.edu.unl.DAO.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.StudentModuleDAO;
import ec.edu.unl.entity.StudentModule;
import ec.edu.unl.utilities.academic.ModuleParallelUtil;
import ec.edu.unl.utilities.academic.StudentCareerUtil;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public class StudentModuleDAOImpl implements StudentModuleDAO {
    
    @Autowired
    HibernateTemplate hibernateTemplate;
    
    @Override
    @Transactional
    public void saveStudentModule(StudentModule studentModule) {
        hibernateTemplate.save(studentModule);
    }
    
    @Override
    @Transactional
    public void updateStudentModule(StudentModule studentModule) {
        hibernateTemplate.update(studentModule);
        
    }
    
    @Override
    @Transactional
    public void saveOrUpdateStudentModule(StudentModule studentModule) {
        hibernateTemplate.saveOrUpdate(studentModule);
    }

	@Override
	@Transactional
	public List<ModuleParallelUtil> returnStudentModules(long idOffer,
			long idCareer, long idStudent) {
		List<ModuleParallelUtil> listFinal = null;
		
		Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ModuleParallelUtil(mod.id,mod.number,mod.parallel) "
                		+"FROM Module mod JOIN mod.studentModules stud "
                		+"JOIN stud.student.studentCareers car "
                		+"JOIN mod.matterModules mat "
                		+"JOIN mat.matter.matterOffers offe "
                		+"WHERE offe.offer.id=? "
                		+"AND car.career.id=? "
                		+"AND stud.student.id=? ");
        
        hql.setLong(0, idOffer);
        hql.setLong(1, idCareer);
        hql.setLong(2, idStudent);
        
        listFinal = hql.list();
		return listFinal;
	}
    
}
