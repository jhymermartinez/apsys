package ec.edu.unl.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.ParticipationDAO;
import ec.edu.unl.entity.Participation;
import ec.edu.unl.utilities.participation.ParticipationUtil;

public class ParticipationDAOImpl implements ParticipationDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;
    
	@Override
	@Transactional
	public void saveParticipation(Participation participation) {
		hibernateTemplate.save(participation);
		
	}

	@Override
	@Transactional
	public void updateParticipation(Participation participation) {
		hibernateTemplate.update(participation);
		
	}

	@Override
	@Transactional
	public void deleteParticipation(Participation participation) {
		hibernateTemplate.delete(participation);
		
	}

	@Override
	@Transactional
	public List<ParticipationUtil> returnListStudentParticipations(
			long idOffer, long idCareer, long idModule,
			long idMatter, long idStudent) {
		List<ParticipationUtil> listFinal = new ArrayList<ParticipationUtil>();

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.utilities.participation.ParticipationUtil(parti.id,parti.num_participation,parti.qualification,parti.annotation,parti.date_participation,ref.general_reference) "
                		+ "FROM Student stud "
                		+ "JOIN stud.participations parti "
                		+ "JOIN parti.matter mat "
                		+ "JOIN parti.reference ref "
                		+ "JOIN stud.studentModules mod "
                		+ "JOIN stud.studentCareers car "
                		+ "JOIN mat.matterOffers offe "
                		+ "WHERE offe.offer.id = ? "
                		+ "AND car.career.id = ? "
                		+ "AND mod.module.id = ? "
                		+ "AND mat.id = ? "
                		+ "AND stud.id = ?");
         
        hql.setLong(0, idOffer);
        hql.setLong(1, idCareer);
        hql.setLong(2, idModule);
        hql.setLong(3, idMatter);
        hql.setLong(4, idStudent);
        
        listFinal = hql.list();
		
		return listFinal;
	}

}
