package ec.edu.unl.DAO.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.LessonDAO;
import ec.edu.unl.entity.Lesson;
import ec.edu.unl.utilities.lesson.LessonUtil;

public class LessonDAOImpl implements LessonDAO{

    @Autowired
    HibernateTemplate hibernateTemplate;
    
	@Override
	@Transactional
	public void saveLesson(Lesson lesson) {
		hibernateTemplate.save(lesson);
		
	}

	@Override
	@Transactional
	public void updateLesson(Lesson lesson) {
		hibernateTemplate.update(lesson);
		
	}

	@Override
	@Transactional
	public void deleteLesson(Lesson lesson) {
		hibernateTemplate.delete(lesson);
		
	}

	@Override
	@Transactional
	public List<LessonUtil> returnListStudentLessons(long idOffer,
			long idCareer, long idModule, long idMatter, long idStudent) {
		
		List<LessonUtil> listFinal = new ArrayList<LessonUtil>();

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.utilities.lesson.LessonUtil(less.id,less.num_lesson,less.qualification,less.annotation,less.date_lesson,ref.general_reference) "
                		+ "FROM Student stud "
                		+ "JOIN stud.lessons less "
                		+ "JOIN less.matter mat "
                		+ "JOIN less.reference ref "
                		+ "JOIN stud.studentModules mod "
                		+ "JOIN stud.studentCareers car "
                		+ "JOIN mat.matterOffers offe "
                		+ "WHERE offe.offer.id = ? "
                		+ "AND car.career.id = ? "
                		+ "AND mod.module.id = ? "
                		+ "AND mat.id = ? "
                		+ "AND stud.id = ?");
         
        hql.setLong(0, idOffer);
        hql.setLong(1, idCareer);
        hql.setLong(2, idModule);
        hql.setLong(3, idMatter);
        hql.setLong(4, idStudent);
        
        listFinal = hql.list();
		
		return listFinal;
	}

}
