package ec.edu.unl.DAO.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.PersonalFactorDAO;
import ec.edu.unl.entity.Career;
import ec.edu.unl.entity.Matter;
import ec.edu.unl.entity.Module;
import ec.edu.unl.entity.Offer;
import ec.edu.unl.entity.PersonalFactor;
import ec.edu.unl.entity.Student;
import ec.edu.unl.entity._PersonalFactor;
import ec.edu.unl.entity.__PersonalFactor;

public class PersonalFactorDAOImpl implements PersonalFactorDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;
    
	@Override
	@Transactional
	public void savePersonalFactor(PersonalFactor personalFactor) {
		hibernateTemplate.save(personalFactor);
		
	}

	@Override
	@Transactional
	public void savePersonalFactor(_PersonalFactor personalFactor) {
		hibernateTemplate.save(personalFactor);
		
	}
	
	@Override
	@Transactional
	public void savePersonalFactor(__PersonalFactor personalFactor) {
		hibernateTemplate.save(personalFactor);
		
	}
	
	@Override
	@Transactional
	public void updatePersonalFactor(PersonalFactor personalFactor) {
		hibernateTemplate.update(personalFactor);
		
	}

	@Override
	@Transactional
	public void updatePersonalFactor(_PersonalFactor personalFactor) {
		hibernateTemplate.update(personalFactor);
		
	}
	
	@Override
	@Transactional
	public void updatePersonalFactor(__PersonalFactor personalFactor) {
		hibernateTemplate.update(personalFactor);
		
	}
	
	@Override
	@Transactional
	public void updateAllNewPersonalFactor(long offerId, long careerId,
			long moduleId, long matterId, long StudentId, PersonalFactor pf,
			_PersonalFactor _pf,
			__PersonalFactor __pf) {
		
		Offer offer = hibernateTemplate.get(Offer.class, offerId);
		Career career = hibernateTemplate.get(Career.class,careerId);
		Module module = hibernateTemplate.get(Module.class,moduleId);
		Matter matter = hibernateTemplate.get(Matter.class, matterId);
		Student student = hibernateTemplate.get(Student.class, StudentId);
		
		pf.setOffer(offer);
		pf.setCareer(career);
		pf.setModule(module);
		pf.setMatter(matter);
		pf.setStudent(student);
		
		_pf.setOffer(offer);
		_pf.setCareer(career);
		_pf.setStudent(student);
		
		__pf.setStudent(student);
		
		updatePersonalFactor(pf);
		updatePersonalFactor(_pf);
		updatePersonalFactor(__pf);
		
	}
	
	@Override
	@Transactional
	public void deletePersonalFactor(PersonalFactor personalFactor) {
		hibernateTemplate.delete(personalFactor);
		
	}

	@Override
	@Transactional
	public void deletePersonalFactor(_PersonalFactor personalFactor) {
		hibernateTemplate.delete(personalFactor);
		
	}
	@Override
	@Transactional
	public void deletePersonalFactor(__PersonalFactor personalFactor) {
		hibernateTemplate.delete(personalFactor);
		
	}
	@Override
	@Transactional
	public void generateDefaultPersonalFactor(long offerId, long careerId,
			long StudentId) {
		Offer offer = hibernateTemplate.get(Offer.class, offerId);
		Career career = hibernateTemplate.get(Career.class,careerId);
		Student student = hibernateTemplate.get(Student.class, StudentId);

		_PersonalFactor _pf = new _PersonalFactor();

		_pf.setOffer(offer);
		_pf.setCareer(career);
		_pf.setStudent(student);
		_pf.setCognitiveCompetence(8);
		_pf.setMotivation(1);
		_pf.setCognitiveConditions(8);
		_pf.setAcademicSelfConcept(2);
		_pf.setPerceivedSelfEfficacy(1);
		_pf.setPsychologicalWelfare(2);
		
		
		savePersonalFactor(_pf);
		
	}

	
	@Override
	@Transactional
	public void generateDefaultPersonalFactor(long StudentId) {
		Student student = hibernateTemplate.get(Student.class, StudentId);

		__PersonalFactor __pf = new __PersonalFactor();

		__pf.setStudent(student);
		
		__pf.setIntelligence(8);
		__pf.setSex(1);
		__pf.setAge(18);
		__pf.setMaritalStatus(0);
		__pf.setDependents(0);
		__pf.setAcademicTrainingSchool(8);
		__pf.setQualificationAccess(8.0);
		
		savePersonalFactor(__pf);
		
	}

	
	
	@Override
	@Transactional
	public void generateDefaultPersonalFactor(long offerId, long careerId,
			long moduleId, long matterId, long StudentId) {
		Offer offer = hibernateTemplate.get(Offer.class, offerId);
		Career career = hibernateTemplate.get(Career.class,careerId);
		Module module = hibernateTemplate.get(Module.class,moduleId);
		Matter matter = hibernateTemplate.get(Matter.class, matterId);
		Student student = hibernateTemplate.get(Student.class, StudentId);

		
		PersonalFactor pf = new PersonalFactor();
		pf.setOffer(offer);
		pf.setCareer(career);
		pf.setModule(module);
		pf.setMatter(matter);
		pf.setStudent(student);

		
		pf.setSatisfactionStudies(1);
		pf.setClassesAssistance(80);
		pf.setSkills(1);

		
		
		savePersonalFactor(pf);

		
	}

	
	@Override
	@Transactional
	public PersonalFactor returnPersonalFactor(long offerId, long careerId,
			long moduleId, long matterId, long studentId) {
		
		PersonalFactor pf = null;
		
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();

		Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.entity.PersonalFactor("
                		+ "pf.id,"
                		+ "pf.satisfactionStudies,"
                		+ "pf.classesAssistance,"
                		+ "pf.skills) "
                        + "FROM PersonalFactor pf "
                        + "WHERE pf.offer.id=? "
                        + "AND pf.career.id=? "
                        + "AND pf.module.id=? "
                        + "AND pf.matter.id=? "
                        + "AND pf.student.id=? ");
        
	     hql.setLong(0, offerId);
	     hql.setLong(1, careerId);
	     hql.setLong(2, moduleId);
	     hql.setLong(3, matterId);
	     hql.setLong(4, studentId);
		
		pf = (PersonalFactor) hql.uniqueResult();
		
		return pf;
	}

	@Override
	@Transactional
	public _PersonalFactor returnPersonalFactor(long offerId, long careerId,long studentId) {
		
		_PersonalFactor pf = null;
		
		
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
	
		Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.entity._PersonalFactor("
                		+ "pf.id,"
                		+ "pf.cognitiveCompetence,"
                		+ "pf.motivation,"
                		+ "pf.cognitiveConditions, "
                		+ "pf.academicSelfConcept, "
                		+ "pf.perceivedSelfEfficacy,"
                		+ "pf.psychologicalWelfare) "
                        + "FROM _PersonalFactor pf "
                        + "WHERE pf.offer.id=? "
                        + "AND pf.career.id=? "
                        + "AND pf.student.id=? ");
        
	     hql.setLong(0, offerId);
	     hql.setLong(1, careerId);
	     hql.setLong(2, studentId);
		
		pf = (_PersonalFactor) hql.uniqueResult();
		System.out.println("autoconcepto: "+pf.getAcademicSelfConcept());
		
		return pf;
	}
	
	
	@Override
	@Transactional
	public __PersonalFactor returnPersonalFactor(long studentId) {
		
		__PersonalFactor pf = hibernateTemplate.get(__PersonalFactor.class, studentId);
		
		return pf;
	}

	@Override
	@Transactional
	public boolean existPersonalFactorForThisStudentCareer(long offerId,
			long careerId, long StudentId) {

		boolean result = false;
		
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
	
		Query hql = session
                .createQuery("SELECT pf "
                        + "FROM _PersonalFactor pf "
                        + "WHERE pf.offer.id=? "
                        + "AND pf.career.id=? "
                        + "AND pf.student.id=? ");
        
	     hql.setLong(0, offerId);
	     hql.setLong(1, careerId);
	     hql.setLong(2, StudentId);
		
	    
		if(hql.uniqueResult() != null){
			result = true;
		}
		
		return result;
	}

}
