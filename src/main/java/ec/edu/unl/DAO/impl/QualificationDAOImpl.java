package ec.edu.unl.DAO.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.ClassWorkDAO;
import ec.edu.unl.DAO.EvaluationDAO;
import ec.edu.unl.DAO.ExtraClassWorkDAO;
import ec.edu.unl.DAO.LaboratoryDAO;
import ec.edu.unl.DAO.LessonDAO;
import ec.edu.unl.DAO.ParticipationDAO;
import ec.edu.unl.DAO.QualificationDAO;
import ec.edu.unl.DAO.ReferenceDAO;
import ec.edu.unl.entity.ClassWork;
import ec.edu.unl.entity.Evaluation;
import ec.edu.unl.entity.ExtraClassWork;
import ec.edu.unl.entity.Laboratory;
import ec.edu.unl.entity.Lesson;
import ec.edu.unl.entity.Matter;
import ec.edu.unl.entity.Participation;
import ec.edu.unl.entity.Reference;
import ec.edu.unl.entity.Student;
import ec.edu.unl.operations.Operations;
import ec.edu.unl.utilities.academic.NumberParameterUtil;
import ec.edu.unl.utilities.academic.ParameterDetailUtil;
import ec.edu.unl.utilities.academic.ParameterEditUtil;
import ec.edu.unl.utilities.academic.ParameterQualificationUtil;
import ec.edu.unl.utilities.academic.StudentSpecificUtil;

public class QualificationDAOImpl implements QualificationDAO{

    @Autowired
    HibernateTemplate hibernateTemplate;
    
    @Autowired
    @Qualifier("operationsImpl")
    Operations operations;
    
    @Autowired
    @Qualifier("referenceDAOImpl")
    ReferenceDAO referenceDAO;
    
    @Autowired
    @Qualifier("evaluationDAOImpl")
    EvaluationDAO evaluationDAO;

    
    @Autowired
    @Qualifier("lessonDAOImpl")
    LessonDAO lessonDAO;
    
    @Autowired
    @Qualifier("classWorkDAOImpl")
    ClassWorkDAO classWorkDAO;
    
    
    @Autowired
    @Qualifier("extraClassWorkDAOImpl")
    ExtraClassWorkDAO extraClassWorkDAO;
    
    @Autowired
    @Qualifier("participationDAOImpl")
    ParticipationDAO participationDAO;

    
    @Autowired
    @Qualifier("laboratoryDAOImpl")
    LaboratoryDAO laboratoryDAO;
    
    
    
	@Override
	@Transactional
	public List<StudentSpecificUtil> returnListStudentSpecific(long offerSelect,
			long careerSelect, long moduleSelect, long matterSelect,
			String teacherIC) {
		
		
		List<StudentSpecificUtil> listFinal = new ArrayList<StudentSpecificUtil>();

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.StudentSpecificUtil(car.career.id,offe.offer.id,mod.module.id,mat.matter.id,teach.teacher.id,s.id) "
                		+ "FROM Student s "
                		+ "JOIN s.studentMatters mat "
                		+ "JOIN s.studentModules mod "
                		+ "JOIN s.studentCareers car "
                		+ "JOIN mat.matter.teacherMatters teach "
                		+ "JOIN mat.matter.matterOffers offe "
                		+ "WHERE offe.offer.id = ? "
                		+ "AND car.career.id = ? "
                		+ "AND mod.module.id = ? "
                		+ "AND mat.matter.id = ? "
                		+ "AND teach.teacher.identification_card = ?");
         
        hql.setLong(0, offerSelect);
        hql.setLong(1, careerSelect);
        hql.setLong(2, moduleSelect);
        hql.setLong(3, matterSelect);
        hql.setString(4, teacherIC);
        
        listFinal = hql.list();
		
		return listFinal;
	}

	@Override
	@Transactional
	public List<ParameterQualificationUtil> returnQualificationParameter(StudentSpecificUtil studentQualification,String parameterStr) {
		
		
		List<ParameterQualificationUtil> listFinal = new ArrayList<ParameterQualificationUtil>();

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        
        Query hql = null;
        
		if(parameterStr.equals("evaluation")){
	        hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterQualificationUtil(car.career.id,offe.offer.id,mod.module.id,mat.id,teach.teacher.id,s.id,eval.id,eval.num_evaluation,eval.qualification,eval.annotation) "
	                		+ "FROM Student s "
	                		+ "JOIN s.evaluations eval "
	                		+ "JOIN eval.matter mat "
	                		+ "JOIN s.studentModules mod "
	                		+ "JOIN s.studentCareers car "
	                		+ "JOIN mat.matterOffers offe "
	                		+ "JOIN mat.teacherMatters teach "
	                		+ "WHERE car.career.id = ? "
	                		+ "AND offe.offer.id = ? "
	                		+ "AND mod.module.id = ? "
	                		+ "AND mat.id = ? "
	                		+ "AND teach.teacher.id = ?"
	                		+ "AND s.id = ? ");
		}
		
		if(parameterStr.equals("lesson")){
	        hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterQualificationUtil(car.career.id,offe.offer.id,mod.module.id,mat.id,teach.teacher.id,s.id,less.id,less.num_lesson,less.qualification,less.annotation) "
	                		+ "FROM Student s "
	                		+ "JOIN s.lessons less "
	                		+ "JOIN less.matter mat "
	                		+ "JOIN s.studentModules mod "
	                		+ "JOIN s.studentCareers car "
	                		+ "JOIN mat.matterOffers offe "
	                		+ "JOIN mat.teacherMatters teach "
	                		+ "WHERE car.career.id = ? "
	                		+ "AND offe.offer.id = ? "
	                		+ "AND mod.module.id = ? "
	                		+ "AND mat.id = ? "
	                		+ "AND teach.teacher.id = ?"
	                		+ "AND s.id = ? ");
		}
		
		if(parameterStr.equals("extraClassWork")){
			
	        hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterQualificationUtil(car.career.id,offe.offer.id,mod.module.id,mat.id,teach.teacher.id,s.id,ecw.id,ecw.num_extraClassWork,ecw.qualification,ecw.annotation) "
	                		+ "FROM Student s "
	                		+ "JOIN s.extraClassWorks ecw "
	                		+ "JOIN ecw.matter mat "
	                		+ "JOIN s.studentModules mod "
	                		+ "JOIN s.studentCareers car "
	                		+ "JOIN mat.matterOffers offe "
	                		+ "JOIN mat.teacherMatters teach "
	                		+ "WHERE car.career.id = ? "
	                		+ "AND offe.offer.id = ? "
	                		+ "AND mod.module.id = ? "
	                		+ "AND mat.id = ? "
	                		+ "AND teach.teacher.id = ?"
	                		+ "AND s.id = ? ");
		}
		
		if(parameterStr.equals("classWork")){
			
	        hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterQualificationUtil(car.career.id,offe.offer.id,mod.module.id,mat.id,teach.teacher.id,s.id,cw.id,cw.num_classWork,cw.qualification,cw.annotation) "
	                		+ "FROM Student s "
	                		+ "JOIN s.classWorks cw "
	                		+ "JOIN cw.matter mat "
	                		+ "JOIN s.studentModules mod "
	                		+ "JOIN s.studentCareers car "
	                		+ "JOIN mat.matterOffers offe "
	                		+ "JOIN mat.teacherMatters teach "
	                		+ "WHERE car.career.id = ? "
	                		+ "AND offe.offer.id = ? "
	                		+ "AND mod.module.id = ? "
	                		+ "AND mat.id = ? "
	                		+ "AND teach.teacher.id = ?"
	                		+ "AND s.id = ? ");
	        
		}
		
		if(parameterStr.equals("participation")){
			
			hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterQualificationUtil(car.career.id,offe.offer.id,mod.module.id,mat.id,teach.teacher.id,s.id,parti.id,parti.num_participation,parti.qualification,parti.annotation) "
	                		+ "FROM Student s "
	                		+ "JOIN s.participations parti "
	                		+ "JOIN parti.matter mat "
	                		+ "JOIN s.studentModules mod "
	                		+ "JOIN s.studentCareers car "
	                		+ "JOIN mat.matterOffers offe "
	                		+ "JOIN mat.teacherMatters teach "
	                		+ "WHERE car.career.id = ? "
	                		+ "AND offe.offer.id = ? "
	                		+ "AND mod.module.id = ? "
	                		+ "AND mat.id = ? "
	                		+ "AND teach.teacher.id = ?"
	                		+ "AND s.id = ? ");
		}
		
		if(parameterStr.equals("laboratory")){
		
			hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterQualificationUtil(car.career.id,offe.offer.id,mod.module.id,mat.id,teach.teacher.id,s.id,lab.id,lab.num_laboratory,lab.qualification,lab.annotation) "
	                		+ "FROM Student s "
	                		+ "JOIN s.laboratories lab "
	                		+ "JOIN lab.matter mat "
	                		+ "JOIN s.studentModules mod "
	                		+ "JOIN s.studentCareers car "
	                		+ "JOIN mat.matterOffers offe "
	                		+ "JOIN mat.teacherMatters teach "
	                		+ "WHERE car.career.id = ? "
	                		+ "AND offe.offer.id = ? "
	                		+ "AND mod.module.id = ? "
	                		+ "AND mat.id = ? "
	                		+ "AND teach.teacher.id = ?"
	                		+ "AND s.id = ? ");
		
		}
        
        hql.setLong(0, studentQualification.getIdCareer());
        hql.setLong(1, studentQualification.getIdOffer());
        hql.setLong(2, studentQualification.getIdModule());
        hql.setLong(3, studentQualification.getIdMatter());
        hql.setLong(4, studentQualification.getIdTeacher());
        hql.setLong(5, studentQualification.getIdStudent());
        
        listFinal = hql.list();
		
		return listFinal;
	}

	
	
	@Override
	@Transactional
	public int returnMaxNumberParameter(long idCareer,long idOffer,long idModule,long idMatter,long idTeacher,String parameterStr) {
		
		List<ParameterQualificationUtil> listFinal = new ArrayList<ParameterQualificationUtil>();

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = null;

        if(parameterStr.equals("evaluation")){
	        hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterQualificationUtil(car.career.id,offe.offer.id,mod.module.id,mat.id,teach.teacher.id,s.id,eval.id,eval.num_evaluation,eval.qualification,eval.annotation) "
	                		+ "FROM Student s "
	                		+ "JOIN s.evaluations eval "
	                		+ "JOIN eval.matter mat "
	                		+ "JOIN s.studentModules mod "
	                		+ "JOIN s.studentCareers car "
	                		+ "JOIN mat.matterOffers offe "
	                		+ "JOIN mat.teacherMatters teach "
	                		+ "WHERE car.career.id = ? "
	                		+ "AND offe.offer.id = ? "
	                		+ "AND mod.module.id = ? "
	                		+ "AND mat.id = ? "
	                		+ "AND teach.teacher.id = ?");
		}
		
		if(parameterStr.equals("lesson")){
	        hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterQualificationUtil(car.career.id,offe.offer.id,mod.module.id,mat.id,teach.teacher.id,s.id,less.id,less.num_lesson,less.qualification,less.annotation) "
	                		+ "FROM Student s "
	                		+ "JOIN s.lessons less "
	                		+ "JOIN less.matter mat "
	                		+ "JOIN s.studentModules mod "
	                		+ "JOIN s.studentCareers car "
	                		+ "JOIN mat.matterOffers offe "
	                		+ "JOIN mat.teacherMatters teach "
	                		+ "WHERE car.career.id = ? "
	                		+ "AND offe.offer.id = ? "
	                		+ "AND mod.module.id = ? "
	                		+ "AND mat.id = ? "
	                		+ "AND teach.teacher.id = ?");
		}
		
		if(parameterStr.equals("extraClassWork")){
			
	        hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterQualificationUtil(car.career.id,offe.offer.id,mod.module.id,mat.id,teach.teacher.id,s.id,ecw.id,ecw.num_extraClassWork,ecw.qualification,ecw.annotation) "
	                		+ "FROM Student s "
	                		+ "JOIN s.extraClassWorks ecw "
	                		+ "JOIN ecw.matter mat "
	                		+ "JOIN s.studentModules mod "
	                		+ "JOIN s.studentCareers car "
	                		+ "JOIN mat.matterOffers offe "
	                		+ "JOIN mat.teacherMatters teach "
	                		+ "WHERE car.career.id = ? "
	                		+ "AND offe.offer.id = ? "
	                		+ "AND mod.module.id = ? "
	                		+ "AND mat.id = ? "
	                		+ "AND teach.teacher.id = ?");
		}
		
		if(parameterStr.equals("classWork")){
			
	        hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterQualificationUtil(car.career.id,offe.offer.id,mod.module.id,mat.id,teach.teacher.id,s.id,cw.id,cw.num_classWork,cw.qualification,cw.annotation) "
	                		+ "FROM Student s "
	                		+ "JOIN s.classWorks cw "
	                		+ "JOIN cw.matter mat "
	                		+ "JOIN s.studentModules mod "
	                		+ "JOIN s.studentCareers car "
	                		+ "JOIN mat.matterOffers offe "
	                		+ "JOIN mat.teacherMatters teach "
	                		+ "WHERE car.career.id = ? "
	                		+ "AND offe.offer.id = ? "
	                		+ "AND mod.module.id = ? "
	                		+ "AND mat.id = ? "
	                		+ "AND teach.teacher.id = ?");
	        
		}
		
		if(parameterStr.equals("participation")){
			
			hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterQualificationUtil(car.career.id,offe.offer.id,mod.module.id,mat.id,teach.teacher.id,s.id,parti.id,parti.num_participation,parti.qualification,parti.annotation) "
	                		+ "FROM Student s "
	                		+ "JOIN s.participations parti "
	                		+ "JOIN parti.matter mat "
	                		+ "JOIN s.studentModules mod "
	                		+ "JOIN s.studentCareers car "
	                		+ "JOIN mat.matterOffers offe "
	                		+ "JOIN mat.teacherMatters teach "
	                		+ "WHERE car.career.id = ? "
	                		+ "AND offe.offer.id = ? "
	                		+ "AND mod.module.id = ? "
	                		+ "AND mat.id = ? "
	                		+ "AND teach.teacher.id = ?");
		}
		
		if(parameterStr.equals("laboratory")){
		
			hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterQualificationUtil(car.career.id,offe.offer.id,mod.module.id,mat.id,teach.teacher.id,s.id,lab.id,lab.num_laboratory,lab.qualification,lab.annotation) "
	                		+ "FROM Student s "
	                		+ "JOIN s.laboratories lab "
	                		+ "JOIN lab.matter mat "
	                		+ "JOIN s.studentModules mod "
	                		+ "JOIN s.studentCareers car "
	                		+ "JOIN mat.matterOffers offe "
	                		+ "JOIN mat.teacherMatters teach "
	                		+ "WHERE car.career.id = ? "
	                		+ "AND offe.offer.id = ? "
	                		+ "AND mod.module.id = ? "
	                		+ "AND mat.id = ? "
	                		+ "AND teach.teacher.id = ?");
		
		}
         
     
        hql.setLong(0, idCareer);
        hql.setLong(1, idOffer);
        hql.setLong(2, idModule);
        hql.setLong(3, idMatter);
        hql.setLong(4, idTeacher);
        
        listFinal = hql.list();
        
        if(listFinal.size() == 0){
        	return 0;
        }else{
        
        	int[]vector = new int[listFinal.size()];
        	int i=0;
        	for(ParameterQualificationUtil ev:listFinal){
        		vector[i] = ev.getNumParameter();
        		i++;
        	}
        
        	int result = operations.calculateMax(vector);
 
			return result;
        }
	}

	
	
	@Override
	@Transactional
	public List<ParameterEditUtil> returnListStudentParameterEdit(long offerSelect, long careerSelect, long moduleSelect,
			long idMatterNew, long idTeacherNew, Integer numEvaluation,String parameterStr) {
		
		
		List<ParameterEditUtil> listFinal = new ArrayList<ParameterEditUtil>();
        Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
        
        Query hql = null; 
        
        if(parameterStr.equals("evaluation")){
        	hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterEditUtil(st.id,ev.id,ref.id,ev.num_evaluation,ev.qualification,ev.annotation,ev.date_evaluation,ref.general_reference) "
                    		+ "FROM Evaluation ev "
                    		+ "JOIN ev.matter mat "
                    		+ "JOIN ev.student st "
                    		+ "JOIN ev.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? "
                    		+ "AND ev.num_evaluation=? ");
		}
		
		if(parameterStr.equals("lesson")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterEditUtil(st.id,less.id,ref.id,less.num_lesson,less.qualification,less.annotation,less.date_lesson,ref.general_reference) "
                    		+ "FROM Lesson less "
                    		+ "JOIN less.matter mat "
                    		+ "JOIN less.student st "
                    		+ "JOIN less.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? "
                    		+ "AND less.num_lesson=? ");
		}
		
		if(parameterStr.equals("extraClassWork")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterEditUtil(st.id,ecw.id,ref.id,ecw.num_extraClassWork,ecw.qualification,ecw.annotation,ecw.date_extraclasswork,ref.general_reference) "
                    		+ "FROM ExtraClassWork ecw "
                    		+ "JOIN ecw.matter mat "
                    		+ "JOIN ecw.student st "
                    		+ "JOIN ecw.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? "
                    		+ "AND ecw.num_extraClassWork=? ");
		}
		
		if(parameterStr.equals("classWork")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterEditUtil(st.id,cw.id,ref.id,cw.num_classWork,cw.qualification,cw.annotation,cw.date_classwork,ref.general_reference) "
                    		+ "FROM ClassWork cw "
                    		+ "JOIN cw.matter mat "
                    		+ "JOIN cw.student st "
                    		+ "JOIN cw.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? "
                    		+ "AND cw.num_classWork=? ");
		}
		
		if(parameterStr.equals("participation")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterEditUtil(st.id,parti.id,ref.id,parti.num_participation,parti.qualification,parti.annotation,parti.date_participation,ref.general_reference) "
                    		+ "FROM Participation parti "
                    		+ "JOIN parti.matter mat "
                    		+ "JOIN parti.student st "
                    		+ "JOIN parti.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? "
                    		+ "AND parti.num_participation=? ");
		}
		
		if(parameterStr.equals("laboratory")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterEditUtil(st.id,lab.id,ref.id,lab.num_laboratory,lab.qualification,lab.annotation,lab.date_laboratory,ref.general_reference) "
                    		+ "FROM Laboratory lab "
                    		+ "JOIN lab.matter mat "
                    		+ "JOIN lab.student st "
                    		+ "JOIN lab.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? "
                    		+ "AND lab.num_laboratory=? ");
		}
        
        
        
        
          
        hql.setLong(0, offerSelect);
        hql.setLong(1, careerSelect);
        hql.setLong(2, moduleSelect);
        hql.setLong(3, idMatterNew);
        hql.setLong(4, idTeacherNew);
        hql.setInteger(5, numEvaluation);
        
       
        
        listFinal = hql.list();
		
		return listFinal;

		
		
	}

	
	
	
	
	@Override
	@Transactional
	public List<NumberParameterUtil> returnListDateParameter(long offerSelect, long careerSelect, long moduleSelect,
			long idMatterNew, long idTeacherNew,String parameterStr) {
		
		
		List<NumberParameterUtil> listFinal = new ArrayList<NumberParameterUtil>();
        Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
        
        Query hql = null; 
        
        if(parameterStr.equals("evaluation")){
        	hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.NumberParameterUtil(ev.num_evaluation,ev.date_evaluation) "
                    		+ "FROM Evaluation ev "
                    		+ "JOIN ev.matter mat "
                    		+ "JOIN ev.student st "
                    		+ "JOIN ev.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? ");
		}
		
		if(parameterStr.equals("lesson")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.NumberParameterUtil(less.num_lesson,less.date_lesson) "
                    		+ "FROM Lesson less "
                    		+ "JOIN less.matter mat "
                    		+ "JOIN less.student st "
                    		+ "JOIN less.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? ");
		}
		
		if(parameterStr.equals("extraClassWork")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.NumberParameterUtil(ecw.num_extraClassWork,ecw.date_extraclasswork) "
                    		+ "FROM ExtraClassWork ecw "
                    		+ "JOIN ecw.matter mat "
                    		+ "JOIN ecw.student st "
                    		+ "JOIN ecw.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? ");
		}
		
		if(parameterStr.equals("classWork")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.NumberParameterUtil(cw.num_classWork,cw.date_classwork) "
                    		+ "FROM ClassWork cw "
                    		+ "JOIN cw.matter mat "
                    		+ "JOIN cw.student st "
                    		+ "JOIN cw.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? ");
		}
		
		if(parameterStr.equals("participation")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.NumberParameterUtil(parti.num_participation,parti.date_participation) "
                    		+ "FROM Participation parti "
                    		+ "JOIN parti.matter mat "
                    		+ "JOIN parti.student st "
                    		+ "JOIN parti.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? ");
		}
		
		if(parameterStr.equals("laboratory")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.NumberParameterUtil(lab.num_laboratory,lab.date_laboratory) "
                    		+ "FROM Laboratory lab "
                    		+ "JOIN lab.matter mat "
                    		+ "JOIN lab.student st "
                    		+ "JOIN lab.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? ");
		}
        
        hql.setLong(0, offerSelect);
        hql.setLong(1, careerSelect);
        hql.setLong(2, moduleSelect);
        hql.setLong(3, idMatterNew);
        hql.setLong(4, idTeacherNew);
        
        listFinal = hql.list();
		
		return listFinal;

		
		
	}

	
	@Override
	@Transactional
	public void saveAllQualificationParameter(List<StudentSpecificUtil> listStudentSpecific,Date dateSelect, Reference reference,int numParam,String parameterStr) {		
		
		for(StudentSpecificUtil studentData: listStudentSpecific){
			
			Matter matter = (Matter) hibernateTemplate.get(Matter.class, studentData.getIdMatter());
			Student student = (Student) hibernateTemplate.get(Student.class, studentData.getIdStudent());
			
			if(parameterStr.equals("evaluation")){
				
				Evaluation evaluation = new Evaluation();
				
				evaluation.setAnnotation(studentData.getAnnotation());
				evaluation.setNum_evaluation(numParam);
				evaluation.setDate_evaluation(dateSelect);
				evaluation.setMatter(matter);
				evaluation.setStudent(student);
				evaluation.setQualification(studentData.getQualification());
				evaluation.setReference(reference);
				
				evaluationDAO.saveEvaluation(evaluation);
			}
			
			if(parameterStr.equals("lesson")){
				
				Lesson lesson = new Lesson();
				
				lesson.setAnnotation(studentData.getAnnotation());
				lesson.setNum_lesson(numParam);
				lesson.setDate_lesson(dateSelect);
				lesson.setMatter(matter);
				lesson.setStudent(student);
				lesson.setQualification(studentData.getQualification());
				lesson.setReference(reference);
				
				lessonDAO.saveLesson(lesson);
				
				
				
			}
			
			if(parameterStr.equals("extraClassWork")){
				
				ExtraClassWork ecw = new ExtraClassWork();
				
				ecw.setAnnotation(studentData.getAnnotation());
				ecw.setNum_extraClassWork(numParam);
				ecw.setDate_extraclasswork(dateSelect);
				ecw.setMatter(matter);
				ecw.setStudent(student);
				ecw.setQualification(studentData.getQualification());
				ecw.setReference(reference);
				
				extraClassWorkDAO.saveExtraClassWork(ecw);
			}
			
			if(parameterStr.equals("classWork")){
				
				ClassWork cw = new ClassWork();
				
				cw.setAnnotation(studentData.getAnnotation());
				cw.setNum_classWork(numParam);
				cw.setDate_classwork(dateSelect);
				cw.setMatter(matter);
				cw.setStudent(student);
				cw.setQualification(studentData.getQualification());
				cw.setReference(reference);
				
				classWorkDAO.saveClassWork(cw);
			}
			
			if(parameterStr.equals("participation")){
				
				Participation parti = new Participation();
				
				parti.setAnnotation(studentData.getAnnotation());
				parti.setNum_participation(numParam);
				parti.setDate_participation(dateSelect);
				parti.setMatter(matter);
				parti.setStudent(student);
				parti.setQualification(studentData.getQualification());
				parti.setReference(reference);
				
				participationDAO.saveParticipation(parti);
				
			}
			
			if(parameterStr.equals("laboratory")){
				
				Laboratory lab = new Laboratory();
				
				lab.setAnnotation(studentData.getAnnotation());
				lab.setNum_laboratory(numParam);
				lab.setDate_laboratory(dateSelect);
				lab.setMatter(matter);
				lab.setStudent(student);
				lab.setQualification(studentData.getQualification());
				lab.setReference(reference);
				
				laboratoryDAO.saveLaboratory(lab);
			}
			
			
			

		}
	}

	
	@Override
	@Transactional
	public void updateSelectedQualificationParameter(List<ParameterEditUtil> listStudentParameterEdit,String parameterStr) {
		
		if(parameterStr.equals("evaluation")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				Evaluation evaluation = hibernateTemplate.get(Evaluation.class, data.getIdParameter());
				evaluation.setQualification(data.getQualification());
				evaluationDAO.updateEvaluation(evaluation);		
			}
		}
		
		if(parameterStr.equals("lesson")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				Lesson lesson = hibernateTemplate.get(Lesson.class, data.getIdParameter());
				lesson.setQualification(data.getQualification());
				lessonDAO.updateLesson(lesson);		
			}
		}
		
		if(parameterStr.equals("extraClassWork")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				ExtraClassWork ecw = hibernateTemplate.get(ExtraClassWork.class, data.getIdParameter());
				ecw.setQualification(data.getQualification());
				extraClassWorkDAO.updateExtraClassWork(ecw);		
			}
		}
		
		if(parameterStr.equals("classWork")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				ClassWork cw = hibernateTemplate.get(ClassWork.class, data.getIdParameter());
				cw.setQualification(data.getQualification());
				classWorkDAO.updateClassWork(cw);		
			}
		}
		
		if(parameterStr.equals("participation")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				Participation parti = hibernateTemplate.get(Participation.class, data.getIdParameter());
				parti.setQualification(data.getQualification());
				participationDAO.updateParticipation(parti);		
			}
		}
		
		if(parameterStr.equals("laboratory")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				Laboratory lab = hibernateTemplate.get(Laboratory.class, data.getIdParameter());
				lab.setQualification(data.getQualification());
				laboratoryDAO.updateLaboratory(lab);		
			}
		}
		
	}

	
	@Override
	@Transactional
	public void updateAllQualificationParameter(List<ParameterEditUtil> listStudentParameterEdit, Date dateSelect,String referenceData,String parameterStr) {
		
		Reference reference = (Reference) hibernateTemplate.get(Reference.class, listStudentParameterEdit.get(0).getIdReference());
		
		if(!reference.getGeneral_reference().equals(referenceData)){
			reference.setGeneral_reference(referenceData);
			referenceDAO.updateReference(reference);
		}
		
		if(parameterStr.equals("evaluation")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				Evaluation evaluation = hibernateTemplate.get(Evaluation.class, data.getIdParameter());
				evaluation.setAnnotation(data.getAnnotation());
				evaluation.setDate_evaluation(dateSelect);
				evaluation.setQualification(data.getQualification());
				evaluationDAO.updateEvaluation(evaluation);		
			}
		}
		
		if(parameterStr.equals("lesson")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				Lesson lesson = hibernateTemplate.get(Lesson.class, data.getIdParameter());
				lesson.setAnnotation(data.getAnnotation());
				lesson.setDate_lesson (dateSelect);
				lesson.setQualification(data.getQualification());
				lessonDAO.updateLesson(lesson);		
			}
		}
		
		if(parameterStr.equals("extraClassWork")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				ExtraClassWork ecw = hibernateTemplate.get(ExtraClassWork.class, data.getIdParameter());
				ecw.setAnnotation(data.getAnnotation());
				ecw.setDate_extraclasswork (dateSelect);
				ecw.setQualification(data.getQualification());
				extraClassWorkDAO.updateExtraClassWork(ecw);		
			}
		}
		
		if(parameterStr.equals("classWork")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				ClassWork cw = hibernateTemplate.get(ClassWork.class, data.getIdParameter());
				cw.setAnnotation(data.getAnnotation());
				cw.setDate_classwork (dateSelect);
				cw.setQualification(data.getQualification());
				classWorkDAO.updateClassWork(cw);		
			}
		}
		
		if(parameterStr.equals("participation")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				Participation parti = hibernateTemplate.get(Participation.class, data.getIdParameter());
				parti.setAnnotation(data.getAnnotation());
				parti.setDate_participation (dateSelect);
				parti.setQualification(data.getQualification());
				participationDAO.updateParticipation(parti);		
			}
		}
		
		if(parameterStr.equals("laboratory")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				Laboratory lab = hibernateTemplate.get(Laboratory.class, data.getIdParameter());
				lab.setAnnotation(data.getAnnotation());
				lab.setDate_laboratory (dateSelect);
				lab.setQualification(data.getQualification());
				laboratoryDAO.updateLaboratory(lab);		
			}
		}
		
	}

	
	
	
	@Override
	@Transactional
	public void deleteAllQualificationParameter(List<ParameterEditUtil> listStudentParameterEdit,String parameterStr) {

		if(parameterStr.equals("evaluation")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				Evaluation evaluation = hibernateTemplate.get(Evaluation.class, data.getIdParameter());
				evaluationDAO.deleteEvaluation(evaluation);	
			}
		}
		
		if(parameterStr.equals("lesson")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				Lesson lesson = hibernateTemplate.get(Lesson.class, data.getIdParameter());
				lessonDAO.deleteLesson(lesson);	
			}
		}
		
		if(parameterStr.equals("extraClassWork")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				ExtraClassWork ecw = hibernateTemplate.get(ExtraClassWork.class, data.getIdParameter());
				extraClassWorkDAO.deleteExtraClassWork(ecw);	
			}
		}
		
		if(parameterStr.equals("classWork")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				ClassWork cw = hibernateTemplate.get(ClassWork.class, data.getIdParameter());
				classWorkDAO.deleteClassWork(cw);	
			}
		}
		
		if(parameterStr.equals("participation")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				Participation parti = hibernateTemplate.get(Participation.class, data.getIdParameter());
				participationDAO.deleteParticipation(parti);	
			}
		}
		
		if(parameterStr.equals("laboratory")){
			for(ParameterEditUtil data:listStudentParameterEdit){
				Laboratory lab = hibernateTemplate.get(Laboratory.class, data.getIdParameter());
				laboratoryDAO.deleteLaboratory(lab);	
			}
		}
		
		Reference reference = (Reference) hibernateTemplate.get(Reference.class, listStudentParameterEdit.get(0).getIdReference());
		referenceDAO.deleteReference(reference);
	}

	@Override
	@Transactional
	public ParameterDetailUtil returnListDataParameterDetail(long offerSelect,long careerSelect, long moduleSelect, long idMatterNew,
			long idTeacherNew, Integer numParam, String parameterStr) {
		
		List<ParameterEditUtil> listFinal = new ArrayList<ParameterEditUtil>();
        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();  
        
        
        Query hql = null; 
        
        if(parameterStr.equals("evaluation")){
        	hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterEditUtil(st.id,ev.id,ref.id,ev.num_evaluation,ev.qualification,ev.annotation,ev.date_evaluation,ref.general_reference) "
                    		+ "FROM Evaluation ev "
                    		+ "JOIN ev.matter mat "
                    		+ "JOIN ev.student st "
                    		+ "JOIN ev.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? "
                    		+ "AND ev.num_evaluation=? ");
		}
		
		if(parameterStr.equals("lesson")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterEditUtil(st.id,less.id,ref.id,less.num_lesson,less.qualification,less.annotation,less.date_lesson,ref.general_reference) "
                    		+ "FROM Lesson less "
                    		+ "JOIN less.matter mat "
                    		+ "JOIN less.student st "
                    		+ "JOIN less.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? "
                    		+ "AND less.num_lesson=? ");
		}
		
		if(parameterStr.equals("extraClassWork")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterEditUtil(st.id,ecw.id,ref.id,ecw.num_extraClassWork,ecw.qualification,ecw.annotation,ecw.date_extraclasswork,ref.general_reference) "
                    		+ "FROM ExtraClassWork ecw "
                    		+ "JOIN ecw.matter mat "
                    		+ "JOIN ecw.student st "
                    		+ "JOIN ecw.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? "
                    		+ "AND ecw.num_extraClassWork=? ");
		}
		
		if(parameterStr.equals("classWork")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterEditUtil(st.id,cw.id,ref.id,cw.num_classWork,cw.qualification,cw.annotation,cw.date_classwork,ref.general_reference) "
                    		+ "FROM ClassWork cw "
                    		+ "JOIN cw.matter mat "
                    		+ "JOIN cw.student st "
                    		+ "JOIN cw.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? "
                    		+ "AND cw.num_classWork=? ");
		}
		
		if(parameterStr.equals("participation")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterEditUtil(st.id,parti.id,ref.id,parti.num_participation,parti.qualification,parti.annotation,parti.date_participation,ref.general_reference) "
                    		+ "FROM Participation parti "
                    		+ "JOIN parti.matter mat "
                    		+ "JOIN parti.student st "
                    		+ "JOIN parti.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? "
                    		+ "AND parti.num_participation=? ");
		}
		
		if(parameterStr.equals("laboratory")){
			hql = session
                    .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ParameterEditUtil(st.id,lab.id,ref.id,lab.num_laboratory,lab.qualification,lab.annotation,lab.date_laboratory,ref.general_reference) "
                    		+ "FROM Laboratory lab "
                    		+ "JOIN lab.matter mat "
                    		+ "JOIN lab.student st "
                    		+ "JOIN lab.reference ref "
                    		+ "JOIN st.studentModules mod "
                    		+ "JOIN mat.teacherMatters teach "
                    		+ "JOIN teach.teacher.teacherCareers car "
                    		+ "JOIN mat.matterOffers offe "
                    		+ "WHERE offe.offer.id=? "
                    		+ "AND car.career.id=? "
                    		+ "AND mod.module.id=? "
                    		+ "AND mat.id=? "
                    		+ "AND teach.teacher.id=? "
                    		+ "AND lab.num_laboratory=? ");
		}
   
        hql.setLong(0, offerSelect);
        hql.setLong(1, careerSelect);
        hql.setLong(2, moduleSelect);
        hql.setLong(3, idMatterNew);
        hql.setLong(4, idTeacherNew);
        hql.setInteger(5, numParam);
        
        listFinal = hql.list();
        
        ParameterDetailUtil ed = new ParameterDetailUtil();
        ed.setDate_parameter(listFinal.get(0).getDate_parameter());
        ed.setDescription_num_parameter(listFinal.get(0).getNum_parameter());
        ed.setGeneral_reference(listFinal.get(0).getGeneral_reference());
       
		return ed;
	}
	
	@Override
	@Transactional
	public List<ParameterDetailUtil> returnListStudentParameterDetail(
			long offerSelect, long careerSelect, long moduleSelect,
			long matterSelect, long idTeacherNew, int numParams, String parameterStr) {
		
		List<ParameterDetailUtil> list = new ArrayList<ParameterDetailUtil>();
		for(int i=1;i<=numParams;i++){
			list.add(returnListDataParameterDetail(offerSelect, careerSelect, moduleSelect,
					matterSelect, idTeacherNew, i ,parameterStr));
		}
		
		return list;
	}
	

}
