package ec.edu.unl.DAO.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.SocialFactorDAO;
import ec.edu.unl.entity.Career;
import ec.edu.unl.entity.Matter;
import ec.edu.unl.entity.Module;
import ec.edu.unl.entity.Offer;
import ec.edu.unl.entity.PersonalFactor;
import ec.edu.unl.entity.SocialFactor;
import ec.edu.unl.entity.Student;

public class SocialFactorDAOImpl implements SocialFactorDAO {

	@Autowired
    HibernateTemplate hibernateTemplate;
	
	@Override
	@Transactional
	public void saveSocialFactor(SocialFactor socialFactor) {
		hibernateTemplate.save(socialFactor);
		
	}
	

	@Override
	@Transactional
	public void updateSocialFactor(SocialFactor socialFactor) {
		hibernateTemplate.update(socialFactor);
		
	}

	
	@Override
	@Transactional
	public void updateAllNewSocialFactor(long StudentId, SocialFactor sf) {
		Student student = hibernateTemplate.get(Student.class, StudentId);

		sf.setStudent(student);
		
		updateSocialFactor(sf);
		
	}

	
	
	@Override
	@Transactional
	public void deleteSocialFactor(SocialFactor socialFactor) {
		hibernateTemplate.delete(socialFactor);
		
	}

	@Override
	@Transactional
	public void generateDefaultSocialFactor(long StudentId) {
		
		Student student = hibernateTemplate.get(Student.class, StudentId);
		
		SocialFactor sf = new SocialFactor();
		sf.setStudent(student);
		sf.setSocialDifference(false);
		sf.setFamilyEnvironment(2);
		sf.setSocioeconomicContext(1);
		sf.setDemographicVariable(2);
		
		
		saveSocialFactor(sf);
		
		
		
	}

	@Override
	@Transactional
	public SocialFactor returnSocialFactor(long StudentId) {
		
		SocialFactor sf = hibernateTemplate.get(SocialFactor.class, StudentId);

		return sf;
	}

	
}
