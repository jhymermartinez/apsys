package ec.edu.unl.DAO.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.InstitutionalFactorDAO;
import ec.edu.unl.entity.Career;
import ec.edu.unl.entity.InstitutionalFactor;
import ec.edu.unl.entity.Matter;
import ec.edu.unl.entity.Module;
import ec.edu.unl.entity.Offer;
import ec.edu.unl.entity.Student;
import ec.edu.unl.entity._InstitutionalFactor;

public class InstitutionalFactorDAOImpl implements InstitutionalFactorDAO {

	@Autowired
    HibernateTemplate hibernateTemplate;
	
	@Override
	@Transactional
	public void saveInstitutionalFactor(InstitutionalFactor institutionalFactor) {
		hibernateTemplate.save(institutionalFactor);
		
	}

	@Override
	@Transactional
	public void saveInstitutionalFactor(_InstitutionalFactor institutionalFactor) {
		hibernateTemplate.save(institutionalFactor);
		
	}
	
	
	@Override
	@Transactional
	public void updateInstitutionalFactor(
			InstitutionalFactor institutionalFactor) {
		hibernateTemplate.update(institutionalFactor);
		
	}
	@Override
	@Transactional
	public void updateInstitutionalFactor(
			_InstitutionalFactor institutionalFactor) {
		hibernateTemplate.update(institutionalFactor);
		
	}
	

	@Override
	@Transactional
	public void updateAllNewInstitutionalFactor(long offerId, long careerId,
			long moduleId, long matterId, long StudentId,
			InstitutionalFactor ifac, _InstitutionalFactor _ifac) {
		
		Offer offer = hibernateTemplate.get(Offer.class, offerId);
		Career career = hibernateTemplate.get(Career.class,careerId);
		Module module = hibernateTemplate.get(Module.class,moduleId);
		Matter matter = hibernateTemplate.get(Matter.class, matterId);
		Student student = hibernateTemplate.get(Student.class, StudentId);
		
		ifac.setOffer(offer);
		ifac.setCareer(career);
		ifac.setModule(module);
		ifac.setMatter(matter);
		ifac.setStudent(student);
		
		_ifac.setOffer(offer);
		_ifac.setCareer(career);
		_ifac.setStudent(student);
		
		updateInstitutionalFactor(ifac);
		updateInstitutionalFactor(_ifac);
		
	}
	
	@Override
	@Transactional
	public void deleteInstitutionalFactor(
			InstitutionalFactor institutionalFactor) {
		hibernateTemplate.delete(institutionalFactor);
		
	}

	@Override
	@Transactional
	public void deleteInstitutionalFactor(
			_InstitutionalFactor institutionalFactor) {
		hibernateTemplate.delete(institutionalFactor);
		
	}
	
	@Override
	@Transactional
	public void generateDefaultInstitutionalFactor(long offerId, long careerId,
			long moduleId, long matterId, long StudentId) {
		Offer offer = hibernateTemplate.get(Offer.class, offerId);
		Career career = hibernateTemplate.get(Career.class,careerId);
		Module module = hibernateTemplate.get(Module.class,moduleId);
		Matter matter = hibernateTemplate.get(Matter.class, matterId);
		Student student = hibernateTemplate.get(Student.class, StudentId);
		
		InstitutionalFactor ifac = new InstitutionalFactor();
		
		ifac.setOffer(offer);
		ifac.setCareer(career);
		ifac.setModule(module);
		ifac.setMatter(matter);
		ifac.setStudent(student);
		
		ifac.setComplexityMatter(5);
		ifac.setRelationshipStudentTeacher(2);

		saveInstitutionalFactor(ifac);
		
	}

	
	@Override
	@Transactional
	public void generateDefaultInstitutionalFactor(long offerId, long careerId,
			long StudentId) {
		Offer offer = hibernateTemplate.get(Offer.class, offerId);
		Career career = hibernateTemplate.get(Career.class,careerId);
		Student student = hibernateTemplate.get(Student.class, StudentId);

		_InstitutionalFactor _ifac = new _InstitutionalFactor();

		
		_ifac.setOffer(offer);
		_ifac.setCareer(career);
		_ifac.setStudent(student);
		
		_ifac.setDesiredCareer(true);
		_ifac.setInstitutionalStatus(8);
		_ifac.setSupportService(true);
		_ifac.setStudentLife(2);
		
		saveInstitutionalFactor(_ifac);
		
	}

	@Override
	@Transactional
	public InstitutionalFactor returnInstitutionalFactor(long offerId,
			long careerId, long moduleId, long matterId, long StudentId) {
		
		InstitutionalFactor ifac = null;
		
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		
		Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.entity.InstitutionalFactor("
                		+ "ifac.id,"
                		+ "ifac.complexityMatter,"
                		+ "ifac.relationshipStudentTeacher) "
                        + "FROM InstitutionalFactor ifac "
                        + "WHERE ifac.offer.id=? "
                        + "AND ifac.career.id=? "
                        + "AND ifac.module.id=? "
                        + "AND ifac.matter.id=? "
                        + "AND ifac.student.id=? ");
		
		 hql.setLong(0, offerId);
	     hql.setLong(1, careerId);
	     hql.setLong(2, moduleId);
	     hql.setLong(3, matterId);
	     hql.setLong(4, StudentId);
	     
	     ifac = (InstitutionalFactor) hql.uniqueResult();
	      
		return ifac;
	}


	@Override
	@Transactional
	public _InstitutionalFactor returnInstitutionalFactor(long offerId,
			long careerId, long StudentId) {
		
		_InstitutionalFactor ifac = null;
		
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();

		Query hql = session
                .createQuery("SELECT NEW ec.edu.unl.entity._InstitutionalFactor("
                		+ "ifac.id,"
                		+ "ifac.desiredCareer,"
                		+ "ifac.institutionalStatus,"
                		+ "ifac.supportService,"
                		+ "ifac.studentLife) "
                        + "FROM _InstitutionalFactor ifac "
                        + "WHERE ifac.offer.id=? "
                        + "AND ifac.career.id=? "
                        + "AND ifac.student.id=? ");
		
		 hql.setLong(0, offerId);
	     hql.setLong(1, careerId);
	     hql.setLong(2, StudentId);
	     
	     ifac = (_InstitutionalFactor) hql.uniqueResult();
	      
		return ifac;
	}

}
