package ec.edu.unl.DAO.impl;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.AcademicDAO;
import ec.edu.unl.DAO.CareerDAO;
import ec.edu.unl.DAO.MatterDAO;
import ec.edu.unl.DAO.ModuleDAO;
import ec.edu.unl.DAO.StudentCareerDAO;
import ec.edu.unl.DAO.StudentDAO;
import ec.edu.unl.DAO.StudentMatterDAO;
import ec.edu.unl.DAO.StudentModuleDAO;
import ec.edu.unl.DAO.TeacherCareerDAO;
import ec.edu.unl.DAO.TeacherDAO;
import ec.edu.unl.DAO.TeacherMatterDAO;
import ec.edu.unl.DAO.TeacherModuleDAO;
import ec.edu.unl.entity.Career;
import ec.edu.unl.entity.Matter;
import ec.edu.unl.entity.Module;
import ec.edu.unl.entity.Student;
import ec.edu.unl.entity.StudentCareer;
import ec.edu.unl.entity.StudentMatter;
import ec.edu.unl.entity.StudentModule;
import ec.edu.unl.entity.Teacher;
import ec.edu.unl.entity.TeacherCareer;
import ec.edu.unl.entity.TeacherMatter;
import ec.edu.unl.entity.TeacherModule;
import ec.edu.unl.operations.Conections;
import ec.edu.unl.operations.Operations;

public class TeacherDAOImpl implements TeacherDAO {

    @Autowired
    @Qualifier("student")
    Student student;

    @Autowired
    @Qualifier("operationsImpl")
    Operations operations;

    @Autowired
    @Qualifier("conectionImpl")
    Conections conectionWS;

    @Autowired
    @Qualifier("student")
    Student studentForSave;

    @Autowired
    @Qualifier("matter")
    Matter matterForSave;

    @Autowired
    @Qualifier("teacher")
    Teacher teacherForSave;

    @Autowired
    @Qualifier("career")
    Career careerForSave;

    @Autowired
    @Qualifier("module")
    Module moduleForSave;

    // //// DAOS /////////
    @Autowired
    @Qualifier("studentDAOImpl")
    StudentDAO studentDAO;

    @Autowired
    @Qualifier("matterDAOImpl")
    MatterDAO matterDAO;

    @Autowired
    @Qualifier("moduleDAOImpl")
    ModuleDAO moduleDAO;

    @Autowired
    @Qualifier("careerDAOImpl")
    CareerDAO careerDAO;

    @Autowired
    @Qualifier("academicDAOImpl")
    AcademicDAO academicDAO;

    @Autowired
    @Qualifier("studentMatterDAOImpl")
    StudentMatterDAO studentMatterDAO;

    @Autowired
    @Qualifier("studentModuleDAOImpl")
    StudentModuleDAO studentModuleDAO;

    @Autowired
    @Qualifier("studentCareerDAOImpl")
    StudentCareerDAO studentCareerDAO;

    @Autowired
    @Qualifier("teacherMatterDAOImpl")
    TeacherMatterDAO teacherMatterDAO;

    @Autowired
    @Qualifier("teacherModuleDAOImpl")
    TeacherModuleDAO teacherModuleDAO;

    @Autowired
    @Qualifier("teacherCareerDAOImpl")
    TeacherCareerDAO teacherCareerDAO;

    // //////////////
    @Autowired
    @Qualifier("studentCareer")
    StudentCareer studentCareerForSave;

    @Autowired
    @Qualifier("studentMatter")
    StudentMatter studentMatterForSave;

    @Autowired
    @Qualifier("studentModule")
    StudentModule studentModuleForSave;

    @Autowired
    @Qualifier("teacherMatter")
    TeacherMatter teacherMatterForSave;

    @Autowired
    @Qualifier("teacherModule")
    TeacherModule teacherModuleForSave;

    @Autowired
    @Qualifier("teacherCareer")
    TeacherCareer teacherCareerForSave;

    @Autowired
    HibernateTemplate hibernateTemplate;

    @Override
    @Transactional
    public Teacher findTeacherByIdentificactionCard(String identification_card) {
        Teacher teacher = null;
        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();
        String hql = "FROM Teacher t WHERE t.identification_card='"
                + identification_card + "'";
        teacher = (Teacher) session.createQuery(hql).uniqueResult();

        return teacher;
    }

    @Override
    @Transactional
    public void saveTeacher(Teacher teacher) {
        hibernateTemplate.save(teacher);

    }

    @Override
    @Transactional
    public void updateTeacher(Teacher teacher) {
        hibernateTemplate.update(teacher);

    }

    @Override
    @Transactional
    public boolean existTeacher(String identification_card) {
        boolean resul = false;
        Teacher teacher = findTeacherByIdentificactionCard(identification_card);
        if (teacher != null) {
            resul = true;
        } else {
            resul = false;
        }
        return resul;
    }

    @Override
    @Transactional
    public long returnTeacherId(String identification_card) {
        long id = this.findTeacherByIdentificactionCard(identification_card).getId();
        return id;
    }

    @Override
    @Transactional
    public void saveOrUpdateTeacher(Teacher teacher) {
        hibernateTemplate.saveOrUpdate(teacher);
    }

    @Override
    @Transactional
    public Teacher returnTeacher(long idTeacher) {
        Teacher teacher = (Teacher) hibernateTemplate.get(Teacher.class, idTeacher);
        return teacher;
    }

}
