
package ec.edu.unl.DAO.impl;

import ec.edu.unl.DAO.MatterModuleDAO;
import ec.edu.unl.entity.MatterModule;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public class MatterModuleDAOImpl implements MatterModuleDAO {
    
    @Autowired
    HibernateTemplate hibernateTemplate;
    
    @Override
    @Transactional
    public void saveMatterModule(MatterModule matterModule) {
        hibernateTemplate.save(matterModule);
    }
    
    @Override
    @Transactional
    public void updateMatterModule(MatterModule matterModule) {
        hibernateTemplate.update(matterModule);
    }
    
    @Override
    @Transactional
    public void saveOrUpdateMatterModule(MatterModule matterModule) {
        hibernateTemplate.saveOrUpdate(matterModule);
        
    }

	@Override
	@Transactional
	public boolean existRelationMatterModule(String matter,
			Double numHoursMatter, Integer moduleNumber, String parallel) {
		boolean resul;

        Session session = hibernateTemplate.getSessionFactory()
                .getCurrentSession();

        Query hql = session
                .createQuery("FROM Matter mat JOIN mat.matterModules mod WHERE mat.name=? AND mat.num_hours=?  AND mod.module.number=? AND mod.module.parallel=?");

        hql.setString(0, matter);
        hql.setDouble(1, numHoursMatter);
        hql.setInteger(2, moduleNumber);
        hql.setString(3, parallel);
      
        resul = hql.uniqueResult() != null;
        
        return resul;
	}
    
}
