package ec.edu.unl.DAO.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.unl.DAO.ConfigurationDAO;
import ec.edu.unl.entity.Career;
import ec.edu.unl.entity.Configuration;
import ec.edu.unl.entity.Matter;
import ec.edu.unl.entity.Module;
import ec.edu.unl.entity.Offer;
import ec.edu.unl.entity.Teacher;
import ec.edu.unl.utilities.academic.ConfigurationUtil;
import ec.edu.unl.utilities.academic.TeacherModuleUtil;

public class ConfigurationDAOImpl implements ConfigurationDAO {

    @Autowired
    HibernateTemplate hibernateTemplate;
    
	@Override
	@Transactional
	public void saveConfiguration(Configuration configuration) {
		hibernateTemplate.save(configuration);

	}

	@Override
	@Transactional
	public void updateConfiguration(Configuration configuration) {
		hibernateTemplate.update(configuration);

	}

	@Override
	@Transactional
	public void deleteConfiguration(Configuration configuration) {
		//corregir delete
		hibernateTemplate.delete(configuration);

	}

	@Override
	@Transactional
	public void generateDefaultConfiguration(long offerData, long careerData,
			long moduleData, long matterData, long idTeacher) {
		
		
		
		Offer offer = hibernateTemplate.get(Offer.class, offerData);
		Career career = hibernateTemplate.get(Career.class,careerData);
		Module module = hibernateTemplate.get(Module.class,moduleData);
		Matter matter = hibernateTemplate.get(Matter.class, matterData);
		Teacher teacher = hibernateTemplate.get(Teacher.class, idTeacher);
		
		 Double [] percentages = new Double[6];
			percentages[0]=0.7;//evaluaciones
			percentages[1]=0.06;//lecciones
			percentages[2]=0.06;//trabajos clase
			percentages[3]=0.06;//deberes - trabajos extraclase
			percentages[4]=0.06;//participaciones
			percentages[5]=0.06;//laboratorios - practicas
			
		
		for(int i=0;i<percentages.length;i++){
			Configuration configuration = new Configuration();
			configuration.setOffer(offer);
			configuration.setCareer(career);
			configuration.setModule(module);
			configuration.setMatter(matter);
			configuration.setTeacher(teacher);
			configuration.setPercentage(percentages[i]);
			configuration.setNumber(3);
			
			saveConfiguration(configuration);
		}
	}
	
	@Override
	@Transactional
	public List<ConfigurationUtil> returnListConfigurations(long offerData, long careerData,
			long moduleData, long matterData, long idTeacher) {
		
		List<ConfigurationUtil> listFinal = null;
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
		
	    Query hql = session
	                .createQuery("SELECT NEW ec.edu.unl.utilities.academic.ConfigurationUtil("
	                		+ "conf.id,conf.percentage,conf.number) "
	                        + "FROM Configuration conf "
	                        + "WHERE conf.offer.id=? "
	                        + "AND conf.career.id=? "
	                        + "AND conf.module.id=? "
	                        + "AND conf.matter.id=? "
	                        + "AND conf.teacher.id=? ");
	        
	     hql.setLong(0, offerData);
	     hql.setLong(1, careerData);
	     hql.setLong(2, moduleData);
	     hql.setLong(3, matterData);
	     hql.setLong(4, idTeacher);
	        
	     listFinal = hql.list();
		
	     return listFinal;
	}

	@Override
	@Transactional
	public void saveAllNewConfiguration(List<ConfigurationUtil> listConf) {
		for(int i=0;i<listConf.size();i++){
			ConfigurationUtil cu = listConf.get(i);
			Configuration configuration = hibernateTemplate.get(Configuration.class,cu.getIdConfiguration());
			configuration.setPercentage(cu.getPercentage());
			configuration.setNumber(cu.getNumberDefault());
			
			updateConfiguration(configuration);
		}
		
	}

}
