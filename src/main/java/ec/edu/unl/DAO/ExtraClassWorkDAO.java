package ec.edu.unl.DAO;

import java.util.List;

import ec.edu.unl.entity.ExtraClassWork;
import ec.edu.unl.utilities.extraClassWork.ExtraClassWorkUtil;

public interface ExtraClassWorkDAO {
	
	public void saveExtraClassWork(ExtraClassWork extraClassWork);
	
	public void updateExtraClassWork(ExtraClassWork extraClassWork);
	
	public void deleteExtraClassWork(ExtraClassWork extraClassWork);

	public List<ExtraClassWorkUtil> returnListStudentExtraClassWorks(
			long offerSelect, long careerSelect, long moduleSelect,
			long idMatter, long idStudent);
}
