package ec.edu.unl.DAO;

import java.util.Date;
import java.util.List;

import ec.edu.unl.entity.Offer;


public interface OfferDAO {

    public void saveOffer(Offer offer);

    public void updateOffer(Offer offer);

    public boolean existOffer(String description,Date start,Date end);
    
    public Offer returnOfferById(long idOffer);

	public long returnOfferId(String descriptionOffer,Date start,Date end);

	public Date returnStartDateOfferById(long offerSelect);

	public Date returnEndDateOfferById(long offerSelect);
	
	public List<Offer> returnAllOffers();
}
