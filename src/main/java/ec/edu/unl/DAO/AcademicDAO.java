package ec.edu.unl.DAO;

import ec.edu.unl.entity.Career;

import java.util.Date;
import java.util.List;

import ec.edu.unl.entity.Matter;
import ec.edu.unl.entity.Module;
import ec.edu.unl.entity.Offer;
import ec.edu.unl.entity.Reference;
import ec.edu.unl.entity.Student;
import ec.edu.unl.entity.Teacher;
import ec.edu.unl.utilities.academic.ModuleStudentUtil;
import ec.edu.unl.utilities.academic.StudentMatterUtil;
import ec.edu.unl.utilities.academic.StudentQualificationUtil;
import ec.edu.unl.utilities.academic.StudentSpecificUtil;
import ec.edu.unl.utilities.academic.TeacherCareerUtil;
import ec.edu.unl.utilities.academic.TeacherModuleMatterUtil;
import ec.edu.unl.utilities.academic.TeacherModuleMatterTempUtil;
import ec.edu.unl.utilities.academic.TeacherModuleUtil;
import ec.edu.unl.utilities.academic.TeacherOfferUtil;

public interface AcademicDAO {

	public List<TeacherOfferUtil> returnListTeacherOffer(
			String identification_card);

	public List<TeacherCareerUtil> returnListTeacherCareer(
			String identification_card);

	public List<TeacherModuleUtil> returnListTeacherModule(
			String identification_card);
	
    public List<TeacherModuleMatterUtil> returnTeacherModuleMatter(
            String identificationCard,long idOffer,long idCareer,long idModule);

    public List<ModuleStudentUtil> returnModulesStudents(String identificationCard);


    public boolean existDataTeacher(String identificationCard, Integer moduleNumber, Character parallel, String matterName);

    public void relationUpdateModuleTeacher(long idModulo, long idTeacher);

    public void relationUpdateModuleTeacher(long idModulo, Teacher teacherForsave);

    public void relationSaveModuleTeacher(Module module, long teacherId);

    public void relationSaveModuleTeacher(Module mod, Teacher teach);

    public void relationUpdateMatterTeacher(long idMatter, long idTeacher);

    public void relationUpdateMatterTeacher(long idMatter, Teacher teacherForSave);

    public void relationSaveMatterTeacher(Matter matterForSave, long idTeacher);

    public void relationSaveMatterTeacher(Matter matterForSave, Teacher teacher);

    public void relationSaveMatterModule(Matter matterForSave, long idModulo);

    public void relationSaveMatterModule(Matter matter, Module module);

    public void relationUpdateMatterModule(long idMatter, long idModulo);

    public void relationUpdateMatterModule(long idMatter, Module module);

    public void relationUpdateCareerTeacher(long idCareer, long idTeacher);
    
    public void relationUpdateCareerTeacher(long idCareer, Teacher teacher);

    public void relationSaveCareerTeacher(Career career, long idTeacher);
    
    public void relationSaveCareerTeacher(Career career, Teacher teacher);

	public void relationSaveMatterOffer(long idMatter, Offer offerForSave);
	
	public void relationSaveMatterOffer(Matter matter, Offer offer);

	public void relationUpdateMatterOffer(long idMatter, long idPeriod);
	
	public void relationUpdateMatterOffer(Matter matter, long idPeriod);

	///////////////////
	
	public void relationSaveOrUpdateCareerStudent(long idCareer, long idStudent);
	
	public void relationSaveOrUpdateCareerStudent(long idCareer, Student student);
	
	public void relationSaveOrUpdateCareerStudent(Career career, long idStudent);
	
	public void relationSaveOrUpdateCareerStudent(Career career, Student student);
	
	//////////////////

	public void relationSaveOrUpdateModuleStudent(long idModule, long idStudent);
	
	public void relationSaveOrUpdateModuleStudent(long idModule, Student student);
	
	public void relationSaveOrUpdateModuleStudent(Module module, long idStudent);
	
	public void relationSaveOrUpdateModuleStudent(Module module, Student student);
	
	//////////////////

	public void relationSaveOrUpdateMatterStudent(long idMatter, long idStudent);
	
	public void relationSaveOrUpdateMatterStudent(long idMatter, Student student);
	
	public void relationSaveOrUpdateMatterStudent(Matter matter, long idStudent);
	
	public void relationSaveOrUpdateMatterStudent(Matter matter, Student student);

	public List<StudentMatterUtil> returnListStudentMatter(long offerSelect,
			long careerSelect, long moduleSelect, long idStudent);



	

	

	

}
