package ec.edu.unl.DAO;

import ec.edu.unl.entity.TeacherModule;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public interface TeacherModuleDAO {

    public void saveTeacherModule(TeacherModule teacherModule);
    
    public void saveOrUpdateTeacherModule(TeacherModule teacherModule);

    public void updateTeacherModule(TeacherModule teacherModule);

	public boolean existRelationTeacherModule(String teacherIC,
			Integer moduleNumber, String parallel);
}
