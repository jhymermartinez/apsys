package ec.edu.unl.DAO;

import ec.edu.unl.entity.TeacherMatter;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public interface TeacherMatterDAO {

    public void saveTeacherMatter(TeacherMatter teacherMatter);

    public void updateTeacherMatter(TeacherMatter teacherMatter);

    public void saveOrUpdateTeacherMatter(TeacherMatter teacherMatterForSave);

	public boolean existRelationTeacherMatter(String teacherIC, String matter,
			Double numHoursMatter);
}
