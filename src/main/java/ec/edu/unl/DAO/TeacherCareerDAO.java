/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ec.edu.unl.DAO;

import ec.edu.unl.entity.TeacherCareer;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public interface TeacherCareerDAO {
 
    public void saveTeacherCareer(TeacherCareer teacherCareer);
    
    public void updateTeacherCareer(TeacherCareer teacherCareer);
    
    public void saveOrUpdateTeacherCareer(TeacherCareer teacherCareer);

	public boolean existRelationTeacherCareer(String teacherIC,
			long codeCareer, String careerName);
    
}
