package ec.edu.unl.DAO;

import java.util.List;

import ec.edu.unl.entity.StudentModule;
import ec.edu.unl.utilities.academic.ModuleParallelUtil;


public interface StudentModuleDAO {

    public void saveStudentModule(StudentModule studentModule);

    public void updateStudentModule(StudentModule studentModule);
    
    public void saveOrUpdateStudentModule(StudentModule studentModule);
    
    public List<ModuleParallelUtil> returnStudentModules(long idOffer, long idCareer, long idStudent);

}
