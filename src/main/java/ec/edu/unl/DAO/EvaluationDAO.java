package ec.edu.unl.DAO;

import java.util.List;

import ec.edu.unl.entity.Evaluation;
import ec.edu.unl.utilities.evaluation.EvaluationUtil;

public interface EvaluationDAO {

	
	public void saveEvaluation(Evaluation evaluation);
	
	public void updateEvaluation(Evaluation evaluation);

	public void deleteEvaluation(Evaluation evaluation);

	public List<EvaluationUtil> returnListStudentEvaluations(long idOffer,
			long idCareer, long idModule, long idMatter, long idStudent);
	
	
}
