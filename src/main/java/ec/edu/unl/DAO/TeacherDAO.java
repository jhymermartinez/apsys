package ec.edu.unl.DAO;

import ec.edu.unl.entity.Module;
import ec.edu.unl.entity.Student;
import ec.edu.unl.entity.Teacher;

public interface TeacherDAO {

    public Teacher findTeacherByIdentificactionCard(String identification_card);

    public boolean existTeacher(String identification_card);

    public void saveTeacher(Teacher teacher);

    public void updateTeacher(Teacher teacher);

    public void saveOrUpdateTeacher(Teacher teacher);

    public Teacher returnTeacher(long idTeacher);

    public long returnTeacherId(String identification_card);

}
