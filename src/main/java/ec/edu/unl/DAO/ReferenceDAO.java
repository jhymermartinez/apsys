package ec.edu.unl.DAO;

import ec.edu.unl.entity.Reference;

public interface ReferenceDAO {
	
	public Reference returnReferenceById(long idReference);
	
	public void saveReference(Reference reference);
	
	public void updateReference(Reference reference);
	
	public void saveOrUpdateReference(Reference reference);

	public void deleteReference(Reference reference);
	
}
