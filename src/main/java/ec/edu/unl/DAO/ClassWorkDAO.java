package ec.edu.unl.DAO;

import java.util.List;

import ec.edu.unl.entity.ClassWork;
import ec.edu.unl.utilities.classWork.ClassWorkUtil;


public interface ClassWorkDAO {
	public void saveClassWork(ClassWork classWork);
	
	public void updateClassWork(ClassWork classWork);
	
	public void deleteClassWork(ClassWork classWork);

	public List<ClassWorkUtil> returnListStudentClassWorks(long offerSelect,
			long careerSelect, long moduleSelect, long idMatter, long idStudent);
}
