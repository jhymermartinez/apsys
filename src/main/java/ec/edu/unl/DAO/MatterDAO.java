package ec.edu.unl.DAO;

import java.util.List;

import ec.edu.unl.entity.Matter;

public interface MatterDAO {

    public void saveMatter(Matter matter);

    public void updateMatter(Matter matter);

    public List<Matter> findAllMatters();

    public boolean existMatter(String matter, Double numHoursMatter);

    public Matter returnMatterById(long idMatter);

    public long returnMatterId(String matter, Double numHoursMatter);

	
}
