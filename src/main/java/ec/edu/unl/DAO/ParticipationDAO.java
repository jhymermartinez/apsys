package ec.edu.unl.DAO;

import java.util.List;

import ec.edu.unl.entity.Participation;
import ec.edu.unl.utilities.participation.ParticipationUtil;

public interface ParticipationDAO {
	
	public void saveParticipation(Participation participation);
	
	public void updateParticipation(Participation participation);
	
	public void deleteParticipation(Participation participation);

	public List<ParticipationUtil> returnListStudentParticipations(
			long offerSelect, long careerSelect, long moduleSelect,
			long idMatter, long idStudent);
}
