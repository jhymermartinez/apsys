package ec.edu.unl.DAO;

import ec.edu.unl.entity.StudentMatter;

public interface StudentMatterDAO {

    public void saveStudentMatter(StudentMatter studentMatter);
    
    public void updateStudentMatter(StudentMatter studentMatter);
    
    public void saveOrUpdateStudentMatter(StudentMatter studentMatter);
    
}
