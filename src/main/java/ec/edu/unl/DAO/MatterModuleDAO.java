
package ec.edu.unl.DAO;

import ec.edu.unl.entity.MatterModule;


public interface MatterModuleDAO {

    public void saveMatterModule(MatterModule matterModule);

    public void updateMatterModule(MatterModule matterModule);
    
    public void saveOrUpdateMatterModule(MatterModule matterModule);

	public boolean existRelationMatterModule(String matter,
			Double numHoursMatter, Integer moduleNumber, String parallel);
}
