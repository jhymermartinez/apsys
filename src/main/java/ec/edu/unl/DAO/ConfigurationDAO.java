package ec.edu.unl.DAO;

import java.util.List;

import ec.edu.unl.entity.Configuration;
import ec.edu.unl.utilities.academic.ConfigurationUtil;

public interface ConfigurationDAO {
	public void saveConfiguration(Configuration configuration);
	
	public void updateConfiguration(Configuration configuration);

	public void deleteConfiguration(Configuration configuration);

	public void generateDefaultConfiguration(long offerData, long careerData,
			long moduleData, long matterData, long idTeacher);

	List<ConfigurationUtil> returnListConfigurations(long offerData,
			long careerData, long moduleData, long matterData, long idTeacher);

	public void saveAllNewConfiguration(List<ConfigurationUtil> listConf);
}
