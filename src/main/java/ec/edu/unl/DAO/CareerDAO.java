package ec.edu.unl.DAO;

import ec.edu.unl.entity.Career;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public interface CareerDAO {

    public void saveCareer(Career career);

    public void updateCareer(Career career);
    
    public boolean existCareer(String name,Long code_career);
    
    public Career returnCareerById(long idCareer);

    public long returnCareerId(String careerName, long codeCareer);

}
