package ec.edu.unl.DAO;

import ec.edu.unl.entity.SocialFactor;

public interface SocialFactorDAO {
	
	public void saveSocialFactor(SocialFactor socialFactor);
	
	public void updateSocialFactor(SocialFactor socialFactor);

	public void updateAllNewSocialFactor(long StudentId,SocialFactor sf);
	
	public void deleteSocialFactor(SocialFactor socialFactor);

	public void generateDefaultSocialFactor(long StudentId);

	public SocialFactor returnSocialFactor(long StudentId);
}
