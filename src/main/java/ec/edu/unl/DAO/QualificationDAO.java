package ec.edu.unl.DAO;

import java.util.Date;
import java.util.List;

import ec.edu.unl.entity.Reference;
import ec.edu.unl.utilities.academic.NumberParameterUtil;
import ec.edu.unl.utilities.academic.ParameterDetailUtil;
import ec.edu.unl.utilities.academic.ParameterEditUtil;
import ec.edu.unl.utilities.academic.ParameterQualificationUtil;
import ec.edu.unl.utilities.academic.StudentQualificationUtil;
import ec.edu.unl.utilities.academic.StudentSpecificUtil;

public interface QualificationDAO {

	public List<StudentSpecificUtil> returnListStudentSpecific(long offerSelect,
			long careerSelect, long moduleSelect, long matterSelect,
			String teacherIC);

	////////////////////////////////
	public List<ParameterQualificationUtil> returnQualificationParameter(StudentSpecificUtil studentQualification,String parameterStr);
	
	public int returnMaxNumberParameter(long idCareer,long idOffer,long idModule,long idMatter,long idTeacher,String parameterStr);

	public List<ParameterEditUtil> returnListStudentParameterEdit(long offerSelect, long careerSelect, long moduleSelect,
			long idMatterNew, long idTeacherNew, Integer numEvaluation,String parameterStr);

	public void saveAllQualificationParameter(List<StudentSpecificUtil> listStudentSpecific,Date dateSelect, Reference reference,int numEval,String parameterStr);

	public void updateAllQualificationParameter(List<ParameterEditUtil> listStudentEvaluationEdit, Date dateSelect,String referenceData,String parameterStr);

	public void deleteAllQualificationParameter(List<ParameterEditUtil> listStudentEvaluationEdit,String parameterStr);

	public ParameterDetailUtil returnListDataParameterDetail(long offerSelect, long careerSelect, long moduleSelect,
			long idMatterNew, long idTeacherNew, Integer numEvaluation,String parameterStr);
	
	public List<ParameterDetailUtil> returnListStudentParameterDetail(long offerSelect, long careerSelect, long moduleSelect,
			long matterSelect, long idTeacherNew, int numEvals,String parameterStr);

	List<NumberParameterUtil> returnListDateParameter(long offerSelect,
			long careerSelect, long moduleSelect, long idMatterNew,
			long idTeacherNew, String parameterStr);

	void updateSelectedQualificationParameter(
			List<ParameterEditUtil> listStudentParameterEdit,
			String parameterStr);

	
}