package ec.edu.unl.DAO;

import ec.edu.unl.entity.InstitutionalFactor;
import ec.edu.unl.entity.SocialFactor;
import ec.edu.unl.entity._InstitutionalFactor;

public interface InstitutionalFactorDAO {

	public void saveInstitutionalFactor(InstitutionalFactor institutionalFactor);
	public void saveInstitutionalFactor(_InstitutionalFactor institutionalFactor);
	
	public void updateInstitutionalFactor(InstitutionalFactor institutionalFactor);
	public void updateInstitutionalFactor(_InstitutionalFactor institutionalFactor);
	
	public void updateAllNewInstitutionalFactor(long offerId, long careerId,
			long moduleId, long matterId, long StudentId,InstitutionalFactor ifac, _InstitutionalFactor _ifac);

	public void deleteInstitutionalFactor(InstitutionalFactor institutionalFactor);
	public void deleteInstitutionalFactor(_InstitutionalFactor institutionalFactor);

	
	public void generateDefaultInstitutionalFactor(long offerId, long careerId,
			long moduleId, long matterId, long StudentId);
	public void generateDefaultInstitutionalFactor(long offerId, long careerId,
			long StudentId);

	public InstitutionalFactor returnInstitutionalFactor(long offerId, long careerId,
			long moduleId, long matterId, long StudentId);
	public _InstitutionalFactor returnInstitutionalFactor(long offerId, long careerId,long StudentId);
}
