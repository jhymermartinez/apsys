package ec.edu.unl.DAO;

import java.util.Date;

import ec.edu.unl.entity.MatterOffer;


public interface MatterOfferDAO {

    public void saveMatterOffer(MatterOffer matterOffer);

    public void updateMatterOffer(MatterOffer matterOffer);

    public void saveOrUpdateMatterOffer(MatterOffer matterOffer);

	public boolean existRelationMatterOffer(String matter,
			Double numHoursMatter, String descriptionPeriod,Date start,Date end);
}
