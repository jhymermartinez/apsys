package ec.edu.unl.rest;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

public class RestClient {

	
	
	public String execute(){
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseURI());

		return target.path("reference")
    		.request()
    		.accept(MediaType.APPLICATION_JSON)
    		.get(String.class);

  }

  public URI getBaseURI() {
    return UriBuilder.fromUri("http://localhost:3000/api").build();

  }


}

