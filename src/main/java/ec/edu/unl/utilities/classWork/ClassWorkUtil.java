package ec.edu.unl.utilities.classWork;

import java.util.Date;

public class ClassWorkUtil {
	long idClassWork;
	Integer numClassWork;
	Double qualificationClassWork;
	String annotation;
	Date dateClassWork;
	String reference;
	
	
	public ClassWorkUtil(long idClassWork, Integer numClassWork,
			Double qualificationClassWork, String annotation,
			Date dateClassWork, String reference) {
		
		this.idClassWork = idClassWork;
		this.numClassWork = numClassWork;
		this.qualificationClassWork = qualificationClassWork;
		this.annotation = annotation;
		this.dateClassWork = dateClassWork;
		this.reference = reference;
	}
	public ClassWorkUtil() {
		// TODO Auto-generated constructor stub
	}
	public long getIdClassWork() {
		return idClassWork;
	}
	public void setIdClassWork(long idClassWork) {
		this.idClassWork = idClassWork;
	}
	public Integer getNumClassWork() {
		return numClassWork;
	}
	public void setNumClassWork(Integer numClassWork) {
		this.numClassWork = numClassWork;
	}
	public Double getQualificationClassWork() {
		return qualificationClassWork;
	}
	public void setQualificationClassWork(Double qualificationClassWork) {
		this.qualificationClassWork = qualificationClassWork;
	}
	public String getAnnotation() {
		return annotation;
	}
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}
	public Date getDateClassWork() {
		return dateClassWork;
	}
	public void setDateClassWork(Date dateClassWork) {
		this.dateClassWork = dateClassWork;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	
	
	
	
}
