package ec.edu.unl.utilities.evaluation;

import java.util.Date;


public class EvaluationUtil {

	
	long idEvaluation;
	Integer numEvaluation;
	Double qualificationEval;
	String annotation;
	Date dateEvaluation;
	String reference;
	
	public EvaluationUtil() {
		// TODO Auto-generated constructor stub
	}
	
	public EvaluationUtil(long idEvaluation, Integer numEvaluation,
			Double qualificationEval, String annotation, Date dateEvaluation,
			String reference) {
		
		this.idEvaluation = idEvaluation;
		this.numEvaluation = numEvaluation;
		this.qualificationEval = qualificationEval;
		this.annotation = annotation;
		this.dateEvaluation = dateEvaluation;
		this.reference = reference;
	}
	public long getIdEvaluation() {
		return idEvaluation;
	}
	public void setIdEvaluation(long idEvaluation) {
		this.idEvaluation = idEvaluation;
	}
	public Integer getNumEvaluation() {
		return numEvaluation;
	}
	public void setNumEvaluation(Integer numEvaluation) {
		this.numEvaluation = numEvaluation;
	}
	public Double getQualificationEval() {
		return qualificationEval;
	}
	public void setQualificationEval(Double qualificationEval) {
		this.qualificationEval = qualificationEval;
	}
	public String getAnnotation() {
		return annotation;
	}
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public Date getDateEvaluation() {
		return dateEvaluation;
	}

	public void setDateEvaluation(Date dateEvaluation) {
		this.dateEvaluation = dateEvaluation;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}

	
	
	
	
}
