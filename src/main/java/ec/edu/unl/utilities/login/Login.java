package ec.edu.unl.utilities.login;

import org.springframework.context.annotation.Scope;

@Scope("session")
public class Login {

	String identification_card="";
	String password="";

	public String getIdentification_card() {
		return identification_card;
	}

	public void setIdentification_card(String identification_card) {
		this.identification_card = identification_card;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
