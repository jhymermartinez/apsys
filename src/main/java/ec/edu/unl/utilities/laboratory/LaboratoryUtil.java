package ec.edu.unl.utilities.laboratory;

import java.util.Date;

public class LaboratoryUtil {
	long idLaboratory;
	Integer numLaboratory;
	Double qualificationLaboratory;
	String annotation;
	Date dateLaboratory;
	
	String reference;
	
	
	
	
	public LaboratoryUtil(long idLaboratory, Integer numLaboratory,
			Double qualificationLaboratory, String annotation,
			Date dateLaboratory, String reference) {
		
		this.idLaboratory = idLaboratory;
		this.numLaboratory = numLaboratory;
		this.qualificationLaboratory = qualificationLaboratory;
		this.annotation = annotation;
		this.dateLaboratory = dateLaboratory;
		this.reference = reference;
	}
	public LaboratoryUtil() {
		// TODO Auto-generated constructor stub
	}
	public long getIdLaboratory() {
		return idLaboratory;
	}
	public void setIdLaboratory(long idLaboratory) {
		this.idLaboratory = idLaboratory;
	}
	public Integer getNumLaboratory() {
		return numLaboratory;
	}
	public void setNumLaboratory(Integer numLaboratory) {
		this.numLaboratory = numLaboratory;
	}
	public Double getQualificationLaboratory() {
		return qualificationLaboratory;
	}
	public void setQualificationLaboratory(Double qualificationLaboratory) {
		this.qualificationLaboratory = qualificationLaboratory;
	}
	public String getAnnotation() {
		return annotation;
	}
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}
	public Date getDateLaboratory() {
		return dateLaboratory;
	}
	public void setDateLaboratory(Date dateLaboratory) {
		this.dateLaboratory = dateLaboratory;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	
	
}
