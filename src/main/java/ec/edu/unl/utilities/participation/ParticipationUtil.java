package ec.edu.unl.utilities.participation;

import java.util.Date;

public class ParticipationUtil {
	long idParticipation;
	Integer numParticipation;
	Double qualificationParticipation;
	String annotation;
	Date dateParticipation;
	String reference;
	

	public ParticipationUtil(long idParticipation, Integer numParticipation,
			Double qualificationParticipation, String annotation,
			Date dateParticipation, String reference) {
		
		this.idParticipation = idParticipation;
		this.numParticipation = numParticipation;
		this.qualificationParticipation = qualificationParticipation;
		this.annotation = annotation;
		this.dateParticipation = dateParticipation;
		this.reference = reference;
	}
	public ParticipationUtil() {
		// TODO Auto-generated constructor stub
	}
	public long getIdParticipation() {
		return idParticipation;
	}
	public void setIdParticipation(long idParticipation) {
		this.idParticipation = idParticipation;
	}
	public Integer getNumParticipation() {
		return numParticipation;
	}
	public void setNumParticipation(Integer numParticipation) {
		this.numParticipation = numParticipation;
	}
	public Double getQualificationParticipation() {
		return qualificationParticipation;
	}
	public void setQualificationParticipation(Double qualificationParticipation) {
		this.qualificationParticipation = qualificationParticipation;
	}
	public String getAnnotation() {
		return annotation;
	}
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}
	public Date getDateParticipation() {
		return dateParticipation;
	}
	public void setDateParticipation(Date dateParticipation) {
		this.dateParticipation = dateParticipation;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	
	
}
