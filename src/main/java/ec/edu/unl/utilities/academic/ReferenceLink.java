package ec.edu.unl.utilities.academic;

public class ReferenceLink {

	String category;
	String description;
	String location;
	String name;
	
	public ReferenceLink(){
		
	}
	
	public ReferenceLink(String category, String description, String location,
			String name) {
		
		this.category = category;
		this.description = description;
		this.location = location;
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
