package ec.edu.unl.utilities.academic;

public class TeacherModuleUtil {

	String teacherIC;
	long idModule;
	Integer moduleNumber;
	String moduleParallel;
	
	
	public TeacherModuleUtil(String teacherIC, long idModule,
			Integer moduleNumber, String moduleParallel) {
		
		this.teacherIC = teacherIC;
		this.idModule = idModule;
		this.moduleNumber = moduleNumber;
		this.moduleParallel = moduleParallel;
	}
	public String getTeacherIC() {
		return teacherIC;
	}
	public void setTeacherIC(String teacherIC) {
		this.teacherIC = teacherIC;
	}
	public long getIdModule() {
		return idModule;
	}
	public void setIdModule(long idModule) {
		this.idModule = idModule;
	}
	public Integer getModuleNumber() {
		return moduleNumber;
	}
	public void setModuleNumber(Integer moduleNumber) {
		this.moduleNumber = moduleNumber;
	}
	public String getModuleParallel() {
		return moduleParallel;
	}
	public void setModuleParallel(String moduleParallel) {
		this.moduleParallel = moduleParallel;
	}
	
	
	
}
