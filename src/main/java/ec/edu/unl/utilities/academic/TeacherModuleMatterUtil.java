package ec.edu.unl.utilities.academic;

public class TeacherModuleMatterUtil {

	String teacherIC;
    long idCareer;
	long idOffer;
    long idModule;
    long idMatter;
    String academicOffer;
    String careerName;
    String matterName;
    Integer moduleNumber;
    String parallel;
    

    public TeacherModuleMatterUtil() {

    }





	public TeacherModuleMatterUtil(String teacherIC, long idCareer, long idOffer,
			long idModule, long idMatter, String academicOffer,
			String careerName, String matterName, Integer moduleNumber,
			String parallel) {
		
		this.teacherIC = teacherIC;
		this.idCareer = idCareer;
		this.idOffer = idOffer;
		this.idModule = idModule;
		this.idMatter = idMatter;
		this.academicOffer = academicOffer;
		this.careerName = careerName;
		this.matterName = matterName;
		this.moduleNumber = moduleNumber;
		this.parallel = parallel;
	}





	public String getTeacherIC() {
		return teacherIC;
	}





	public void setTeacherIC(String teacherIC) {
		this.teacherIC = teacherIC;
	}





	public String getAcademicOffer() {
		return academicOffer;
	}





	public void setAcademicOffer(String academicOffer) {
		this.academicOffer = academicOffer;
	}





	public long getIdCareer() {
		return idCareer;
	}





	public void setIdCareer(long idCareer) {
		this.idCareer = idCareer;
	}





	public long getIdOffer() {
		return idOffer;
	}





	public void setIdOffer(long idOffer) {
		this.idOffer = idOffer;
	}





	public long getIdModule() {
		return idModule;
	}





	public void setIdModule(long idModule) {
		this.idModule = idModule;
	}





	public long getIdMatter() {
		return idMatter;
	}





	public void setIdMatter(long idMatter) {
		this.idMatter = idMatter;
	}









	public String getCareerName() {
		return careerName;
	}





	public void setCareerName(String careerName) {
		this.careerName = careerName;
	}





	public String getMatterName() {
		return matterName;
	}





	public void setMatterName(String matterName) {
		this.matterName = matterName;
	}


	public Integer getModuleNumber() {
		return moduleNumber;
	}

	public void setModuleNumber(Integer moduleNumber) {
		this.moduleNumber = moduleNumber;
	}

	public String getParallel() {
		return parallel;
	}


	public void setParallel(String parallel) {
		this.parallel = parallel;
	}


}
