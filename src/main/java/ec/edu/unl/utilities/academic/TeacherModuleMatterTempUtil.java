package ec.edu.unl.utilities.academic;

public class TeacherModuleMatterTempUtil {

	String teacherIC;
	long idCareer;
	long idOffer;
    long idModule;
    long idMatter;
    

    public TeacherModuleMatterTempUtil() {

    }

    

	public TeacherModuleMatterTempUtil(String teacherIC, long idOffer) {
		
		this.teacherIC = teacherIC;
		this.idOffer = idOffer;
	}

	

	public TeacherModuleMatterTempUtil(String teacherIC, long idOffer, long idMatter) {
		
		this.teacherIC = teacherIC;
		this.idOffer = idOffer;
		this.idMatter = idMatter;
	}



	public TeacherModuleMatterTempUtil(String teacherIC, long idCareer,
			long idOffer, long idMatter) {
		
		this.teacherIC = teacherIC;
		this.idCareer = idCareer;
		this.idOffer = idOffer;
		this.idMatter = idMatter;
	}



	public TeacherModuleMatterTempUtil(String teacherIC, long idCareer,
			long idOffer, long idModule, long idMatter) {
		
		this.teacherIC = teacherIC;
		this.idCareer = idCareer;
		this.idOffer = idOffer;
		this.idModule = idModule;
		this.idMatter = idMatter;
	}


	public String getTeacherIC() {
		return teacherIC;
	}


	public void setTeacherIC(String teacherIC) {
		this.teacherIC = teacherIC;
	}


	public long getIdCareer() {
		return idCareer;
	}


	public void setIdCareer(long idCareer) {
		this.idCareer = idCareer;
	}


	public long getIdOffer() {
		return idOffer;
	}


	public void setIdOffer(long idOffer) {
		this.idOffer = idOffer;
	}


	public long getIdModule() {
		return idModule;
	}


	public void setIdModule(long idModule) {
		this.idModule = idModule;
	}


	public long getIdMatter() {
		return idMatter;
	}


	public void setIdMatter(long idMatter) {
		this.idMatter = idMatter;
	}

	
	

 

 
}
