package ec.edu.unl.utilities.academic;

public class StudentCareerUtil {
	long idCareer;
	String careerName;
	
	public StudentCareerUtil() {
	
	}

	public StudentCareerUtil(long idCareer, String careerName) {
		
		this.idCareer = idCareer;
		this.careerName = careerName;
	}

	public long getIdCareer() {
		return idCareer;
	}

	public void setIdCareer(long idCareer) {
		this.idCareer = idCareer;
	}

	public String getCareerName() {
		return careerName;
	}

	public void setCareerName(String careerName) {
		this.careerName = careerName;
	}
	
	
}
