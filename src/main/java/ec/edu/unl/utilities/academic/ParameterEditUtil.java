package ec.edu.unl.utilities.academic;

import java.util.Date;

public class ParameterEditUtil {
	int rowCount;
	long idStudent;
	long idParameter;
	long idReference;
	Integer num_parameter;
	Double qualification;
	String annotation;
	Date date_parameter;
	String general_reference;
	String student_name;
	
	public ParameterEditUtil() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public ParameterEditUtil(long idStudent, long idParameter, long idReference,
			Integer num_parameter, Double qualification, String annotation,
			Date date_parameter, String general_reference) {
		
		this.idStudent = idStudent;
		this.idParameter = idParameter;
		this.idReference = idReference;
		this.num_parameter = num_parameter;
		this.qualification = qualification;
		this.annotation = annotation;
		this.date_parameter = date_parameter;
		this.general_reference = general_reference;
	}

	

	public int getRowCount() {
		return rowCount;
	}



	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}



	public long getIdStudent() {
		return idStudent;
	}



	public void setIdStudent(long idStudent) {
		this.idStudent = idStudent;
	}



	public long getIdParameter() {
		return idParameter;
	}



	public void setIdParameter(long idParameter) {
		this.idParameter = idParameter;
	}



	public long getIdReference() {
		return idReference;
	}



	public void setIdReference(long idReference) {
		this.idReference = idReference;
	}



	public Integer getNum_parameter() {
		return num_parameter;
	}



	public void setNum_parameter(Integer num_parameter) {
		this.num_parameter = num_parameter;
	}



	public Double getQualification() {
		return qualification;
	}



	public void setQualification(Double qualification) {
		this.qualification = qualification;
	}



	public String getAnnotation() {
		return annotation;
	}



	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}



	public Date getDate_parameter() {
		return date_parameter;
	}



	public void setDate_parameter(Date date_parameter) {
		this.date_parameter = date_parameter;
	}



	public String getGeneral_reference() {
		return general_reference;
	}



	public void setGeneral_reference(String general_reference) {
		this.general_reference = general_reference;
	}



	public String getStudent_name() {
		return student_name;
	}



	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}
	
	
	
	
}
