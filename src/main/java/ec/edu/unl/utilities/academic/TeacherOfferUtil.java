package ec.edu.unl.utilities.academic;

public class TeacherOfferUtil {

	String teacherIC;	
	long idOffer;
	String offerDescription;
	public TeacherOfferUtil(String teacherIC, long idOffer,
			String offerDescription) {
		
		this.teacherIC = teacherIC;
		this.idOffer = idOffer;
		this.offerDescription = offerDescription;
	}
	public String getTeacherIC() {
		return teacherIC;
	}
	public void setTeacherIC(String teacherIC) {
		this.teacherIC = teacherIC;
	}
	public long getIdOffer() {
		return idOffer;
	}
	public void setIdOffer(long idOffer) {
		this.idOffer = idOffer;
	}
	public String getOfferDescription() {
		return offerDescription;
	}
	public void setOfferDescription(String offerDescription) {
		this.offerDescription = offerDescription;
	}
	
	
	
	
}
