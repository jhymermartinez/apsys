package ec.edu.unl.utilities.academic;

public class StudentMatterUtil {
	long idMatter;
	String matterName;
	
	
	
	public StudentMatterUtil(long idMatter, String matterName) {
		
		this.idMatter = idMatter;
		this.matterName = matterName;
	}
	public long getIdMatter() {
		return idMatter;
	}
	public void setIdMatter(long idMatter) {
		this.idMatter = idMatter;
	}
	public String getMatterName() {
		return matterName;
	}
	public void setMatterName(String matterName) {
		this.matterName = matterName;
	}
	
	
}
