package ec.edu.unl.utilities.academic;

public class BasicParameterData {

	long idParameter;
	Double qualification;
	
	public BasicParameterData(){
		
	}
	public BasicParameterData(long idParameter, Double qualification) {
		this.idParameter = idParameter;
		this.qualification = qualification;
	}
	
	public long getIdParameter() {
		return idParameter;
	}
	public void setIdParameter(long idParameter) {
		this.idParameter = idParameter;
	}
	public Double getQualification() {
		return qualification;
	}
	public void setQualification(Double qualification) {
		this.qualification = qualification;
	}



	
	
	
}
