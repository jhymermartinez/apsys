package ec.edu.unl.utilities.academic;

import java.util.Date;

public class StudentSpecificUtil {
	int rowCount;
    long idCareer;
	long idOffer;
    long idModule;
    long idMatter;
    long idTeacher;
    long idStudent;
	String studentName;
	Double qualification;
	Date qualificationDate;
	String annotation;
	
	Double average;
	
	
	Double averageEval;
	Double averageLess;
	Double averageCW;
	Double averageECW;
	Double averagePart;
	Double averageLab;
	
	Double total;
	
	public StudentSpecificUtil(){
		
	}








	public StudentSpecificUtil(long idCareer, long idOffer, long idModule,
			long idMatter, long idTeacher, long idStudent) {
		
		this.idCareer = idCareer;
		this.idOffer = idOffer;
		this.idModule = idModule;
		this.idMatter = idMatter;
		this.idTeacher = idTeacher;
		this.idStudent = idStudent;
	}

	public long getIdCareer() {
		return idCareer;
	}
	public void setIdCareer(long idCareer) {
		this.idCareer = idCareer;
	}

	public long getIdOffer() {
		return idOffer;
	}
	public void setIdOffer(long idOffer) {
		this.idOffer = idOffer;
	}

	public long getIdModule() {
		return idModule;
	}
	public void setIdModule(long idModule) {
		this.idModule = idModule;
	}

	public long getIdMatter() {
		return idMatter;
	}
	public void setIdMatter(long idMatter) {
		this.idMatter = idMatter;
	}

	public long getIdTeacher() {
		return idTeacher;
	}
	public void setIdTeacher(long idTeacher) {
		this.idTeacher = idTeacher;
	}
	public long getIdStudent() {
		return idStudent;
	}

	public void setIdStudent(long idStudent) {
		this.idStudent = idStudent;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public Double getQualification() {
		return qualification;
	}
	public void setQualification(Double qualification) {
		this.qualification = qualification;
	}
	public Date getQualificationDate() {
		return qualificationDate;
	}
	public void setQualificationDate(Date qualificationDate) {
		this.qualificationDate = qualificationDate;
	}

	public String getAnnotation() {
		return annotation;
	}
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}
	public Double getAverage() {
		return average;
	}

	public void setAverage(Double average) {
		this.average = average;
	}
	public int getRowCount() {
		return rowCount;
	}
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	public Double getAverageEval() {
		return averageEval;
	}
	public void setAverageEval(Double averageEval) {
		this.averageEval = averageEval;
	}
	public Double getAverageLess() {
		return averageLess;
	}
	public void setAverageLess(Double averageLess) {
		this.averageLess = averageLess;
	}
	public Double getAverageCW() {
		return averageCW;
	}
	public void setAverageCW(Double averageCW) {
		this.averageCW = averageCW;
	}

	public Double getAverageECW() {
		return averageECW;
	}
	public void setAverageECW(Double averageECW) {
		this.averageECW = averageECW;
	}
	public Double getAveragePart() {
		return averagePart;
	}
	public void setAveragePart(Double averagePart) {
		this.averagePart = averagePart;
	}
	public Double getAverageLab() {
		return averageLab;
	}
	public void setAverageLab(Double averageLab) {
		this.averageLab = averageLab;
	}








	public Double getTotal() {
		return total;
	}








	public void setTotal(Double total) {
		this.total = total;
	}	
	
}
