package ec.edu.unl.utilities.academic;

public class TeacherCareerUtil {

	String teacherIC;
	long idCareer;
	String careerName;
	public TeacherCareerUtil(String teacherIC, long idCareer, String careerName) {
		
		this.teacherIC = teacherIC;
		this.idCareer = idCareer;
		this.careerName = careerName;
	}
	public String getTeacherIC() {
		return teacherIC;
	}
	public void setTeacherIC(String teacherIC) {
		this.teacherIC = teacherIC;
	}
	public long getIdCareer() {
		return idCareer;
	}
	public void setIdCareer(long idCareer) {
		this.idCareer = idCareer;
	}
	public String getCareerName() {
		return careerName;
	}
	public void setCareerName(String careerName) {
		this.careerName = careerName;
	}
	
	
}
