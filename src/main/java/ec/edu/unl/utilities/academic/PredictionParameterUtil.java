package ec.edu.unl.utilities.academic;

public class PredictionParameterUtil {

	long idPredictionParameter;
	Double value;
	
	public PredictionParameterUtil(){
		
	}

	public PredictionParameterUtil(long idPredictionParameter, Double value) {
		
		this.idPredictionParameter = idPredictionParameter;
		this.value = value;
	}

	public long getIdPredictionParameter() {
		return idPredictionParameter;
	}

	public void setIdPredictionParameter(long idPredictionParameter) {
		this.idPredictionParameter = idPredictionParameter;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}
	
	
}
