package ec.edu.unl.utilities.academic;

import java.util.Date;

public class NumberParameterUtil {

	Integer numParam;
	Date dateParam;
	
	
	public NumberParameterUtil(Integer numParam, Date dateParam) {
		
		this.numParam = numParam;
		this.dateParam = dateParam;
	}
	public Integer getNumParam() {
		return numParam;
	}
	public void setNumParam(Integer numParam) {
		this.numParam = numParam;
	}
	public Date getDateParam() {
		return dateParam;
	}
	public void setDateParam(Date dateParam) {
		this.dateParam = dateParam;
	}
	
	
}
