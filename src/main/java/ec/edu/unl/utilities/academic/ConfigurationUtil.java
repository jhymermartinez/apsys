package ec.edu.unl.utilities.academic;

public class ConfigurationUtil {

	
	long idConfiguration;
	Double percentage;
	Integer numberDefault;
	
	public ConfigurationUtil() {
	
	}
	
	public ConfigurationUtil(long idConfiguration, Double percentage,
			Integer numberDefault) {
		
		this.idConfiguration = idConfiguration;
		this.percentage = percentage;
		this.numberDefault = numberDefault;
	}

	public long getIdConfiguration() {
		return idConfiguration;
	}

	public void setIdConfiguration(long idConfiguration) {
		this.idConfiguration = idConfiguration;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	public Integer getNumberDefault() {
		return numberDefault;
	}

	public void setNumberDefault(Integer numberDefault) {
		this.numberDefault = numberDefault;
	}
	
	
	
}
