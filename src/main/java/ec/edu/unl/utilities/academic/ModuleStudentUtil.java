package ec.edu.unl.utilities.academic;

public class ModuleStudentUtil {
	
	Integer moduleNumber;
	Character parallel;
	
	public ModuleStudentUtil(){
		
	}

	public ModuleStudentUtil(Integer moduleName, Character parallel) {
		this.moduleNumber = moduleName;
		this.parallel = parallel;
	}

    public Integer getModuleName() {
        return moduleNumber;
    }

    public void setModuleName(Integer moduleName) {
        this.moduleNumber = moduleName;
    }





	public Character getParallel() {
		return parallel;
	}

	public void setParallel(Character parallel) {
		this.parallel = parallel;
	}
	
	
	
	public String toString(){
		return "Modulo "+moduleNumber+" Paralelo "+parallel;
	}
}
