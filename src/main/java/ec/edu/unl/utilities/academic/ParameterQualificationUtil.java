package ec.edu.unl.utilities.academic;

public class ParameterQualificationUtil {
	
    long idCareer;
	long idOffer;
    long idModule;
    long idMatter;
    long idTeacher;
    long idStudent;
    long idParameter;
    Integer numParameter;
    Double qualification;
    String annotation;
	
    public ParameterQualificationUtil(){
    	
    }

    
    
	public ParameterQualificationUtil(long idCareer, long idOffer, long idModule,
			long idMatter, long idTeacher, long idStudent, long idEvaluation,
			Integer numEvaluation, Double qualification, String annotation) {
		
		this.idCareer = idCareer;
		this.idOffer = idOffer;
		this.idModule = idModule;
		this.idMatter = idMatter;
		this.idTeacher = idTeacher;
		this.idStudent = idStudent;
		this.idParameter = idEvaluation;
		this.numParameter = numEvaluation;
		this.qualification = qualification;
		this.annotation = annotation;
	}



	public long getIdCareer() {
		return idCareer;
	}



	public void setIdCareer(long idCareer) {
		this.idCareer = idCareer;
	}



	public long getIdOffer() {
		return idOffer;
	}



	public void setIdOffer(long idOffer) {
		this.idOffer = idOffer;
	}



	public long getIdModule() {
		return idModule;
	}



	public void setIdModule(long idModule) {
		this.idModule = idModule;
	}



	public long getIdMatter() {
		return idMatter;
	}



	public void setIdMatter(long idMatter) {
		this.idMatter = idMatter;
	}



	public long getIdTeacher() {
		return idTeacher;
	}



	public void setIdTeacher(long idTeacher) {
		this.idTeacher = idTeacher;
	}



	public long getIdStudent() {
		return idStudent;
	}



	public void setIdStudent(long idStudent) {
		this.idStudent = idStudent;
	}



	public long getIdParameter() {
		return idParameter;
	}



	public void setIdParameter(long idParameter) {
		this.idParameter = idParameter;
	}



	public Integer getNumParameter() {
		return numParameter;
	}



	public void setNumParameter(Integer numParameter) {
		this.numParameter = numParameter;
	}



	public Double getQualification() {
		return qualification;
	}



	public void setQualification(Double qualification) {
		this.qualification = qualification;
	}



	public String getAnnotation() {
		return annotation;
	}



	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}



}
