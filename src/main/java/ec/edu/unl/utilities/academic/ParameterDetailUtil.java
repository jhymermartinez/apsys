package ec.edu.unl.utilities.academic;

import java.util.Date;

public class ParameterDetailUtil {

	Integer description_num_parameter;
	
	Date date_parameter;
	
	String general_reference;

	public ParameterDetailUtil(){
		
	}
	public ParameterDetailUtil(Integer description_num_parameter, Date date_parameter,
			String general_reference) {
		
		this.description_num_parameter = description_num_parameter;
		this.date_parameter = date_parameter;
		this.general_reference = general_reference;
	}
	public Integer getDescription_num_parameter() {
		return description_num_parameter;
	}
	public void setDescription_num_parameter(Integer description_num_parameter) {
		this.description_num_parameter = description_num_parameter;
	}
	public Date getDate_parameter() {
		return date_parameter;
	}
	public void setDate_parameter(Date date_parameter) {
		this.date_parameter = date_parameter;
	}
	public String getGeneral_reference() {
		return general_reference;
	}
	public void setGeneral_reference(String general_reference) {
		this.general_reference = general_reference;
	}

	
	
	
	
}
