package ec.edu.unl.utilities.academic;

public class StudentQualificationUtil {


	long idMatter;
    long idCareer;
	long idOffer;
    long idModule;
	long idTeacher;
	long idStudent;
	String studentName;
	
	public StudentQualificationUtil(){
		
	}

	
	public StudentQualificationUtil(long idMatter, long idCareer, long idOffer,
			long idModule, long idTeacher, long idStudent, String studentName) {
		
		this.idMatter = idMatter;
		this.idCareer = idCareer;
		this.idOffer = idOffer;
		this.idModule = idModule;
		this.idTeacher = idTeacher;
		this.idStudent = idStudent;
		this.studentName = studentName;
	}


	public long getIdMatter() {
		return idMatter;
	}

	public void setIdMatter(long idMatter) {
		this.idMatter = idMatter;
	}

	public long getIdCareer() {
		return idCareer;
	}

	public void setIdCareer(long idCareer) {
		this.idCareer = idCareer;
	}

	public long getIdOffer() {
		return idOffer;
	}

	public void setIdOffer(long idOffer) {
		this.idOffer = idOffer;
	}

	public long getIdModule() {
		return idModule;
	}

	public void setIdModule(long idModule) {
		this.idModule = idModule;
	}

	public long getIdTeacher() {
		return idTeacher;
	}

	public void setIdTeacher(long idTeacher) {
		this.idTeacher = idTeacher;
	}

	public long getIdStudent() {
		return idStudent;
	}

	public void setIdStudent(long idStudent) {
		this.idStudent = idStudent;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}


	
}
