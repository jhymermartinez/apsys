package ec.edu.unl.utilities.academic;

public class TeacherUtil {

	long idTeacher;
	String identificationCard;
	
	
	public TeacherUtil(long idTeacher, String identificationCard) {
		this.idTeacher = idTeacher;
		this.identificationCard = identificationCard;
	}
	public long getIdTeacher() {
		return idTeacher;
	}
	public void setIdTeacher(long idTeacher) {
		this.idTeacher = idTeacher;
	}
	public String getIdentificationCard() {
		return identificationCard;
	}
	public void setIdentificationCard(String identificationCard) {
		this.identificationCard = identificationCard;
	}
	
	
}
