package ec.edu.unl.utilities.academic;

public class ModuleParallelUtil {

	
	long idModule;
	Integer moduleNumber;
	String parallel;
	
	public ModuleParallelUtil(){
		
	}

	public ModuleParallelUtil(long idModule, Integer moduleNumber,
			String parallel) {
		
		this.idModule = idModule;
		this.moduleNumber = moduleNumber;
		this.parallel = parallel;
	}

	public long getIdModule() {
		return idModule;
	}

	public void setIdModule(long idModule) {
		this.idModule = idModule;
	}

	public Integer getModuleNumber() {
		return moduleNumber;
	}

	public void setModuleNumber(Integer moduleNumber) {
		this.moduleNumber = moduleNumber;
	}

	public String getParallel() {
		return parallel;
	}

	public void setParallel(String parallel) {
		this.parallel = parallel;
	}


	
}
