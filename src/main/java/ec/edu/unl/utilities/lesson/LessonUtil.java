package ec.edu.unl.utilities.lesson;

import java.util.Date;

public class LessonUtil {
	long idLesson;
	Integer numLesson;
	Double qualificationLesson;
	String annotation;
	Date dateLesson;
	String reference;
	

	public LessonUtil(long idLesson, Integer numLesson,
			Double qualificationLesson, String annotation, Date dateLesson,
			String reference) {
		
		this.idLesson = idLesson;
		this.numLesson = numLesson;
		this.qualificationLesson = qualificationLesson;
		this.annotation = annotation;
		this.dateLesson = dateLesson;
		this.reference = reference;
	}
	public LessonUtil() {
		// TODO Auto-generated constructor stub
	}
	public long getIdLesson() {
		return idLesson;
	}
	public void setIdLesson(long idLesson) {
		this.idLesson = idLesson;
	}
	public Integer getNumLesson() {
		return numLesson;
	}
	public void setNumLesson(Integer numLesson) {
		this.numLesson = numLesson;
	}
	public Double getQualificationLesson() {
		return qualificationLesson;
	}
	public void setQualificationLesson(Double qualificationLesson) {
		this.qualificationLesson = qualificationLesson;
	}
	public String getAnnotation() {
		return annotation;
	}
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}
	public Date getDateLesson() {
		return dateLesson;
	}
	public void setDateLesson(Date dateLesson) {
		this.dateLesson = dateLesson;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	
	
	
}
