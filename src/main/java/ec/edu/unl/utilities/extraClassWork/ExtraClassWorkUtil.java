package ec.edu.unl.utilities.extraClassWork;

import java.util.Date;

public class ExtraClassWorkUtil {
	long idExtraClassWork;
	Integer numExtraClassWork;
	Double qualificationExtraClassWork;
	String annotation;
	Date dateExtraClassWork;
	String reference;
	
	
	public ExtraClassWorkUtil(long idExtraClassWork, Integer numExtraClassWork,
			Double qualificationExtraClassWork, String annotation,
			Date dateExtraClassWork, String reference) {
	
		this.idExtraClassWork = idExtraClassWork;
		this.numExtraClassWork = numExtraClassWork;
		this.qualificationExtraClassWork = qualificationExtraClassWork;
		this.annotation = annotation;
		this.dateExtraClassWork = dateExtraClassWork;
		this.reference = reference;
	}
	public ExtraClassWorkUtil() {
		// TODO Auto-generated constructor stub
	}
	public long getIdExtraClassWork() {
		return idExtraClassWork;
	}
	public void setIdExtraClassWork(long idExtraClassWork) {
		this.idExtraClassWork = idExtraClassWork;
	}
	public Integer getNumExtraClassWork() {
		return numExtraClassWork;
	}
	public void setNumExtraClassWork(Integer numExtraClassWork) {
		this.numExtraClassWork = numExtraClassWork;
	}
	public Double getQualificationExtraClassWork() {
		return qualificationExtraClassWork;
	}
	public void setQualificationExtraClassWork(Double qualificationExtraClassWork) {
		this.qualificationExtraClassWork = qualificationExtraClassWork;
	}
	public String getAnnotation() {
		return annotation;
	}
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}
	public Date getDateExtraClassWork() {
		return dateExtraClassWork;
	}
	public void setDateExtraClassWork(Date dateExtraClassWork) {
		this.dateExtraClassWork = dateExtraClassWork;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	
	
}
