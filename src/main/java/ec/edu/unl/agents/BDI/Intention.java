package ec.edu.unl.agents.BDI;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ec.edu.unl.entity.InstitutionalFactor;
import ec.edu.unl.entity.PersonalFactor;
import ec.edu.unl.entity.SocialFactor;
import ec.edu.unl.entity._InstitutionalFactor;
import ec.edu.unl.entity._PersonalFactor;
import ec.edu.unl.entity.__PersonalFactor;
import ec.edu.unl.operations.Operations;
import ec.edu.unl.operations.impl.OperationsImpl;
import ec.edu.unl.utilities.classWork.ClassWorkUtil;
import ec.edu.unl.utilities.evaluation.EvaluationUtil;
import ec.edu.unl.utilities.extraClassWork.ExtraClassWorkUtil;
import ec.edu.unl.utilities.laboratory.LaboratoryUtil;
import ec.edu.unl.utilities.lesson.LessonUtil;
import ec.edu.unl.utilities.participation.ParticipationUtil;

public class Intention {

	
	public static final double MINIMUM_QUALIFICATION_POSSIBLE = 7.0;
	public static final double REAL_QUALIFICATION_VALUE = 5.0;
	public static final double MAX_VALUE = 0.3;
	public static final double MED_VALUE = 0.2;
	public static final double MIN_VALUE = 0.1;
	public static final double MIN_MIN_VALUE = 0.05;
	
	//OTROS VALORES
	public static final double VALUE_10 = 10.0;
	public static final double VALUE_2 = 2.0;
	public static final double VALUE_3 = 3.0;
	public static final double VALUE_100 = 100.0;
	
	

	
	public boolean chekingDataQualification(List<?> listParam, double min_average, int missingParam,double percentage){	
		double cumSum =0.0;
		double result =0.0;
		
		if(listParam.get(0) instanceof EvaluationUtil){
			
			for(int i=0;i<listParam.size();i++){
				EvaluationUtil eu = (EvaluationUtil) listParam.get(i);
				
				cumSum = cumSum + eu.getQualificationEval();
			}
		}

		if(listParam.get(0) instanceof LessonUtil){
			
			for(int i=0;i<listParam.size();i++){
				LessonUtil eu = (LessonUtil) listParam.get(i);
				cumSum = cumSum + eu.getQualificationLesson();
			}
		}
		
		if(listParam.get(0) instanceof ClassWorkUtil){
			
			for(int i=0;i<listParam.size();i++){
				ClassWorkUtil eu = (ClassWorkUtil) listParam.get(i);
				cumSum = cumSum + eu.getQualificationClassWork();
			}
		}
		
		if(listParam.get(0) instanceof ExtraClassWorkUtil){
			
			for(int i=0;i<listParam.size();i++){
				ExtraClassWorkUtil eu = (ExtraClassWorkUtil) listParam.get(i);
				cumSum = cumSum + eu.getQualificationExtraClassWork();
			}
		}
		
		if(listParam.get(0) instanceof ParticipationUtil){
			
			for(int i=0;i<listParam.size();i++){
				ParticipationUtil eu = (ParticipationUtil) listParam.get(i);
				cumSum = cumSum + eu.getQualificationParticipation();
			}
		}
		
		if(listParam.get(0) instanceof LaboratoryUtil){
			
			for(int i=0;i<listParam.size();i++){
				LaboratoryUtil eu = (LaboratoryUtil) listParam.get(i);
				cumSum = cumSum + eu.getQualificationLaboratory();
			}
		}
		
		if(missingParam!=0){
			/**En caso de que las notas sean demasiado bajas se 
			 * completa las restantes con la calificacion 
			 * mas alta (10) en caso de no ser suficiente, 
			 * retorna un false*/
			for(int i=0;i<missingParam;i++){
				cumSum = cumSum + 10;
			}
			
			result = (cumSum / (listParam.size() + missingParam))* percentage;
			
	
		}else{
			result = (cumSum /listParam.size())* percentage;
		}
		
		if(result >= min_average){
			return true;
		}else{
			return false;
		}
		
	}
	

	@SuppressWarnings("unchecked")
	public List<Object> generatePrediction(List<?> listParam,
			double percentage,
			int totalNumberParams, 
			int daysToCompleteOffer,
			int totalDaysOffer,
			Date dateLastParam,
			Date dateEndOffer,
			PersonalFactor personalFactor,
			_PersonalFactor _personalFactor,
			__PersonalFactor __personalFactor,
			SocialFactor socialFactor,
			InstitutionalFactor institutionalFactor,
			_InstitutionalFactor _institutionalFactor) {

			List<Object> listFinal = new ArrayList<Object>();

			/*promedio evaluaciones*/
			double aux=0.0;
			
			/*Para evaluaciones*/
			if(listParam.get(0) instanceof EvaluationUtil){
				for(int i =0;i<listParam.size();i++){
					EvaluationUtil data = (EvaluationUtil) listParam.get(i);
					aux += data.getQualificationEval();
				}
				
				listFinal.addAll((List<EvaluationUtil>)listParam);
			}
			
			/*Para lecciones*/
			if(listParam.get(0) instanceof LessonUtil){
				for(int i =0;i<listParam.size();i++){
					LessonUtil data = (LessonUtil) listParam.get(i);
					aux += data.getQualificationLesson();
				}
				
				listFinal.addAll((List<LessonUtil>)listParam);
			}
			
			/*Para trabajo autonomos*/
			if(listParam.get(0) instanceof ClassWorkUtil){
				for(int i =0;i<listParam.size();i++){
					ClassWorkUtil data = (ClassWorkUtil) listParam.get(i);
					aux += data.getQualificationClassWork();
				}
				
				listFinal.addAll((List<ClassWorkUtil>)listParam);
			}
			
			/*Para deberes - tareas*/
			if(listParam.get(0) instanceof ExtraClassWorkUtil){
				for(int i =0;i<listParam.size();i++){
					ExtraClassWorkUtil data = (ExtraClassWorkUtil) listParam.get(i);
					aux += data.getQualificationExtraClassWork();
				}
				
				listFinal.addAll((List<ExtraClassWorkUtil>)listParam);
			}
			
			/*Para participaciones*/
			if(listParam.get(0) instanceof ParticipationUtil){
				for(int i =0;i<listParam.size();i++){
					ParticipationUtil data = (ParticipationUtil) listParam.get(i);
					aux += data.getQualificationParticipation();
				}
				
				listFinal.addAll((List<ParticipationUtil>)listParam);
			}
			
			/*Para laboratorios - talleres*/
			if(listParam.get(0) instanceof LaboratoryUtil){
				for(int i =0;i<listParam.size();i++){
					LaboratoryUtil data = (LaboratoryUtil) listParam.get(i);
					aux += data.getQualificationLaboratory();
				}
				
				listFinal.addAll((List<LaboratoryUtil>)listParam);
			}
			
			/*promedio*/
			double generalAverage = aux / listParam.size();

			/*minimo necesario 70% (7.0)*/
			double min_average = percentage * MINIMUM_QUALIFICATION_POSSIBLE;
			
			/*cantidad de parametrso que faltan por calificar*/
			int missingParams = (totalNumberParams - listParam.size());
			
			/*Método para verificar si las calificaciones son 
			* muy bajas */
			Boolean isNotLow = chekingDataQualification(listParam, min_average, missingParams,percentage);
			
			Operations operation = new OperationsImpl();

			int listSize = listParam.size();
			
			/*almacenara las calificaciones actuales mas la ultima
			* prediccion*/
			double [] newQualification = new double[listSize+1];
			
			boolean isCompleteAllEvaluations=false;


			if(missingParams != 0){

				double [] finalValues = new double[26];
				
				
				/*factores personales*/
				finalValues[0] = (_personalFactor.getCognitiveCompetence() * MAX_VALUE) / VALUE_10;
				finalValues[1] = (_personalFactor.getMotivation() * MED_VALUE) / VALUE_2;
				finalValues[2] = (_personalFactor.getCognitiveConditions() * MAX_VALUE) / VALUE_10;
				finalValues[3] = (_personalFactor.getAcademicSelfConcept() * MAX_VALUE) / VALUE_3;
				finalValues[4] = (_personalFactor.getPerceivedSelfEfficacy() * MED_VALUE) / VALUE_2;
				finalValues[5] = (_personalFactor.getPsychologicalWelfare() * MED_VALUE) / VALUE_3;
				finalValues[6] = (personalFactor.getSatisfactionStudies() * MAX_VALUE) / VALUE_2;
				finalValues[7] = (personalFactor.getClassesAssistance() * MIN_VALUE) / VALUE_100;
				finalValues[8] = (__personalFactor.getIntelligence() * MAX_VALUE) / VALUE_10;
				finalValues[9] = (personalFactor.getSkills() * MED_VALUE) / VALUE_2;
				
				if(__personalFactor.getSex() == 0){
					finalValues[10] = MIN_MIN_VALUE;
				}else{
					finalValues[10] = 0.0;
				}
				
				if(__personalFactor.getAge() >= 0 && __personalFactor.getAge() <= 25 ){
					finalValues[11] = MIN_MIN_VALUE;
				}else{
					finalValues[11] = 0.0;
				}
				
				if(__personalFactor.getMaritalStatus() == 0){
					finalValues[12] = MIN_MIN_VALUE;
				}else{
					finalValues[12] = 0.0;
				}
				
				if(__personalFactor.getDependents() == 0){
					finalValues[13] = MED_VALUE;
				}else{
					if(__personalFactor.getDependents() >= 1 && __personalFactor.getDependents() <= 3){
						finalValues[13] = MIN_VALUE;
					}else{
						finalValues[13] = 0.0;
					}
				}
				

				finalValues[14] = (__personalFactor.getAcademicTrainingSchool() * MIN_VALUE) / VALUE_10;
				finalValues[15] = (__personalFactor.getQualificationAccess() * MIN_VALUE) / VALUE_10;
				
				/*factores sociales*/
				if(!socialFactor.isSocialDifference()){
					finalValues[16] = MIN_MIN_VALUE;
				}else{
					finalValues[16] = 0.0;	
				}
				
				finalValues[17] = (socialFactor.getFamilyEnvironment() * MED_VALUE) / VALUE_3;
				finalValues[18] = (socialFactor.getSocioeconomicContext() * MIN_VALUE) / VALUE_2;
				finalValues[19] = (socialFactor.getDemographicVariable() * MED_VALUE) / VALUE_2;
				
				/*factores institucionales*/
				if(_institutionalFactor.isDesiredCareer()){
					finalValues[20] = MAX_VALUE;
				}else{
					finalValues[20] = 0.0;
				}
				
				/*para la complejidad*/
				double varAux = (institutionalFactor.getComplexityMatter() * MAX_VALUE) / VALUE_10;
				finalValues[21] = MAX_VALUE - varAux;
				
				finalValues[22] = (_institutionalFactor.getInstitutionalStatus() * MED_VALUE) / VALUE_10;			
				
				if(_institutionalFactor.isSupportService()){
					finalValues[23] = MIN_VALUE;
				}else{
					finalValues[23] = 0.0;
				}
				
				finalValues[24] = (_institutionalFactor.getStudentLife() * MAX_VALUE) / VALUE_3;
				finalValues[25] = (institutionalFactor.getRelationshipStudentTeacher() * MAX_VALUE) / VALUE_3;			


				//almacena calificaciones reales
				for(int i=0;i<listSize;i++){
					/*Para evaluaciones*/
					if(listParam.get(0) instanceof EvaluationUtil){
						EvaluationUtil data = (EvaluationUtil) listParam.get(i);
						newQualification[i] = operation.roundNumber(data.getQualificationEval(),2);
					}
					
					/*Para lecciones*/
					if(listParam.get(0) instanceof LessonUtil){
						LessonUtil data = (LessonUtil) listParam.get(i);
						newQualification[i] = operation.roundNumber(data.getQualificationLesson(),2);
					}
					
					/*Para trabajos autonomos*/
					if(listParam.get(0) instanceof ClassWorkUtil){
						ClassWorkUtil data = (ClassWorkUtil) listParam.get(i);
						newQualification[i] = operation.roundNumber(data.getQualificationClassWork(),2);
					}
					
					/*Para deberes*/
					if(listParam.get(0) instanceof ExtraClassWorkUtil){
						ExtraClassWorkUtil data = (ExtraClassWorkUtil) listParam.get(i);
						newQualification[i] = operation.roundNumber(data.getQualificationExtraClassWork(),2);
					}
					
					/*Para participaciones*/
					if(listParam.get(0) instanceof ParticipationUtil){
						ParticipationUtil data = (ParticipationUtil) listParam.get(i);
						newQualification[i] = operation.roundNumber(data.getQualificationParticipation(),2);
					}
					
					/*Para laboratorios*/
					if(listParam.get(0) instanceof LaboratoryUtil){
						LaboratoryUtil data = (LaboratoryUtil) listParam.get(i);
						newQualification[i] = operation.roundNumber(data.getQualificationLaboratory(),2);
					}
					
				}


				/*se realiza suma de parametros establecidos
				* anteriormente*/
				double cumSum = 0.0;
				for(int i =0;i<finalValues.length;i++){
					
					cumSum += finalValues[i];
				}
				/*se adiciona el promedio 
				* de calificaciones reales*/
				double valAux =(generalAverage * REAL_QUALIFICATION_VALUE)/VALUE_10;
				double finalQualification = valAux + cumSum;
				newQualification[listSize] = finalQualification;




				/*si no quedan dias antes de terminar
				* de evaluar todos los parametros establecidos*/
				if(daysToCompleteOffer <missingParams ){
					for(int i=0;i<newQualification.length;i++){
						
						/*para evaluaciones*/
						if(listParam.get(0) instanceof EvaluationUtil){
							EvaluationUtil newParam = new EvaluationUtil();
							if(i==(newQualification.length-1)){			
								newParam.setDateEvaluation(dateEndOffer);
							}else{
								EvaluationUtil data = (EvaluationUtil) listParam.get(i);
								newParam.setDateEvaluation(data.getDateEvaluation());
							}
							newParam.setQualificationEval(newQualification[i]);
							listFinal.add(newParam);
						}
						
						/*para lecciones*/
						if(listParam.get(0) instanceof LessonUtil){
							LessonUtil newParam = new LessonUtil();
							if(i==(newQualification.length-1)){			
								newParam.setDateLesson(dateEndOffer);
							}else{
								LessonUtil data = (LessonUtil) listParam.get(i);
								newParam.setDateLesson(data.getDateLesson());
							}
							newParam.setQualificationLesson(newQualification[i]);
							listFinal.add(newParam);
						}
						
						
						/*para trabajos autonomos*/
						if(listParam.get(0) instanceof ClassWorkUtil){
							ClassWorkUtil newParam = new ClassWorkUtil();
							if(i==(newQualification.length-1)){			
								newParam.setDateClassWork(dateEndOffer);
							}else{
								ClassWorkUtil data = (ClassWorkUtil) listParam.get(i);
								newParam.setDateClassWork(data.getDateClassWork());
							}
							newParam.setQualificationClassWork(newQualification[i]);
							listFinal.add(newParam);
						}
						
						
						/*para deberes*/
						if(listParam.get(0) instanceof ExtraClassWorkUtil){
							ExtraClassWorkUtil newParam = new ExtraClassWorkUtil();
							if(i==(newQualification.length-1)){			
								newParam.setDateExtraClassWork(dateEndOffer);
							}else{
								ExtraClassWorkUtil data = (ExtraClassWorkUtil) listParam.get(i);
								newParam.setDateExtraClassWork(data.getDateExtraClassWork());
							}
							newParam.setQualificationExtraClassWork(newQualification[i]);
							listFinal.add(newParam);
						}
						
						
						/*para participaciones*/
						if(listParam.get(0) instanceof ParticipationUtil){
							ParticipationUtil newParam = new ParticipationUtil();
							if(i==(newQualification.length-1)){			
								newParam.setDateParticipation(dateEndOffer);
							}else{
								EvaluationUtil data = (EvaluationUtil) listParam.get(i);
								newParam.setDateParticipation(data.getDateEvaluation());
							}
							newParam.setQualificationParticipation(newQualification[i]);
							listFinal.add(newParam);
						}
						
						
						/*para laboratorios*/
						if(listParam.get(0) instanceof LaboratoryUtil){
							LaboratoryUtil newParam = new LaboratoryUtil();
							if(i==(newQualification.length-1)){			
								newParam.setDateLaboratory(dateEndOffer);
							}else{
								LaboratoryUtil data = (LaboratoryUtil) listParam.get(i);
								newParam.setDateLaboratory(data.getDateLaboratory());
							}
							newParam.setQualificationLaboratory(newQualification[i]);
							listFinal.add(newParam);
						}
					}

				}else{

					/*dias a añadir*/
					int daysToAdd = daysToCompleteOffer / missingParams;
					Date dateNewParam = dateLastParam;

					for(int i=0;i<newQualification.length;i++){
						
						/*Para evaluaciones*/
						if(listParam.get(0) instanceof EvaluationUtil){
							EvaluationUtil newParam = new EvaluationUtil();
							
							if(i==(newQualification.length-1)){
								dateNewParam = operation.addDate(dateNewParam, daysToAdd);
								newParam.setDateEvaluation(dateNewParam);
							}else{
								EvaluationUtil data = (EvaluationUtil) listParam.get(i);
								newParam.setDateEvaluation(data.getDateEvaluation());
							}
							newParam.setQualificationEval(newQualification[i]);
							listFinal.add(newParam);
						}
						
						/*para lecciones*/
						if(listParam.get(0) instanceof LessonUtil){
							LessonUtil newParam = new LessonUtil();
							if(i==(newQualification.length-1)){	
								dateNewParam = operation.addDate(dateNewParam, daysToAdd);
								newParam.setDateLesson(dateNewParam);
							}else{
								LessonUtil data = (LessonUtil) listParam.get(i);
								newParam.setDateLesson(data.getDateLesson());
							}
							newParam.setQualificationLesson(newQualification[i]);
							listFinal.add(newParam);
						}
						
						
						/*para trabajos autonomos*/
						if(listParam.get(0) instanceof ClassWorkUtil){
							ClassWorkUtil newParam = new ClassWorkUtil();
							if(i==(newQualification.length-1)){			
								dateNewParam = operation.addDate(dateNewParam, daysToAdd);
								newParam.setDateClassWork(dateNewParam);
							}else{
								ClassWorkUtil data = (ClassWorkUtil) listParam.get(i);
								newParam.setDateClassWork(data.getDateClassWork());
							}
							newParam.setQualificationClassWork(newQualification[i]);
							listFinal.add(newParam);
						}
						
						
						/*para deberes*/
						if(listParam.get(0) instanceof ExtraClassWorkUtil){
							ExtraClassWorkUtil newParam = new ExtraClassWorkUtil();
							if(i==(newQualification.length-1)){		
								dateNewParam = operation.addDate(dateNewParam, daysToAdd);
								newParam.setDateExtraClassWork(dateNewParam);
							}else{
								ExtraClassWorkUtil data = (ExtraClassWorkUtil) listParam.get(i);
								newParam.setDateExtraClassWork(data.getDateExtraClassWork());
							}
							newParam.setQualificationExtraClassWork(newQualification[i]);
							listFinal.add(newParam);
						}
						
						
						/*para participaciones*/
						if(listParam.get(0) instanceof ParticipationUtil){
							ParticipationUtil newParam = new ParticipationUtil();
							if(i==(newQualification.length-1)){		
								dateNewParam = operation.addDate(dateNewParam, daysToAdd);
								newParam.setDateParticipation(dateNewParam);
							}else{
								ParticipationUtil data = (ParticipationUtil) listParam.get(i);
								newParam.setDateParticipation(data.getDateParticipation());
							}
							newParam.setQualificationParticipation(newQualification[i]);
							listFinal.add(newParam);
						}
						
						
						/*para laboratorios*/
						if(listParam.get(0) instanceof LaboratoryUtil){
							LaboratoryUtil newParam = new LaboratoryUtil();
							if(i==(newQualification.length-1)){		
								dateNewParam = operation.addDate(dateNewParam, daysToAdd);
								newParam.setDateLaboratory(dateNewParam);
							}else{
								LaboratoryUtil data = (LaboratoryUtil) listParam.get(i);
								newParam.setDateLaboratory(data.getDateLaboratory());
							}
							newParam.setQualificationLaboratory(newQualification[i]);
							listFinal.add(newParam);
						}
						
						
					}	
				}	

			//se ha completado el proceso de calificación
			}else{
				isCompleteAllEvaluations = true;
			}



			List<Object> listObject = new ArrayList<Object>();
			//1ro lista
			listObject.add(listFinal);
			
			//2do booleano
			listObject.add(isNotLow);
			
			//3ro booleano
			listObject.add(isCompleteAllEvaluations);
			
			return listObject;

	}

}
