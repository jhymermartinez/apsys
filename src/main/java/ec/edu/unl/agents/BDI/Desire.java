package ec.edu.unl.agents.BDI;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ec.edu.unl.agents.objects.Parameter;
import ec.edu.unl.operations.Operations;
import ec.edu.unl.operations.impl.OperationsImpl;
import ec.edu.unl.utilities.classWork.ClassWorkUtil;
import ec.edu.unl.utilities.evaluation.EvaluationUtil;
import ec.edu.unl.utilities.extraClassWork.ExtraClassWorkUtil;
import ec.edu.unl.utilities.laboratory.LaboratoryUtil;
import ec.edu.unl.utilities.lesson.LessonUtil;
import ec.edu.unl.utilities.participation.ParticipationUtil;

public class Desire {
	

	public Belief generateOptions(Belief beliefs, Intention intentions) {

		Operations operation = new OperationsImpl();
			
		int totalDaysOffer = (operation.dateSubtraction(beliefs.getEndOffer(), beliefs.getStartOffer()))+1;
		
		/*Para evaluaciones*/
		if(!beliefs.getEvaluations().isEmpty()){
			
			/*promedio de evaluaciones perteneciente al 70%
			 * de la nota final*/
			double average = beliefs.getEvaluationAverage();
			
			
			/*porcentaje para el promedio de
			 * evaluaciones seleccionado por docente*/
			double percentage = beliefs.getEvaluationPercentage();

			Date dateLastEvaluation = beliefs.getEvaluations().get(beliefs.getEvaluations().size()-1).getDateEvaluation();		
			
			int daysToCompleteOffer = operation.dateSubtraction(beliefs.getEndOffer(),dateLastEvaluation);	
			
			
			List<Object> listObject = intentions.generatePrediction(beliefs.getEvaluations(),
					beliefs.getEvaluationPercentage(),
					beliefs.getTotalNumberEvaluations(),
					daysToCompleteOffer,
					totalDaysOffer,
					dateLastEvaluation,
					beliefs.getEndOffer(),
					beliefs.getPersonalFactor(),
					beliefs.get_personalFactor(),
					beliefs.get__personalFactor(),
					beliefs.getSocialFactor(),
					beliefs.getInstitutionalFactor(),
					beliefs.get_institutionalFactor());
			
			List<EvaluationUtil> listEvaluationNew  = (List<EvaluationUtil>) listObject.get(0);
			Boolean isNotLow = (Boolean)listObject.get(1);
			Boolean isCompleteAllEvaluations = (Boolean)listObject.get(2);

			
			/*modificacion de creencias*/
			beliefs.setEvaluations(listEvaluationNew);
			beliefs.setLowEval(isNotLow);
			beliefs.setIsCompleteEvals(isCompleteAllEvaluations);
		}
		
		/*para lecciones*/
		if(!(beliefs.getLessons().isEmpty())){
			
			double average = beliefs.getLessonAverage();
			double percentage = beliefs.getLaboratoryPercentage();
			Date dateLastLesson = beliefs.getLessons().get(beliefs.getLessons().size()-1).getDateLesson();	

			int daysToCompleteOffer = operation.dateSubtraction(beliefs.getEndOffer(),dateLastLesson);	
			
			List<Object> listObject = intentions.generatePrediction(beliefs.getLessons(),
					beliefs.getLessonPercentage(),
					beliefs.getTotalNumberLessons(),
					daysToCompleteOffer,
					totalDaysOffer,
					dateLastLesson,
					beliefs.getEndOffer(),
					beliefs.getPersonalFactor(),
					beliefs.get_personalFactor(),
					beliefs.get__personalFactor(),
					beliefs.getSocialFactor(),
					beliefs.getInstitutionalFactor(),
					beliefs.get_institutionalFactor());
			 
			List<LessonUtil> listLessonNew = (List<LessonUtil>)listObject.get(0);
			Boolean isNotLow = (Boolean)listObject.get(1);
			Boolean isCompleteAllLessons = (Boolean)listObject.get(2);
			/**modificacion en creencias*/
			beliefs.setLessons(listLessonNew);	
			beliefs.setLowLess(isNotLow);
			beliefs.setIsCompleteLess(isCompleteAllLessons);
		}
			
		/*Para trabajos autonomos*/
		if(!(beliefs.getClassWorks().isEmpty())){
			
			double average = beliefs.getClassWorkAverage();
			double percentage = beliefs.getClassWorkPercentage();
			Date dateLastClassWork = beliefs.getClassWorks().get(beliefs.getClassWorks().size()-1).getDateClassWork();	
			int daysToCompleteOffer = operation.dateSubtraction(beliefs.getEndOffer(),dateLastClassWork);	
			
			List<Object> listObject = intentions.generatePrediction(beliefs.getClassWorks(),
					beliefs.getClassWorkPercentage(),
					beliefs.getTotalNumberClassWorks(),
					daysToCompleteOffer,
					totalDaysOffer,
					dateLastClassWork,
					beliefs.getEndOffer(),
					beliefs.getPersonalFactor(),
					beliefs.get_personalFactor(),
					beliefs.get__personalFactor(),
					beliefs.getSocialFactor(),
					beliefs.getInstitutionalFactor(),
					beliefs.get_institutionalFactor());
			 
			List<ClassWorkUtil> listClassWorkNew = (List<ClassWorkUtil>) listObject.get(0);
			Boolean isNotLow = (Boolean)listObject.get(1);
			Boolean isCompleteAllClassWorks = (Boolean)listObject.get(2);
			
			/**modificacion en creencias*/
			beliefs.setClassWorks(listClassWorkNew);
			beliefs.setLowCW(isNotLow);
			beliefs.setIsCompleteCW(isCompleteAllClassWorks);
		}			
		
		/*Para deberes tareas*/
		if(!(beliefs.getExtraClassWorks().isEmpty())){
			
			double average = beliefs.getExtraClassWorkAverage();
			double percentage = beliefs.getExtraClassWorkPercentage();
			Date dateLastExtraClassWork = beliefs.getExtraClassWorks().get(beliefs.getExtraClassWorks().size()-1).getDateExtraClassWork();	
			int daysToCompleteOffer = operation.dateSubtraction(beliefs.getEndOffer(),dateLastExtraClassWork);	
			
			List<Object> listObject = intentions.generatePrediction(beliefs.getExtraClassWorks(),
					beliefs.getExtraClassWorkPercentage(),
					beliefs.getTotalNumberExtraClassWorks(),
					daysToCompleteOffer,
					totalDaysOffer,
					dateLastExtraClassWork,
					beliefs.getEndOffer(),
					beliefs.getPersonalFactor(),
					beliefs.get_personalFactor(),
					beliefs.get__personalFactor(),
					beliefs.getSocialFactor(),
					beliefs.getInstitutionalFactor(),
					beliefs.get_institutionalFactor());
			
			List<ExtraClassWorkUtil> listExtraClassWorkNew  = (List<ExtraClassWorkUtil>) listObject.get(0);
			Boolean isNotLow = (Boolean)listObject.get(1);
			Boolean isCompleteAllExtraClassWorks = (Boolean)listObject.get(2);
			
			/**modificacion en creencias*/
			beliefs.setExtraClassWorks(listExtraClassWorkNew);
			beliefs.setLowECW(isNotLow);
			beliefs.setIsCompleteECW(isCompleteAllExtraClassWorks);
		}
		
		/*Para participaciones*/
		if(!(beliefs.getParticipations().isEmpty())){
			
			double average = beliefs.getParticipationAverage();
			double percentage = beliefs.getParticipationPercentage();
			Date dateLastParticipation = beliefs.getParticipations().get(beliefs.getParticipations().size()-1).getDateParticipation();	
			int daysToCompleteOffer = operation.dateSubtraction(beliefs.getEndOffer(),dateLastParticipation);	
			
			 
			List<Object> listObject = intentions.generatePrediction(beliefs.getParticipations(),
					beliefs.getParticipationPercentage(),
					beliefs.getTotalNumberParticipations(),
					daysToCompleteOffer,
					totalDaysOffer,
					dateLastParticipation,
					beliefs.getEndOffer(),
					beliefs.getPersonalFactor(),
					beliefs.get_personalFactor(),
					beliefs.get__personalFactor(),
					beliefs.getSocialFactor(),
					beliefs.getInstitutionalFactor(),
					beliefs.get_institutionalFactor());
			 
			List<ParticipationUtil> listParticipationNew = (List<ParticipationUtil>)listObject.get(0);
			Boolean isNotLow = (Boolean)listObject.get(1);
			Boolean isCompleteAllParticipations = (Boolean)listObject.get(2);
			
			/**modificacion en creencias*/
			beliefs.setParticipations(listParticipationNew);
			beliefs.setLowPart(isNotLow);
			beliefs.setIsCompletePart(isCompleteAllParticipations);
		}
		
		/*Para laboratorios*/
		if(!(beliefs.getLaboratories().isEmpty())){
			
			double average = beliefs.getLaboratoryAverage();
			double percentage = beliefs.getLaboratoryPercentage();
			Date dateLastLaboratory = beliefs.getLaboratories().get(beliefs.getLaboratories().size()-1).getDateLaboratory();	
			int daysToCompleteOffer = operation.dateSubtraction(beliefs.getEndOffer(),dateLastLaboratory);	
			
			 
			List<Object> listObject = intentions.generatePrediction(beliefs.getLaboratories(),
					beliefs.getLaboratoryPercentage(),
					beliefs.getTotalNumberLaboratories(),
					daysToCompleteOffer,
					totalDaysOffer,
					dateLastLaboratory,
					beliefs.getEndOffer(),
					beliefs.getPersonalFactor(),
					beliefs.get_personalFactor(),
					beliefs.get__personalFactor(),
					beliefs.getSocialFactor(),
					beliefs.getInstitutionalFactor(),
					beliefs.get_institutionalFactor());
			 
			List<LaboratoryUtil> listLaboratoryNew = (List<LaboratoryUtil>)listObject.get(0);
			Boolean isNotLow = (Boolean)listObject.get(1);
			Boolean isCompleteAllLaboratories = (Boolean)listObject.get(2);
			
			/**modificacion en creencias*/
			beliefs.setLaboratories(listLaboratoryNew);
			beliefs.setLowLab(isNotLow);
			beliefs.setIsCompleteLab(isCompleteAllLaboratories);
		}
			
		return beliefs;
	}
	

}
