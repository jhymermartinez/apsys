package ec.edu.unl.agents.BDI;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ec.edu.unl.agents.objects.Parameter;
import ec.edu.unl.entity.InstitutionalFactor;
import ec.edu.unl.entity.PersonalFactor;
import ec.edu.unl.entity.SocialFactor;
import ec.edu.unl.entity._InstitutionalFactor;
import ec.edu.unl.entity._PersonalFactor;
import ec.edu.unl.entity.__PersonalFactor;
import ec.edu.unl.utilities.academic.PredictionParameterUtil;
import ec.edu.unl.utilities.classWork.ClassWorkUtil;
import ec.edu.unl.utilities.evaluation.EvaluationUtil;
import ec.edu.unl.utilities.extraClassWork.ExtraClassWorkUtil;
import ec.edu.unl.utilities.laboratory.LaboratoryUtil;
import ec.edu.unl.utilities.lesson.LessonUtil;
import ec.edu.unl.utilities.participation.ParticipationUtil;

public class Belief implements Serializable{

	private List<EvaluationUtil> evaluations;
	private List<LessonUtil> lessons;
	private List<ClassWorkUtil> classWorks;
	private List<ExtraClassWorkUtil> extraClassWorks;
	private List<ParticipationUtil> participations;
	private List<LaboratoryUtil> laboratories;
	
	private Boolean lowEval;
	private Boolean lowLess;
	private Boolean lowCW;
	private Boolean lowECW;
	private Boolean lowPart;
	private Boolean lowLab;
	
	/*verificar que se completo la lista 
	 *de calificaciones*/
	private Boolean isCompleteEvals;
	private Boolean isCompleteLess;
	private Boolean isCompleteCW;
	private Boolean isCompleteECW;
	private Boolean isCompletePart;
	private Boolean isCompleteLab;
	
	/*porcentajes y catidad de parametrsos
	 * cofigurados por el docente*/
	private Double evaluationPercentage;
	private Integer totalNumberEvaluations;
	private Double lessonPercentage;
	private Integer totalNumberLessons;
	private Double classWorkPercentage;
	private Integer totalNumberClassWorks;
	private Double extraClassWorkPercentage;
	private Integer totalNumberExtraClassWorks;
	private Double participationPercentage;
	private Integer totalNumberParticipations;
	private Double laboratoryPercentage;
	private Integer totalNumberLaboratories;
	
	private Parameter parameters;
	
	/*fechas de inicio y fin de oferta
	 * academica*/
	private Date startOffer;
	private Date endOffer;

	/*promedios*/
	private Double evaluationAverage;
	private Double lessonAverage;
	private Double classWorkAverage;
	private Double extraClassWorkAverage;
	private Double participationAverage;
	private Double laboratoryAverage;
	
	/*promedio general*/
	private Double generalAverage;
	
	
	/*factores rendimiento estudiantes*/
	PersonalFactor personalFactor;
	_PersonalFactor _personalFactor;
	__PersonalFactor __personalFactor;
	
	SocialFactor socialFactor;
	InstitutionalFactor institutionalFactor;
	_InstitutionalFactor _institutionalFactor;

	public Belief() {

	}

	public Belief(Parameter parameters) {
		this.parameters = parameters;
	}
	
	public void setParameters(Parameter parameters){
		this.parameters = parameters;
	}
	
	
	public void generateInitialBeliefs(Parameter parameters) {
		
		evaluations = parameters.getEvaluations();
		lessons = parameters.getLessons();
		classWorks = parameters.getClassWorks();
		extraClassWorks = parameters.getExtraClassWorks();
		participations = parameters.getParticipations();
		laboratories = parameters.getLaboratories();
		
		startOffer = parameters.getStartOffer();
		endOffer = parameters.getEndOffer();
		
		/*configuraciones y promedios docente*/
		evaluationPercentage = parameters.getListConfigurations().get(0).getPercentage();
		totalNumberEvaluations = parameters.getListConfigurations().get(0).getNumberDefault();
		lessonPercentage = parameters.getListConfigurations().get(1).getPercentage();
		totalNumberLessons = parameters.getListConfigurations().get(1).getNumberDefault();;
		classWorkPercentage = parameters.getListConfigurations().get(2).getPercentage();
		totalNumberClassWorks = parameters.getListConfigurations().get(2).getNumberDefault();;
		extraClassWorkPercentage = parameters.getListConfigurations().get(3).getPercentage();
		totalNumberExtraClassWorks = parameters.getListConfigurations().get(3).getNumberDefault();;
		participationPercentage = parameters.getListConfigurations().get(4).getPercentage();
		totalNumberParticipations = parameters.getListConfigurations().get(4).getNumberDefault();;
		laboratoryPercentage = parameters.getListConfigurations().get(5).getPercentage();
		totalNumberLaboratories = parameters.getListConfigurations().get(5).getNumberDefault();;
		

		/*configuraciones del estudiante*/
		personalFactor = parameters.getPersonalFactor();
		_personalFactor = parameters.get_personalFactor();
		__personalFactor = parameters.get__personalFactor();
		
		socialFactor = parameters.getSocialFactor();
		institutionalFactor = parameters.getInstitutionalFactor();
		_institutionalFactor = parameters.get_institutionalFactor();
		
		
		Double cumSumEval = 0.0;
		for(EvaluationUtil data: evaluations){
			cumSumEval = cumSumEval + data.getQualificationEval();
		}
		evaluationAverage = (cumSumEval / evaluations.size())*evaluationPercentage;
	
		Double less = 0.0;
		for(LessonUtil data: lessons){
			less = less + data.getQualificationLesson();
		}
		lessonAverage = (less /  lessons.size())*lessonPercentage;
		
		Double cw = 0.0;
		for(ClassWorkUtil data: classWorks){
			cw = cw + data.getQualificationClassWork();
		}
		classWorkAverage = (cw / classWorks.size())*classWorkPercentage;
		
		Double ecw = 0.0;
		for(ExtraClassWorkUtil data: extraClassWorks){
			ecw = ecw + data.getQualificationExtraClassWork();
		}
		extraClassWorkAverage = (ecw / extraClassWorks.size())*extraClassWorkPercentage;
		
		Double part = 0.0;
		for(ParticipationUtil data: participations){
			part = part + data.getQualificationParticipation();
		}
		participationAverage = (part / participations.size())*participationPercentage;
		
		Double lab = 0.0;
		for(LaboratoryUtil data: laboratories){
			lab = lab + data.getQualificationLaboratory();
		}
		laboratoryAverage = (lab / laboratories.size())*laboratoryPercentage;
		
		generalAverage = evaluationAverage 
				+ lessonAverage
				+ classWorkAverage
				+ extraClassWorkAverage
				+ participationAverage
				+ laboratoryAverage;
	}

	
	
	public List<EvaluationUtil> getEvaluations() {
		return evaluations;
	}

	public void setEvaluations(List<EvaluationUtil> evaluations) {
		this.evaluations = evaluations;
	}

	public Date getStartOffer() {
		return startOffer;
	}

	public void setStartOffer(Date startOffer) {
		this.startOffer = startOffer;
	}

	public Date getEndOffer() {
		return endOffer;
	}

	public void setEndOffer(Date endOffer) {
		this.endOffer = endOffer;
	}


	public Parameter getParameters() {
		return parameters;
	}

	public List<LessonUtil> getLessons() {
		return lessons;
	}

	public void setLessons(List<LessonUtil> lessons) {
		this.lessons = lessons;
	}

	public List<ClassWorkUtil> getClassWorks() {
		return classWorks;
	}

	public void setClassWorks(List<ClassWorkUtil> classWorks) {
		this.classWorks = classWorks;
	}

	public List<ExtraClassWorkUtil> getExtraClassWorks() {
		return extraClassWorks;
	}

	public void setExtraClassWorks(List<ExtraClassWorkUtil> extraClassWorks) {
		this.extraClassWorks = extraClassWorks;
	}

	public List<ParticipationUtil> getParticipations() {
		return participations;
	}

	public void setParticipations(List<ParticipationUtil> participations) {
		this.participations = participations;
	}

	public List<LaboratoryUtil> getLaboratories() {
		return laboratories;
	}

	public void setLaboratories(List<LaboratoryUtil> laboratories) {
		this.laboratories = laboratories;
	}

	public Double getEvaluationAverage() {
		return evaluationAverage;
	}

	public void setEvaluationAverage(Double evaluationAverage) {
		this.evaluationAverage = evaluationAverage;
	}

	public Double getLessonAverage() {
		return lessonAverage;
	}

	public void setLessonAverage(Double lessonAverage) {
		this.lessonAverage = lessonAverage;
	}

	public Double getClassWorkAverage() {
		return classWorkAverage;
	}

	public void setClassWorkAverage(Double classWorkAverage) {
		this.classWorkAverage = classWorkAverage;
	}

	public Double getExtraClassWorkAverage() {
		return extraClassWorkAverage;
	}

	public void setExtraClassWorkAverage(Double extraClassWorkAverage) {
		this.extraClassWorkAverage = extraClassWorkAverage;
	}

	public Double getParticipationAverage() {
		return participationAverage;
	}

	public void setParticipationAverage(Double participationAverage) {
		this.participationAverage = participationAverage;
	}

	public Double getLaboratoryAverage() {
		return laboratoryAverage;
	}

	public void setLaboratoryAverage(Double laboratoryAverage) {
		this.laboratoryAverage = laboratoryAverage;
	}

	public Double getGeneralAverage() {
		return generalAverage;
	}

	public void setGeneralAverage(Double generalAverage) {
		this.generalAverage = generalAverage;
	}

	public Double getEvaluationPercentage() {
		return evaluationPercentage;
	}

	public void setEvaluationPercentage(Double evaluationPercentage) {
		this.evaluationPercentage = evaluationPercentage;
	}

	public Integer getTotalNumberEvaluations() {
		return totalNumberEvaluations;
	}

	public void setTotalNumberEvaluations(Integer totalNumberEvaluations) {
		this.totalNumberEvaluations = totalNumberEvaluations;
	}

	public Double getLessonPercentage() {
		return lessonPercentage;
	}

	public void setLessonPercentage(Double lessonPercentage) {
		this.lessonPercentage = lessonPercentage;
	}

	public Integer getTotalNumberLessons() {
		return totalNumberLessons;
	}

	public void setTotalNumberLessons(Integer totalNumberLessons) {
		this.totalNumberLessons = totalNumberLessons;
	}

	public Double getClassWorkPercentage() {
		return classWorkPercentage;
	}

	public void setClassWorkPercentage(Double classWorkPercentage) {
		this.classWorkPercentage = classWorkPercentage;
	}

	public Integer getTotalNumberClassWorks() {
		return totalNumberClassWorks;
	}

	public void setTotalNumberClassWorks(Integer totalNumberClassWorks) {
		this.totalNumberClassWorks = totalNumberClassWorks;
	}

	public Double getExtraClassWorkPercentage() {
		return extraClassWorkPercentage;
	}

	public void setExtraClassWorkPercentage(Double extraClassWorkPercentage) {
		this.extraClassWorkPercentage = extraClassWorkPercentage;
	}

	public Integer getTotalNumberExtraClassWorks() {
		return totalNumberExtraClassWorks;
	}

	public void setTotalNumberExtraClassWorks(Integer totalNumberExtraClassWorks) {
		this.totalNumberExtraClassWorks = totalNumberExtraClassWorks;
	}

	public Double getParticipationPercentage() {
		return participationPercentage;
	}

	public void setParticipationPercentage(Double participationPercentage) {
		this.participationPercentage = participationPercentage;
	}

	public Integer getTotalNumberParticipations() {
		return totalNumberParticipations;
	}

	public void setTotalNumberParticipations(Integer totalNumberParticipations) {
		this.totalNumberParticipations = totalNumberParticipations;
	}

	public Double getLaboratoryPercentage() {
		return laboratoryPercentage;
	}

	public void setLaboratoryPercentage(Double laboratoryPercentage) {
		this.laboratoryPercentage = laboratoryPercentage;
	}

	public Integer getTotalNumberLaboratories() {
		return totalNumberLaboratories;
	}

	public void setTotalNumberLaboratories(Integer totalNumberLaboratories) {
		this.totalNumberLaboratories = totalNumberLaboratories;
	}

	public Boolean getLowEval() {
		return lowEval;
	}

	public void setLowEval(Boolean lowEval) {
		this.lowEval = lowEval;
	}

	public Boolean getLowLess() {
		return lowLess;
	}

	public void setLowLess(Boolean lowLess) {
		this.lowLess = lowLess;
	}



	public Boolean getLowCW() {
		return lowCW;
	}

	public void setLowCW(Boolean lowCW) {
		this.lowCW = lowCW;
	}

	public Boolean getLowECW() {
		return lowECW;
	}

	public void setLowECW(Boolean lowECW) {
		this.lowECW = lowECW;
	}

	public Boolean getLowPart() {
		return lowPart;
	}

	public void setLowPart(Boolean lowPart) {
		this.lowPart = lowPart;
	}

	public Boolean getLowLab() {
		return lowLab;
	}

	public void setLowLab(Boolean lowLab) {
		this.lowLab = lowLab;
	}


	public Boolean getIsCompleteEvals() {
		return isCompleteEvals;
	}

	public void setIsCompleteEvals(Boolean isCompleteEvals) {
		this.isCompleteEvals = isCompleteEvals;
	}

	public Boolean getIsCompleteLess() {
		return isCompleteLess;
	}

	public void setIsCompleteLess(Boolean isCompleteLess) {
		this.isCompleteLess = isCompleteLess;
	}

	public Boolean getIsCompleteCW() {
		return isCompleteCW;
	}

	public void setIsCompleteCW(Boolean isCompleteCW) {
		this.isCompleteCW = isCompleteCW;
	}

	public Boolean getIsCompleteECW() {
		return isCompleteECW;
	}

	public void setIsCompleteECW(Boolean isCompleteECW) {
		this.isCompleteECW = isCompleteECW;
	}

	public Boolean getIsCompletePart() {
		return isCompletePart;
	}

	public void setIsCompletePart(Boolean isCompletePart) {
		this.isCompletePart = isCompletePart;
	}

	public Boolean getIsCompleteLab() {
		return isCompleteLab;
	}

	public void setIsCompleteLab(Boolean isCompleteLab) {
		this.isCompleteLab = isCompleteLab;
	}

	public PersonalFactor getPersonalFactor() {
		return personalFactor;
	}

	public void setPersonalFactor(PersonalFactor personalFactor) {
		this.personalFactor = personalFactor;
	}

	public SocialFactor getSocialFactor() {
		return socialFactor;
	}

	public void setSocialFactor(SocialFactor socialFactor) {
		this.socialFactor = socialFactor;
	}

	public InstitutionalFactor getInstitutionalFactor() {
		return institutionalFactor;
	}

	public void setInstitutionalFactor(InstitutionalFactor institutionalFactor) {
		this.institutionalFactor = institutionalFactor;
	}

	public _PersonalFactor get_personalFactor() {
		return _personalFactor;
	}

	public void set_personalFactor(_PersonalFactor _personalFactor) {
		this._personalFactor = _personalFactor;
	}

	public __PersonalFactor get__personalFactor() {
		return __personalFactor;
	}

	public void set__personalFactor(__PersonalFactor __personalFactor) {
		this.__personalFactor = __personalFactor;
	}

	public _InstitutionalFactor get_institutionalFactor() {
		return _institutionalFactor;
	}

	public void set_institutionalFactor(_InstitutionalFactor _institutionalFactor) {
		this._institutionalFactor = _institutionalFactor;
	}




	
}
