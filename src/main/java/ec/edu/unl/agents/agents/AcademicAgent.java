package ec.edu.unl.agents.agents;


import ec.edu.unl.agents.BDI.Belief;
import ec.edu.unl.agents.BDI.Desire;
import ec.edu.unl.agents.BDI.Intention;
import ec.edu.unl.agents.objects.Parameter;
import jade.core.behaviours.Behaviour;
import jade.wrapper.gateway.GatewayAgent;

public class AcademicAgent extends GatewayAgent{
	
	Parameter parameters = null;
	
	@Override
	protected void processCommand(Object object) {
		
		parameters = (Parameter) object;
		
        /**COMPORTAMIENTO*/
		addBehaviour(new Behaviour(this) {
			
			@Override
			public boolean done() {
				return true;
			}
			
			@Override
			public void action() {
				
				//System.out.println("Dentro de mi comportamiento: " + getAgent().getLocalName());
				
				Belief beliefs = new Belief();
				beliefs.generateInitialBeliefs(parameters);
				Desire desires = new Desire();
				Intention intentions = new Intention();
				
				Belief beliefsNew = desires.generateOptions(beliefs,intentions);
				
				
				parameters.setEvaluations(beliefsNew.getEvaluations());
				parameters.setLessons(beliefsNew.getLessons());
				parameters.setClassWorks(beliefsNew.getClassWorks());
				parameters.setExtraClassWorks(beliefsNew.getExtraClassWorks());
				parameters.setParticipations(beliefsNew.getParticipations());
				parameters.setLaboratories(beliefsNew.getLaboratories());
				
				parameters.setLowEval(beliefsNew.getLowEval());
				parameters.setLowLess(beliefsNew.getLowLess());
				parameters.setLowCW(beliefsNew.getLowCW());
				parameters.setLowECW(beliefsNew.getLowECW());
				parameters.setLowPart(beliefsNew.getLowPart());
				parameters.setLowLab(beliefsNew.getLowLab());
				
				
				
				releaseCommand(parameters);
				
			}
		});
        
		
       
		
	}
	
	

    //inicializar agentes
    @Override
    protected void setup() {
        super.setup(); 


    }

    //matar agente
    @Override
    protected void takeDown() {
        super.takeDown(); 
    }
}
