package ec.edu.unl.agents.objects;

public class Percentage {
	
	Double evaluation;
	Double lesson;
	Double classWork;
	Double extraClassWork;
	Double participation;
	Double laboratory;
	
	
	public Percentage() {
		// TODO Auto-generated constructor stub
	}
	public Percentage(Double evaluation, Double lesson, Double classWork,
			Double extraClassWork, Double participation, Double laboratory) {
		
		this.evaluation = evaluation;
		this.lesson = lesson;
		this.classWork = classWork;
		this.extraClassWork = extraClassWork;
		this.participation = participation;
		this.laboratory = laboratory;
	}

	public Double getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(Double evaluation) {
		this.evaluation = evaluation;
	}

	public Double getLesson() {
		return lesson;
	}

	public void setLesson(Double lesson) {
		this.lesson = lesson;
	}

	public Double getClassWork() {
		return classWork;
	}

	public void setClassWork(Double classWork) {
		this.classWork = classWork;
	}

	public Double getExtraClassWork() {
		return extraClassWork;
	}

	public void setExtraClassWork(Double extraClassWork) {
		this.extraClassWork = extraClassWork;
	}

	public Double getParticipation() {
		return participation;
	}

	public void setParticipation(Double participation) {
		this.participation = participation;
	}

	public Double getLaboratory() {
		return laboratory;
	}

	public void setLaboratory(Double laboratory) {
		this.laboratory = laboratory;
	}
	
	
	
	
}
