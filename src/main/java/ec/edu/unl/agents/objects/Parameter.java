package ec.edu.unl.agents.objects;

import java.util.Date;
import java.util.List;

import ec.edu.unl.entity.InstitutionalFactor;
import ec.edu.unl.entity.PersonalFactor;
import ec.edu.unl.entity.SocialFactor;
import ec.edu.unl.entity._InstitutionalFactor;
import ec.edu.unl.entity._PersonalFactor;
import ec.edu.unl.entity.__PersonalFactor;
import ec.edu.unl.utilities.academic.ConfigurationUtil;
import ec.edu.unl.utilities.academic.PredictionParameterUtil;
import ec.edu.unl.utilities.classWork.ClassWorkUtil;
import ec.edu.unl.utilities.evaluation.EvaluationUtil;
import ec.edu.unl.utilities.extraClassWork.ExtraClassWorkUtil;
import ec.edu.unl.utilities.laboratory.LaboratoryUtil;
import ec.edu.unl.utilities.lesson.LessonUtil;
import ec.edu.unl.utilities.participation.ParticipationUtil;



public class Parameter {
	List<EvaluationUtil> evaluations;
	List<LessonUtil> lessons;
	List<ClassWorkUtil> classWorks;
	List<ExtraClassWorkUtil> extraClassWorks;
	List<ParticipationUtil> participations;
	List<LaboratoryUtil> laboratories;
	
	//configuraciones del docente
	List<ConfigurationUtil> listConfigurations;
	
	//factores del rendimiento del estudiante
	PersonalFactor personalFactor;
	_PersonalFactor _personalFactor;
	__PersonalFactor __personalFactor;
	SocialFactor socialFactor;
	InstitutionalFactor institutionalFactor;
	_InstitutionalFactor _institutionalFactor;
	
	Boolean lowEval;
	Boolean lowLess;
	Boolean lowCW;
	Boolean lowECW;
	Boolean lowPart;
	Boolean lowLab;

	
	Date startOffer;
	Date endOffer;
	
	
	public Parameter() {
		
	}
	
	public Parameter(List<EvaluationUtil> evaluations,
			List<LessonUtil> lessons, List<ClassWorkUtil> classWorks,
			List<ExtraClassWorkUtil> extraClassWorks,
			List<ParticipationUtil> participations,
			List<LaboratoryUtil> laboratories,
			List<ConfigurationUtil> listConfigurations, Date startOffer,
			Date endOffer) {
		
		this.evaluations = evaluations;
		this.lessons = lessons;
		this.classWorks = classWorks;
		this.extraClassWorks = extraClassWorks;
		this.participations = participations;
		this.laboratories = laboratories;
		this.listConfigurations = listConfigurations;
		this.startOffer = startOffer;
		this.endOffer = endOffer;

	}


	

	public Parameter(List<EvaluationUtil> evaluations,
			List<LessonUtil> lessons, List<ClassWorkUtil> classWorks,
			List<ExtraClassWorkUtil> extraClassWorks,
			List<ParticipationUtil> participations,
			List<LaboratoryUtil> laboratories,
			List<ConfigurationUtil> listConfigurations,
			PersonalFactor personalFactor, SocialFactor socialFactor,
			InstitutionalFactor institutionalFactor, Boolean lowEval,
			Boolean lowLess, Boolean lowCW, Boolean lowECW, Boolean lowPart,
			Boolean lowLab, Date startOffer, Date endOffer) {
		
		this.evaluations = evaluations;
		this.lessons = lessons;
		this.classWorks = classWorks;
		this.extraClassWorks = extraClassWorks;
		this.participations = participations;
		this.laboratories = laboratories;
		this.listConfigurations = listConfigurations;
		this.personalFactor = personalFactor;
		this.socialFactor = socialFactor;
		this.institutionalFactor = institutionalFactor;
		this.lowEval = lowEval;
		this.lowLess = lowLess;
		this.lowCW = lowCW;
		this.lowECW = lowECW;
		this.lowPart = lowPart;
		this.lowLab = lowLab;
		this.startOffer = startOffer;
		this.endOffer = endOffer;
	}

	public List<EvaluationUtil> getEvaluations() {
		return evaluations;
	}
	public void setEvaluations(List<EvaluationUtil> evaluations) {
		this.evaluations = evaluations;
	}
	public List<LessonUtil> getLessons() {
		return lessons;
	}
	public void setLessons(List<LessonUtil> lessons) {
		this.lessons = lessons;
	}
	public List<ClassWorkUtil> getClassWorks() {
		return classWorks;
	}
	public void setClassWorks(List<ClassWorkUtil> classWorks) {
		this.classWorks = classWorks;
	}
	public List<ExtraClassWorkUtil> getExtraClassWorks() {
		return extraClassWorks;
	}
	public void setExtraClassWorks(List<ExtraClassWorkUtil> extraClassWorks) {
		this.extraClassWorks = extraClassWorks;
	}
	public List<ParticipationUtil> getParticipations() {
		return participations;
	}
	public void setParticipations(List<ParticipationUtil> participations) {
		this.participations = participations;
	}
	public List<LaboratoryUtil> getLaboratories() {
		return laboratories;
	}
	public void setLaboratories(List<LaboratoryUtil> laboratories) {
		this.laboratories = laboratories;
	}
	public Date getStartOffer() {
		return startOffer;
	}
	public void setStartOffer(Date startOffer) {
		this.startOffer = startOffer;
	}
	public Date getEndOffer() {
		return endOffer;
	}
	public void setEndOffer(Date endOffer) {
		this.endOffer = endOffer;
	}
	public List<ConfigurationUtil> getListConfigurations() {
		return listConfigurations;
	}
	public void setListConfigurations(List<ConfigurationUtil> listConfigurations) {
		this.listConfigurations = listConfigurations;
	}

	public Boolean getLowEval() {
		return lowEval;
	}

	public void setLowEval(Boolean lowEval) {
		this.lowEval = lowEval;
	}

	public Boolean getLowLess() {
		return lowLess;
	}

	public void setLowLess(Boolean lowLess) {
		this.lowLess = lowLess;
	}

	public Boolean getLowCW() {
		return lowCW;
	}

	public void setLowCW(Boolean lowCW) {
		this.lowCW = lowCW;
	}

	public Boolean getLowECW() {
		return lowECW;
	}

	public void setLowECW(Boolean lowECW) {
		this.lowECW = lowECW;
	}

	public Boolean getLowPart() {
		return lowPart;
	}

	public void setLowPart(Boolean lowPart) {
		this.lowPart = lowPart;
	}

	public Boolean getLowLab() {
		return lowLab;
	}

	public void setLowLab(Boolean lowLab) {
		this.lowLab = lowLab;
	}

	public PersonalFactor getPersonalFactor() {
		return personalFactor;
	}

	public void setPersonalFactor(PersonalFactor personalFactor) {
		this.personalFactor = personalFactor;
	}

	public SocialFactor getSocialFactor() {
		return socialFactor;
	}

	public void setSocialFactor(SocialFactor socialFactor) {
		this.socialFactor = socialFactor;
	}

	public InstitutionalFactor getInstitutionalFactor() {
		return institutionalFactor;
	}

	public void setInstitutionalFactor(InstitutionalFactor institutionalFactor) {
		this.institutionalFactor = institutionalFactor;
	}

	public _PersonalFactor get_personalFactor() {
		return _personalFactor;
	}

	public void set_personalFactor(_PersonalFactor _personalFactor) {
		this._personalFactor = _personalFactor;
	}

	public __PersonalFactor get__personalFactor() {
		return __personalFactor;
	}

	public void set__personalFactor(__PersonalFactor __personalFactor) {
		this.__personalFactor = __personalFactor;
	}

	public _InstitutionalFactor get_institutionalFactor() {
		return _institutionalFactor;
	}

	public void set_institutionalFactor(_InstitutionalFactor _institutionalFactor) {
		this._institutionalFactor = _institutionalFactor;
	}



}
