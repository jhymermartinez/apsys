package ec.edu.unl.operations.impl;

import java.util.Map;

import javax.xml.ws.BindingProvider;

import ec.edu.unl.operations.Conections;
import ec.edu.unl.ws.sgaws.wsacademica.soap.SGAWebServicesAcademica;
import ec.edu.unl.ws.sgaws.wsacademica.soap.SGAWebServicesAcademicaPortType;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.SGAWebServicesEstadistica;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.SGAWebServicesEstadisticaPortType;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.SGAWebServicesInstitucional;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.SGAWebServicesInstitucionalPortType;
import ec.edu.unl.ws.sgaws.wspersonal.soap.SGAWebServicesPersonal;
import ec.edu.unl.ws.sgaws.wspersonal.soap.SGAWebServicesPersonalPortType;
import ec.edu.unl.ws.sgaws.wsvalidacion.soap.SGAWebServicesValidacion;
import ec.edu.unl.ws.sgaws.wsvalidacion.soap.SGAWebServicesValidacionPortType;

public class ConectionImpl implements Conections {
	
	String usernameWS;

	public String getUsernameWS() {
		return usernameWS;
	}

	public void setUsernameWS(String usernameWS) {
		this.usernameWS = usernameWS;
	}
	

	String passwordWS;
	
	public String getPasswordWS() {
		return passwordWS;
	}

	public void setPasswordWS(String passwordWS) {
		this.passwordWS = passwordWS;
	}

	@Override
	public SGAWebServicesValidacionPortType conectionValidacionWS() {
		
		SGAWebServicesValidacion sgaWebServicesValidacion = new SGAWebServicesValidacion();

		SGAWebServicesValidacionPortType sgaValidacion = sgaWebServicesValidacion
				.getSGAWebServicesValidacionPortType();

		BindingProvider bp = (BindingProvider) sgaValidacion;
		Map<String, Object> map = bp.getRequestContext();
		map.put(bp.USERNAME_PROPERTY, usernameWS);
		map.put(bp.PASSWORD_PROPERTY, passwordWS);

		return sgaValidacion;

	}

	@Override
	public SGAWebServicesAcademicaPortType conectionAcademicaWS() {
		SGAWebServicesAcademica sgaWebServicesAcademica = new SGAWebServicesAcademica();

		SGAWebServicesAcademicaPortType sgaAcademica = sgaWebServicesAcademica
				.getSGAWebServicesAcademicaPortType();

		BindingProvider bp = (BindingProvider) sgaAcademica;
		Map<String, Object> map = bp.getRequestContext();
		map.put(bp.USERNAME_PROPERTY, usernameWS);
		map.put(bp.PASSWORD_PROPERTY, passwordWS);

		return sgaAcademica;
	}

	@Override
	public SGAWebServicesInstitucionalPortType conectionInstitucionalWS() {
		SGAWebServicesInstitucional sgaWebServicesInstitucional = new SGAWebServicesInstitucional();

		SGAWebServicesInstitucionalPortType sgaInstitucional = sgaWebServicesInstitucional
				.getSGAWebServicesInstitucionalPortType();

		BindingProvider bp = (BindingProvider) sgaInstitucional;
		Map<String, Object> map = bp.getRequestContext();
		map.put(bp.USERNAME_PROPERTY, usernameWS);
		map.put(bp.PASSWORD_PROPERTY, passwordWS);

		return sgaInstitucional;

	}

	@Override
	public SGAWebServicesEstadisticaPortType conectionEstadisticaWS() {
		SGAWebServicesEstadistica sgaWebServicesEstadistica = new SGAWebServicesEstadistica();

		SGAWebServicesEstadisticaPortType sgaEstadistica = sgaWebServicesEstadistica
				.getSGAWebServicesEstadisticaPortType();
		BindingProvider bp = (BindingProvider) sgaEstadistica;
		Map<String, Object> map = bp.getRequestContext();
		map.put(bp.USERNAME_PROPERTY, usernameWS);
		map.put(bp.PASSWORD_PROPERTY, passwordWS);
		return sgaEstadistica;
	}

	@Override
	public SGAWebServicesPersonalPortType conectionPersonalWS() {
		SGAWebServicesPersonal sgaWebServicesPersonal = new SGAWebServicesPersonal();

		SGAWebServicesPersonalPortType sgaPersonal = sgaWebServicesPersonal
				.getSGAWebServicesPersonalPortType();
		BindingProvider bp = (BindingProvider) sgaPersonal;
		Map<String, Object> map = bp.getRequestContext();
		map.put(bp.USERNAME_PROPERTY, usernameWS);
		map.put(bp.PASSWORD_PROPERTY, passwordWS);
		return sgaPersonal;
	}
}
