package ec.edu.unl.operations.impl;

import java.io.Serializable;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import ec.edu.unl.DAO.StudentDAO;
import ec.edu.unl.DAO.TeacherDAO;
import ec.edu.unl.operations.Operations;

import java.util.Map;

public class OperationsImpl implements Operations,Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
    @Qualifier("studentDAOImpl")
    StudentDAO studentDAO;
    
    @Autowired
    @Qualifier("teacherDAOImpl")
    TeacherDAO teacherDAO;
    
    
    static HttpServletRequest httpServletRequest;
    static FacesContext faceContext;

    public String processXML(String text_xml) {
        try {
            DocumentBuilder db;
            db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(text_xml));
            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("result");
            Element element = (Element) nodes.item(0);
            String text = getCharacterDataFromElement(element);
            return text;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "";
    }

    public JSONArray processJSON(String text_json) {
        Object obj = JSONValue.parse(text_json);
       
        JSONArray array = (JSONArray) obj;
        return array;
    }
    
    public JSONObject processJSONObject(String text_json) {
        Object obj = JSONValue.parse(text_json);
       
        JSONObject jsonObject = (JSONObject) obj;
        return jsonObject;
    }

    public Date convertStringtoDate(String date_string) {
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = date_string;
        Date date = null;
        try {
            date = date_format.parse(strDate);
            
            return date;
        } catch (ParseException ex) {
            ex.printStackTrace();
            return date;
        }

    }

    // solo en produccion
    public String getDirectory() {
        String directorio = System.getProperty("user.dir");
        return directorio;
    }


    @Override
    public String validarDocentePrueba(String identification_card) {
      if(teacherDAO.existTeacher(identification_card)){
    	  return "true";
      }else{
    	  return "false";
      }
    }

    @Override
    public String returnPeriodsDescription(Map<String, String> listPeriods, String periods) {
        String data = "";
        for (Map.Entry<String, String> entry : listPeriods.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
          
            if (periods.equalsIgnoreCase(value)) {
                data = key;
                break;
            }
        }
        return data;
    }

	@Override
	public String generateNameStudent(String first_name, String last_name) {
        String nom = first_name + " " +last_name;
        
        return nom.toUpperCase();
	}

	@Override
	public int calculateMax(int[] vector) {
		int result = vector[0];
		for (int i = 0; i < vector.length; i++) {
			if (vector[i] > result) {
				result = vector[i];
			}
		}
		return result;
	}

	@Override
	public long returnIdMatter(Map<String, String> listMatter,
			String evaluationMatterSelect) {
	
		
		  long data = 0;
		  
	        for (Map.Entry<String, String> entry : listMatter.entrySet()) {
	            String key = entry.getKey();
	            String value = entry.getValue();
	          System.out.println("key: "+key+"valor: "+value+"materia Select:"+evaluationMatterSelect);
	            if (value.equalsIgnoreCase(evaluationMatterSelect)) {
	                data = Long.parseLong(value);
	                break;
	            }
	        }

		return data;
	}

	@Override
	public String returnElementMap(Map<String, String> listAcademicOffer,
			String offer) {
	
		String val = "";
		
	    for (Map.Entry<String, String> entry : listAcademicOffer.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
          
            if (value.equalsIgnoreCase(offer)) {
            	val = key;
                break;
            }
        }
		
		return val;
	}

	@Override
	public String validarEstudiantePrueba(String identification_card) {
		if(studentDAO.existStudent(identification_card)){
			return "true";
		}else{
			return "false";
		}
	}

	@Override
	public Date returnSystemDate() {
		
		        //Instanciamos el objeto Calendar
		        //en fecha obtenemos la fecha y hora del sistema
		        Calendar fecha = new GregorianCalendar();
		        //Obtenemos el valor del año, mes, dia,
		        //hora, minuto y segundo del sistema
		        //usando el metodo get y el parametro correspondiente
		        int anio = fecha.get(Calendar.YEAR);
		        int mes = fecha.get(Calendar.MONTH);
		        int dia = fecha.get(Calendar.DAY_OF_MONTH);
		        
		        String dateStr = dia + "/" + (mes+1) + "/" + anio;
		        Date date = new Date();
		        
		        try {
		        	
		        	SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");       	
		        	date = formateador.parse(dateStr);
				
		        } catch (ParseException e) {
					e.printStackTrace();
				}

		return date;
	}
	
	@Override
	 public Date addDate(Date fecha, int dias){

		       Calendar calendar = Calendar.getInstance();
		       calendar.setTime(fecha);
		       calendar.add(Calendar.DAY_OF_YEAR, dias); 
		       return calendar.getTime();

		  }
	


	
	@Override
	public int dateSubtraction(Date maxDate, Date minDate) {
		long diferenciaEn_ms = maxDate.getTime() - minDate.getTime();
		long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24);
		return (int) dias;
	}
	
	@Override
	public double roundNumber( double numero, int decimales ) {
	    return Math.round(numero*Math.pow(10,decimales))/Math.pow(10,decimales);
	  }

	@Override
	public String generateRoute(String dir) {
		/**solo en produccion*/
		String result = dir.substring(0,dir.length()-3)+"webapps\\wsdl\\";
		return result;
	}
}
