package ec.edu.unl.operations;

import ec.edu.unl.ws.sgaws.wsacademica.soap.SGAWebServicesAcademicaPortType;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.SGAWebServicesEstadisticaPortType;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.SGAWebServicesInstitucionalPortType;
import ec.edu.unl.ws.sgaws.wspersonal.soap.SGAWebServicesPersonalPortType;
import ec.edu.unl.ws.sgaws.wsvalidacion.soap.SGAWebServicesValidacionPortType;

public interface Conections {
	public SGAWebServicesValidacionPortType conectionValidacionWS() ;
	
	public SGAWebServicesAcademicaPortType conectionAcademicaWS() ;
	
	public SGAWebServicesInstitucionalPortType conectionInstitucionalWS() ;
	
	public SGAWebServicesEstadisticaPortType conectionEstadisticaWS() ;
	
	public SGAWebServicesPersonalPortType conectionPersonalWS() ;
}
