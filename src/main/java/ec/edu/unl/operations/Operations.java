package ec.edu.unl.operations;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.swing.JOptionPane;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Element;

import ec.edu.unl.agents.objects.Percentage;


public interface Operations {

	public String processXML(String text_xml);

	public JSONArray processJSON(String text_json);
	
	public JSONObject processJSONObject(String text_json);
	
	public Date convertStringtoDate(String date_string);

	public String getCharacterDataFromElement(Element e);

	public String getDirectory();
   
    public String validarDocentePrueba(String cedula);

    public String returnPeriodsDescription(Map<String, String> listPeriods, String periods);
    
    public String generateNameStudent(String firstName,String lastName);
    
    public int calculateMax(int[] vector);

	public long returnIdMatter(Map<String, String> listMatter,String evaluationMatterSelect);

	public String returnElementMap(Map<String, String> listAcademicOffer,String offer);

	public String validarEstudiantePrueba(String identification_card);
	
	public Date returnSystemDate();
	
	public Date addDate(Date fecha, int dias);
		
	int dateSubtraction(Date maxDate, Date minDate);

	double roundNumber(double numero, int decimales);
	
	public String generateRoute(String dir);

}

