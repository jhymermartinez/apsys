package ec.edu.unl.entity;

import javax.persistence.Column;

import java.io.Serializable;

public class MatterModuleId implements Serializable{

	@Column(name = "matter_id")
	private long mattersId;

	@Column(name = "module_id")
	private long modulesId;

	public MatterModuleId() {
		
	}

	public long getMattersId() {
		return mattersId;
	}

	public void setMattersId(long mattersId) {
		this.mattersId = mattersId;
	}

	public long getModulesId() {
		return modulesId;
	}

	public void setModulesId(long modulesId) {
		this.modulesId = modulesId;
	}
	
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof MatterModuleId)) {
            return false;
        }
        MatterModuleId castOther = (MatterModuleId) other;

        return (this.getMattersId() == castOther.getMattersId())
                && (this.getModulesId() == castOther.getModulesId());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (int) this.getMattersId();
        result = 37 * result + (int) this.getModulesId();
        return result;
    }
	
}
