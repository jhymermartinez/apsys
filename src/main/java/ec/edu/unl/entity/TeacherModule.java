package ec.edu.unl.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "teacher_module")
@IdClass(TeacherModuleId.class)
public class TeacherModule implements Serializable {

    @Id
    @Column(name = "teacher_id")
    private long teachersId;
    @Id
    @Column(name = "module_id")
    private long modulesId;

    @ManyToOne
    @JoinColumn(name = "teacher_id", insertable = false, updatable = false)
    private Teacher teacher;

    @ManyToOne
    @JoinColumn(name = "module_id", insertable = false, updatable = false)
    private Module module;

    public TeacherModule() {
    }

    public TeacherModule(long teachersId, long modulesId, Teacher teacher, Module module) {
        this.teachersId = teachersId;
        this.modulesId = modulesId;
        this.teacher = teacher;
        this.module = module;
    }

    public long getTeachersId() {
        return teachersId;
    }

    public void setTeachersId(long teachersId) {
        this.teachersId = teachersId;
    }

    public long getModulesId() {
        return modulesId;
    }

    public void setModulesId(long modulesId) {
        this.modulesId = modulesId;
    }

    public Teacher getTeacher() {
        return this.teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Module getModule() {
        return this.module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

}
