package ec.edu.unl.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
@Table(name = "__personalfactor")
public class __PersonalFactor {

	public __PersonalFactor(){
		
	}
	
	
	@Id
	@Column(name = "__personalfactor_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	
	@Column(name = "intelligence")
	private int intelligence;

	@Column(name = "sex")
	private int sex;
	
	@Column(name = "age")
	private int age;
	
	@Column(name = "marital_status")
	private int maritalStatus;
	
	@Column(name = "dependents")
	private int dependents;
	
	@Column(name = "academic_training_school")
	private int academicTrainingSchool;
	
	@Column(name = "qualification_access")
	private double qualificationAccess;

	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private Student student;
	

	public __PersonalFactor(long id,
			int intelligence,
			int sex, 
			int age, 
			int maritalStatus, 
			int dependents,
			int academicTrainingSchool, 
			double qualificationAccess) {
		
		this.id = id;
		this.intelligence = intelligence;
		this.sex = sex;
		this.age = age;
		this.maritalStatus = maritalStatus;
		this.dependents = dependents;
		this.academicTrainingSchool = academicTrainingSchool;
		this.qualificationAccess = qualificationAccess;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getIntelligence() {
		return intelligence;
	}

	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}


	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public int getAcademicTrainingSchool() {
		return academicTrainingSchool;
	}

	public void setAcademicTrainingSchool(int academicTrainingSchool) {
		this.academicTrainingSchool = academicTrainingSchool;
	}

	public double getQualificationAccess() {
		return qualificationAccess;
	}

	public void setQualificationAccess(double qualificationAccess) {
		this.qualificationAccess = qualificationAccess;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(int maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public int getDependents() {
		return dependents;
	}

	public void setDependents(int dependents) {
		this.dependents = dependents;
	}
	
	
	


	
	
}
