package ec.edu.unl.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "socialfactor")
public class SocialFactor {

	public SocialFactor() {

	}

	@Id
	@Column(name = "socialfactor_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "social_difference")
	private boolean socialDifference;
	
	@Column(name = "family_environment")
	private int familyEnvironment;
	
	@Column(name = "socioeconomic_context")
	private int socioeconomicContext;
	
	@Column(name = "demographic_variable")
	private int demographicVariable;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private Student student;
	
	
	
	
	
	public SocialFactor(long id, 
			boolean socialDifference,
			int familyEnvironment, 
			int socioeconomicContext,
			int demographicVariable) {
		
		this.id = id;
		this.socialDifference = socialDifference;
		this.familyEnvironment = familyEnvironment;
		this.socioeconomicContext = socioeconomicContext;
		this.demographicVariable = demographicVariable;
	}

	public boolean isSocialDifference() {
		return socialDifference;
	}

	public void setSocialDifference(boolean socialDifference) {
		this.socialDifference = socialDifference;
	}

	public int getFamilyEnvironment() {
		return familyEnvironment;
	}

	public void setFamilyEnvironment(int familyEnvironment) {
		this.familyEnvironment = familyEnvironment;
	}

	public int getSocioeconomicContext() {
		return socioeconomicContext;
	}

	public void setSocioeconomicContext(int socioeconomicContext) {
		this.socioeconomicContext = socioeconomicContext;
	}

	public int getDemographicVariable() {
		return demographicVariable;
	}

	public void setDemographicVariable(int demographicVariable) {
		this.demographicVariable = demographicVariable;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	
	
	
	
	
	
	
}
