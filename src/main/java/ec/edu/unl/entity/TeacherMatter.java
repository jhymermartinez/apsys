package ec.edu.unl.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "teacher_matter")
@IdClass(TeacherMatterId.class)
public class TeacherMatter implements Serializable {

    @Id
    @Column(name = "teacher_id")
    private long teachersId;

    @Id
    @Column(name = "matter_id")
    private long mattersId;

    @ManyToOne
    @JoinColumn(name = "teacher_id", insertable = false, updatable = false)
    private Teacher teacher;

    @ManyToOne
    @JoinColumn(name = "matter_id", insertable = false, updatable = false)
    private Matter matter;

    public TeacherMatter() {
    }

    public TeacherMatter(long teachersId, long mattersId, Teacher teacher, Matter matter) {
        this.teachersId = teachersId;
        this.mattersId = mattersId;
        this.teacher = teacher;
        this.matter = matter;
    }

    
    public Teacher getTeacher() {
        return this.teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Matter getMatter() {
        return this.matter;
    }

    public void setMatter(Matter matter) {
        this.matter = matter;
    }

    public long getTeachersId() {
        return teachersId;
    }

    public void setTeachersId(long teachersId) {
        this.teachersId = teachersId;
    }

    public long getMattersId() {
        return mattersId;
    }

    public void setMattersId(long mattersId) {
        this.mattersId = mattersId;
    }

}
