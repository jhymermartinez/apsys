package ec.edu.unl.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
@Entity
@Table(name = "student")
public class Student implements Serializable {

    public Student() {
    }

    @Id
    @Column(name = "student_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String identification_card;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<Lesson> lessons = new ArrayList<Lesson>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<Evaluation> evaluations = new ArrayList<Evaluation>();
//
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<ClassWork> classWorks = new ArrayList<ClassWork>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<ExtraClassWork> extraClassWorks = new ArrayList<ExtraClassWork>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<Participation> participations = new ArrayList<Participation>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<Laboratory> laboratories = new ArrayList<Laboratory>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<StudentMatter> studentMatters = new ArrayList<StudentMatter>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<StudentCareer> studentCareers = new ArrayList<StudentCareer>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<StudentModule> studentModules = new ArrayList<StudentModule>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
	private List<PersonalFactor> personalFactors = new ArrayList<PersonalFactor>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
	private List<InstitutionalFactor> institutionalFactors = new ArrayList<InstitutionalFactor>();
    
    @Transient
    private String first_name;

    @Transient
    private String last_name;



    public String toString() {

        String[] nombresArray = this.first_name.split(" ");
        String[] apellidosArray = this.last_name.split(" ");

       String nom = nombresArray[0] +" "+ apellidosArray[0];

       return nom.toUpperCase();

    }

    public String generateName() {

        String[] nombresArray = this.first_name.split(" ");
        String[] apellidosArray = this.last_name.split(" ");
        
        String nom = first_name + " " +last_name;
        
        return nom.toUpperCase();
   
    }


    /**
     * ********** getters setters **************
     */
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdentification_card() {
        return identification_card;
    }

    public void setIdentification_card(String identification_card) {
        this.identification_card = identification_card;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public List<Laboratory> getLaboratories() {
        return laboratories;
    }

    public void setLaboratories(List<Laboratory> laboratories) {
        this.laboratories = laboratories;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }
//

    public List<Evaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }
//

    public List<ClassWork> getClassWorks() {
        return classWorks;
    }

    public void setClassWorks(List<ClassWork> classWorks) {
        this.classWorks = classWorks;
    }

    public List<ExtraClassWork> getExtraClassWorks() {
        return extraClassWorks;
    }

    public void setExtraClassWorks(List<ExtraClassWork> extraClassWorks) {
        this.extraClassWorks = extraClassWorks;
    }

    public List<Participation> getParticipations() {
        return participations;
    }

    public void setParticipations(List<Participation> participations) {
        this.participations = participations;
    }

    public List<StudentCareer> getStudentCarrers() {
        return studentCareers;
    }

    public void setStudentCarrers(List<StudentCareer> studentCareers) {
        this.studentCareers = studentCareers;
    }

    public List<StudentMatter> getStudentMatters() {
        return studentMatters;
    }

    public void setStudentMatters(List<StudentMatter> studentMatters) {
        this.studentMatters = studentMatters;
    }

    public List<StudentModule> getStudentModules() {
        return studentModules;
    }

    public void setStudentModules(List<StudentModule> studentModules) {
        this.studentModules = studentModules;
    }

	public List<StudentCareer> getStudentCareers() {
		return studentCareers;
	}

	public void setStudentCareers(List<StudentCareer> studentCareers) {
		this.studentCareers = studentCareers;
	}




}
