package ec.edu.unl.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "matter_module")
@IdClass(MatterModuleId.class)
public class MatterModule implements Serializable {

	@Id
	@Column(name = "matter_id")
	private long mattersId;

	@Id
	@Column(name = "module_id")
	private long modulesId;

	@ManyToOne
	@JoinColumn(name = "matter_id", insertable = false, updatable = false)
	private Matter matter;

	@ManyToOne
	@JoinColumn(name = "module_id", insertable = false, updatable = false)
	private Module module;

	public MatterModule() {
	
	}

	public long getMattersId() {
		return mattersId;
	}

	public void setMattersId(long mattersId) {
		this.mattersId = mattersId;
	}

	public long getModulesId() {
		return modulesId;
	}

	public void setModulesId(long modulesId) {
		this.modulesId = modulesId;
	}

	public Matter getMatter() {
		return matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}
	
	

}
