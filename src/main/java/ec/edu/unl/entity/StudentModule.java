package ec.edu.unl.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
@Entity
@Table(name = "student_module")
@IdClass(StudentModuleId.class)
public class StudentModule implements Serializable {

    @Id
    @Column(name = "student_id")
    private long studentsId;

    @Id
    @Column(name = "module_id")
    private long modulesId;

    @ManyToOne
    @JoinColumn(name = "student_id", insertable = false, updatable = false)
    private Student student;

    @ManyToOne
    @JoinColumn(name = "module_id", insertable = false, updatable = false)
    private Module module;

    public StudentModule() {
    }

    public StudentModule(long studentsId, long modulesId, Student student, Module module) {
        this.studentsId = studentsId;
        this.modulesId = modulesId;
        this.student = student;
        this.module = module;
    }

    

    public Student getStudent() {
        return this.student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Module getModule() {
        return this.module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public long getStudentsId() {
        return studentsId;
    }

    public void setStudentsId(long studentsId) {
        this.studentsId = studentsId;
    }

    public long getModulesId() {
        return modulesId;
    }

    public void setModulesId(long modulesId) {
        this.modulesId = modulesId;
    }

}
