package ec.edu.unl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "_personalfactor")
public class _PersonalFactor {

	public _PersonalFactor(){
		
	}
	
	
	@Id
	@Column(name = "_personalfactor_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "cognitive_competence")
	private int cognitiveCompetence;
	
	@Column(name = "motivation")
	private int motivation;
	
	@Column(name = "cognitive_conditions")
	private int cognitiveConditions;
	
	@Column(name = "academic_self_concept")
	private int academicSelfConcept;
	
	@Column(name = "perceived_self_efficacy")
	private int perceivedSelfEfficacy;
	
	@Column(name = "psychological_welfare")
	private int psychologicalWelfare;
	


	@ManyToOne
    @JoinColumn(name = "offer_id")
	private Offer offer;
	
	@ManyToOne
    @JoinColumn(name = "career_id")
	private Career career;
	

	
	@ManyToOne
    @JoinColumn(name = "student_id")
	private Student student;
	

	public _PersonalFactor(long id, 
			int cognitiveCompetence, 
			int motivation,
			int cognitiveConditions, 
			int academicSelfConcept,
			int perceivedSelfEfficacy, 
			int psychologicalWelfare) {
		
		this.id = id;
		this.cognitiveCompetence = cognitiveCompetence;
		this.motivation = motivation;
		this.cognitiveConditions = cognitiveConditions;
		this.academicSelfConcept = academicSelfConcept;
		this.perceivedSelfEfficacy = perceivedSelfEfficacy;
		this.psychologicalWelfare = psychologicalWelfare;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getCognitiveCompetence() {
		return cognitiveCompetence;
	}

	public void setCognitiveCompetence(int cognitiveCompetence) {
		this.cognitiveCompetence = cognitiveCompetence;
	}

	public int getMotivation() {
		return motivation;
	}

	public void setMotivation(int motivation) {
		this.motivation = motivation;
	}

	public double getCognitiveConditions() {
		return cognitiveConditions;
	}

	public void setCognitiveConditions(int cognitiveConditions) {
		this.cognitiveConditions = cognitiveConditions;
	}

	public int getAcademicSelfConcept() {
		return academicSelfConcept;
	}

	public void setAcademicSelfConcept(int academicSelfConcept) {
		this.academicSelfConcept = academicSelfConcept;
	}

	public int getPerceivedSelfEfficacy() {
		return perceivedSelfEfficacy;
	}

	public void setPerceivedSelfEfficacy(int perceivedSelfEfficacy) {
		this.perceivedSelfEfficacy = perceivedSelfEfficacy;
	}

	public int getPsychologicalWelfare() {
		return psychologicalWelfare;
	}

	public void setPsychologicalWelfare(int psychologicalWelfare) {
		this.psychologicalWelfare = psychologicalWelfare;
	}



	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public Career getCareer() {
		return career;
	}

	public void setCareer(Career career) {
		this.career = career;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
}
