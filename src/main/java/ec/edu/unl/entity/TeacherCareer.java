package ec.edu.unl.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */

@Entity
@Table(name = "teacher_career")
@IdClass(TeacherCareerId.class)
public class TeacherCareer implements Serializable {

	@Id
	@Column(name = "teacher_id")
	private long teachersId;
	@Id
	@Column(name = "career_id")
	private long careersId;

	@ManyToOne
	@JoinColumn(name = "teacher_id", insertable = false, updatable = false)
	private Teacher teacher;

	@ManyToOne
	@JoinColumn(name = "career_id", insertable = false, updatable = false)
	private Career career;

	public TeacherCareer() {

	}

	
	public TeacherCareer(long teachersId, long careersId, Teacher teacher,
			Career career) {
		
		this.teachersId = teachersId;
		this.careersId = careersId;
		this.teacher = teacher;
		this.career = career;
	}


	public long getTeachersId() {
		return teachersId;
	}


	public void setTeachersId(long teachersId) {
		this.teachersId = teachersId;
	}


	public long getCareersId() {
		return careersId;
	}


	public void setCareersId(long careersId) {
		this.careersId = careersId;
	}


	public Teacher getTeacher() {
		return teacher;
	}


	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}


	public Career getCareer() {
		return career;
	}


	public void setCareer(Career career) {
		this.career = career;
	}




}
