package ec.edu.unl.entity;

import javax.persistence.Column;
import java.io.Serializable;

public class StudentMatterId implements Serializable {

    @Column(name = "student_id")
    private long studentsId;

    @Column(name = "matter_id")
    private long mattersId;

    public StudentMatterId() {
    }

    public StudentMatterId(long studentsId, long mattersId) {
        this.studentsId = studentsId;
        this.mattersId = mattersId;
    }

    public long getStudentsId() {
        return this.studentsId;
    }

    public void setStudentsId(long studentsId) {
        this.studentsId = studentsId;
    }

    public long getMattersId() {
        return this.mattersId;
    }

    public void setMattersId(long mattersId) {
        this.mattersId = mattersId;
    }

    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof StudentMatterId)) {
            return false;
        }
        StudentMatterId castOther = (StudentMatterId) other;

        return (this.getStudentsId() == castOther.getStudentsId())
                && (this.getMattersId() == castOther.getMattersId());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (int) this.getStudentsId();
        result = 37 * result + (int) this.getMattersId();
        return result;
    }

}
