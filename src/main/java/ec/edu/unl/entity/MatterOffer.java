package ec.edu.unl.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "matter_offer")
@IdClass(MatterOfferId.class)
public class MatterOffer implements Serializable{

	
	@Id
	@Column(name = "matter_id")
	private long mattersId;

	@Id
	@Column(name = "offer_id")
	private long offersId;

	@ManyToOne
	@JoinColumn(name = "matter_id", insertable = false, updatable = false)
	private Matter matter;

	@ManyToOne
	@JoinColumn(name = "offer_id", insertable = false, updatable = false)
	private Offer offer;
	
	public MatterOffer(){
		
	}

	public long getMattersId() {
		return mattersId;
	}

	public void setMattersId(long mattersId) {
		this.mattersId = mattersId;
	}



	public Matter getMatter() {
		return matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}

	public long getOffersId() {
		return offersId;
	}

	public void setOffersId(long offersId) {
		this.offersId = offersId;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}



	
	
	
}
