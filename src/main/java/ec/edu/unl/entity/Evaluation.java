package ec.edu.unl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
@Entity
@Table(name = "evaluation")
public class Evaluation implements Serializable{

	public Evaluation() {

	}

	@Id
	@Column(name = "evaluation_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "num_evaluation")
	private Integer num_evaluation;

	@Column(name = "qualification")
	private Double qualification;

	@Column(name = "annotation")
	private String annotation;

	@Column(name = "date_evaluation")
	private Date date_evaluation;
	
	@ManyToOne
    @JoinColumn(name = "matter_id")
	private Matter matter;

	@ManyToOne
    @JoinColumn(name = "student_id")
	private Student student;

	@ManyToOne
    @JoinColumn(name = "reference_id")
	private Reference reference;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getNum_evaluation() {
		return num_evaluation;
	}

	public void setNum_evaluation(Integer num_evaluation) {
		this.num_evaluation = num_evaluation;
	}

	public Double getQualification() {
		return qualification;
	}

	public void setQualification(Double qualification) {
		this.qualification = qualification;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public Matter getMatter() {
		return matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Date getDate_evaluation() {
		return date_evaluation;
	}

	public void setDate_evaluation(Date date_evaluation) {
		this.date_evaluation = date_evaluation;
	}

	public Reference getReference() {
		return reference;
	}

	public void setReference(Reference reference) {
		this.reference = reference;
	}

}
