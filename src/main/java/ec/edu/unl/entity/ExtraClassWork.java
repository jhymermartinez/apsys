package ec.edu.unl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.JoinColumn;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
@Entity
@Table(name = "extraclasswork")
public class ExtraClassWork implements Serializable {

    public ExtraClassWork() {

    }

    @Id
    @Column(name = "extraclasswork_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "num_extraclasswork")
    private Integer num_extraClassWork;

    @Column(name = "qualification")
    private Double qualification;

    @Column(name = "annotation")
    private String annotation;
    
	@Column(name = "date_extraclasswork")
	private Date date_extraclasswork;
	

    /**
     * relacion muchos a uno
     */
    @ManyToOne
    @JoinColumn(name = "matter_id")
    private Matter matter;
    
    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;
    
	@ManyToOne
    @JoinColumn(name = "reference_id")
	private Reference reference;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getNum_extraClassWork() {
        return num_extraClassWork;
    }

    public void setNum_extraClassWork(Integer num_extraClassWork) {
        this.num_extraClassWork = num_extraClassWork;
    }

    public Double getQualification() {
        return qualification;
    }

    public void setQualification(Double qualification) {
        this.qualification = qualification;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public Matter getMatter() {
        return matter;
    }

    public void setMatter(Matter matter) {
        this.matter = matter;
    }

	public Date getDate_extraclasswork() {
		return date_extraclasswork;
	}

	public void setDate_extraclasswork(Date date_extraclasswork) {
		this.date_extraclasswork = date_extraclasswork;
	}

	public Reference getReference() {
		return reference;
	}

	public void setReference(Reference reference) {
		this.reference = reference;
	}

}
