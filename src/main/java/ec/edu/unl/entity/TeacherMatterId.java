package ec.edu.unl.entity;

import javax.persistence.Column;
import java.io.Serializable;

public class TeacherMatterId implements Serializable {

    @Column(name = "teacher_id")
    private long teachersId;

    @Column(name = "matter_id")
    private long mattersId;

    public TeacherMatterId() {
    }

    public TeacherMatterId(long teachersId, long mattersId) {
        this.teachersId = teachersId;
        this.mattersId = mattersId;
    }

    public long getTeachersId() {
        return this.teachersId;
    }

    public void setTeachersId(long teachersId) {
        this.teachersId = teachersId;
    }

    public long getMattersId() {
        return this.mattersId;
    }

    public void setMattersId(long mattersId) {
        this.mattersId = mattersId;
    }

    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof TeacherMatterId)) {
            return false;
        }
        TeacherMatterId castOther = (TeacherMatterId) other;

        return (this.getTeachersId() == castOther.getTeachersId())
                && (this.getMattersId() == castOther.getMattersId());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (int) this.getTeachersId();
        result = 37 * result + (int) this.getMattersId();
        return result;
    }

}
