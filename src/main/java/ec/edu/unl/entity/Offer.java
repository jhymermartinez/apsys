package ec.edu.unl.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "offer")
public class Offer implements Serializable{

	public Offer(){
		
	}
	
	@Id
	@Column(name = "offer_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "description")
	private String description;
	
	
	@Column(name = "start_date")
	private Date startDate; 
	
	@Column(name = "end_date")
	private Date endDate;
	

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "offer")
	private List<MatterOffer> matterOffers = new ArrayList<MatterOffer>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "offer")
	private List<Configuration> configurations = new ArrayList<Configuration>();
	

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "offer")
	private List<PersonalFactor> personalFactors = new ArrayList<PersonalFactor>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "offer")
	private List<_PersonalFactor> _personalFactors = new ArrayList<_PersonalFactor>();
	

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "offer")
	private List<InstitutionalFactor> institutionalFactors = new ArrayList<InstitutionalFactor>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "offer")
	private List<_InstitutionalFactor> _institutionalFactors = new ArrayList<_InstitutionalFactor>();
	
	/**
	 * ********** getters setters ***************
	 */
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}



	public List<MatterOffer> getMatterOffers() {
		return matterOffers;
	}

	public void setMatterOffers(List<MatterOffer> matterOffers) {
		this.matterOffers = matterOffers;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<Configuration> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(List<Configuration> configurations) {
		this.configurations = configurations;
	}

	public List<PersonalFactor> getPersonalFactors() {
		return personalFactors;
	}

	public void setPersonalFactors(List<PersonalFactor> personalFactors) {
		this.personalFactors = personalFactors;
	}

	public List<InstitutionalFactor> getInstitutionalFactors() {
		return institutionalFactors;
	}

	public void setInstitutionalFactors(
			List<InstitutionalFactor> institutionalFactors) {
		this.institutionalFactors = institutionalFactors;
	}

	public List<_PersonalFactor> get_personalFactors() {
		return _personalFactors;
	}

	public void set_personalFactors(List<_PersonalFactor> _personalFactors) {
		this._personalFactors = _personalFactors;
	}

	public List<_InstitutionalFactor> get_institutionalFactors() {
		return _institutionalFactors;
	}

	public void set_institutionalFactors(
			List<_InstitutionalFactor> _institutionalFactors) {
		this._institutionalFactors = _institutionalFactors;
	}


	
}
