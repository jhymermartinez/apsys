package ec.edu.unl.entity;

import javax.persistence.Column;
import java.io.Serializable;


public class StudentCareerId implements Serializable {

    @Column(name = "student_id")
    private long studentsId;
    
    @Column(name = "career_id")
    private long careersId;

    public StudentCareerId() {
    }

    public StudentCareerId(long studentsId, long careersId) {
        this.studentsId = studentsId;
        this.careersId = careersId;
    }

   
    public long getStudentsId() {
		return studentsId;
	}

	public void setStudentsId(long studentsId) {
		this.studentsId = studentsId;
	}

	public long getCareersId() {
		return careersId;
	}

	public void setCareersId(long careersId) {
		this.careersId = careersId;
	}

	public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof StudentCareerId)) {
            return false;
        }
        StudentCareerId castOther = (StudentCareerId) other;

        return (this.getStudentsId() == castOther.getStudentsId())
                && (this.getCareersId() == castOther.getCareersId());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (int) this.getStudentsId();
        result = 37 * result + (int) this.getCareersId();
        return result;
    }

}
