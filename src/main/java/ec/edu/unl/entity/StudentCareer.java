package ec.edu.unl.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */

@Entity
@Table(name = "student_career")
@IdClass(StudentCareerId.class)
public class StudentCareer implements Serializable {

    @Id
    @Column(name = "student_id")
    private long studentsId;

    @Id
    @Column(name = "career_id")
    private long careersId;

    @ManyToOne
    @JoinColumn(name = "student_id", insertable = false, updatable = false)
    private Student student;

    @ManyToOne
    @JoinColumn(name = "career_id", insertable = false, updatable = false)
    private Career career;

    public StudentCareer() {
    }

    public StudentCareer(long studentsId, long careersId, Student student, Career career) {
        this.studentsId = studentsId;
        this.careersId = careersId;
        this.student = student;
        this.career = career;
    }

	public long getStudentsId() {
		return studentsId;
	}

	public void setStudentsId(long studentsId) {
		this.studentsId = studentsId;
	}

	public long getCareersId() {
		return careersId;
	}

	public void setCareersId(long careersId) {
		this.careersId = careersId;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Career getCareer() {
		return career;
	}

	public void setCareer(Career career) {
		this.career = career;
	}


}
