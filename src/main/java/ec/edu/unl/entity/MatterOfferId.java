package ec.edu.unl.entity;

import javax.persistence.Column;

import java.io.Serializable;

public class MatterOfferId implements Serializable{

	@Column(name = "matter_id")
	private long mattersId;

	@Column(name = "offer_id")
	private long offersId;
	
	public MatterOfferId(){
		
	}

	public long getMattersId() {
		return mattersId;
	}

	public void setMattersId(long mattersId) {
		this.mattersId = mattersId;
	}



    public long getOffersId() {
		return offersId;
	}

	public void setOffersId(long offersId) {
		this.offersId = offersId;
	}

	public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof MatterOfferId)) {
            return false;
        }
        MatterOfferId castOther = (MatterOfferId) other;

        return (this.getMattersId() == castOther.getMattersId())
                && (this.getOffersId() == castOther.getOffersId());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (int) this.getMattersId();
        result = 37 * result + (int) this.getOffersId();
        return result;
    }	
	
}
