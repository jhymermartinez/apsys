package ec.edu.unl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.JoinColumn;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
@Entity
@Table(name = "classwork")
public class ClassWork implements Serializable {

    public ClassWork() {

    }

    @Id
    @Column(name = "classwork_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "num_classwork")
    private Integer num_classWork;

    @Column(name = "qualification")
    private Double qualification;

    @Column(name = "annotation")
    private String annotation;
    
	@Column(name = "date_classwork")
	private Date date_classwork;

    @ManyToOne
    @JoinColumn(name = "matter_id")
    private Matter matter;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;
    
	@ManyToOne
    @JoinColumn(name = "reference_id")
	private Reference reference;

    /**
     * ****** getters setters ****************
     */
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getNum_classWork() {
        return num_classWork;
    }

    public void setNum_classWork(Integer num_classWork) {
        this.num_classWork = num_classWork;
    }

    public Double getQualification() {
        return qualification;
    }

    public void setQualification(Double qualification) {
        this.qualification = qualification;
    }

    public Matter getMatter() {
        return matter;
    }

    public void setMatter(Matter matter) {
        this.matter = matter;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

	public Date getDate_classwork() {
		return date_classwork;
	}

	public void setDate_classwork(Date date_classwork) {
		this.date_classwork = date_classwork;
	}

	public Reference getReference() {
		return reference;
	}

	public void setReference(Reference reference) {
		this.reference = reference;
	}

}
