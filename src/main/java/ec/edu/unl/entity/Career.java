package ec.edu.unl.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.io.Serializable;

@Entity
@Table(name = "career")
public class Career implements Serializable {

	public Career() {

	}

	@Id
	@Column(name = "career_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name")
	private String name;

	@Column(name = "code_career")
	private Long code_career;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "career")
	private List<StudentCareer> studentCareers = new ArrayList<StudentCareer>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "career")
	private List<TeacherCareer> teacherCareers = new ArrayList<TeacherCareer>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "career")
	private List<Configuration> configurations = new ArrayList<Configuration>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "career")
	private List<_PersonalFactor> _personalFactors = new ArrayList<_PersonalFactor>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "career")
	private List<PersonalFactor> personalFactors = new ArrayList<PersonalFactor>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "career")
	private List<InstitutionalFactor> institutionalFactors = new ArrayList<InstitutionalFactor>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "career")
	private List<_InstitutionalFactor> _institutionalFactors = new ArrayList<_InstitutionalFactor>();
	

	public Career(long id, String name, Long codeCareer,
			List<StudentCareer> studentCareers) {
		this.id = id;
		this.name = name;
		this.code_career = codeCareer;
		this.studentCareers = studentCareers;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCode_career() {
		return code_career;
	}

	public void setCode_career(Long code_career) {
		this.code_career = code_career;
	}

	public List<StudentCareer> getStudentCareers() {
		return studentCareers;
	}

	public void setStudentCareers(List<StudentCareer> studentCareers) {
		this.studentCareers = studentCareers;
	}

	public List<TeacherCareer> getTeacherCareers() {
		return teacherCareers;
	}

	public void setTeacherCareers(List<TeacherCareer> teacherCareers) {
		this.teacherCareers = teacherCareers;
	}

	public List<Configuration> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(List<Configuration> configurations) {
		this.configurations = configurations;
	}

	public List<PersonalFactor> getPersonalFactors() {
		return personalFactors;
	}

	public void setPersonalFactors(List<PersonalFactor> personalFactors) {
		this.personalFactors = personalFactors;
	}

	public List<InstitutionalFactor> getInstitutionalFactors() {
		return institutionalFactors;
	}

	public void setInstitutionalFactors(
			List<InstitutionalFactor> institutionalFactors) {
		this.institutionalFactors = institutionalFactors;
	}

	public List<_PersonalFactor> get_personalFactors() {
		return _personalFactors;
	}

	public void set_personalFactors(List<_PersonalFactor> _personalFactors) {
		this._personalFactors = _personalFactors;
	}

	public List<_InstitutionalFactor> get_institutionalFactors() {
		return _institutionalFactors;
	}

	public void set_institutionalFactors(
			List<_InstitutionalFactor> _institutionalFactors) {
		this._institutionalFactors = _institutionalFactors;
	}


}
