package ec.edu.unl.entity;

import java.io.Serializable;
import javax.persistence.Column;

public class TeacherModuleId implements Serializable {

    @Column(name = "teacher_id")
    private long teachersId;

    @Column(name = "module_id")
    private long modulesId;

    public TeacherModuleId() {
    }

    public TeacherModuleId(long teachersId, long modulesId) {
        this.teachersId = teachersId;
        this.modulesId = modulesId;
    }

    public long getTeachersId() {
        return this.teachersId;
    }

    public void setTeachersId(long teachersId) {
        this.teachersId = teachersId;
    }

    public long getModulesId() {
        return this.modulesId;
    }

    public void setModulesId(long modulesId) {
        this.modulesId = modulesId;
    }

    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof TeacherModuleId)) {
            return false;
        }
        TeacherModuleId castOther = (TeacherModuleId) other;

        return (this.getTeachersId() == castOther.getTeachersId())
                && (this.getModulesId() == castOther.getModulesId());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (int) this.getTeachersId();
        result = 37 * result + (int) this.getModulesId();
        return result;
    }

}
