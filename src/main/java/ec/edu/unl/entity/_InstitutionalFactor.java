package ec.edu.unl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "_institutionalfactor")
public class _InstitutionalFactor {

	
	public _InstitutionalFactor() {
	}
	@Id
	@Column(name = "_institutionalfactor_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "desired_career")
	private boolean desiredCareer;
	
	@Column(name = "institutional_status")
	private int institutionalStatus;
	
	@Column(name = "support_service")
	private boolean supportService;
	
	@Column(name = "student_life")
	private int studentLife;
	
	
	@ManyToOne
    @JoinColumn(name = "offer_id")
	private Offer offer;
	
	@ManyToOne
    @JoinColumn(name = "career_id")
	private Career career;

	
	@ManyToOne
    @JoinColumn(name = "student_id")
	private Student student;

	
	
	public _InstitutionalFactor(long id, 
			boolean desiredCareer,
			int institutionalStatus,
			boolean supportService, 
			int studentLife) {
		
		this.id = id;
		this.desiredCareer = desiredCareer;
		this.institutionalStatus = institutionalStatus;
		this.supportService = supportService;
		this.studentLife = studentLife;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isDesiredCareer() {
		return desiredCareer;
	}

	public void setDesiredCareer(boolean desiredCareer) {
		this.desiredCareer = desiredCareer;
	}


	public int getInstitutionalStatus() {
		return institutionalStatus;
	}

	public void setInstitutionalStatus(int institutionalStatus) {
		this.institutionalStatus = institutionalStatus;
	}

	public boolean isSupportService() {
		return supportService;
	}

	public void setSupportService(boolean supportService) {
		this.supportService = supportService;
	}

	public int getStudentLife() {
		return studentLife;
	}

	public void setStudentLife(int studentLife) {
		this.studentLife = studentLife;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public Career getCareer() {
		return career;
	}

	public void setCareer(Career career) {
		this.career = career;
	}


	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	
	
	
	
	
	
}
