package ec.edu.unl.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "student_matter")
@IdClass(StudentMatterId.class)
public class StudentMatter implements Serializable {

    @Id
    @Column(name = "student_id")
    private long studentsId;

    @Id
    @Column(name = "matter_id")
    private long mattersId;

    @ManyToOne
    @JoinColumn(name = "matter_id", insertable = false, updatable = false)
    private Matter matter;

    @ManyToOne
    @JoinColumn(name = "student_id", insertable = false, updatable = false)
    private Student student;

    public StudentMatter() {
    }

    public StudentMatter(long studentsId, long mattersId, Matter matter, Student student) {
        this.studentsId = studentsId;
        this.mattersId = mattersId;
        this.matter = matter;
        this.student = student;
    }

    

    public Matter getMatter() {
        return this.matter;
    }

    public void setMatter(Matter matter) {
        this.matter = matter;
    }

    public Student getStudent() {
        return this.student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public long getStudentsId() {
        return studentsId;
    }

    public void setStudentsId(long studentsId) {
        this.studentsId = studentsId;
    }

    public long getMattersId() {
        return mattersId;
    }

    public void setMattersId(long mattersId) {
        this.mattersId = mattersId;
    }

}
