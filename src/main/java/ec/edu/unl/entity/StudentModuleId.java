package ec.edu.unl.entity;

import javax.persistence.Column;
import java.io.Serializable;

public class StudentModuleId implements Serializable {

    @Column(name = "student_id")
    private long studentsId;

    @Column(name = "module_id")
    private long modulesId;

    public StudentModuleId() {
    }

    public StudentModuleId(long studentsId, long modulesId) {
        this.studentsId = studentsId;
        this.modulesId = modulesId;
    }

  

    public long getStudentsId() {
        return this.studentsId;
    }

    public void setStudentsId(long studentsId) {
        this.studentsId = studentsId;
    }

    public long getModulesId() {
        return this.modulesId;
    }

    public void setModulesId(long modulesId) {
        this.modulesId = modulesId;
    }

    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof StudentModuleId)) {
            return false;
        }
        StudentModuleId castOther = (StudentModuleId) other;

        return (this.getStudentsId() == castOther.getStudentsId())
                && (this.getModulesId() == castOther.getModulesId());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (int) this.getStudentsId();
        result = 37 * result + (int) this.getModulesId();
        return result;
    }

}
