package ec.edu.unl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "personalfactor")
public class PersonalFactor {

	public PersonalFactor(){
		
	}
	
	
	@Id
	@Column(name = "personalfactor_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	
	@Column(name = "satisfaction_studies")
	private int satisfactionStudies;
	
	@Column(name = "classes_assistance")
	private int classesAssistance;

	@Column(name = "skills")
	private int skills;

	


	
	@ManyToOne
    @JoinColumn(name = "offer_id")
	private Offer offer;
	
	@ManyToOne
    @JoinColumn(name = "career_id")
	private Career career;
	
	@ManyToOne
    @JoinColumn(name = "module_id")
	private Module module;
	
	@ManyToOne
    @JoinColumn(name = "matter_id")
	private Matter matter;
	
	@ManyToOne
    @JoinColumn(name = "student_id")
	private Student student;
	

	public PersonalFactor(long id,
			int satisfactionStudies, 
			int classesAssistance,
			int skills) {
		
		this.id = id;
		this.satisfactionStudies = satisfactionStudies;
		this.classesAssistance = classesAssistance;
		this.skills = skills;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
	public int getSatisfactionStudies() {
		return satisfactionStudies;
	}

	public void setSatisfactionStudies(int satisfactionStudies) {
		this.satisfactionStudies = satisfactionStudies;
	}

	public double getClassesAssistance() {
		return classesAssistance;
	}

	public void setClassesAssistance(int classesAssistance) {
		this.classesAssistance = classesAssistance;
	}

	
	public int getSkills() {
		return skills;
	}

	public void setSkills(int skills) {
		this.skills = skills;
	}

	
	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public Career getCareer() {
		return career;
	}

	public void setCareer(Career career) {
		this.career = career;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public Matter getMatter() {
		return matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

}
