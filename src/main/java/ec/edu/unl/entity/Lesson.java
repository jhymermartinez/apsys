package ec.edu.unl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
@Entity
@Table(name = "lesson")
public class Lesson implements Serializable {

    public Lesson() {

    }

    @Id
    @Column(name = "lesson_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "num_lesson")
    private Integer num_lesson;

    @Column(name = "qualification")
    private Double qualification;

    @Column(name = "annotation")
    private String annotation;

    @Column(name = "date_lesson")
	private Date date_lesson;
    
    @ManyToOne
    @JoinColumn(name = "matter_id")
    private Matter matter;
    
    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;
    
	@ManyToOne
    @JoinColumn(name = "reference_id")
	private Reference reference;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getNum_lesson() {
        return num_lesson;
    }

    public void setNum_lesson(Integer num_lesson) {
        this.num_lesson = num_lesson;
    }

    public Double getQualification() {
        return qualification;
    }

    public void setQualification(Double qualification) {
        this.qualification = qualification;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public Matter getMatter() {
        return matter;
    }

    public void setMatter(Matter matter) {
        this.matter = matter;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

	public Date getDate_lesson() {
		return date_lesson;
	}

	public void setDate_lesson(Date date_lesson) {
		this.date_lesson = date_lesson;
	}

	public Reference getReference() {
		return reference;
	}

	public void setReference(Reference reference) {
		this.reference = reference;
	}

}
