package ec.edu.unl.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "reference")
public class Reference implements Serializable{

	public Reference() {
	
	}
	@Id
	@Column(name = "reference_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "general_reference")
	private String general_reference;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "reference")
	private List<Lesson> lessons = new ArrayList<Lesson>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "reference")
	private List<Evaluation> evaluations = new ArrayList<Evaluation>();
	//
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "reference")
	private List<ClassWork> classWorks = new ArrayList<ClassWork>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "reference")
	private List<ExtraClassWork> extraClassWorks = new ArrayList<ExtraClassWork>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "reference")
	private List<Participation> participations = new ArrayList<Participation>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "reference")
	private List<Laboratory> laboratories = new ArrayList<Laboratory>();

	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getGeneral_reference() {
		return general_reference;
	}

	public void setGeneral_reference(String general_reference) {
		this.general_reference = general_reference;
	}

	public List<Lesson> getLessons() {
		return lessons;
	}

	public void setLessons(List<Lesson> lessons) {
		this.lessons = lessons;
	}

	public List<Evaluation> getEvaluations() {
		return evaluations;
	}

	public void setEvaluations(List<Evaluation> evaluations) {
		this.evaluations = evaluations;
	}

	public List<ClassWork> getClassWorks() {
		return classWorks;
	}

	public void setClassWorks(List<ClassWork> classWorks) {
		this.classWorks = classWorks;
	}

	public List<ExtraClassWork> getExtraClassWorks() {
		return extraClassWorks;
	}

	public void setExtraClassWorks(List<ExtraClassWork> extraClassWorks) {
		this.extraClassWorks = extraClassWorks;
	}

	public List<Participation> getParticipations() {
		return participations;
	}

	public void setParticipations(List<Participation> participations) {
		this.participations = participations;
	}

	public List<Laboratory> getLaboratories() {
		return laboratories;
	}

	public void setLaboratories(List<Laboratory> laboratories) {
		this.laboratories = laboratories;
	}
	
	
	
	
}
