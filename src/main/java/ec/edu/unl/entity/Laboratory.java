package ec.edu.unl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.JoinColumn;

@Entity
@Table(name = "laboratory")
public class Laboratory implements Serializable {

    public Laboratory() {

    }

    @Id
    @Column(name = "laboratory_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "num_laboratory")
    private Integer num_laboratory;

    @Column(name = "qualification")
    private Double qualification;

    @Column(name = "annotation")
    private String annotation;
    
    @Column(name = "date_laboratory")
	private Date date_laboratory;

    @ManyToOne
    @JoinColumn(name = "matter_id")
    private Matter matter;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

	@ManyToOne
    @JoinColumn(name = "reference_id")
	private Reference reference;
	
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getNum_laboratory() {
        return num_laboratory;
    }

    public void setNum_laboratory(Integer num_laboratory) {
        this.num_laboratory = num_laboratory;
    }

    public Double getQualification() {
        return qualification;
    }

    public void setQualification(Double qualification) {
        this.qualification = qualification;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public Matter getMatter() {
        return matter;
    }

    public void setMatter(Matter matter) {
        this.matter = matter;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

	public Date getDate_laboratory() {
		return date_laboratory;
	}

	public void setDate_laboratory(Date date_laboratory) {
		this.date_laboratory = date_laboratory;
	}

	public Reference getReference() {
		return reference;
	}

	public void setReference(Reference reference) {
		this.reference = reference;
	}

}
