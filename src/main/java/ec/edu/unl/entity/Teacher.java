package ec.edu.unl.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.io.Serializable;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
@Entity
@Table(name = "teacher")
public class Teacher implements Serializable {

	public Teacher() {
	}

	@Id
	@Column(name = "teacher_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "identification_card")
	private String identification_card;

	/**
	 * relacion Materia
	 */
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "teacher")
	private List<TeacherMatter> teacherMatters = new ArrayList<TeacherMatter>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "teacher")
	private List<TeacherModule> teacherModules = new ArrayList<TeacherModule>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "teacher")
	private List<TeacherCareer> teacherCareers = new ArrayList<TeacherCareer>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "teacher")
	private List<Configuration> configurations = new ArrayList<Configuration>();
	
	
	
	
	@Transient
	private String first_name;

	@Transient
	private String last_name;



	public Teacher(long id, String identification_card) {
		this.id = id;
		this.identification_card = identification_card;
	}

	// @Transient
	// public void addMatter(Matter matter) {
	// this.matters.add(matter);
	// }
	//
	// public void addModule(Module module) {
	// this.modules.add(module);
	// }
	public String toString() {

		String[] nombresArray = this.first_name.split(" ");
		String[] apellidosArray = this.last_name.split(" ");

		String nombre = nombresArray[0];
		String apellido = apellidosArray[0];

		return nombre + " " + apellido;
	}

	/**
	 * ************* getters setters *****************
	 */
	public String getIdentification_card() {
		return identification_card;
	}

	public void setIdentification_card(String identification_card) {
		this.identification_card = identification_card;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<TeacherMatter> getTeacherMatters() {
		return teacherMatters;
	}

	public void setTeacherMatters(List<TeacherMatter> teacherMatters) {
		this.teacherMatters = teacherMatters;
	}

	public List<TeacherModule> getTeacherModules() {
		return teacherModules;
	}

	public void setTeacherModules(List<TeacherModule> teacherModules) {
		this.teacherModules = teacherModules;
	}

	public List<TeacherCareer> getTeacherCarrers() {
		return teacherCareers;
	}

	public void setTeacherCarrers(List<TeacherCareer> teacherCareers) {
		this.teacherCareers = teacherCareers;
	}

	public List<Configuration> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(List<Configuration> configurations) {
		this.configurations = configurations;
	}



}
