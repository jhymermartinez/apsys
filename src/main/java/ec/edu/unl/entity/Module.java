package ec.edu.unl.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
@Entity
@Table(name = "module")
public class Module implements Serializable {

	public Module() {
	}

	@Id
	@Column(name = "module_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "number")
	private Integer number;

	@Column(name = "parallel")
	private String parallel;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "module")
	private List<StudentModule> studentModules = new ArrayList<StudentModule>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "module")
	private List<TeacherModule> teacherModules = new ArrayList<TeacherModule>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "module")
	private List<MatterModule> matterModules = new ArrayList<MatterModule>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "module")
	private List<Configuration> configurations = new ArrayList<Configuration>();
	

	public Module(long id, Integer number, String parallel) {
		this.id = id;
		this.number = number;
		this.parallel = parallel;
	}

	/**
	 * ********** getters setters ***************
	 */

	public List<TeacherModule> getTeacherModules() {
		return teacherModules;
	}

	public void setTeacherModules(List<TeacherModule> teacherModules) {
		this.teacherModules = teacherModules;
	}

	public long getId() {
		return id;
	}

	public List<MatterModule> getMatterModules() {
		return matterModules;
	}

	public void setMatterModules(List<MatterModule> matterModules) {
		this.matterModules = matterModules;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}



	public String getParallel() {
		return parallel;
	}

	public void setParallel(String parallel) {
		this.parallel = parallel;
	}

	public List<StudentModule> getStudentModules() {
		return studentModules;
	}

	public void setStudentModules(List<StudentModule> studentModules) {
		this.studentModules = studentModules;
	}

	public List<Configuration> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(List<Configuration> configurations) {
		this.configurations = configurations;
	}

}
