package ec.edu.unl.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.io.Serializable;

/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
@Entity
@Table(name = "matter")
public class Matter implements Serializable {

	public Matter() {

	}

	@Id
	@Column(name = "matter_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name")
	private String name;

	@Column(name = "num_hours")
	private double num_hours;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<StudentMatter> studentMatters = new ArrayList<StudentMatter>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<TeacherMatter> teacherMatters = new ArrayList<TeacherMatter>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<Lesson> lessons = new ArrayList<Lesson>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<Evaluation> evaluations = new ArrayList<Evaluation>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<ClassWork> classWorks = new ArrayList<ClassWork>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<ExtraClassWork> extraClassWorks = new ArrayList<ExtraClassWork>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<Participation> participations = new ArrayList<Participation>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<Laboratory> laboratories = new ArrayList<Laboratory>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<MatterModule> matterModules = new ArrayList<MatterModule>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<MatterOffer> matterOffers = new ArrayList<MatterOffer>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<Configuration> configurations = new ArrayList<Configuration>();
	

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<PersonalFactor> personalFactors = new ArrayList<PersonalFactor>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matter")
	private List<InstitutionalFactor> institutionalFactors = new ArrayList<InstitutionalFactor>();
	
	public Matter(long id, String name, double num_hours) {

		this.id = id;
		this.name = name;
		this.num_hours = num_hours;
	}

	/**
	 * ******** getters setters ************
	 */
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getNum_hours() {
		return num_hours;
	}

	public void setNum_hours(double num_hours) {
		this.num_hours = num_hours;
	}

	public List<StudentMatter> getStudentMatters() {
		return studentMatters;
	}

	public void setStudentMatters(List<StudentMatter> studentMatters) {
		this.studentMatters = studentMatters;
	}

	public List<TeacherMatter> getTeacherMatters() {
		return teacherMatters;
	}

	public void setTeacherMatters(List<TeacherMatter> teacherMatters) {
		this.teacherMatters = teacherMatters;
	}

	public List<Lesson> getLessons() {
		return lessons;
	}

	public void setLessons(List<Lesson> lessons) {
		this.lessons = lessons;
	}

	public List<Evaluation> getEvaluations() {
		return evaluations;
	}

	public void setEvaluations(List<Evaluation> evaluations) {
		this.evaluations = evaluations;
	}

	public List<ClassWork> getClassWorks() {
		return classWorks;
	}

	public void setClassWorks(List<ClassWork> classWorks) {
		this.classWorks = classWorks;
	}

	public List<ExtraClassWork> getExtraClassWorks() {
		return extraClassWorks;
	}

	public void setExtraClassWorks(List<ExtraClassWork> extraClassWorks) {
		this.extraClassWorks = extraClassWorks;
	}

	public List<Participation> getParticipations() {
		return participations;
	}

	public void setParticipations(List<Participation> participations) {
		this.participations = participations;
	}

	public List<Laboratory> getLaboratories() {
		return laboratories;
	}

	public void setLaboratories(List<Laboratory> laboratories) {
		this.laboratories = laboratories;
	}

	public List<MatterModule> getMatterModules() {
		return matterModules;
	}

	public void setMatterModules(List<MatterModule> matterModules) {
		this.matterModules = matterModules;
	}

	public List<MatterOffer> getMatterOffers() {
		return matterOffers;
	}

	public void setMatterOffers(List<MatterOffer> matterOffers) {
		this.matterOffers = matterOffers;
	}

	public List<Configuration> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(List<Configuration> configurations) {
		this.configurations = configurations;
	}

	public List<PersonalFactor> getPersonalFactors() {
		return personalFactors;
	}

	public void setPersonalFactors(List<PersonalFactor> personalFactors) {
		this.personalFactors = personalFactors;
	}

	public List<InstitutionalFactor> getInstitutionalFactors() {
		return institutionalFactors;
	}

	public void setInstitutionalFactors(
			List<InstitutionalFactor> institutionalFactors) {
		this.institutionalFactors = institutionalFactors;
	}



}
