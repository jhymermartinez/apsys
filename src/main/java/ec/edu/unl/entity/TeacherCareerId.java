package ec.edu.unl.entity;

import java.io.Serializable;
import javax.persistence.Column;
/**
 *
 * @author Jhymer Martínez <jamartinezg@unl.edu.ec>
 */
public class TeacherCareerId implements Serializable{

    @Column(name = "teacher_id")
    private long teachersId;

    @Column(name = "career_id")
    private long careersId;

    public TeacherCareerId() {

    }

    public TeacherCareerId(long teachersId, long careersId) {
		this.teachersId = teachersId;
		this.careersId = careersId;
	}

	public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof TeacherCareerId)) {
            return false;
        }
        TeacherCareerId castOther = (TeacherCareerId) other;

        return (this.getTeachersId() == castOther.getTeachersId())
                && (this.getCareersId() == castOther.getCareersId());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (int) this.getTeachersId();
        result = 37 * result + (int) this.getCareersId();
        return result;
    }

	public long getTeachersId() {
		return teachersId;
	}

	public void setTeachersId(long teachersId) {
		this.teachersId = teachersId;
	}

	public long getCareersId() {
		return careersId;
	}

	public void setCareersId(long careersId) {
		this.careersId = careersId;
	}


}
