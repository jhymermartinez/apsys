package ec.edu.unl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "institutionalfactor")
public class InstitutionalFactor {

	
	public InstitutionalFactor() {
	}
	@Id
	@Column(name = "institutionalfactor_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "complexity_matter")
	private int complexityMatter;
	
	
	@Column(name = "relationship_student_teacher")
	private int relationshipStudentTeacher;
	
	
	@ManyToOne
    @JoinColumn(name = "offer_id")
	private Offer offer;
	
	@ManyToOne
    @JoinColumn(name = "career_id")
	private Career career;
	
	@ManyToOne
    @JoinColumn(name = "module_id")
	private Module module;
	
	@ManyToOne
    @JoinColumn(name = "matter_id")
	private Matter matter;
	
	@ManyToOne
    @JoinColumn(name = "student_id")
	private Student student;

	
	
	public InstitutionalFactor(long id,
			int complexityMatter, 
			int relationshipStudentTeacher) {
		
		this.id = id;
		this.complexityMatter = complexityMatter;
		this.relationshipStudentTeacher = relationshipStudentTeacher;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	public int getRelationshipStudentTeacher() {
		return relationshipStudentTeacher;
	}

	public void setRelationshipStudentTeacher(int relationshipStudentTeacher) {
		this.relationshipStudentTeacher = relationshipStudentTeacher;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public Career getCareer() {
		return career;
	}

	public void setCareer(Career career) {
		this.career = career;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public Matter getMatter() {
		return matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public int getComplexityMatter() {
		return complexityMatter;
	}

	public void setComplexityMatter(int complexityMatter) {
		this.complexityMatter = complexityMatter;
	}
	
	
	
	
	
	
	
}
