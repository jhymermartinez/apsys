
package ec.edu.unl.ws.sgaws.wsestadistica.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.ObjectFactory;


@WebService(name = "SGAWebServicesEstadistica_PortType", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface SGAWebServicesEstadisticaPortType {


    /**
     * 
     *         Este metodo recibe como parametro de entrada el id de la Oferta Academica y las siglas del Area.
     * 	Retorna el datos de la oferta academica, datos del areas y el numero de estudiantes aprobados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descripcion de la oferta academica, el nombre del area y el numero de estudiantes aprobados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param siglas
     * @param idOferta
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_naprobados_area", action = "sgaws_naprobados_area")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_naprobados_area", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosArea")
    @ResponseWrapper(localName = "sgaws_naprobados_areaResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosAreaResponse")
    public String sgawsNaprobadosArea(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta,
        @WebParam(name = "siglas", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String siglas);

    /**
     * 
     *         Este metodo recibe como parametro de entrada el id de la Oferta Academica y el id de la Carrera.
     * 	Retorna el datos de la oferta academica, datos de la carreras y el numero de estudiantes aprobados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descripcion de la oferta academica, el nombre de la carrera y el numero de estudiantes aprobados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param idOferta
     * @param idCarrera
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_naprobados_carrera", action = "sgaws_naprobados_carrera")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_naprobados_carrera", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosCarrera")
    @ResponseWrapper(localName = "sgaws_naprobados_carreraResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosCarreraResponse")
    public String sgawsNaprobadosCarrera(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta,
        @WebParam(name = "id_carrera", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idCarrera);

    /**
     * 
     *         Este metodo recibe como parametro de entrada el id de la Oferta Academica y el id del Modulo.
     * 	Retorna el datos de la oferta academica, datos de la carrera, datos del modulo y el numero de estudiantes aprobados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descripcion de la oferta academica, el nombre de la carrera, el numero del modulo y el numero de estudiantes aprobados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param idModulo
     * @param idOferta
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_naprobados_modulo", action = "sgaws_naprobados_modulo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_naprobados_modulo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosModulo")
    @ResponseWrapper(localName = "sgaws_naprobados_moduloResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosModuloResponse")
    public String sgawsNaprobadosModulo(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta,
        @WebParam(name = "id_modulo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idModulo);

    /**
     * 
     *         Este metodo recibe como parametro de entrada el id de la Oferta Academica.
     * 	Retorna el datos de la oferta academica, y el numero de estudiantes aprobados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	El id de la oferta academica, la descripcion de la oferta academica y el numero de estudiantes aprobados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param idOferta
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_naprobados_oferta", action = "sgaws_naprobados_oferta")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_naprobados_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosOferta")
    @ResponseWrapper(localName = "sgaws_naprobados_ofertaResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosOfertaResponse")
    public String sgawsNaprobadosOferta(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta);

    /**
     * 
     *         Este metodo recibe como parametro el id del paralelo.
     * 	Retorna el datos de la oferta academica, datos de la carrera, datos del modulo, datos del paralelo y el numero de estudiantes aprobados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descripcion de la oferta academica, el nombre de la carrera, el numero del modulo, nombre del paralelo y el numero de estudiantes aprobados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param idParalelo
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_naprobados_paralelo", action = "sgaws_naprobados_paralelo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_naprobados_paralelo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosParalelo")
    @ResponseWrapper(localName = "sgaws_naprobados_paraleloResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosParaleloResponse")
    public String sgawsNaprobadosParalelo(
        @WebParam(name = "id_paralelo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idParalelo);

    /**
     * 
     *         @miltonlab
     *         Este metodo recibe como parametro la categoria laborar de los docentes y
     *         el id de la oferta en donde se va a contar
     * 	Retorna el id de la oferta , la descripcion de la oferta y el numero de docentes
     *         en pares clave:valor
     * 	La salida de este metodo se encuentra formateada con JSON
     *         
     * 
     * @param tipoDocente
     * @param idOferta
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_ndocentes_oferta", action = "sgaws_ndocentes_oferta")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_ndocentes_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNdocentesOferta")
    @ResponseWrapper(localName = "sgaws_ndocentes_ofertaResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNdocentesOfertaResponse")
    public String sgawsNdocentesOferta(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta,
        @WebParam(name = "tipo_docente", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String tipoDocente);

    /**
     * 
     * 	Este metodo recibe como parametro de entrada el id de la Oferta Academica y las siglas del Area.
     * 	Retorna el datos de la oferta academica, datos del areas y el numero de estudiantes matriculados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descripcion de la oferta academica, el nombre del area y el numero de estudiantes matriculados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     * 	
     * 
     * @param siglas
     * @param idOferta
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_nmatriculados_area", action = "sgaws_nmatriculados_area")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_nmatriculados_area", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosArea")
    @ResponseWrapper(localName = "sgaws_nmatriculados_areaResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosAreaResponse")
    public String sgawsNmatriculadosArea(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta,
        @WebParam(name = "siglas", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String siglas);

    /**
     * 
     * 	Este metodo recibe como parametro de entrada el id de la Oferta Academica y el id de la Carrera.
     * 	Retorna el datos de la oferta academica, datos de la carreras y el numero de estudiantes matriculados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descripcion de la oferta academica, el nombre de la carrera y el numero de estudiantes matriculados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     * 	
     * 
     * @param idOferta
     * @param idCarrera
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_nmatriculados_carrera", action = "sgaws_nmatriculados_carrera")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_nmatriculados_carrera", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosCarrera")
    @ResponseWrapper(localName = "sgaws_nmatriculados_carreraResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosCarreraResponse")
    public String sgawsNmatriculadosCarrera(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta,
        @WebParam(name = "id_carrera", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idCarrera);

    /**
     * 
     * 	Este metodo recibe como parametro de entrada el id de la Oferta Academica y el id del Modulo.
     * 	Retorna el datos de la oferta academica, datos de la carrera, datos del modulo y el numero de estudiantes matriculados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descripcion de la oferta academica, el nombre de la carrera, el numero del modulo y el numero de estudiantes matriculados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     * 	
     * 
     * @param idModulo
     * @param idOferta
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_nmatriculados_modulo", action = "sgaws_nmatriculados_modulo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_nmatriculados_modulo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosModulo")
    @ResponseWrapper(localName = "sgaws_nmatriculados_moduloResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosModuloResponse")
    public String sgawsNmatriculadosModulo(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta,
        @WebParam(name = "id_modulo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idModulo);

    /**
     * 
     * 	Este metodo recibe como parametro de entrada el id de la Oferta Academica.
     * 	Retorna el datos de la oferta academica, y el numero de estudiantes matriculados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	El id de la oferta academica, la descripcion de la oferta academica y el numero de estudiantes matriculados
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     * 	
     * 
     * @param idOferta
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_nmatriculados_oferta", action = "sgaws_nmatriculados_oferta")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_nmatriculados_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosOferta")
    @ResponseWrapper(localName = "sgaws_nmatriculados_ofertaResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosOfertaResponse")
    public String sgawsNmatriculadosOferta(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta);

    /**
     * 
     * 	Este metodo recibe como parametro el id del paralelo.
     * 	Retorna el datos de la oferta academica, datos de la carrera, datos del modulo, datos del paralelo y el numero de estudiantes matriculados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descripcion de la oferta academica, el nombre de la carrera, el numero del modulo, nombre del paralelo y el numero de estudiantes matriculados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     * 	
     * 
     * @param idParalelo
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_nmatriculados_paralelo", action = "sgaws_nmatriculados_paralelo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_nmatriculados_paralelo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosParalelo")
    @ResponseWrapper(localName = "sgaws_nmatriculados_paraleloResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosParaleloResponse")
    public String sgawsNmatriculadosParalelo(
        @WebParam(name = "id_paralelo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idParalelo);

    /**
     * 
     *         Este metodo recibe como parametro de entrada el id de la Oferta Academica y las siglas del Area.
     * 	Retorna el datos de la oferta academica, datos del areas y el numero de estudiantes reprobados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descripcion de la oferta academica, el nombre del area y el numero de estudiantes reprobados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param siglas
     * @param idOferta
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_nreprobados_area", action = "sgaws_nreprobados_area")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_nreprobados_area", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosArea")
    @ResponseWrapper(localName = "sgaws_nreprobados_areaResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosAreaResponse")
    public String sgawsNreprobadosArea(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta,
        @WebParam(name = "siglas", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String siglas);

    /**
     * 
     *         Este metodo recibe como parametro de entrada el id de la Oferta Academica y el id de la Carrera.
     * 	Retorna el datos de la oferta academica, datos de la carreras y el numero de estudiantes aprobados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descripcion de la oferta academica, el nombre de la carrera y el numero de estudiantes aprobados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param idOferta
     * @param idCarrera
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_nreprobados_carrera", action = "sgaws_nreprobados_carrera")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_nreprobados_carrera", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosCarrera")
    @ResponseWrapper(localName = "sgaws_nreprobados_carreraResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosCarreraResponse")
    public String sgawsNreprobadosCarrera(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta,
        @WebParam(name = "id_carrera", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idCarrera);

    /**
     * 
     *             Este metodo recibe como parametro de entrada el id de la Oferta Academica y el id del Modulo.
     * 	Retorna el datos de la oferta academica, datos de la carrera, datos del modulo y el numero de estudiantes reprobados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descripcion de la oferta academica, el nombre de la carrera, el numero del modulo y el numero de estudiantes reprobados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param idModulo
     * @param idOferta
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_nreprobados_modulo", action = "sgaws_nreprobados_modulo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_nreprobados_modulo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosModulo")
    @ResponseWrapper(localName = "sgaws_nreprobados_moduloResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosModuloResponse")
    public String sgawsNreprobadosModulo(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta,
        @WebParam(name = "id_modulo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idModulo);

    /**
     * 
     *         Este metodo recibe como parametro de entrada el id de la Oferta Academica.
     * 	Retorna el datos de la oferta academica, y el numero de estudiantes reprobados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	El id de la oferta academica, la descripcion de la oferta academica y el numero de estudiantes reprobados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param idOferta
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_nreprobados_oferta", action = "sgaws_nreprobados_oferta")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_nreprobados_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosOferta")
    @ResponseWrapper(localName = "sgaws_nreprobados_ofertaResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosOfertaResponse")
    public String sgawsNreprobadosOferta(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idOferta);

    /**
     * 
     *         Este metodo recibe como parametro el id del paralelo.
     * 	Retorna el datos de la oferta academica, datos de la carrera, datos del modulo, datos del paralelo y el numero de estudiantes reprobados de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descripcion de la oferta academica, el nombre de la carrera, el numero del modulo, nombre del paralelo y el numero de estudiantes reprobados.
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param idParalelo
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_nreprobados_paralelo", action = "sgaws_nreprobados_paralelo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
    @RequestWrapper(localName = "sgaws_nreprobados_paralelo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosParalelo")
    @ResponseWrapper(localName = "sgaws_nreprobados_paraleloResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types", className = "ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosParaleloResponse")
    public String sgawsNreprobadosParalelo(
        @WebParam(name = "id_paralelo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsestadistica/soap/types")
        String idParalelo);

}
