
package ec.edu.unl.ws.sgaws.wsvalidacion.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import ec.edu.unl.ws.sgaws.wsvalidacion.soap.types.ObjectFactory;


@WebService(
		name = "SGAWebServicesValidacion_PortType",
		targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/")

@XmlSeeAlso({
    ObjectFactory.class
})
public interface SGAWebServicesValidacionPortType {


    /**
     * 
     * 	Este metodo recibe como parametros de entrada la cedula y la clave del docente.
     * 	Retorna true o false dependiendo de los si la informacion enviada pertenece a un docente.
     * 	
     * 
     * @param clave
     * @param cedula
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_validar_docente", action = "sgaws_validar_docente")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types")
    @RequestWrapper(localName = "sgaws_validar_docente", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types", className = "ec.edu.unl.ws.sgaws.wsvalidacion.soap.types.SgawsValidarDocente")
    @ResponseWrapper(localName = "sgaws_validar_docenteResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types", className = "ec.edu.unl.ws.sgaws.wsvalidacion.soap.types.SgawsValidarDocenteResponse")
    public String sgawsValidarDocente(
        @WebParam(name = "cedula", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types")
        String cedula,
        @WebParam(name = "clave", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types")
        String clave);

    /**
     * 
     *         Este metodo recibe como parametros de entrada la cedula y la clave del estudiante.
     * 	Retorna true o false dependiendo de los si la informacion enviada pertenece a un estudiante.
     * 	
     * 
     * @param clave
     * @param cedula
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_validar_estudiante", action = "sgaws_validar_estudiante")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types")
    @RequestWrapper(localName = "sgaws_validar_estudiante", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types", className = "ec.edu.unl.ws.sgaws.wsvalidacion.soap.types.SgawsValidarEstudiante")
    
    @ResponseWrapper(localName = "sgaws_validar_estudianteResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types", className = "ec.edu.unl.ws.sgaws.wsvalidacion.soap.types.SgawsValidarEstudianteResponse")
    public String sgawsValidarEstudiante(
        @WebParam(name = "cedula", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types")
        String cedula,
        @WebParam(name = "clave", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types")
        String clave);

    /**
     * 
     *             Metodo de comprobacion para servicios
     * 	
     * 
     * @param b
     * @param a
     * @return
     *     returns java.lang.String
     */
    @WebMethod(action = "suma")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types")
    @RequestWrapper(localName = "suma", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types", className = "ec.edu.unl.ws.sgaws.wsvalidacion.soap.types.Suma")
    @ResponseWrapper(localName = "sumaResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types", className = "ec.edu.unl.ws.sgaws.wsvalidacion.soap.types.SumaResponse")
    public String suma(
        @WebParam(name = "a", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types")
        String a,
        @WebParam(name = "b", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/types")
        String b);

}
