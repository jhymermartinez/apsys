
package ec.edu.unl.ws.sgaws.wspersonal.soap;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

import org.springframework.beans.factory.annotation.Autowired;

import ec.edu.unl.operations.Operations;
import ec.edu.unl.operations.impl.OperationsImpl;
import ec.edu.unl.ws.sgaws.wspersonal.soap.SGAWebServicesPersonalPortType;


@WebServiceClient
public class SGAWebServicesPersonal extends Service{
	
    private final static URL SGAWEBSERVICESPERSONAL_WSDL_LOCATION;
    private final static WebServiceException SGAWEBSERVICESPERSONAL_EXCEPTION;
    private final static QName SGAWEBSERVICESPERSONAL_QNAME = new QName("http://ws.unl.edu.ec/sgaws/wspersonal/soap/", "SGAWebServicesPersonal");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
        	OperationsImpl op = new OperationsImpl();
        	String dir = op.generateRoute(op.getDirectory())+"personal.wsdl";	      	
        	url = new URL("file:/"+dir);
            
            //url = new URL("file:/C:/En_Desarrollo/Tesis/APSYS/src/main/resources/wsdl/personal.wsdl");
            
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SGAWEBSERVICESPERSONAL_WSDL_LOCATION = url;
        SGAWEBSERVICESPERSONAL_EXCEPTION = e;
    }

    public SGAWebServicesPersonal() {
        super(__getWsdlLocation(), SGAWEBSERVICESPERSONAL_QNAME);
    }

    public SGAWebServicesPersonal(WebServiceFeature... features) {
        super(__getWsdlLocation(), SGAWEBSERVICESPERSONAL_QNAME, features);
    }

    public SGAWebServicesPersonal(URL wsdlLocation) {
        super(wsdlLocation, SGAWEBSERVICESPERSONAL_QNAME);
    }

    public SGAWebServicesPersonal(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SGAWEBSERVICESPERSONAL_QNAME, features);
    }

    public SGAWebServicesPersonal(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SGAWebServicesPersonal(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns SGAWebServicesPersonalPortType
     */
    @WebEndpoint(name = "SGAWebServicesPersonal_PortType")
    public SGAWebServicesPersonalPortType getSGAWebServicesPersonalPortType() {
        return super.getPort(new QName("http://ws.unl.edu.ec/sgaws/wspersonal/soap/", "SGAWebServicesPersonal_PortType"), SGAWebServicesPersonalPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns SGAWebServicesPersonalPortType
     */
    @WebEndpoint(name = "SGAWebServicesPersonal_PortType")
    public SGAWebServicesPersonalPortType getSGAWebServicesPersonalPortType(WebServiceFeature... features) {
        return super.getPort(new QName("http://ws.unl.edu.ec/sgaws/wspersonal/soap/", "SGAWebServicesPersonal_PortType"), SGAWebServicesPersonalPortType.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SGAWEBSERVICESPERSONAL_EXCEPTION!= null) {
            throw SGAWEBSERVICESPERSONAL_EXCEPTION;
        }
        return SGAWEBSERVICESPERSONAL_WSDL_LOCATION;
    }

}
