
package ec.edu.unl.ws.sgaws.wsestadistica.soap.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id_oferta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipo_docente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idOferta",
    "tipoDocente"
})
@XmlRootElement(name = "sgaws_ndocentes_oferta")
public class SgawsNdocentesOferta {

    @XmlElement(name = "id_oferta", required = true)
    protected String idOferta;
    @XmlElement(name = "tipo_docente", required = true)
    protected String tipoDocente;

    /**
     * Gets the value of the idOferta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdOferta() {
        return idOferta;
    }

    /**
     * Sets the value of the idOferta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdOferta(String value) {
        this.idOferta = value;
    }

    /**
     * Gets the value of the tipoDocente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocente() {
        return tipoDocente;
    }

    /**
     * Sets the value of the tipoDocente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocente(String value) {
        this.tipoDocente = value;
    }

}
