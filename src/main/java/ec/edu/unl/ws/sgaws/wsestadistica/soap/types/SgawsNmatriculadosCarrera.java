
package ec.edu.unl.ws.sgaws.wsestadistica.soap.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id_oferta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="id_carrera" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idOferta",
    "idCarrera"
})
@XmlRootElement(name = "sgaws_nmatriculados_carrera")
public class SgawsNmatriculadosCarrera {

    @XmlElement(name = "id_oferta", required = true)
    protected String idOferta;
    @XmlElement(name = "id_carrera", required = true)
    protected String idCarrera;

    /**
     * Gets the value of the idOferta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdOferta() {
        return idOferta;
    }

    /**
     * Sets the value of the idOferta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdOferta(String value) {
        this.idOferta = value;
    }

    /**
     * Gets the value of the idCarrera property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCarrera() {
        return idCarrera;
    }

    /**
     * Sets the value of the idCarrera property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCarrera(String value) {
        this.idCarrera = value;
    }

}
