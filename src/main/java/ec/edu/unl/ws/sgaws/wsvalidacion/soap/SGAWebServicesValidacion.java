package ec.edu.unl.ws.sgaws.wsvalidacion.soap;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.handler.MessageContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.sun.xml.bind.v2.runtime.BinderImpl;

import ec.edu.unl.operations.Operations;
import ec.edu.unl.operations.impl.OperationsImpl;


@WebServiceClient
public class SGAWebServicesValidacion extends Service {

	
	private final static URL SGAWEBSERVICESVALIDACION_WSDL_LOCATION;
	private final static WebServiceException SGAWEBSERVICESVALIDACION_EXCEPTION;
	private final static QName SGAWEBSERVICESVALIDACION_QNAME = new QName(
			"http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/",
			"SGAWebServicesValidacion");
	static {
		URL url = null;
		WebServiceException e = null;
		try {
        	OperationsImpl op = new OperationsImpl();
        	String dir = op.generateRoute(op.getDirectory())+"validacion.wsdl";	      	
        	url = new URL("file:/"+dir);
			
			//url = new URL("file:/C:/En_Desarrollo/Tesis/APSYS/src/main/resources/wsdl/validacion.wsdl");

		} catch (MalformedURLException ex) {
			e = new WebServiceException(ex);
		}
		SGAWEBSERVICESVALIDACION_WSDL_LOCATION = url;
		SGAWEBSERVICESVALIDACION_EXCEPTION = e;
	}

	public SGAWebServicesValidacion() {
		super(__getWsdlLocation(), SGAWEBSERVICESVALIDACION_QNAME);
	}

	public SGAWebServicesValidacion(WebServiceFeature... features) {
		super(__getWsdlLocation(), SGAWEBSERVICESVALIDACION_QNAME, features);
	}

	public SGAWebServicesValidacion(URL wsdlLocation) {
		super(wsdlLocation, SGAWEBSERVICESVALIDACION_QNAME);
	}

	public SGAWebServicesValidacion(URL wsdlLocation,
			WebServiceFeature... features) {
		super(wsdlLocation, SGAWEBSERVICESVALIDACION_QNAME, features);
	}

	public SGAWebServicesValidacion(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public SGAWebServicesValidacion(URL wsdlLocation, QName serviceName,
			WebServiceFeature... features) {
		super(wsdlLocation, serviceName, features);
	}

	/**
	 * 
	 * @return returns SGAWebServicesValidacionPortType
	 */
	@WebEndpoint(name = "SGAWebServicesValidacion_PortType")
	public SGAWebServicesValidacionPortType getSGAWebServicesValidacionPortType() {
		return super.getPort(new QName(
				"http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/",
				"SGAWebServicesValidacion_PortType"),
				SGAWebServicesValidacionPortType.class);
	}

	/**
	 * 
	 * @param features
	 *            A list of {@link javax.xml.ws.WebServiceFeature} to configure
	 *            on the proxy. Supported features not in the
	 *            <code>features</code> parameter will have their default
	 *            values.
	 * @return returns SGAWebServicesValidacionPortType
	 */
	@WebEndpoint(name = "SGAWebServicesValidacion_PortType")
	public SGAWebServicesValidacionPortType getSGAWebServicesValidacionPortType(
			WebServiceFeature... features) {
		return super.getPort(new QName(
				"http://ws.unl.edu.ec/sgaws/wsvalidacion/soap/",
				"SGAWebServicesValidacion_PortType"),
				SGAWebServicesValidacionPortType.class, features);
	}

	private static URL __getWsdlLocation() {
		if (SGAWEBSERVICESVALIDACION_EXCEPTION != null) {
			throw SGAWEBSERVICESVALIDACION_EXCEPTION;
		}
		return SGAWEBSERVICESVALIDACION_WSDL_LOCATION;
	}

}
