
package ec.edu.unl.ws.sgaws.wsestadistica.soap;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

import ec.edu.unl.operations.impl.OperationsImpl;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.SGAWebServicesEstadisticaPortType;



@WebServiceClient
public class SGAWebServicesEstadistica
    extends Service
{

    private final static URL SGAWEBSERVICESESTADISTICA_WSDL_LOCATION;
    private final static WebServiceException SGAWEBSERVICESESTADISTICA_EXCEPTION;
    private final static QName SGAWEBSERVICESESTADISTICA_QNAME = new QName("http://ws.unl.edu.ec/sgaws/wsestadistica/soap/", "SGAWebServicesEstadistica");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
        	
        	OperationsImpl op = new OperationsImpl();
        	String dir = op.generateRoute(op.getDirectory())+"estadistica.wsdl";	      	
        	url = new URL("file:/"+dir);
        	
            //url = new URL("file:/C:/En_Desarrollo/Tesis/APSYS/src/main/resources/wsdl/estadistica.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SGAWEBSERVICESESTADISTICA_WSDL_LOCATION = url;
        SGAWEBSERVICESESTADISTICA_EXCEPTION = e;
    }

    public SGAWebServicesEstadistica() {
        super(__getWsdlLocation(), SGAWEBSERVICESESTADISTICA_QNAME);
    }

    public SGAWebServicesEstadistica(WebServiceFeature... features) {
        super(__getWsdlLocation(), SGAWEBSERVICESESTADISTICA_QNAME, features);
    }

    public SGAWebServicesEstadistica(URL wsdlLocation) {
        super(wsdlLocation, SGAWEBSERVICESESTADISTICA_QNAME);
    }

    public SGAWebServicesEstadistica(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SGAWEBSERVICESESTADISTICA_QNAME, features);
    }

    public SGAWebServicesEstadistica(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SGAWebServicesEstadistica(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns SGAWebServicesEstadisticaPortType
     */
    @WebEndpoint(name = "SGAWebServicesEstadistica_PortType")
    public SGAWebServicesEstadisticaPortType getSGAWebServicesEstadisticaPortType() {
        return super.getPort(new QName("http://ws.unl.edu.ec/sgaws/wsestadistica/soap/", "SGAWebServicesEstadistica_PortType"), SGAWebServicesEstadisticaPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns SGAWebServicesEstadisticaPortType
     */
    @WebEndpoint(name = "SGAWebServicesEstadistica_PortType")
    public SGAWebServicesEstadisticaPortType getSGAWebServicesEstadisticaPortType(WebServiceFeature... features) {
        return super.getPort(new QName("http://ws.unl.edu.ec/sgaws/wsestadistica/soap/", "SGAWebServicesEstadistica_PortType"), SGAWebServicesEstadisticaPortType.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SGAWEBSERVICESESTADISTICA_EXCEPTION!= null) {
            throw SGAWEBSERVICESESTADISTICA_EXCEPTION;
        }
        return SGAWEBSERVICESESTADISTICA_WSDL_LOCATION;
    }

}
