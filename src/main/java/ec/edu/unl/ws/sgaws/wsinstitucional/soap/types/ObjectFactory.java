
package ec.edu.unl.ws.sgaws.wsinstitucional.soap.types;

import javax.xml.bind.annotation.XmlRegistry;

import ec.edu.unl.ws.sgaws.wsinstitucional.soap.types.SgawsCarrerasArea;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.types.SgawsCarrerasAreaResponse;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.types.SgawsDatosArea;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.types.SgawsDatosAreaResponse;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.types.SgawsDatosCarreras;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.types.SgawsDatosCarrerasResponse;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.types.SgawsListaAreas;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.types.SgawsListaAreasResponse;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.types.SgawsModulosCarrera;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.types.SgawsModulosCarreraResponse;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.types.SgawsParalelosCarrera;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.types.SgawsParalelosCarreraResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.unl.ws.sgaws.wsinstitucional.soap.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.unl.ws.sgaws.wsinstitucional.soap.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SgawsCarrerasArea }
     * 
     */
    public SgawsCarrerasArea createSgawsCarrerasArea() {
        return new SgawsCarrerasArea();
    }

    /**
     * Create an instance of {@link SgawsListaAreas }
     * 
     */
    public SgawsListaAreas createSgawsListaAreas() {
        return new SgawsListaAreas();
    }

    /**
     * Create an instance of {@link SgawsDatosCarrerasResponse }
     * 
     */
    public SgawsDatosCarrerasResponse createSgawsDatosCarrerasResponse() {
        return new SgawsDatosCarrerasResponse();
    }

    /**
     * Create an instance of {@link SgawsModulosCarreraResponse }
     * 
     */
    public SgawsModulosCarreraResponse createSgawsModulosCarreraResponse() {
        return new SgawsModulosCarreraResponse();
    }

    /**
     * Create an instance of {@link SgawsListaAreasResponse }
     * 
     */
    public SgawsListaAreasResponse createSgawsListaAreasResponse() {
        return new SgawsListaAreasResponse();
    }

    /**
     * Create an instance of {@link SgawsDatosArea }
     * 
     */
    public SgawsDatosArea createSgawsDatosArea() {
        return new SgawsDatosArea();
    }

    /**
     * Create an instance of {@link SgawsModulosCarrera }
     * 
     */
    public SgawsModulosCarrera createSgawsModulosCarrera() {
        return new SgawsModulosCarrera();
    }

    /**
     * Create an instance of {@link SgawsParalelosCarreraResponse }
     * 
     */
    public SgawsParalelosCarreraResponse createSgawsParalelosCarreraResponse() {
        return new SgawsParalelosCarreraResponse();
    }

    /**
     * Create an instance of {@link SgawsDatosCarreras }
     * 
     */
    public SgawsDatosCarreras createSgawsDatosCarreras() {
        return new SgawsDatosCarreras();
    }

    /**
     * Create an instance of {@link SgawsDatosAreaResponse }
     * 
     */
    public SgawsDatosAreaResponse createSgawsDatosAreaResponse() {
        return new SgawsDatosAreaResponse();
    }

    /**
     * Create an instance of {@link SgawsCarrerasAreaResponse }
     * 
     */
    public SgawsCarrerasAreaResponse createSgawsCarrerasAreaResponse() {
        return new SgawsCarrerasAreaResponse();
    }

    /**
     * Create an instance of {@link SgawsParalelosCarrera }
     * 
     */
    public SgawsParalelosCarrera createSgawsParalelosCarrera() {
        return new SgawsParalelosCarrera();
    }

}
