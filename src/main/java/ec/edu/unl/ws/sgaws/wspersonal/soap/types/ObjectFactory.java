
package ec.edu.unl.ws.sgaws.wspersonal.soap.types;

import javax.xml.bind.annotation.XmlRegistry;

import ec.edu.unl.ws.sgaws.wspersonal.soap.types.SgawsDatosDocente;
import ec.edu.unl.ws.sgaws.wspersonal.soap.types.SgawsDatosDocenteResponse;
import ec.edu.unl.ws.sgaws.wspersonal.soap.types.SgawsDatosEstudiante;
import ec.edu.unl.ws.sgaws.wspersonal.soap.types.SgawsDatosEstudianteResponse;
import ec.edu.unl.ws.sgaws.wspersonal.soap.types.SgawsDatosPersonales;
import ec.edu.unl.ws.sgaws.wspersonal.soap.types.SgawsDatosPersonalesResponse;
import ec.edu.unl.ws.sgaws.wspersonal.soap.types.SgawsDatosUsuario;
import ec.edu.unl.ws.sgaws.wspersonal.soap.types.SgawsDatosUsuarioResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.unl.ws.sgaws.wspersonal.soap.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.unl.ws.sgaws.wspersonal.soap.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SgawsDatosUsuarioResponse }
     * 
     */
    public SgawsDatosUsuarioResponse createSgawsDatosUsuarioResponse() {
        return new SgawsDatosUsuarioResponse();
    }

    /**
     * Create an instance of {@link SgawsDatosPersonales }
     * 
     */
    public SgawsDatosPersonales createSgawsDatosPersonales() {
        return new SgawsDatosPersonales();
    }

    /**
     * Create an instance of {@link SgawsDatosPersonalesResponse }
     * 
     */
    public SgawsDatosPersonalesResponse createSgawsDatosPersonalesResponse() {
        return new SgawsDatosPersonalesResponse();
    }

    /**
     * Create an instance of {@link SgawsDatosDocente }
     * 
     */
    public SgawsDatosDocente createSgawsDatosDocente() {
        return new SgawsDatosDocente();
    }

    /**
     * Create an instance of {@link SgawsDatosEstudianteResponse }
     * 
     */
    public SgawsDatosEstudianteResponse createSgawsDatosEstudianteResponse() {
        return new SgawsDatosEstudianteResponse();
    }

    /**
     * Create an instance of {@link SgawsDatosEstudiante }
     * 
     */
    public SgawsDatosEstudiante createSgawsDatosEstudiante() {
        return new SgawsDatosEstudiante();
    }

    /**
     * Create an instance of {@link SgawsDatosUsuario }
     * 
     */
    public SgawsDatosUsuario createSgawsDatosUsuario() {
        return new SgawsDatosUsuario();
    }

    /**
     * Create an instance of {@link SgawsDatosDocenteResponse }
     * 
     */
    public SgawsDatosDocenteResponse createSgawsDatosDocenteResponse() {
        return new SgawsDatosDocenteResponse();
    }

}
