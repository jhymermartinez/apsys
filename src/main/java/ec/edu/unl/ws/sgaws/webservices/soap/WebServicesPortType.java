
package ec.edu.unl.ws.sgaws.webservices.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import ec.edu.unl.ws.sgaws.webservices.soap.types.ObjectFactory;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "WebServices_PortType", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface WebServicesPortType {


    /**
     * 
     *             retorna la informacion de componentes academicos de aranceles pagados del estudiante
     *         
     * 
     * @param cedula
     * @param salt
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sga_acad_arancelinfo", action = "sga_acad_arancelinfo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
    @RequestWrapper(localName = "sga_acad_arancelinfo", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadArancelinfo")
    @ResponseWrapper(localName = "sga_acad_arancelinfoResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadArancelinfoResponse")
    public String sgaAcadArancelinfo(
        @WebParam(name = "cedula", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String cedula,
        @WebParam(name = "salt", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String salt);

    /**
     * 
     *             Devuelve la informacion de una carrera a partir de su carrera_id
     *             carrera_id,nombre,titulo,modalidad
     *         
     * 
     * @param carreraId
     * @param salt
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sga_acad_carrerainfo", action = "sga_acad_carrerainfo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
    @RequestWrapper(localName = "sga_acad_carrerainfo", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadCarrerainfo")
    @ResponseWrapper(localName = "sga_acad_carrerainfoResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadCarrerainfoResponse")
    public String sgaAcadCarrerainfo(
        @WebParam(name = "carrera_id", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String carreraId,
        @WebParam(name = "salt", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String salt);

    /**
     * 
     *             Devuelve la informacion de un modulo a partir de su modulo_id
     *             modulo_id,numero,nombre,objeto_transformacion
     *         
     * 
     * @param moduloId
     * @param salt
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sga_acad_moduloinfo", action = "sga_acad_moduloinfo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
    @RequestWrapper(localName = "sga_acad_moduloinfo", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadModuloinfo")
    @ResponseWrapper(localName = "sga_acad_moduloinfoResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadModuloinfoResponse")
    public String sgaAcadModuloinfo(
        @WebParam(name = "modulo_id", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String moduloId,
        @WebParam(name = "salt", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String salt);

    /**
     * 
     *             retorna la informacion de horarios semana de un paralelo
     *         
     * 
     * @param idParalelo
     * @param salt
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sga_acad_paralelo_hs", action = "sga_acad_paralelo_hs")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
    @RequestWrapper(localName = "sga_acad_paralelo_hs", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadParaleloHs")
    @ResponseWrapper(localName = "sga_acad_paralelo_hsResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadParaleloHsResponse")
    public String sgaAcadParaleloHs(
        @WebParam(name = "id_paralelo", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String idParalelo,
        @WebParam(name = "salt", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String salt);

    /**
     * 
     *             Devuelve la informacion de un paralelo a partir de su id
     *             paralelo_id,nombre
     *         
     * 
     * @param paraleloId
     * @param salt
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sga_acad_paraleloinfo", action = "sga_acad_paraleloinfo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
    @RequestWrapper(localName = "sga_acad_paraleloinfo", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadParaleloinfo")
    @ResponseWrapper(localName = "sga_acad_paraleloinfoResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadParaleloinfoResponse")
    public String sgaAcadParaleloinfo(
        @WebParam(name = "paralelo_id", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String paraleloId,
        @WebParam(name = "salt", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String salt);

    /**
     * 
     *             Retorna los datos de un estudiante
     *         
     * 
     * @param username
     * @param salt
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sga_acad_userinfo", action = "sga_acad_userinfo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
    @RequestWrapper(localName = "sga_acad_userinfo", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadUserinfo")
    @ResponseWrapper(localName = "sga_acad_userinfoResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadUserinfoResponse")
    public String sgaAcadUserinfo(
        @WebParam(name = "username", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String username,
        @WebParam(name = "salt", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String salt);

    /**
     * 
     *             Autentifica un docente en el sistema SGA, retorna 'true' o 'false'
     *         
     * 
     * @param username
     * @param password
     * @param salt
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sga_auth_docente_login", action = "sga_auth_docente_login")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
    @RequestWrapper(localName = "sga_auth_docente_login", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthDocenteLogin")
    @ResponseWrapper(localName = "sga_auth_docente_loginResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthDocenteLoginResponse")
    public String sgaAuthDocenteLogin(
        @WebParam(name = "username", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String username,
        @WebParam(name = "password", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String password,
        @WebParam(name = "salt", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String salt);

    /**
     * 
     *             Verificar si el usuario existe en la base de datos sga
     *         
     * 
     * @param username
     * @param salt
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sga_auth_user_exists", action = "sga_auth_user_exists")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
    @RequestWrapper(localName = "sga_auth_user_exists", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthUserExists")
    @ResponseWrapper(localName = "sga_auth_user_existsResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthUserExistsResponse")
    public String sgaAuthUserExists(
        @WebParam(name = "username", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String username,
        @WebParam(name = "salt", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String salt);

    /**
     * 
     *             Autentifica un usuario en el sistema SGA, retorna 'true' o 'false'
     *         
     * 
     * @param username
     * @param password
     * @param salt
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sga_auth_user_login", action = "sga_auth_user_login")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
    @RequestWrapper(localName = "sga_auth_user_login", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthUserLogin")
    @ResponseWrapper(localName = "sga_auth_user_loginResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthUserLoginResponse")
    public String sgaAuthUserLogin(
        @WebParam(name = "username", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String username,
        @WebParam(name = "password", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String password,
        @WebParam(name = "salt", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String salt);

    /**
     * 
     *             Retornar una lista con todos los usernames separados por ',' solo de estudiantes
     *         
     * 
     * @param salt
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sga_auth_userlist", action = "sga_auth_userlist")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
    @RequestWrapper(localName = "sga_auth_userlist", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthUserlist")
    @ResponseWrapper(localName = "sga_auth_userlistResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthUserlistResponse")
    public String sgaAuthUserlist(
        @WebParam(name = "salt", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String salt);

    /**
     * Obtener la informacion de carreras de un docente
     * 
     * @param cedula
     * @param salt
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sga_docente_carreras", action = "sga_docente_carreras")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
    @RequestWrapper(localName = "sga_docente_carreras", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaDocenteCarreras")
    @ResponseWrapper(localName = "sga_docente_carrerasResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaDocenteCarrerasResponse")
    public String sgaDocenteCarreras(
        @WebParam(name = "cedula", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String cedula,
        @WebParam(name = "salt", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String salt);

    /**
     * Obtener la informacion de un docente, info basica 
     * 
     * @param cedula
     * @param salt
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sga_docente_info", action = "sga_docente_info")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
    @RequestWrapper(localName = "sga_docente_info", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaDocenteInfo")
    @ResponseWrapper(localName = "sga_docente_infoResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types", className = "ec.edu.unl.ws.sgaws.webservices.soap.types.SgaDocenteInfoResponse")
    public String sgaDocenteInfo(
        @WebParam(name = "cedula", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String cedula,
        @WebParam(name = "salt", targetNamespace = "http://ws.unl.edu.ec/sgaws/webservices/soap/types")
        String salt);

}
