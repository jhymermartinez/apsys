
package ec.edu.unl.ws.sgaws.wsestadistica.soap.types;

import javax.xml.bind.annotation.XmlRegistry;

import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosArea;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosAreaResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosCarrera;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosCarreraResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosModulo;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosModuloResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosOferta;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosOfertaResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosParalelo;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNaprobadosParaleloResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNdocentesOferta;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNdocentesOfertaResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosArea;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosAreaResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosCarrera;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosCarreraResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosModulo;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosModuloResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosOferta;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosOfertaResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosParalelo;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNmatriculadosParaleloResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosArea;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosAreaResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosCarrera;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosCarreraResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosModulo;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosModuloResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosOferta;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosOfertaResponse;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosParalelo;
import ec.edu.unl.ws.sgaws.wsestadistica.soap.types.SgawsNreprobadosParaleloResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.unl.ws.sgaws.wsestadistica.soap.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.unl.ws.sgaws.wsestadistica.soap.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SgawsNdocentesOferta }
     * 
     */
    public SgawsNdocentesOferta createSgawsNdocentesOferta() {
        return new SgawsNdocentesOferta();
    }

    /**
     * Create an instance of {@link SgawsNreprobadosParalelo }
     * 
     */
    public SgawsNreprobadosParalelo createSgawsNreprobadosParalelo() {
        return new SgawsNreprobadosParalelo();
    }

    /**
     * Create an instance of {@link SgawsNaprobadosParaleloResponse }
     * 
     */
    public SgawsNaprobadosParaleloResponse createSgawsNaprobadosParaleloResponse() {
        return new SgawsNaprobadosParaleloResponse();
    }

    /**
     * Create an instance of {@link SgawsNaprobadosModulo }
     * 
     */
    public SgawsNaprobadosModulo createSgawsNaprobadosModulo() {
        return new SgawsNaprobadosModulo();
    }

    /**
     * Create an instance of {@link SgawsNmatriculadosParalelo }
     * 
     */
    public SgawsNmatriculadosParalelo createSgawsNmatriculadosParalelo() {
        return new SgawsNmatriculadosParalelo();
    }

    /**
     * Create an instance of {@link SgawsNdocentesOfertaResponse }
     * 
     */
    public SgawsNdocentesOfertaResponse createSgawsNdocentesOfertaResponse() {
        return new SgawsNdocentesOfertaResponse();
    }

    /**
     * Create an instance of {@link SgawsNreprobadosParaleloResponse }
     * 
     */
    public SgawsNreprobadosParaleloResponse createSgawsNreprobadosParaleloResponse() {
        return new SgawsNreprobadosParaleloResponse();
    }

    /**
     * Create an instance of {@link SgawsNmatriculadosAreaResponse }
     * 
     */
    public SgawsNmatriculadosAreaResponse createSgawsNmatriculadosAreaResponse() {
        return new SgawsNmatriculadosAreaResponse();
    }

    /**
     * Create an instance of {@link SgawsNmatriculadosCarrera }
     * 
     */
    public SgawsNmatriculadosCarrera createSgawsNmatriculadosCarrera() {
        return new SgawsNmatriculadosCarrera();
    }

    /**
     * Create an instance of {@link SgawsNmatriculadosModulo }
     * 
     */
    public SgawsNmatriculadosModulo createSgawsNmatriculadosModulo() {
        return new SgawsNmatriculadosModulo();
    }

    /**
     * Create an instance of {@link SgawsNmatriculadosModuloResponse }
     * 
     */
    public SgawsNmatriculadosModuloResponse createSgawsNmatriculadosModuloResponse() {
        return new SgawsNmatriculadosModuloResponse();
    }

    /**
     * Create an instance of {@link SgawsNreprobadosCarreraResponse }
     * 
     */
    public SgawsNreprobadosCarreraResponse createSgawsNreprobadosCarreraResponse() {
        return new SgawsNreprobadosCarreraResponse();
    }

    /**
     * Create an instance of {@link SgawsNaprobadosOferta }
     * 
     */
    public SgawsNaprobadosOferta createSgawsNaprobadosOferta() {
        return new SgawsNaprobadosOferta();
    }

    /**
     * Create an instance of {@link SgawsNreprobadosOferta }
     * 
     */
    public SgawsNreprobadosOferta createSgawsNreprobadosOferta() {
        return new SgawsNreprobadosOferta();
    }

    /**
     * Create an instance of {@link SgawsNmatriculadosCarreraResponse }
     * 
     */
    public SgawsNmatriculadosCarreraResponse createSgawsNmatriculadosCarreraResponse() {
        return new SgawsNmatriculadosCarreraResponse();
    }

    /**
     * Create an instance of {@link SgawsNmatriculadosOfertaResponse }
     * 
     */
    public SgawsNmatriculadosOfertaResponse createSgawsNmatriculadosOfertaResponse() {
        return new SgawsNmatriculadosOfertaResponse();
    }

    /**
     * Create an instance of {@link SgawsNaprobadosArea }
     * 
     */
    public SgawsNaprobadosArea createSgawsNaprobadosArea() {
        return new SgawsNaprobadosArea();
    }

    /**
     * Create an instance of {@link SgawsNmatriculadosOferta }
     * 
     */
    public SgawsNmatriculadosOferta createSgawsNmatriculadosOferta() {
        return new SgawsNmatriculadosOferta();
    }

    /**
     * Create an instance of {@link SgawsNaprobadosAreaResponse }
     * 
     */
    public SgawsNaprobadosAreaResponse createSgawsNaprobadosAreaResponse() {
        return new SgawsNaprobadosAreaResponse();
    }

    /**
     * Create an instance of {@link SgawsNreprobadosCarrera }
     * 
     */
    public SgawsNreprobadosCarrera createSgawsNreprobadosCarrera() {
        return new SgawsNreprobadosCarrera();
    }

    /**
     * Create an instance of {@link SgawsNaprobadosCarreraResponse }
     * 
     */
    public SgawsNaprobadosCarreraResponse createSgawsNaprobadosCarreraResponse() {
        return new SgawsNaprobadosCarreraResponse();
    }

    /**
     * Create an instance of {@link SgawsNmatriculadosParaleloResponse }
     * 
     */
    public SgawsNmatriculadosParaleloResponse createSgawsNmatriculadosParaleloResponse() {
        return new SgawsNmatriculadosParaleloResponse();
    }

    /**
     * Create an instance of {@link SgawsNaprobadosOfertaResponse }
     * 
     */
    public SgawsNaprobadosOfertaResponse createSgawsNaprobadosOfertaResponse() {
        return new SgawsNaprobadosOfertaResponse();
    }

    /**
     * Create an instance of {@link SgawsNaprobadosCarrera }
     * 
     */
    public SgawsNaprobadosCarrera createSgawsNaprobadosCarrera() {
        return new SgawsNaprobadosCarrera();
    }

    /**
     * Create an instance of {@link SgawsNreprobadosModuloResponse }
     * 
     */
    public SgawsNreprobadosModuloResponse createSgawsNreprobadosModuloResponse() {
        return new SgawsNreprobadosModuloResponse();
    }

    /**
     * Create an instance of {@link SgawsNaprobadosParalelo }
     * 
     */
    public SgawsNaprobadosParalelo createSgawsNaprobadosParalelo() {
        return new SgawsNaprobadosParalelo();
    }

    /**
     * Create an instance of {@link SgawsNaprobadosModuloResponse }
     * 
     */
    public SgawsNaprobadosModuloResponse createSgawsNaprobadosModuloResponse() {
        return new SgawsNaprobadosModuloResponse();
    }

    /**
     * Create an instance of {@link SgawsNreprobadosModulo }
     * 
     */
    public SgawsNreprobadosModulo createSgawsNreprobadosModulo() {
        return new SgawsNreprobadosModulo();
    }

    /**
     * Create an instance of {@link SgawsNreprobadosOfertaResponse }
     * 
     */
    public SgawsNreprobadosOfertaResponse createSgawsNreprobadosOfertaResponse() {
        return new SgawsNreprobadosOfertaResponse();
    }

    /**
     * Create an instance of {@link SgawsNmatriculadosArea }
     * 
     */
    public SgawsNmatriculadosArea createSgawsNmatriculadosArea() {
        return new SgawsNmatriculadosArea();
    }

    /**
     * Create an instance of {@link SgawsNreprobadosArea }
     * 
     */
    public SgawsNreprobadosArea createSgawsNreprobadosArea() {
        return new SgawsNreprobadosArea();
    }

    /**
     * Create an instance of {@link SgawsNreprobadosAreaResponse }
     * 
     */
    public SgawsNreprobadosAreaResponse createSgawsNreprobadosAreaResponse() {
        return new SgawsNreprobadosAreaResponse();
    }

}
