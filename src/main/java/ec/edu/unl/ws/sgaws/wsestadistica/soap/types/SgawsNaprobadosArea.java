
package ec.edu.unl.ws.sgaws.wsestadistica.soap.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id_oferta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="siglas" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idOferta",
    "siglas"
})
@XmlRootElement(name = "sgaws_naprobados_area")
public class SgawsNaprobadosArea {

    @XmlElement(name = "id_oferta", required = true)
    protected String idOferta;
    @XmlElement(required = true)
    protected String siglas;

    /**
     * Gets the value of the idOferta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdOferta() {
        return idOferta;
    }

    /**
     * Sets the value of the idOferta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdOferta(String value) {
        this.idOferta = value;
    }

    /**
     * Gets the value of the siglas property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiglas() {
        return siglas;
    }

    /**
     * Sets the value of the siglas property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiglas(String value) {
        this.siglas = value;
    }

}
