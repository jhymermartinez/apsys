
package ec.edu.unl.ws.sgaws.wsvalidacion.soap.types;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.unl.ws.sgaws.wsvalidacion.soap.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.unl.ws.sgaws.wsvalidacion.soap.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SgawsValidarEstudianteResponse }
     * 
     */
    public SgawsValidarEstudianteResponse createSgawsValidarEstudianteResponse() {
        return new SgawsValidarEstudianteResponse();
    }

    /**
     * Create an instance of {@link SgawsValidarDocente }
     * 
     */
    public SgawsValidarDocente createSgawsValidarDocente() {
        return new SgawsValidarDocente();
    }

    /**
     * Create an instance of {@link Suma }
     * 
     */
    public Suma createSuma() {
        return new Suma();
    }

    /**
     * Create an instance of {@link SumaResponse }
     * 
     */
    public SumaResponse createSumaResponse() {
        return new SumaResponse();
    }

    /**
     * Create an instance of {@link SgawsValidarEstudiante }
     * 
     */
    public SgawsValidarEstudiante createSgawsValidarEstudiante() {
        return new SgawsValidarEstudiante();
    }

    /**
     * Create an instance of {@link SgawsValidarDocenteResponse }
     * 
     */
    public SgawsValidarDocenteResponse createSgawsValidarDocenteResponse() {
        return new SgawsValidarDocenteResponse();
    }

}
