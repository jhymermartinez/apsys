
package ec.edu.unl.ws.sgaws.webservices.soap.types;

import javax.xml.bind.annotation.XmlRegistry;

import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadArancelinfo;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadArancelinfoResponse;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadCarrerainfo;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadCarrerainfoResponse;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadModuloinfo;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadModuloinfoResponse;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadParaleloHs;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadParaleloHsResponse;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadParaleloinfo;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadParaleloinfoResponse;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadUserinfo;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAcadUserinfoResponse;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthDocenteLogin;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthDocenteLoginResponse;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthUserExists;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthUserExistsResponse;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthUserLogin;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthUserLoginResponse;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthUserlist;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaAuthUserlistResponse;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaDocenteCarreras;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaDocenteCarrerasResponse;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaDocenteInfo;
import ec.edu.unl.ws.sgaws.webservices.soap.types.SgaDocenteInfoResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.unl.ws.sgaws.webservices.soap.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.unl.ws.sgaws.webservices.soap.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SgaDocenteInfo }
     * 
     */
    public SgaDocenteInfo createSgaDocenteInfo() {
        return new SgaDocenteInfo();
    }

    /**
     * Create an instance of {@link SgaAuthUserLoginResponse }
     * 
     */
    public SgaAuthUserLoginResponse createSgaAuthUserLoginResponse() {
        return new SgaAuthUserLoginResponse();
    }

    /**
     * Create an instance of {@link SgaAcadCarrerainfoResponse }
     * 
     */
    public SgaAcadCarrerainfoResponse createSgaAcadCarrerainfoResponse() {
        return new SgaAcadCarrerainfoResponse();
    }

    /**
     * Create an instance of {@link SgaAuthUserExistsResponse }
     * 
     */
    public SgaAuthUserExistsResponse createSgaAuthUserExistsResponse() {
        return new SgaAuthUserExistsResponse();
    }

    /**
     * Create an instance of {@link SgaAcadModuloinfoResponse }
     * 
     */
    public SgaAcadModuloinfoResponse createSgaAcadModuloinfoResponse() {
        return new SgaAcadModuloinfoResponse();
    }

    /**
     * Create an instance of {@link SgaAcadArancelinfoResponse }
     * 
     */
    public SgaAcadArancelinfoResponse createSgaAcadArancelinfoResponse() {
        return new SgaAcadArancelinfoResponse();
    }

    /**
     * Create an instance of {@link SgaAcadParaleloinfoResponse }
     * 
     */
    public SgaAcadParaleloinfoResponse createSgaAcadParaleloinfoResponse() {
        return new SgaAcadParaleloinfoResponse();
    }

    /**
     * Create an instance of {@link SgaAcadParaleloHs }
     * 
     */
    public SgaAcadParaleloHs createSgaAcadParaleloHs() {
        return new SgaAcadParaleloHs();
    }

    /**
     * Create an instance of {@link SgaAuthUserExists }
     * 
     */
    public SgaAuthUserExists createSgaAuthUserExists() {
        return new SgaAuthUserExists();
    }

    /**
     * Create an instance of {@link SgaAcadArancelinfo }
     * 
     */
    public SgaAcadArancelinfo createSgaAcadArancelinfo() {
        return new SgaAcadArancelinfo();
    }

    /**
     * Create an instance of {@link SgaAuthUserlistResponse }
     * 
     */
    public SgaAuthUserlistResponse createSgaAuthUserlistResponse() {
        return new SgaAuthUserlistResponse();
    }

    /**
     * Create an instance of {@link SgaAuthDocenteLogin }
     * 
     */
    public SgaAuthDocenteLogin createSgaAuthDocenteLogin() {
        return new SgaAuthDocenteLogin();
    }

    /**
     * Create an instance of {@link SgaAcadUserinfo }
     * 
     */
    public SgaAcadUserinfo createSgaAcadUserinfo() {
        return new SgaAcadUserinfo();
    }

    /**
     * Create an instance of {@link SgaAuthUserlist }
     * 
     */
    public SgaAuthUserlist createSgaAuthUserlist() {
        return new SgaAuthUserlist();
    }

    /**
     * Create an instance of {@link SgaAcadModuloinfo }
     * 
     */
    public SgaAcadModuloinfo createSgaAcadModuloinfo() {
        return new SgaAcadModuloinfo();
    }

    /**
     * Create an instance of {@link SgaAcadUserinfoResponse }
     * 
     */
    public SgaAcadUserinfoResponse createSgaAcadUserinfoResponse() {
        return new SgaAcadUserinfoResponse();
    }

    /**
     * Create an instance of {@link SgaDocenteCarreras }
     * 
     */
    public SgaDocenteCarreras createSgaDocenteCarreras() {
        return new SgaDocenteCarreras();
    }

    /**
     * Create an instance of {@link SgaAuthUserLogin }
     * 
     */
    public SgaAuthUserLogin createSgaAuthUserLogin() {
        return new SgaAuthUserLogin();
    }

    /**
     * Create an instance of {@link SgaAuthDocenteLoginResponse }
     * 
     */
    public SgaAuthDocenteLoginResponse createSgaAuthDocenteLoginResponse() {
        return new SgaAuthDocenteLoginResponse();
    }

    /**
     * Create an instance of {@link SgaDocenteCarrerasResponse }
     * 
     */
    public SgaDocenteCarrerasResponse createSgaDocenteCarrerasResponse() {
        return new SgaDocenteCarrerasResponse();
    }

    /**
     * Create an instance of {@link SgaDocenteInfoResponse }
     * 
     */
    public SgaDocenteInfoResponse createSgaDocenteInfoResponse() {
        return new SgaDocenteInfoResponse();
    }

    /**
     * Create an instance of {@link SgaAcadParaleloHsResponse }
     * 
     */
    public SgaAcadParaleloHsResponse createSgaAcadParaleloHsResponse() {
        return new SgaAcadParaleloHsResponse();
    }

    /**
     * Create an instance of {@link SgaAcadCarrerainfo }
     * 
     */
    public SgaAcadCarrerainfo createSgaAcadCarrerainfo() {
        return new SgaAcadCarrerainfo();
    }

    /**
     * Create an instance of {@link SgaAcadParaleloinfo }
     * 
     */
    public SgaAcadParaleloinfo createSgaAcadParaleloinfo() {
        return new SgaAcadParaleloinfo();
    }

}
