
package ec.edu.unl.ws.sgaws.webservices.soap;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

import ec.edu.unl.operations.impl.OperationsImpl;
import ec.edu.unl.ws.sgaws.webservices.soap.WebServicesPortType;


@WebServiceClient
public class WebServices extends Service{

    private final static URL WEBSERVICES_WSDL_LOCATION;
    private final static WebServiceException WEBSERVICES_EXCEPTION;
    private final static QName WEBSERVICES_QNAME = new QName("http://ws.unl.edu.ec/sgaws/webservices/soap/", "WebServices");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
        	
        	OperationsImpl op = new OperationsImpl();
        	String dir = op.generateRoute(op.getDirectory())+"migrados.wsdl";	      	
        	url = new URL("file:/"+dir);
        	
            //url = new URL("file:/C:/En_Desarrollo/Tesis/APSYS/src/main/resources/wsdl/migrados.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        WEBSERVICES_WSDL_LOCATION = url;
        WEBSERVICES_EXCEPTION = e;
    }

    public WebServices() {
        super(__getWsdlLocation(), WEBSERVICES_QNAME);
    }

    public WebServices(WebServiceFeature... features) {
        super(__getWsdlLocation(), WEBSERVICES_QNAME, features);
    }

    public WebServices(URL wsdlLocation) {
        super(wsdlLocation, WEBSERVICES_QNAME);
    }

    public WebServices(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, WEBSERVICES_QNAME, features);
    }

    public WebServices(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public WebServices(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns WebServicesPortType
     */
    @WebEndpoint(name = "WebServices_PortType")
    public WebServicesPortType getWebServicesPortType() {
        return super.getPort(new QName("http://ws.unl.edu.ec/sgaws/webservices/soap/", "WebServices_PortType"), WebServicesPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns WebServicesPortType
     */
    @WebEndpoint(name = "WebServices_PortType")
    public WebServicesPortType getWebServicesPortType(WebServiceFeature... features) {
        return super.getPort(new QName("http://ws.unl.edu.ec/sgaws/webservices/soap/", "WebServices_PortType"), WebServicesPortType.class, features);
    }

    private static URL __getWsdlLocation() {
        if (WEBSERVICES_EXCEPTION!= null) {
            throw WEBSERVICES_EXCEPTION;
        }
        return WEBSERVICES_WSDL_LOCATION;
    }

}
