
package ec.edu.unl.ws.sgaws.wsacademica.soap;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

import ec.edu.unl.operations.impl.OperationsImpl;


@WebServiceClient
public class SGAWebServicesAcademica extends Service{

    private final static URL SGAWEBSERVICESACADEMICA_WSDL_LOCATION;
    private final static WebServiceException SGAWEBSERVICESACADEMICA_EXCEPTION;
    private final static QName SGAWEBSERVICESACADEMICA_QNAME = new QName("http://ws.unl.edu.ec/sgaws/wsacademica/soap/", "SGAWebServicesAcademica");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
        	
        	OperationsImpl op = new OperationsImpl();
        	String dir = op.generateRoute(op.getDirectory())+"academica.wsdl";	      	
        	url = new URL("file:/"+dir);
        	
            //url = new URL("file:/C:/En_Desarrollo/Tesis/APSYS/src/main/resources/wsdl/academica.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SGAWEBSERVICESACADEMICA_WSDL_LOCATION = url;
        SGAWEBSERVICESACADEMICA_EXCEPTION = e;
    }

    public SGAWebServicesAcademica() {
        super(__getWsdlLocation(), SGAWEBSERVICESACADEMICA_QNAME);
    }

    public SGAWebServicesAcademica(WebServiceFeature... features) {
        super(__getWsdlLocation(), SGAWEBSERVICESACADEMICA_QNAME, features);
    }

    public SGAWebServicesAcademica(URL wsdlLocation) {
        super(wsdlLocation, SGAWEBSERVICESACADEMICA_QNAME);
    }

    public SGAWebServicesAcademica(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SGAWEBSERVICESACADEMICA_QNAME, features);
    }

    public SGAWebServicesAcademica(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SGAWebServicesAcademica(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns SGAWebServicesAcademicaPortType
     */
    @WebEndpoint(name = "SGAWebServicesAcademica_PortType")
    public SGAWebServicesAcademicaPortType getSGAWebServicesAcademicaPortType() {
        return super.getPort(new QName("http://ws.unl.edu.ec/sgaws/wsacademica/soap/", "SGAWebServicesAcademica_PortType"), SGAWebServicesAcademicaPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns SGAWebServicesAcademicaPortType
     */
    @WebEndpoint(name = "SGAWebServicesAcademica_PortType")
    public SGAWebServicesAcademicaPortType getSGAWebServicesAcademicaPortType(WebServiceFeature... features) {
        return super.getPort(new QName("http://ws.unl.edu.ec/sgaws/wsacademica/soap/", "SGAWebServicesAcademica_PortType"), SGAWebServicesAcademicaPortType.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SGAWEBSERVICESACADEMICA_EXCEPTION!= null) {
            throw SGAWEBSERVICESACADEMICA_EXCEPTION;
        }
        return SGAWEBSERVICESACADEMICA_WSDL_LOCATION;
    }

}
