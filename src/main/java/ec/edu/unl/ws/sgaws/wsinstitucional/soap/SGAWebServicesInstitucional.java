
package ec.edu.unl.ws.sgaws.wsinstitucional.soap;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

import ec.edu.unl.operations.impl.OperationsImpl;
import ec.edu.unl.ws.sgaws.wsinstitucional.soap.SGAWebServicesInstitucionalPortType;



@WebServiceClient
public class SGAWebServicesInstitucional extends Service{

    private final static URL SGAWEBSERVICESINSTITUCIONAL_WSDL_LOCATION;
    private final static WebServiceException SGAWEBSERVICESINSTITUCIONAL_EXCEPTION;
    private final static QName SGAWEBSERVICESINSTITUCIONAL_QNAME = new QName("http://ws.unl.edu.ec/sgaws/wsinstitucional/soap/", "SGAWebServicesInstitucional");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
        	
        	OperationsImpl op = new OperationsImpl();
        	String dir = op.generateRoute(op.getDirectory())+"institucional.wsdl";	      	
        	url = new URL("file:/"+dir);
        	
            //url = new URL("file:/C:/En_Desarrollo/Tesis/APSYS/src/main/resources/wsdl/institucional.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SGAWEBSERVICESINSTITUCIONAL_WSDL_LOCATION = url;
        SGAWEBSERVICESINSTITUCIONAL_EXCEPTION = e;
    }

    public SGAWebServicesInstitucional() {
        super(__getWsdlLocation(), SGAWEBSERVICESINSTITUCIONAL_QNAME);
    }

    public SGAWebServicesInstitucional(WebServiceFeature... features) {
        super(__getWsdlLocation(), SGAWEBSERVICESINSTITUCIONAL_QNAME, features);
    }

    public SGAWebServicesInstitucional(URL wsdlLocation) {
        super(wsdlLocation, SGAWEBSERVICESINSTITUCIONAL_QNAME);
    }

    public SGAWebServicesInstitucional(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SGAWEBSERVICESINSTITUCIONAL_QNAME, features);
    }

    public SGAWebServicesInstitucional(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SGAWebServicesInstitucional(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns SGAWebServicesInstitucionalPortType
     */
    @WebEndpoint(name = "SGAWebServicesInstitucional_PortType")
    public SGAWebServicesInstitucionalPortType getSGAWebServicesInstitucionalPortType() {
        return super.getPort(new QName("http://ws.unl.edu.ec/sgaws/wsinstitucional/soap/", "SGAWebServicesInstitucional_PortType"), SGAWebServicesInstitucionalPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns SGAWebServicesInstitucionalPortType
     */
    @WebEndpoint(name = "SGAWebServicesInstitucional_PortType")
    public SGAWebServicesInstitucionalPortType getSGAWebServicesInstitucionalPortType(WebServiceFeature... features) {
        return super.getPort(new QName("http://ws.unl.edu.ec/sgaws/wsinstitucional/soap/", "SGAWebServicesInstitucional_PortType"), SGAWebServicesInstitucionalPortType.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SGAWEBSERVICESINSTITUCIONAL_EXCEPTION!= null) {
            throw SGAWEBSERVICESINSTITUCIONAL_EXCEPTION;
        }
        return SGAWEBSERVICESINSTITUCIONAL_WSDL_LOCATION;
    }

}
