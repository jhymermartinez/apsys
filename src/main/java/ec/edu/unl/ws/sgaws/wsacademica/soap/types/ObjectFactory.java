
package ec.edu.unl.ws.sgaws.wsacademica.soap.types;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.unl.ws.sgaws.wsacademica.soap.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.unl.ws.sgaws.wsacademica.soap.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SgawsEgresados }
     * 
     */
    public SgawsEgresados createSgawsEgresados() {
        return new SgawsEgresados();
    }

    /**
     * Create an instance of {@link SgawsOfertasAcademicas }
     * 
     */
    public SgawsOfertasAcademicas createSgawsOfertasAcademicas() {
        return new SgawsOfertasAcademicas();
    }

    /**
     * Create an instance of {@link SgawsPlanEstudioResponse }
     * 
     */
    public SgawsPlanEstudioResponse createSgawsPlanEstudioResponse() {
        return new SgawsPlanEstudioResponse();
    }

    /**
     * Create an instance of {@link SgawsEstudianteMatriculasResponse }
     * 
     */
    public SgawsEstudianteMatriculasResponse createSgawsEstudianteMatriculasResponse() {
        return new SgawsEstudianteMatriculasResponse();
    }

    /**
     * Create an instance of {@link SgawsEstudianteMatriculas }
     * 
     */
    public SgawsEstudianteMatriculas createSgawsEstudianteMatriculas() {
        return new SgawsEstudianteMatriculas();
    }

    /**
     * Create an instance of {@link SgawsIsMatriculadoResponse }
     * 
     */
    public SgawsIsMatriculadoResponse createSgawsIsMatriculadoResponse() {
        return new SgawsIsMatriculadoResponse();
    }

    /**
     * Create an instance of {@link SgawsIsMatriculado }
     * 
     */
    public SgawsIsMatriculado createSgawsIsMatriculado() {
        return new SgawsIsMatriculado();
    }

    /**
     * Create an instance of {@link SgawsFechasMatriculaoaResponse }
     * 
     */
    public SgawsFechasMatriculaoaResponse createSgawsFechasMatriculaoaResponse() {
        return new SgawsFechasMatriculaoaResponse();
    }

    /**
     * Create an instance of {@link SgawsUnidadesDocentesParalelo }
     * 
     */
    public SgawsUnidadesDocentesParalelo createSgawsUnidadesDocentesParalelo() {
        return new SgawsUnidadesDocentesParalelo();
    }

    /**
     * Create an instance of {@link SgawsCarrerasEstudianteResponse }
     * 
     */
    public SgawsCarrerasEstudianteResponse createSgawsCarrerasEstudianteResponse() {
        return new SgawsCarrerasEstudianteResponse();
    }

    /**
     * Create an instance of {@link SgawsCarrerasEstudiante }
     * 
     */
    public SgawsCarrerasEstudiante createSgawsCarrerasEstudiante() {
        return new SgawsCarrerasEstudiante();
    }

    /**
     * Create an instance of {@link SgawsOfertasAcademicasResponse }
     * 
     */
    public SgawsOfertasAcademicasResponse createSgawsOfertasAcademicasResponse() {
        return new SgawsOfertasAcademicasResponse();
    }

    /**
     * Create an instance of {@link SgawsCargaHorariaDocenteResponse }
     * 
     */
    public SgawsCargaHorariaDocenteResponse createSgawsCargaHorariaDocenteResponse() {
        return new SgawsCargaHorariaDocenteResponse();
    }

    /**
     * Create an instance of {@link SgawsEstadoestudiantesParaleloResponse }
     * 
     */
    public SgawsEstadoestudiantesParaleloResponse createSgawsEstadoestudiantesParaleloResponse() {
        return new SgawsEstadoestudiantesParaleloResponse();
    }

    /**
     * Create an instance of {@link SgawsUnidadesDocentesParaleloResponse }
     * 
     */
    public SgawsUnidadesDocentesParaleloResponse createSgawsUnidadesDocentesParaleloResponse() {
        return new SgawsUnidadesDocentesParaleloResponse();
    }

    /**
     * Create an instance of {@link SgawsFechasMatriculaoa }
     * 
     */
    public SgawsFechasMatriculaoa createSgawsFechasMatriculaoa() {
        return new SgawsFechasMatriculaoa();
    }

    /**
     * Create an instance of {@link SgawsCargaHorariaDocente }
     * 
     */
    public SgawsCargaHorariaDocente createSgawsCargaHorariaDocente() {
        return new SgawsCargaHorariaDocente();
    }

    /**
     * Create an instance of {@link SgawsReporteMatricula }
     * 
     */
    public SgawsReporteMatricula createSgawsReporteMatricula() {
        return new SgawsReporteMatricula();
    }

    /**
     * Create an instance of {@link SgawsInfoMatriculaResponse }
     * 
     */
    public SgawsInfoMatriculaResponse createSgawsInfoMatriculaResponse() {
        return new SgawsInfoMatriculaResponse();
    }

    /**
     * Create an instance of {@link SgawsPeriodosLectivosResponse }
     * 
     */
    public SgawsPeriodosLectivosResponse createSgawsPeriodosLectivosResponse() {
        return new SgawsPeriodosLectivosResponse();
    }

    /**
     * Create an instance of {@link SgawsNotasEstudiante }
     * 
     */
    public SgawsNotasEstudiante createSgawsNotasEstudiante() {
        return new SgawsNotasEstudiante();
    }

    /**
     * Create an instance of {@link SgawsReporteMatriculaResponse }
     * 
     */
    public SgawsReporteMatriculaResponse createSgawsReporteMatriculaResponse() {
        return new SgawsReporteMatriculaResponse();
    }

    /**
     * Create an instance of {@link SgawsEgresadosResponse }
     * 
     */
    public SgawsEgresadosResponse createSgawsEgresadosResponse() {
        return new SgawsEgresadosResponse();
    }

    /**
     * Create an instance of {@link SgawsNotasEstudianteResponse }
     * 
     */
    public SgawsNotasEstudianteResponse createSgawsNotasEstudianteResponse() {
        return new SgawsNotasEstudianteResponse();
    }

    /**
     * Create an instance of {@link SgawsExpedienteCarrera }
     * 
     */
    public SgawsExpedienteCarrera createSgawsExpedienteCarrera() {
        return new SgawsExpedienteCarrera();
    }

    /**
     * Create an instance of {@link SgawsPlanEstudio }
     * 
     */
    public SgawsPlanEstudio createSgawsPlanEstudio() {
        return new SgawsPlanEstudio();
    }

    /**
     * Create an instance of {@link SgawsInfoMatricula }
     * 
     */
    public SgawsInfoMatricula createSgawsInfoMatricula() {
        return new SgawsInfoMatricula();
    }

    /**
     * Create an instance of {@link SgawsExpedienteCarreraResponse }
     * 
     */
    public SgawsExpedienteCarreraResponse createSgawsExpedienteCarreraResponse() {
        return new SgawsExpedienteCarreraResponse();
    }

    /**
     * Create an instance of {@link SgawsEstadoestudiantesParalelo }
     * 
     */
    public SgawsEstadoestudiantesParalelo createSgawsEstadoestudiantesParalelo() {
        return new SgawsEstadoestudiantesParalelo();
    }

    /**
     * Create an instance of {@link SgawsPeriodosLectivos }
     * 
     */
    public SgawsPeriodosLectivos createSgawsPeriodosLectivos() {
        return new SgawsPeriodosLectivos();
    }

}
