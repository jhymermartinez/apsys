
package ec.edu.unl.ws.sgaws.wsacademica.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import ec.edu.unl.ws.sgaws.wsacademica.soap.types.ObjectFactory;


@WebService(name = "SGAWebServicesAcademica_PortType", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface SGAWebServicesAcademicaPortType {


    /**
     * 
     * 	Este metodo recibe como parametro de entrada el id de una oferta academica y la cedula de un docente.
     * 	Retorna los horarios semana de este docente para una oferta academica de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Titulo de docente, nombre del docente, apellidos del docente, nombre de la carrera, nombre de la unidad, duracion de la unidad, numero del modulo y numero de paralelo
     * 	    La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param idOferta
     * @param cedula
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_carga_horaria_docente", action = "sgaws_carga_horaria_docente")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_carga_horaria_docente", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsCargaHorariaDocente")
    @ResponseWrapper(localName = "sgaws_carga_horaria_docenteResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsCargaHorariaDocenteResponse")
    public String sgawsCargaHorariaDocente(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String idOferta,
        @WebParam(name = "cedula", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String cedula);

    /**
     * 
     * 	Este metodo recibe como parametro de entrada la cedula de un estudiante.
     * 	Retorna los datos del estudiante y los datos de la carreras de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Nombres del estudiante, apellidos del estudiante, cedula del estudiante, el id de la carrera, el nombre de la carrera y la modalidad de la carrera.
     * 	    La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param cedula
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_carreras_estudiante", action = "sgaws_carreras_estudiante")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_carreras_estudiante", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsCarrerasEstudiante")
    @ResponseWrapper(localName = "sgaws_carreras_estudianteResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsCarrerasEstudianteResponse")
    public String sgawsCarrerasEstudiante(
        @WebParam(name = "cedula", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String cedula);

    /**
     * 
     * 	Este metodo retorna una lista con los datos de los egresados y la carrera o carreras en las que culmino sus estudios
     * 	academicos
     *         
     * 
     * @param fecha
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_egresados", action = "sgaws_egresados")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_egresados", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsEgresados")
    @ResponseWrapper(localName = "sgaws_egresadosResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsEgresadosResponse")
    public String sgawsEgresados(
        @WebParam(name = "fecha", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String fecha);

    /**
     * 
     * 	Este metodo recibe como parametro de entrada el id de un Paralelo.
     * 	Retorna datos de la Oferta Academica, Carrera Programa, Modulo, Paralelo  ademas la lista de los estudiantes de un paralelo de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	descripcion de la oferta academica, nombre de la carrera, nombre del modulo, numero del modulo, nombre del paralelo, lista de los estudiante con: apellidos, nombres, cedula y estado de la matricula
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param idParalelo
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_estadoestudiantes_paralelo", action = "sgaws_estadoestudiantes_paralelo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_estadoestudiantes_paralelo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsEstadoestudiantesParalelo")
    @ResponseWrapper(localName = "sgaws_estadoestudiantes_paraleloResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsEstadoestudiantesParaleloResponse")
    public String sgawsEstadoestudiantesParalelo(
        @WebParam(name = "id_paralelo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String idParalelo);

    /**
     * 
     *             @autor: patovala
     *             Obtener una lista de matriculas dada la carrera, retorna:
     *             oferta_academica.id,
     *             matricula.id,
     *             matricula.paralelo.nombre,
     *             matricula.modulo.numero,
     *             matricula.modulo.nombre,
     *             matricula.estado.estado
     *         
     * 
     * @param idCarrera
     * @param cedula
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_estudiante_matriculas", action = "sgaws_estudiante_matriculas")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_estudiante_matriculas", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsEstudianteMatriculas")
    @ResponseWrapper(localName = "sgaws_estudiante_matriculasResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsEstudianteMatriculasResponse")
    public String sgawsEstudianteMatriculas(
        @WebParam(name = "id_carrera", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String idCarrera,
        @WebParam(name = "cedula", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String cedula);

    /**
     * 
     *         @autor: dmunoz.
     *         @date: 	20-05-2014.
     *         @param cedula: La cedula del estudiante.
     *         @param id_carrera: El id de la carrera.
     *         @description: Obtiene el detalle del expediente de una carrera para el estudiante indicado (codificado en JSON).
     *         @return [fecha_inicio, fecha_fin, fecha_grado, nivel_estudio, modalidad, titulo, tema_tesis]
     *         
     * 
     * @param idCarrera
     * @param cedula
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_expediente_carrera", action = "sgaws_expediente_carrera")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_expediente_carrera", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsExpedienteCarrera")
    @ResponseWrapper(localName = "sgaws_expediente_carreraResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsExpedienteCarreraResponse")
    public String sgawsExpedienteCarrera(
        @WebParam(name = "cedula", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String cedula,
        @WebParam(name = "id_carrera", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String idCarrera);

    /**
     * 
     * 	Este metodo recibe como parametro de entrada el id de la Oferta Academica.
     * 	Retorna las fechas de matriculas de una oferta academica de acuerdo a la informacion ingresada.
     * 	Los datos de retorno son:
     * 	La descripcion de la Oferta Academica, tipo de matricula, fecha de inicio de matriculas y la fecha de fin de matriculas
     * 	    La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     * 	
     * 
     * @param idOferta
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_fechas_matriculaoa", action = "sgaws_fechas_matriculaoa")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_fechas_matriculaoa", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsFechasMatriculaoa")
    @ResponseWrapper(localName = "sgaws_fechas_matriculaoaResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsFechasMatriculaoaResponse")
    public String sgawsFechasMatriculaoa(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String idOferta);

    /**
     * 
     *             @autor: patovala
     *             Obtener toda la informacion de una matricula a partid de su id
     *         
     * 
     * @param mid
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_info_matricula", action = "sgaws_info_matricula")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_info_matricula", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsInfoMatricula")
    @ResponseWrapper(localName = "sgaws_info_matriculaResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsInfoMatriculaResponse")
    public String sgawsInfoMatricula(
        @WebParam(name = "mid", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String mid);

    /**
     * 
     *         @autor: dmunoz.
     *         @description: Verifica si un estudiante esta matriculado actualmente en alguna carrera.
     *         @param cedula: Cedula del estudiante.
     *         @return true o false segun sea el caso.
     *         
     * 
     * @param cedula
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_is_matriculado", action = "sgaws_is_matriculado")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_is_matriculado", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsIsMatriculado")
    @ResponseWrapper(localName = "sgaws_is_matriculadoResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsIsMatriculadoResponse")
    public String sgawsIsMatriculado(
        @WebParam(name = "cedula", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String cedula);

    /**
     * 
     * 	Este metodo recibe como parametro de entrada el id de una carrera y el id de la oferta academica.
     * 	Retorna los datos del estudiante, datos de la carrera, datos del modulo, datos del paralelo y el registro de acreditacion del estudiante de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Nombres del estudiante, apellidos del estudiante, nombre de la carrera, modalidad de la carrera, numero de modulo, nombre del paralelo, el estado de la matricula, nombnre de la unidad y la acreditacion de la unidad
     * 	Obtener los datos academicos de un estudiante con sus notas
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     * 	
     * 
     * @param idOferta
     * @param idCarrera
     * @param cedula
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_notas_estudiante", action = "sgaws_notas_estudiante")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_notas_estudiante", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsNotasEstudiante")
    @ResponseWrapper(localName = "sgaws_notas_estudianteResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsNotasEstudianteResponse")
    public String sgawsNotasEstudiante(
        @WebParam(name = "cedula", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String cedula,
        @WebParam(name = "id_carrera", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String idCarrera,
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String idOferta);

    /**
     * 
     * 	Este metodo recibe como parametro de entrada el id del periodo lectivo.
     * 	Retorna una lista de las ofertas academicas de acuerdo al periodo lectivo ingresado.
     * 	Los datos de retorno son:
     * 	id, descripcion, fecha_inicio_clases, fecha_fin_clases
     *         La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     * 	
     * 
     * @param idPeriodo
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_ofertas_academicas", action = "sgaws_ofertas_academicas")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_ofertas_academicas", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsOfertasAcademicas")
    @ResponseWrapper(localName = "sgaws_ofertas_academicasResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsOfertasAcademicasResponse")
    public String sgawsOfertasAcademicas(
        @WebParam(name = "id_periodo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String idPeriodo);

    /**
     * 
     * 	Este metodo no recibe ningun parametro de entrada.
     * 	Retorna una lista con los datos de todos los periodo lectivos.
     * 	Los datos de retorno son:
     * 	id y descripcion
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     * 	
     * 
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_periodos_lectivos", action = "sgaws_periodos_lectivos")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_periodos_lectivos", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsPeriodosLectivos")
    @ResponseWrapper(localName = "sgaws_periodos_lectivosResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsPeriodosLectivosResponse")
    public String sgawsPeriodosLectivos();

    /**
     * 
     * 	Este metodo recibe como parametro de entrada el id de un Paralelo.
     * 	Retorna datos de las carrera, modulo, paralelo y el plan de estudio del paralelo a la informacion ingresada.
     * 	Los datos que retornan son:
     * 	Nombre de la carrera, nombre del modulo, numero del modulo, nombre del paralelo, el nombre del plan de estudio, el nombre, duracion, creditos y la obligatoriedad de cada unidad pertenececinete al plan de estudio
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     * 	
     * 
     * @param idParalelo
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_plan_estudio", action = "sgaws_plan_estudio")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_plan_estudio", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsPlanEstudio")
    @ResponseWrapper(localName = "sgaws_plan_estudioResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsPlanEstudioResponse")
    public String sgawsPlanEstudio(
        @WebParam(name = "id_paralelo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String idParalelo);

    /**
     * 
     * 	Este metodo recibe como parametro de entrada el id de la oferta academica y la cedula del estudiante.
     * 	Retorna datos de la oferta academica, datos del estudiante y datos de la matricula de acuerdo a la informacion ingresada.
     * 	Los datos que retorna son:
     * 	Descricion de la oferta academica, nombres del estudiante, apellidos del estudiante, la nota de la matricula, el porcentaje de asistencia y el estado de la matricula.
     *         La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     *         
     * 
     * @param idOferta
     * @param cedula
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_reporte_matricula", action = "sgaws_reporte_matricula")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_reporte_matricula", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsReporteMatricula")
    @ResponseWrapper(localName = "sgaws_reporte_matriculaResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsReporteMatriculaResponse")
    public String sgawsReporteMatricula(
        @WebParam(name = "id_oferta", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String idOferta,
        @WebParam(name = "cedula", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String cedula);

    /**
     * 
     * 	autor: miltonlab
     * 	fecha: junio 2012
     * 	Este metodo fue creado inicialmente por la necesidad del Sistema de Evalacion Docente de
     * contar rapidamente con la informacion academica completa
     * 	de los estudiantes, docentes y unidades. Esta basado en el metodo sgaws_plan_estudio.
     * 	Retorna a mas de nombre de la carrera, nombre del modulo, numero del modulo, nombre del paralelo, el plan de estudio
     * 	con la siguiente informacion de las unidades "horarios_semana":
     * 	id_horario_semana, unidad, horas, creditos, obligatoria, inicio, fin, cedula del docente,
     * nombres del docente, apellidos del docente, titulo del docente
     * 	La salida de este metodo se encuentra formateada con JSON, por lo que para su utilizacion el resultado debe de ser decodificado con JSON
     * 	
     * 
     * @param idParalelo
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "sgaws_unidades_docentes_paralelo", action = "sgaws_unidades_docentes_paralelo")
    @WebResult(name = "result", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
    @RequestWrapper(localName = "sgaws_unidades_docentes_paralelo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsUnidadesDocentesParalelo")
    @ResponseWrapper(localName = "sgaws_unidades_docentes_paraleloResponse", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types", className = "ec.edu.unl.ws.sgaws.wsacademica.soap.types.SgawsUnidadesDocentesParaleloResponse")
    public String sgawsUnidadesDocentesParalelo(
        @WebParam(name = "id_paralelo", targetNamespace = "http://ws.unl.edu.ec/sgaws/wsacademica/soap/types")
        String idParalelo);

}
