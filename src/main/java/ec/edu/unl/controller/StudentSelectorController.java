package ec.edu.unl.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import ec.edu.unl.DAO.AcademicDAO;
import ec.edu.unl.DAO.OfferDAO;
import ec.edu.unl.DAO.StudentCareerDAO;
import ec.edu.unl.DAO.StudentDAO;
import ec.edu.unl.DAO.StudentModuleDAO;
import ec.edu.unl.entity.Offer;
import ec.edu.unl.entity.Student;
import ec.edu.unl.operations.Operations;
import ec.edu.unl.utilities.academic.ModuleParallelUtil;
import ec.edu.unl.utilities.academic.StudentCareerUtil;

public class StudentSelectorController {

	static HttpServletRequest httpServletRequest;
	static FacesContext faceContext;
	FacesMessage facesMessage;
	static Flash flash;

	@Autowired
	@Qualifier("academicDAOImpl")
	AcademicDAO academicDAO;

	@Autowired
	@Qualifier("operationsImpl")
	Operations operations;
	
    @Autowired
    @Qualifier("offerDAOImpl")
    OfferDAO offerDAO;
    
    @Autowired
    @Qualifier("studentDAOImpl")
    StudentDAO studentDAO;
    
    @Autowired
    @Qualifier("studentCareerDAOImpl")
    StudentCareerDAO studentCareerDAO;
    
    @Autowired
    @Qualifier("studentModuleDAOImpl")
    StudentModuleDAO studentModuleDAO;

    private Map<String, String> listAcademicOffer;
    private Map<String, String> listCareer;
    private Map<String, String> listModulesParallel;
    
	private String offer;
	private String career;
	private String moduleParallel;
	
	private long idStudent;
	private long idOffer;
	private long idCareer;

	String msgTeacher;
	
	public StudentSelectorController(){
		
	}

	
    public void init(){
    	faceContext = FacesContext.getCurrentInstance();
		httpServletRequest = (HttpServletRequest) faceContext.getExternalContext().getRequest();
		msgTeacher =  "Aun no existe suficiente información"
        		+ " en base de datos. Solicite al docente que ingrese "
        		+ "información al sistema";
		
    }
    
	public void loadDataStudent(){
		/**recupera id del estudiante*/
		init();
		Student stud = (Student) httpServletRequest.getSession().getAttribute("studentSession");	
		idStudent = studentDAO.returnStudentId(stud.getIdentification_card());
	}
	
	public void loadOffers(){
		
		listAcademicOffer = new TreeMap<String, String>();
		List<Offer> listOfferRet = offerDAO.returnAllOffers();
		for(Offer off:listOfferRet){
			listAcademicOffer.put(off.getDescription(), off.getId()+"");
		}
	}
	
	public void loadListCareer(){
		init();
		
		if (!offer.equals("")) {
		
			loadDataStudent();
			
			listCareer = new HashMap<String, String>();		
			idOffer = Long.parseLong(offer);
			List<StudentCareerUtil> listStudCareers= studentCareerDAO.returnListStudentCareers(idOffer, idStudent);
			
			if(!(listStudCareers.isEmpty())){
				for(StudentCareerUtil sc:listStudCareers){
					listCareer.put(sc.getCareerName(), sc.getIdCareer()+"");
				}
			}else{
				msgWarn(msgTeacher);     
	           clearData();
			}
			
		}else{
            clearData();;   		
		}
		
	}
	
	public void loadModulesParallel(){
		init();
		
		if (!career.equals("")) {
			
			loadDataStudent();
			
			listModulesParallel = new HashMap<String, String>();	
			idCareer = Long.parseLong(career);
			List<ModuleParallelUtil> listModules = new ArrayList<ModuleParallelUtil>();
			listModules = studentModuleDAO.returnStudentModules(idOffer, idCareer, idStudent);
			
			if(!(listModules.isEmpty())){
				for(ModuleParallelUtil mp:listModules){
					String text = "Modulo " + mp.getModuleNumber()+" Paralelo " + mp.getParallel();
					listModulesParallel.put(text, mp.getIdModule()+"");
				}
			}else{
				msgWarn(msgTeacher);
	            clearData();
			}
		}else{
            clearData(); 
		}
	}

	public void clearData(){
        
        listAcademicOffer = new TreeMap<String, String>();
        listCareer = new HashMap<String, String>();
		listModulesParallel = new HashMap<String, String>();
        offer = "";
        career = "";
        moduleParallel = ""; 
	}
	
	public Map<String, String> getListAcademicOffer() {
		loadOffers();
		return listAcademicOffer;
	}

	public void msgWarn(String message){
		 facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,"Advertencia", message);
	     faceContext.addMessage(null, facesMessage);
	}

   
   /*getters - setters*/
   
	public void setListAcademicOffer(Map<String, String> listAcademicOffer) {
		this.listAcademicOffer = listAcademicOffer;
	}

	public Map<String, String> getListCareer() {
		return listCareer;
	}

	public void setListCareer(Map<String, String> listCareer) {
		this.listCareer = listCareer;
	}

	public Map<String, String> getListModulesParallel() {
		return listModulesParallel;
	}

	public void setListModulesParallel(Map<String, String> listModulesParallel) {
		this.listModulesParallel = listModulesParallel;
	}

	public String getOffer() {
		return offer;
	}

	public void setOffer(String offer) {
		this.offer = offer;
	}

	public String getCareer() {
		return career;
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public String getModuleParallel() {
		return moduleParallel;
	}
	public void setModuleParallel(String moduleParallel) {
		this.moduleParallel = moduleParallel;
	}
	
	

}
