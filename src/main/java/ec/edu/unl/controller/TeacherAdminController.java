package ec.edu.unl.controller;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.json.simple.JSONArray;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.TabChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import ec.edu.unl.DAO.AcademicDAO;
import ec.edu.unl.DAO.ConfigurationDAO;
import ec.edu.unl.DAO.MatterDAO;
import ec.edu.unl.DAO.ModuleDAO;
import ec.edu.unl.DAO.OfferDAO;
import ec.edu.unl.DAO.QualificationDAO;
import ec.edu.unl.DAO.ReferenceDAO;
import ec.edu.unl.DAO.StudentDAO;
import ec.edu.unl.DAO.TeacherDAO;
import ec.edu.unl.entity.Reference;
import ec.edu.unl.entity.Teacher;
import ec.edu.unl.operations.Conections;
import ec.edu.unl.operations.Operations;
import ec.edu.unl.utilities.academic.BasicParameterData;
import ec.edu.unl.utilities.academic.ConfigurationUtil;
import ec.edu.unl.utilities.academic.NumberParameterUtil;
import ec.edu.unl.utilities.academic.ParameterDetailUtil;
import ec.edu.unl.utilities.academic.ParameterEditUtil;
import ec.edu.unl.utilities.academic.ParameterQualificationUtil;
import ec.edu.unl.utilities.academic.StudentSpecificUtil;
import ec.edu.unl.utilities.academic.TeacherCareerUtil;
import ec.edu.unl.utilities.academic.TeacherModuleMatterUtil;
import ec.edu.unl.utilities.academic.TeacherModuleUtil;
import ec.edu.unl.utilities.academic.TeacherOfferUtil;

public class TeacherAdminController implements Serializable{

	@Autowired
	@Qualifier("operationsImpl")
	private Operations operations;

	@Autowired
	@Qualifier("moduleDAOImpl")
	private ModuleDAO moduleDAO;

	@Autowired
	@Qualifier("teacherDAOImpl")
	private TeacherDAO teacherDAO;

	@Autowired
	@Qualifier("studentDAOImpl")
	private StudentDAO studentDAO;

    @Autowired
    @Qualifier("referenceDAOImpl")
    ReferenceDAO referenceDAO;
    
	@Autowired
	@Qualifier("academicDAOImpl")
	private AcademicDAO academicDAO;
	
	@Autowired
	@Qualifier("qualificationDAOImpl")
	private QualificationDAO qualificationDAO;
	
	@Autowired
	@Qualifier("matterDAOImpl")
	private MatterDAO matterDAO;
	 
	@Autowired
    @Qualifier("offerDAOImpl")
    private OfferDAO offerDAO;
	
    @Autowired
    @Qualifier("configurationDAOImpl")
    private ConfigurationDAO configurationDAO;


	@Autowired
	@Qualifier("conectionImpl")
	private Conections conectionWS;

	static HttpServletRequest httpServletRequest;
	static FacesContext faceContext;
	FacesMessage facesMessage;
	static Flash flash;

	private long offerSelect;
	private long careerSelect;
	private long moduleSelect;
	private long matterSelect;
	private int val;
	private Integer matrixSize;
	private Date dateSelect;

	private String tableTitle;
	private String referenceData;
	private String teacherIC;
	private StudentSpecificUtil selectedStudent;
	private Teacher teacherSession;
	private Map.Entry<String, List<String>> entry ;
	private List<StudentSpecificUtil> listStudentSpecific;
	private List<TeacherModuleMatterUtil> listTeacherModuleMatters;
	
	
	private String parameterMatterSelect;
	private String parameterNumberSelect;
	private Map<String, String> listParameterNumber;
	private List<ParameterEditUtil> listStudentParameterGeneral;
	private List<ParameterDetailUtil> listStudentParameterDetail;
	private Map<StudentSpecificUtil, List<BasicParameterData>> listPresentationParameter;
	private Map<StudentSpecificUtil, List<BasicParameterData>> listPresentationParameterTemp;
	
	private String generalVar;
	
	///////////////
	
	private String offer;
	private String career;
	private String moduleParallel;
	
    private Map<String, String> listAcademicOffer;
    private Map<String, String> listCareer;
    private Map<String, String> listModulesParallel;
	
    private Date startOffer;
    private Date endOffer;
    
    private TabView messagesTab;
    
    private int activeTabIndex;
	
    private double paramAverage;
    
    
    private String msgEval;
    private String msgLess;
    private String msgCW;
    private String msgECW;
    private String msgPart;
    private String msgLab;
    
    private Double evaluationPerc;
    private Double lessonPerc;
    private Double classWorkPerc;
    private Double extraClassWorkPerc;
    private Double participationPerc;
    private Double laboratoryPerc;

    private Integer evaluationNumber;
    private Integer lessonNumber;
    private Integer classWorkNumber;
    private Integer extraClassWorkNumber;
    private Integer participationNumber;
    private Integer laboratoryNumber;
    
    List<ConfigurationUtil> listConf;
    private String msgNone;
    private String paramTitle;
    
	private String offerStr;
	private String careerStr;
	private String moduleStr;
	private String parallelStr;
	private String matterStr;
	
	private int check=0;
		
    public void init(){
    	faceContext = FacesContext.getCurrentInstance();
		httpServletRequest = (HttpServletRequest) faceContext.getExternalContext().getRequest();
    }
    
    public boolean checkConnection(){
    	boolean result = true;
    	try {	
            URL url = new URL("http://ws.unl.edu.ec/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.connect();
            if (con.getResponseCode() == 200){
            	result = true;
            }
        } catch (Exception exception) {
        	result = false;
            
        }
    	
    	return result;
    	
    }
    
	public TeacherAdminController() {	
		this.listTeacherModuleMatters = new ArrayList<TeacherModuleMatterUtil>();
		this.matrixSize = new Integer(0);
		this.listStudentSpecific = new ArrayList<StudentSpecificUtil>();
		this.dateSelect = new Date();
		this.selectedStudent = new StudentSpecificUtil();	
	}

	public void loadTeacherData(){
		
		init();
		
		listAcademicOffer = new TreeMap<String, String>();
	    listCareer = new TreeMap<String, String>();  
	    listModulesParallel = new TreeMap<String, String>(); 
	    teacherSession = (Teacher) httpServletRequest.getSession().getAttribute("teacherSession");
		
	}
	
	public void loadOffers(){
		
		loadTeacherData();
		
		List<TeacherOfferUtil> listTO = academicDAO.returnListTeacherOffer(teacherSession.getIdentification_card());
		for(TeacherOfferUtil data:listTO){
			listAcademicOffer.put(data.getOfferDescription(), data.getIdOffer()+""); 
		}
	}

	public void loadCareers(){
		
		loadTeacherData();
	
		List<TeacherCareerUtil> listTC = academicDAO.returnListTeacherCareer(teacherSession.getIdentification_card());
	
		for(TeacherCareerUtil data:listTC){
			listCareer.put(data.getCareerName(), data.getIdCareer()+""); 
		}		
	}
	
	public void loadModulesParallel(){
			
		loadTeacherData();
		
		List<TeacherModuleUtil> listTM = academicDAO.returnListTeacherModule(teacherSession.getIdentification_card());
		for(TeacherModuleUtil data:listTM){
			String text = "Modulo: "+data.getModuleNumber()+" Paralelo: "+data.getModuleParallel(); 
			listModulesParallel.put(text, data.getIdModule()+""); 
		}	
	}

	public void loadDataTableMatters(){
		init();
		
		if(offer.equals("")|| career.equals("")|| moduleParallel.equals("")){
			msgError("Faltan parametros por seleccionar");
		}else{
		
			loadTeacherData();
			
			long offerNew = Long.parseLong(offer);
			long careerNew = Long.parseLong(career);
			long moduleParallelNew = Long.parseLong(moduleParallel);
			listTeacherModuleMatters = new ArrayList<TeacherModuleMatterUtil>();
			List<TeacherModuleMatterUtil> lstTemp = academicDAO.returnTeacherModuleMatter(teacherSession.getIdentification_card(),offerNew,careerNew,moduleParallelNew);              
				
			for(TeacherModuleMatterUtil tmmu: lstTemp){		
				List<StudentSpecificUtil> lst2 =  qualificationDAO.returnListStudentSpecific(offerNew,careerNew,moduleParallelNew,tmmu.getIdMatter(),teacherSession.getIdentification_card());
				
				if(!lst2.isEmpty()){	
					listTeacherModuleMatters.add(tmmu);
				}else{
					continue;
				}
			}
					
			if(listTeacherModuleMatters.isEmpty() || listTeacherModuleMatters == null ){
				msgError("La combinación de datos es incorrecta. Por favor revise los parametros seleccionados");	
			}
		}
	}

	public String newRegistration() {
		
		init();

		if(!checkConnection()){
    		msgFatalConnection();
    		return "";
    	}else{
			flash = faceContext.getExternalContext().getFlash();
			msgInfo("Proceda a crear un nuevo registro");
			flash.setKeepMessages(true);
	
			return "techer_new_reg";
    	}
	}
	
	
	public String proccessConfig(){
		
		String matterName = matterDAO.returnMatterById(matterSelect).getName();
		Integer moduleNumber = moduleDAO.returnModuleById(moduleSelect).getNumber();
		String parallel = moduleDAO.returnModuleById(moduleSelect).getParallel();;
		String offerDescription = offerDAO.returnOfferById(offerSelect).getDescription();

		/**titulo de tabla*/
		tableTitle = matterName + " || Módulo " + moduleNumber + " Paralelo "+ parallel +" || "+ offerDescription;
		long idTeacher = teacherDAO.returnTeacherId(teacherIC);	
		listConf = configurationDAO.returnListConfigurations(offerSelect, careerSelect,moduleSelect, matterSelect, idTeacher);
		
		evaluationPerc = listConf.get(0).getPercentage();
		lessonPerc = listConf.get(1).getPercentage();
		classWorkPerc = listConf.get(2).getPercentage();
		extraClassWorkPerc = listConf.get(3).getPercentage();
		participationPerc = listConf.get(4).getPercentage();
		laboratoryPerc = listConf.get(5).getPercentage();
			
		evaluationNumber= listConf.get(0).getNumberDefault();
		lessonNumber = listConf.get(1).getNumberDefault();
		classWorkNumber = listConf.get(2).getNumberDefault();
		extraClassWorkNumber = listConf.get(3).getNumberDefault();
		participationNumber = listConf.get(4).getNumberDefault();
		laboratoryNumber = listConf.get(5).getNumberDefault();
		
		return "teacher_configuration";
	}
	
	public String loadConfiguration(){
		
		
		loadRequestParameters();
		
		String route = proccessConfig();
		
		return route;
	}
		
	public int returnNumberLastElement(String optionSelect){
		
		List<ParameterQualificationUtil> listParamQualification = qualificationDAO.returnQualificationParameter(listStudentSpecific.get(0),optionSelect);	
		
		if(!listParamQualification.isEmpty()){
			int [] vector = new int[listParamQualification.size()];
			int i=0;
			for(ParameterQualificationUtil pqu: listParamQualification){
				vector[i] = pqu.getNumParameter();
			}
			int resp = operations.calculateMax(vector);		
			
			return resp;
		}else{
			return 0;
		}
	}
		
	public String saveConfiguration(){
		init();
		/**lista de estudiantes*/
		listStudentSpecific = qualificationDAO.returnListStudentSpecific(offerSelect,careerSelect,moduleSelect,matterSelect,teacherIC);
		
		String msgNumberElem1 = "Ya existen ";
		String msgNumberElem2 = " que se subieron al sistema. No se puede especificar una cantidad inferior";	
		String msgNumberElem3 = " que se subieron al sistema. No se puede especificar un porcentaje de 0%";
		
		int numEvals = returnNumberLastElement("evaluation");		
		if(evaluationNumber < numEvals ){
	        msgError(msgNumberElem1 + "evaluaciones" + msgNumberElem2);
	        return "";
		}
		
		int numLess = returnNumberLastElement("lesson");
		if(lessonNumber < numLess ){
			msgError(msgNumberElem1 + "lecciones" + msgNumberElem2);
	        return "";
		}else{
			if((lessonPerc == 0.0) && (numLess > 0) ){
				msgError(msgNumberElem1 + "lecciones" + msgNumberElem3);
		        return "";
			}
		}
	
		int numCW = returnNumberLastElement("classWork");
		if(classWorkNumber < numCW ){
			msgError( msgNumberElem1 + "trabajos autónomos" + msgNumberElem2);  
	        return "";
		}else{
			if((classWorkPerc == 0.0) && (numCW > 0) ){
				msgError(msgNumberElem1 + "trabajos autónomos" + msgNumberElem3);
		        return "";
			}
		}
			
		int numECW = returnNumberLastElement("extraClassWork");
		if(extraClassWorkNumber < numECW ){
			msgError(msgNumberElem1 + "deberes - tareas" + msgNumberElem2);
	        return "";
		}else{
			if((extraClassWorkPerc == 0.0) && (numECW > 0) ){
				msgError(msgNumberElem1 + "deberes - tareas" + msgNumberElem3);
		        return "";
			}
		}
		
		int numPart = returnNumberLastElement("participation");
		if(participationNumber < numPart ){
			msgError(msgNumberElem1 + "participaciones" + msgNumberElem2);
	        return "";
		}else{
			if((participationPerc == 0.0) && (numPart > 0) ){
				msgError(msgNumberElem1 + "participaciones" + msgNumberElem3);
		        return "";
			}
		}
		
		int numLab = returnNumberLastElement("laboratory");
		if(laboratoryNumber < numLab ){
			msgError(msgNumberElem1 + "laboratorios" + msgNumberElem2);
	        return "";
		}else{
			if((laboratoryPerc == 0.0) && (numLab > 0) ){
				msgError(msgNumberElem1 + "laboratorios" + msgNumberElem3);
		        return "";
			}
		}
		
		String msg1 = "Se ha especificado 0 cantidad de ";
		String msg2 = " por lo que el porcentaje debe ser 0%";
		String msg3 = "Se ha especificado 0% de peso para ";
		String msg4 = " por lo que la cantidad debe ser 0";
		
		if(lessonNumber == 0 && lessonPerc != 0.0){
			msgError(msg1 + "lecciones" + msg2); 
	        return "";
		}
		
		if(lessonNumber != 0 && lessonPerc == 0.0){
			msgError( msg3 +"lecciones"+msg4); 
	        return "";
		}
		
		
		if(classWorkNumber == 0 && classWorkPerc != 0.0){
			msgError(msg1 + "trabajos autónomos" + msg2);
	        return "";
		}
		
		if(classWorkNumber != 0 && classWorkPerc == 0.0){
			msgError(msg3 + "trabajos autónomos" + msg4);
	        return "";
		}
		
		if(extraClassWorkNumber == 0 && extraClassWorkPerc != 0.0){
			msgError(msg1 + "deberes - tareas" + msg2);
	        return "";
		}
		
		if(extraClassWorkNumber != 0 && extraClassWorkPerc == 0.0){
			msgError(msg3 + "deberes - tareas" + msg4);
	        return "";
		}
		
		if(participationNumber == 0 && participationPerc != 0.0){
			msgError(msg1 + "participaciones" + msg2);
	        return "";
		}
		
		if(participationNumber != 0 && participationPerc == 0.0){
			msgError(msg3 + "participaciones" + msg4);
	        return "";
		}	
		
		if(laboratoryNumber == 0 && laboratoryPerc != 0.0){
			msgError(msg1 + "laboratorios - talleres" + msg2);
	        return "";
		}
		
		if(laboratoryNumber != 0 && laboratoryPerc == 0.0){
			msgError(msg3 + "laboratorios - talleres" + msg4);
	        return "";
		}
		
		
		listConf.get(0).setPercentage(evaluationPerc);
		listConf.get(0).setNumberDefault(evaluationNumber);
		listConf.get(1).setPercentage(lessonPerc);
		listConf.get(1).setNumberDefault(lessonNumber);
		listConf.get(2).setPercentage(classWorkPerc);
		listConf.get(2).setNumberDefault(classWorkNumber);
		listConf.get(3).setPercentage(extraClassWorkPerc);
		listConf.get(3).setNumberDefault(extraClassWorkNumber);
		listConf.get(4).setPercentage(participationPerc);
		listConf.get(4).setNumberDefault(participationNumber);
		listConf.get(5).setPercentage(laboratoryPerc);
		listConf.get(5).setNumberDefault(laboratoryNumber);
		
		
		double perc=0.0;
		for(ConfigurationUtil cu : listConf){
			perc=perc + (cu.getPercentage() * 100);
		}
		if(perc != 100){
			msgError("El total de porcentajes debe ser 100%");
			return "";
		}
		configurationDAO.saveAllNewConfiguration(listConf);
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,"Hecho", "Informacion Actualizada Correctamente");
        FacesContext.getCurrentInstance().addMessage(null, msg);
		return "";
	}

	
	public String newLoadParametersQualification() {
		
		initParameterData();
		finalParameterData();
		
		return "teacher_qualification_data";
	}
	
	public void initParameterData(){
		this.parameterMatterSelect = "";
		this.parameterNumberSelect = "";
		this.listPresentationParameter = new LinkedHashMap<StudentSpecificUtil, List<BasicParameterData>>();	
		this.listParameterNumber = new LinkedHashMap<String, String>();		
		this.listStudentParameterGeneral = new ArrayList<ParameterEditUtil>();

		this.matrixSize = new Integer(0);
		this.listStudentSpecific = new ArrayList<StudentSpecificUtil>();
		
		this.messagesTab = new TabView();

		msgNone = "";
	}
	
	public void finalParameterData(){
		init();
		int failConnection = 0;
		
		/**Se asigna valor inicial y final de
		 * fechas segun la oferta seleccionada*/
		startOffer = offerDAO.returnStartDateOfferById(offerSelect);
		endOffer = offerDAO.returnEndDateOfferById(offerSelect);
		
		/**se recupera configuraciones*/
		long idTeacher = teacherDAO.returnTeacherId(teacherIC);
		listConf = configurationDAO.returnListConfigurations(offerSelect, careerSelect,moduleSelect, matterSelect, idTeacher);
		String matterName = matterDAO.returnMatterById(matterSelect).getName();
		Integer moduleNumber = moduleDAO.returnModuleById(moduleSelect).getNumber();
		String parallel = moduleDAO.returnModuleById(moduleSelect).getParallel();;
		String offerDescription = offerDAO.returnOfferById(offerSelect).getDescription();

		/**titulo de tabla*/
		tableTitle = matterName + " || Módulo "+ moduleNumber + " Paralelo "+ parallel +" || "+ offerDescription;

		/**lista de estudiantes segun la materia que selecciona 
		 * el docente*/
		listStudentSpecific = qualificationDAO.returnListStudentSpecific(offerSelect,careerSelect,moduleSelect,matterSelect,teacherIC);
		
		int i=1;
		
		/**proceso para recuperar y formatear los nombres
		 * de estudiantes de los Web Services*/
		for(StudentSpecificUtil data:listStudentSpecific){
			
			//////// en caso de fallo de conexion a los WS //////////
			
			String studentID = "";
			String studentData = "";
			
			try {
				
				studentID = studentDAO.returnStudentById(data.getIdStudent()).getIdentification_card();				
				studentData = conectionWS.conectionPersonalWS().sgawsDatosEstudiante(studentID);
				JSONArray array = operations.processJSON(studentData);
				data.setRowCount(i);
				data.setStudentName(operations.generateNameStudent(array.get(1)+"", array.get(2)+""));	
				i++;
				
			} catch (Exception e) {
				
				studentID = studentDAO.returnStudentById(data.getIdStudent()).getIdentification_card();				
				data.setRowCount(i);
				data.setStudentName(operations.generateNameStudent(studentID, ""));	
				i++;
				failConnection++;
			}

			////////////////////////////////////////////////
		}
		
		loadParameters("evaluation");
		generalVar = "evaluation";
		activeTabIndex = 0;
		
		if(failConnection>0){
			msgFatalConnection2();
		}
	}
	
	public void onSpinnerRefresh(){
		
	}
	public String loadParametersQualification() {
		
		initParameterData();
		
		loadRequestParameters();
		
		finalParameterData();
		
		return "teacher_qualification_data";
	}

	public void exportPDF(ActionEvent event){
		init();
		
		List<StudentSpecificUtil> listNew = new ArrayList<StudentSpecificUtil>();
		Map<String,Object> parameters= new HashMap<String,Object>();
		String userName = httpServletRequest.getSession().getAttribute("teacherSession")+"";
		parameters.put("TEACHER",userName);
		parameters.put("OFFER", offerStr);
		parameters.put("CAREER", careerStr);
		parameters.put("MATTER", matterStr);
		parameters.put("MODULE", moduleStr);
		parameters.put("PARALLEL", parallelStr);
		
		for(StudentSpecificUtil data:listStudentSpecific ){
			StudentSpecificUtil stud = new StudentSpecificUtil();
			
			stud.setRowCount(data.getRowCount());
			stud.setStudentName(data.getStudentName());
			
			stud.setAverageEval(operations.roundNumber(data.getAverageEval(), 2));
			parameters.put("EVAL_PERC",msgEval);
			parameters.put("LESS_PERC", msgLess);
			parameters.put("CW_PERC", msgCW);
			parameters.put("ECW_PERC", msgECW);
			parameters.put("PART_PERC", msgPart);
			parameters.put("LAB_PERC", msgLab);
			
			parameters.put("T_EVAL","Eval.");
			
			if(!msgLess.equals("")){
				stud.setAverageLess(operations.roundNumber(data.getAverageLess(), 2));
				parameters.put("T_LESS","Lec.");
			}else{
				stud.setAverageLess(-1.0);
				parameters.put("T_LESS","");
			}
			
			
			if(!msgCW.equals("")){
				stud.setAverageCW(operations.roundNumber(data.getAverageCW(), 2));
				parameters.put("T_CW","T.A.");
			}else{
				stud.setAverageCW(-1.0);
				parameters.put("T_CW","");
			}
			
			
			if(!msgECW.equals("")){
				stud.setAverageECW(operations.roundNumber(data.getAverageECW(), 2));
				parameters.put("T_ECW","Deb.");
			}else{
				stud.setAverageECW(-1.0);
				parameters.put("T_ECW","");
			}
			
			if(!msgPart.equals("")){
				stud.setAveragePart(operations.roundNumber(data.getAveragePart(), 2));
				parameters.put("T_PART","Part.");
			}else{
				stud.setAveragePart(-1.0);
				parameters.put("T_PART","");
			}
			
			if(!msgLab.equals("")){
				stud.setAverageLab(operations.roundNumber(data.getAverageLab(), 2));
				parameters.put("T_LAB","Lab.");
			}else{
				stud.setAverageLab(-1.0);
				parameters.put("T_LAB","");
			}	
			
			stud.setTotal(operations.roundNumber(data.getTotal(),2));
			
			listNew.add(stud);
		}
		
		
		
		

		

		
		
		try {
			File jasper = new File(faceContext.getExternalContext().getRealPath("/reports/generalAverage.jasper"));
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(),parameters, new JRBeanCollectionDataSource(listNew));
			HttpServletResponse response = (HttpServletResponse) faceContext.getExternalContext().getResponse();
			response.addHeader("Content-disposition","attachment; filename=Promedios.pdf");
			ServletOutputStream stream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
			stream.flush();
			stream.close();
			faceContext.responseComplete();
			System.out.println("terminado");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadRequestParameters(){
		/**recuperacion de parametros*/
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		
		offerSelect = Long.parseLong(params.get("offerSelect"));
		careerSelect = Long.parseLong(params.get("careerSelect"));
		moduleSelect = Long.parseLong(params.get("moduleSelect"));
		matterSelect = Long.parseLong(params.get("matterSelect"));
		teacherIC = params.get("teacherIC");
		offerStr = params.get("offerStr");
		careerStr = params.get("careerStr");
		moduleStr = params.get("moduleStr");
		parallelStr = params.get("parallelStr");
		matterStr = params.get("matterStr");
		
		String matterName = matterDAO.returnMatterById(matterSelect).getName();
		Integer moduleNumber = moduleDAO.returnModuleById(moduleSelect).getNumber();
		String parallel = moduleDAO.returnModuleById(moduleSelect).getParallel();;
		String offerDescription = offerDAO.returnOfferById(offerSelect).getDescription();

		/**titulo de tabla*/
		tableTitle = matterName + " || Módulo "+ moduleNumber + " Paralelo "+ parallel +" || "+ offerDescription;
	}
	
	
	public void initAvergeData(){
		this.listStudentSpecific = new ArrayList<StudentSpecificUtil>();
		
	}
	
	public void finalAverageData(){
		init();
		int failConnection = 0;
		
		startOffer = offerDAO.returnStartDateOfferById(offerSelect);
		endOffer = offerDAO.returnEndDateOfferById(offerSelect);
		
		/**configuraciones*/
		long idTeacher = teacherDAO.returnTeacherId(teacherIC);
		listConf = configurationDAO.returnListConfigurations(offerSelect, careerSelect,moduleSelect, matterSelect, idTeacher);
		
		/**lista de estudiantes*/
		listStudentSpecific = qualificationDAO.returnListStudentSpecific(offerSelect,careerSelect,moduleSelect,matterSelect,teacherIC);
		
		int i=1;
		/**proceso para recuperar y formatear los nombres
		 * de estudiantes de los Web Services*/
		for(StudentSpecificUtil data:listStudentSpecific){	
			
			String studentID = "";
			String studentData = "";
			
			try {
				studentID = studentDAO.returnStudentById(data.getIdStudent()).getIdentification_card();				
				studentData = conectionWS.conectionPersonalWS().sgawsDatosEstudiante(studentID);
				JSONArray array = operations.processJSON(studentData);
				data.setRowCount(i);
				data.setStudentName(operations.generateNameStudent(array.get(1)+"", array.get(2)+""));	
				i++;
		
			} catch (Exception e) {
				
				studentID = studentDAO.returnStudentById(data.getIdStudent()).getIdentification_card();				
				data.setRowCount(i);
				data.setStudentName(operations.generateNameStudent(studentID, ""));	
				i++;
				failConnection++;
			}
		
		
		}
		
		
		
		
			
		
		

		for(StudentSpecificUtil stud: listStudentSpecific){	
			double perc0 = listConf.get(0).getPercentage();
			stud.setAverageEval(returnAverageForStudent(stud,"evaluation",perc0));			
			double perc1 = listConf.get(1).getPercentage();
			stud.setAverageLess(returnAverageForStudent(stud,"lesson",perc1));
			double perc2 = listConf.get(2).getPercentage();
			stud.setAverageCW(returnAverageForStudent(stud,"classWork",perc2));	
			double perc3 = listConf.get(3).getPercentage();
			stud.setAverageECW(returnAverageForStudent(stud,"extraClassWork",perc3));
			double perc4 = listConf.get(4).getPercentage();
			stud.setAveragePart(returnAverageForStudent(stud,"participation",perc4));
			double perc5 = listConf.get(5).getPercentage();
			stud.setAverageLab(returnAverageForStudent(stud,"laboratory",perc5));
			
			stud.setTotal(stud.getAverageEval()+
					stud.getAverageLess()+
					stud.getAverageCW()+
					stud.getAverageECW()+
					stud.getAveragePart()+
					stud.getAverageLab());
		}
		
		msgEval = "";
		msgLess = "";
		msgCW = "";
		msgECW = "";
		msgPart = "";
		msgLab = "";
		
		msgEval =  operations.roundNumber((listConf.get(0).getPercentage() * 100),2) +"%";
		
		if(listConf.get(1).getPercentage() != 0.00){
			msgLess = operations.roundNumber((listConf.get(1).getPercentage() * 100),2) +"%";
		}
		
		if(listConf.get(2).getPercentage() != 0.00){
			msgCW = operations.roundNumber((listConf.get(2).getPercentage() * 100),2) +"%";
		}
		
		if(listConf.get(3).getPercentage() != 0.00){
			msgECW = operations.roundNumber((listConf.get(3).getPercentage() * 100),2) +"%";
		}
		
		if(listConf.get(4).getPercentage() != 0.00){
			msgPart = operations.roundNumber((listConf.get(4).getPercentage() * 100),2) +"%";
		}
		
		if(listConf.get(5).getPercentage() != 0.00){
			msgLab = operations.roundNumber((listConf.get(5).getPercentage() * 100),2) +"%";
		}
		
		
		if(failConnection>0){
			msgFatalConnection2();
		}

	}
	
	public String newLoadDetailAverageMatter(){
		initAvergeData();
		finalAverageData();
		
		return "teacher_detail_average";
	}
	
	public String loadDetailAverageMatter(){
		
		initAvergeData();
		loadRequestParameters();
		finalAverageData();
		
		return "teacher_detail_average";
	}
		
	public double returnAverageForStudent(StudentSpecificUtil stud,String optionSelect, Double percentage){
		
		 List<ParameterQualificationUtil> listParamQualification = qualificationDAO.returnQualificationParameter(stud,optionSelect);
		 
		 if(!listParamQualification.isEmpty()){
		 
			 double sum =0.0;
			 int count =0;
			 double resp =0.0;
			 for(ParameterQualificationUtil data:listParamQualification){	
					sum=sum+data.getQualification();
					count++;
			}
			 	
			resp = (sum / count) * percentage;
			return resp;
		 }else{
			 return 0.00;
		 }
	}

	public void saveQualificationOnly(String newVal){
		
		init();
		
		/*recuperacion de parametros*/
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();		
		String paramSelect = params.get("paramSelect");
		
		
    	for(int i=0;i<=matrixSize;i++){
    		
    		List<ParameterEditUtil> listFinal = new ArrayList<ParameterEditUtil>();

    		Iterator it = listPresentationParameter.keySet().iterator();
	    	while(it.hasNext()){
	    	  StudentSpecificUtil key = (StudentSpecificUtil) it.next();
	    	  
	    	  ParameterEditUtil peu = new ParameterEditUtil();
	    	  peu.setIdParameter(listPresentationParameter.get(key).get(i).getIdParameter());
	    	  peu.setQualification(Double.valueOf(listPresentationParameter.get(key).get(i).getQualification()));
	    	  listFinal.add(peu);
	    	
	    	}
	    	
	    	/*lego de la edicion actualiza información*/
			qualificationDAO.updateSelectedQualificationParameter(listFinal,paramSelect);
			//loadParameters(paramSelect);
    	}
    	
    	FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Nueva calificación ingresada: "+ newVal);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    	
	}
	
	public void onCellEditNew(CellEditEvent event) {
    	init();
		
				
    	String newVal = String.valueOf(event.getNewValue());
    	String oldVal = String.valueOf(event.getOldValue());
    	
    	
    	
    	if(newVal.equals("")){
    	
        	for(int i=0;i<=matrixSize;i++){
        		Iterator it = listPresentationParameter.keySet().iterator();
    	    	while(it.hasNext()){
    	    	  StudentSpecificUtil key = (StudentSpecificUtil) it.next();
    	    	  if((listPresentationParameter.get(key).get(i).getQualification()+"").equals("")){
    	    		  listPresentationParameter.get(key).get(i).setQualification(0.0); 
    	    	  }
    	    	}
        	}
        	
        }

    	saveQualificationOnly(newVal);
	

    }
    
	public void onCellEdit(CellEditEvent event) {
    	init();

        String oldValue = String.valueOf(event.getOldValue());
        String newValue = String.valueOf(event.getNewValue());
        
        if(!newValue.equals("") && !newValue.equals(oldValue)) {
        	 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Nueva calificación: ", newValue);
             FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
    }
    
	public void onTabChange(TabChangeEvent event) {
		init();		
		/*se recupera configuraciones*/
		
		check=0;
	    
		String msg1 = "¡No se calificará este parámetro!. Revise sus configuraciones";
		msgNone = "";
		String idTab = event.getTab().getId();
		if(idTab.equals("tabeval")){
			generalVar = "evaluation";
			loadParameters("evaluation");
			
			TabView tv = (TabView) event.getComponent();
		    this.setActiveTabIndex(tv.getActiveIndex());	
		}
		if(idTab.equals("tabless")){
			generalVar = "lesson";	
			loadParameters("lesson");
			
			
			TabView tv = (TabView) event.getComponent();
		
		    this.setActiveTabIndex(tv.getActiveIndex());
		    double perc = listConf.get(1).getPercentage();
		    if(perc == 0.00){
		    	msgNone =msg1;
		    	check = 1;
		    }
		}
		if(idTab.equals("tabclasswork")){
			generalVar = "classWork";
			loadParameters("classWork");
			
			TabView tv = (TabView) event.getComponent();
		    this.setActiveTabIndex(tv.getActiveIndex());
		    double perc = listConf.get(2).getPercentage();
		    if(perc == 0.00){
		    	msgNone =msg1;
		    	check = 1;
		    }
			
		}
		if(idTab.equals("tabextrawork")){
			generalVar = "extraClassWork";
			loadParameters("extraClassWork");
			
			TabView tv = (TabView) event.getComponent();
		    this.setActiveTabIndex(tv.getActiveIndex());
		    double perc = listConf.get(3).getPercentage();
		    if(perc == 0.00){
		    	msgNone =msg1;
		    	check = 1;
		    }
			
		}
		if(idTab.equals("tabparticip")){
			generalVar = "participation";
			loadParameters("participation");
		
			TabView tv = (TabView) event.getComponent();
		    this.setActiveTabIndex(tv.getActiveIndex());
		    double perc = listConf.get(4).getPercentage();
		    if(perc == 0.00){
		    	msgNone =msg1;
		    	check = 1;
		    }
		}
		
		if(idTab.equals("tablab")){
			generalVar = "laboratory";
			loadParameters("laboratory");
			
			TabView tv = (TabView) event.getComponent();
		    this.setActiveTabIndex(tv.getActiveIndex());
		    double perc = listConf.get(5).getPercentage();
		    if(perc == 0.00){
		    	msgNone =msg1;
		    	check = 1;
		    }
		}
		msgInfo("Se ha seleccionado "+ event.getTab().getTitle());
	}
	
	public void loadParameters(String optionSelect){
		
		int [] vector = new int[listStudentSpecific.size()];
		int i=0;
		
		/*se hace recorrido por cada estudiante de la lista*/
		for(StudentSpecificUtil stud: listStudentSpecific){
			List<ParameterQualificationUtil> listParamQualification = new ArrayList<ParameterQualificationUtil>();	
			listParamQualification = qualificationDAO.returnQualificationParameter(stud,optionSelect);	
			List<BasicParameterData> qualificationIndv = new ArrayList<BasicParameterData>();

			double sum =0;
			int count =0;
			for(ParameterQualificationUtil data2:listParamQualification){
				BasicParameterData bpd = new BasicParameterData();
				bpd.setIdParameter(data2.getIdParameter());
				bpd.setQualification(data2.getQualification());
				qualificationIndv.add(bpd);
				sum=sum+data2.getQualification();
				count++;
			}
			
			/*promedios*/
			double average = sum/count;
			stud.setAverage(average);
			vector[i] = qualificationIndv.size();
			
			/*se agrega al mapa el nombre del estudiante y la lista
			 * de calificaciones*/
			listPresentationParameter.put(stud, qualificationIndv);
			i++;
		}
		/*tamaño para lista de calific*/
		matrixSize=(operations.calculateMax(vector)-1);
	}
	
	public String redirectNewParameter(){
		
		init();
		
		/*recuperacion de parametros*/
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();		
		String paramSelect = params.get("paramSelect");
		
		/*Redirecciona hacia la pagina donde se creará una nueva
		 * evaluación, se asignan valores iniciales a los objetos
		 * de la lista*/
		for(StudentSpecificUtil data:listStudentSpecific){
			data.setQualification(0.00);
			data.setAnnotation("");
		}
		
		/*Verifica el rango es que se
		 * encuentra la fecha del sistema
		 * y los compara con los de la oferta
		 * segun esos datos presenta la informacion*/
		Date dateSystem = operations.returnSystemDate();
		
		if(!(dateSystem.after(startOffer) && dateSystem.before(operations.addDate(endOffer,1)))){
			if(dateSystem.compareTo(startOffer) != 0){
					dateSelect = startOffer;	
			}
		}
		


		String route="";
		
		String msg1 = "Se ha llegado al límite de ";
		String msg2 = " Revise sus configuraciones";
		if(paramSelect.equals("evaluation")){

			int val = listConf.get(0).getNumberDefault();
			if((matrixSize+1)==val){
				 msgError(msg1 + "evaluaciones."+msg2);
	             return "";
			}else{
				route = "teacher_evaluation_new";
			}
			
		
		}
		
		if(paramSelect.equals("lesson")){
			
			int val = listConf.get(1).getNumberDefault();
			if((matrixSize+1)==val){
				msgError(msg1 + "lecciones." + msg2);
				return "";
			}else{
				route = "teacher_lesson_new";
			}
			
		}
		if(paramSelect.equals("classWork")){
			int val = listConf.get(2).getNumberDefault();
			if((matrixSize+1)==val){
				msgError(msg1 + "trabajos autónomos." + msg2);
	            return "";
			}else{
				route = "teacher_classWork_new";
			}
		}	
		if(paramSelect.equals("extraClassWork")){
			int val = listConf.get(3).getNumberDefault();
			if((matrixSize+1)==val){
				msgError(msg1 + "deberes - tareas." + msg2);
	            return "";
			}else{
				route = "teacher_extraClassWork_new";
			}
		}		

		if(paramSelect.equals("participation")){
			int val = listConf.get(4).getNumberDefault();
			if((matrixSize+1)==val){
				msgError(msg1 + "participaciones." + msg2);
	            return "";
			}else{
				route = "teacher_participation_new";
			}
		}
		if(paramSelect.equals("laboratory")){
			int val = listConf.get(5).getNumberDefault();
			if((matrixSize+1)==val){
				msgError(msg1 + "laboratorios - talleres." + msg2);
	            return "";
			}else{
				route = "teacher_laboratory_new";
			}
		}
		return route;
	}
	
	public String processParameter(){
		
		init();
		
		/*recuperacion de parametros*/
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();		
		String paramSelect = params.get("paramSelect");
		
		/*referencia hacia la que apunta cada evaluaci�n
		 * que se ha creado*/
		Reference reference = new Reference();
		
		/*se recupera y guarda la inform ingresada*/
		reference.setGeneral_reference(referenceData);
		referenceDAO.saveReference(reference);
		
		long idTeacherNew = teacherDAO.returnTeacherId(teacherIC);

		/*el numero de la ultima evaluacion*/
		int numParam = qualificationDAO.returnMaxNumberParameter(careerSelect, offerSelect, moduleSelect, matterSelect,idTeacherNew,paramSelect);
		
		/*numero actual*/
		numParam = numParam + 1;
		
		/*se guarda el resto de datos*/
		qualificationDAO.saveAllQualificationParameter(listStudentSpecific,dateSelect,reference,numParam,paramSelect);
	
		/*limpiar variables*/
		referenceData = "";
		dateSelect = new Date();
		
		/*se actualiza los datos*/
		loadParameters(generalVar);
		
		return "teacher_qualification_data";
	}
	
	public String redirectParameterEdit(){
		init();
		
		/*recuperacion de parametros*/
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String paramSelect = params.get("paramSelect");
		
		if(matrixSize != -1){

			listParameterNumber = new LinkedHashMap<String, String>();
			listStudentParameterGeneral = new ArrayList<ParameterEditUtil>();
			dateSelect = new Date();
			referenceData = "";
			parameterNumberSelect = "";
			

			/*Verifica el rango es que se
			 * encuentra la fecha del sistema
			 * y los compara con los de la oferta
			 * segun esos datos presenta la informacion*/
			Date dateSystem = operations.returnSystemDate();
			
			if(!(dateSystem.after(startOffer) && dateSystem.before(endOffer))){
				if(dateSystem.compareTo(startOffer) != 0){
					if(dateSystem.before(startOffer)){
						dateSelect = startOffer;
					}else{
						dateSelect = endOffer;
					}
					
				}
			}
	
			String route ="";
			if(paramSelect.equals("evaluation")){
				route = "teacher_evaluation_edit";
			}
			if(paramSelect.equals("lesson")){
				route = "teacher_lesson_edit";
			}
			if(paramSelect.equals("classWork")){
				route = "teacher_classWork_edit";
			}
			if(paramSelect.equals("extraClassWork")){
				route = "teacher_extraClassWork_edit";
			}			
			if(paramSelect.equals("participation")){
				route = "teacher_participation_edit";
			}
			if(paramSelect.equals("laboratory")){
				route = "teacher_laboratory_edit";
			}
				
			return route;	
		
		}else{
			String message ="";
			
			if(paramSelect.equals("evaluation")){
				message = "evaluaciones";
			}
			if(paramSelect.equals("lesson")){
				message = "lecciones";
			}
			if(paramSelect.equals("classWork")){
				message = "trabajos autónomos";
			}
			if(paramSelect.equals("extraClassWork")){
				message = "tareas-deberes";
			}		
			if(paramSelect.equals("participation")){
				message = "participaciones";
			}
			if(paramSelect.equals("laboratory")){
				message = "talleres-laboratorios";
			}
			
			
			 msgError("La operacion no puede ser ejecutada. No existen registros de "+message);
			 return "";
		}
	}
	
	/*getter modificado*/
	public Map<String, String> getListParameterNumber() {
		loadListParameterNumber();
		return listParameterNumber;
	}
	
	public void loadListParameterNumber(){
		
		val = this.matrixSize + 1;	
		String paramSelect = this.generalVar;
		long idTeacherNew = teacherDAO.returnTeacherId(teacherIC);
		
		List<NumberParameterUtil> listPresent = qualificationDAO.returnListDateParameter(offerSelect,careerSelect,moduleSelect,matterSelect,idTeacherNew,paramSelect);
		
		
		
		
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		
		for(NumberParameterUtil npu:listPresent){
			Integer number = npu.getNumParam();
			
			///////////////////////
			/*Lista con los elementos
		 	* a editar*/
			List<ParameterEditUtil> lst = qualificationDAO.returnListStudentParameterEdit(offerSelect,careerSelect,moduleSelect,matterSelect,idTeacherNew,number,paramSelect);
		
			/*valores de fecha y referencia general*/
			String refer = lst.get(0).getGeneral_reference();
			
			if(refer.equals("")){
				refer = "- Sin referencia -";
			}else{
				if(refer.length() > 35){
					refer = refer.substring(0,36) + "...";
				}
			}
			///////////////////////////////
			
			
			
			String date = format.format(npu.getDateParam());
			String data = npu.getNumParam() + " ("+date+") "+ refer ;
			listParameterNumber.put(data,number+"");
		}
	}
	
	public void loadDataTableParameterEdit(){
		
		init();
		int failConecction = 0;
			
		this.check = 1;
		
		/*recuperacion de parametros*/
		String paramSelect = this.generalVar;
		
		Integer numParamSelect = Integer.parseInt(parameterNumberSelect);
		long idTeacherNew = teacherDAO.returnTeacherId(teacherIC);
		
		/*Lista con los elementos
	 	* a editar*/
		listStudentParameterGeneral = qualificationDAO.returnListStudentParameterEdit(offerSelect,careerSelect,moduleSelect,matterSelect,idTeacherNew,numParamSelect,paramSelect);
	
		/*valores de fecha y referencia general*/
		referenceData = listStudentParameterGeneral.get(0).getGeneral_reference();
		dateSelect = listStudentParameterGeneral.get(0).getDate_parameter();
		int i=1;
		for(ParameterEditUtil data:listStudentParameterGeneral){
			
			String studentID = studentDAO.returnStudentById(data.getIdStudent()).getIdentification_card();			
			String studentData = "";
			try{
				studentData= conectionWS.conectionPersonalWS().sgawsDatosEstudiante(studentID);
				JSONArray array = operations.processJSON(studentData);
				/*recupera del WS y muestra el nombre del estudiante formateado*/
				data.setRowCount(i);
				data.setStudent_name(operations.generateNameStudent(array.get(1)+"", array.get(2)+""));	
				i++;
			} catch (Exception e) {
				
				studentID = studentDAO.returnStudentById(data.getIdStudent()).getIdentification_card();				
				data.setRowCount(i);
				data.setStudent_name(operations.generateNameStudent(studentID, ""));	
				i++;
				failConecction++;
				
			}
		
		}
		
		if(failConecction>0){
			msgFatalConnection2();
		}
		
	}
	
	public String processParameterEdit(){
		init();
		
		if(listStudentParameterGeneral.isEmpty()){
			msgError("Debe seleccionar datos antes de proceder a eliminar");
            return "";
		}
		
		/*recuperacion de parametros*/
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String paramSelect = params.get("paramSelect");
		
		/*lego de la edicion actualiza información*/
		qualificationDAO.updateAllQualificationParameter(listStudentParameterGeneral,dateSelect,referenceData,paramSelect);
	
		/*se reinicializan variables*/
		listParameterNumber = new LinkedHashMap<String, String>();
		listStudentParameterGeneral = new ArrayList<ParameterEditUtil>();
		dateSelect = new Date();
		referenceData = "";
		parameterNumberSelect = "";
		
		loadParameters(generalVar);
		this.check = 0;
		return "teacher_qualification_data";
	}
	
	public String processParameterDelete(){
		init();
		
		if(listStudentParameterGeneral.isEmpty()){
			msgError("Debe seleccionar datos antes de proceder a eliminar");
            return "";
		}
		
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String paramSelect = params.get("paramSelect");
	
		int rangeParams = Integer.parseInt(parameterNumberSelect);
		
		/**solo permitir borrar la ultima evaluacion ingresada*/	
		if(val == rangeParams){
			qualificationDAO.deleteAllQualificationParameter(listStudentParameterGeneral,paramSelect);

			listParameterNumber = new LinkedHashMap<String, String>();
			listStudentParameterGeneral = new ArrayList<ParameterEditUtil>();
			listPresentationParameter = new LinkedHashMap<StudentSpecificUtil, List<BasicParameterData>>();
			dateSelect = new Date();
			referenceData = "";
			parameterNumberSelect = "";
			this.check = 0;
		
			loadParameters(generalVar);
		
			return "teacher_qualification_data";
		}else{
			String message ="";
			
			if(paramSelect.equals("evaluation")){
				message = "evaluaciones";
			}
			if(paramSelect.equals("lesson")){
				message = "lecciones";
			}
			if(paramSelect.equals("classWork")){
				message = "trabajos autónomos";
			}	
			if(paramSelect.equals("extraClassWork")){
				message = "tareas-deberes";
			}		
			if(paramSelect.equals("participation")){
				message = "participaciones";
			}
			if(paramSelect.equals("laboratory")){
				message = "talleres-laboratorios";
			}
			
			msgError("Solo es posible eliminar el último registro de "+message);
            return "";
		}
	}

	public String redirectParameterDetail(){
		
		
		/**recuperacion de parametros*/
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String paramSelect = params.get("paramSelect");
		
		
		if(matrixSize != -1){
			
			long idTeacher = teacherDAO.returnTeacherId(teacherIC);
			listConf = configurationDAO.returnListConfigurations(offerSelect, careerSelect,moduleSelect, matterSelect, idTeacher);
			listStudentParameterGeneral = new ArrayList<ParameterEditUtil>();
			parameterNumberSelect = "";
			this.listStudentParameterDetail = new ArrayList<ParameterDetailUtil>();
			long idTeacherNew = teacherDAO.returnTeacherId(teacherIC);
			int numEvals = this.matrixSize + 1;
			
			String route="";
			
			/**lista que presenta unicamete informacion general
			 * de cada evaluación*/
			listStudentParameterDetail = qualificationDAO.returnListStudentParameterDetail(offerSelect,careerSelect,moduleSelect,matterSelect,idTeacherNew,numEvals,paramSelect);
			
		
			
			if(paramSelect.equals("evaluation")){
				route="teacher_evaluation_detail";
				loadParameters(paramSelect);
				paramTitle = "EVALUACIONES";
				paramAverage = listConf.get(0).getPercentage();
			}
			
			if(paramSelect.equals("lesson")){
				route="teacher_lesson_detail";
				loadParameters(paramSelect);
				paramTitle = "LECCIONES";
				paramAverage = listConf.get(1).getPercentage();
			}
			if(paramSelect.equals("classWork")){
				route="teacher_classWork_detail";
				loadParameters(paramSelect);
				paramTitle = "TRABAJOS AUTÓNOMOS";
				paramAverage = listConf.get(2).getPercentage();
			}
			if(paramSelect.equals("extraClassWork")){
				route="teacher_extraClassWork_detail";
				loadParameters(paramSelect);
				paramTitle = "DEBERES - TAREAS";
				paramAverage = listConf.get(3).getPercentage();
			}	
			if(paramSelect.equals("participation")){
				route="teacher_participation_detail";
				loadParameters(paramSelect);
				paramTitle = "PARTICIPACIONES EN CLASE";
				paramAverage = listConf.get(4).getPercentage();
			}
			
			if(paramSelect.equals("laboratory")){
				route="teacher_laboratory_detail";
				loadParameters(paramSelect);
				paramTitle = "TALLERES - LABORATORIOS";
				paramAverage = listConf.get(5).getPercentage();
			}
			
			return route;
			
		
		}else{
			
			String message ="";
			
			if(paramSelect.equals("evaluation")){
				message = "evaluaciones creadas";
			}
			if(paramSelect.equals("lesson")){
				message = "lecciones creadas";
			}
			if(paramSelect.equals("classWork")){
				message = "trabajos autónomos creados";
			}
			if(paramSelect.equals("extraClassWork")){
				message = "tareas-deberes creados";
			}			
			if(paramSelect.equals("participation")){
				message = "participaciones creadas";
			}
			if(paramSelect.equals("laboratory")){
				message = "talleres-laboratorios creados";
			}
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No existen registros de "+message);
            FacesContext.getCurrentInstance().addMessage(null, msg);
			
			return "";
		}
	}
	
	public String returnQualificationData(){
		this.check = 0;
		cleanData();
		loadParameters(generalVar);
		return "teacher_qualification_data";
	}
	
	public void cleanData(){
		dateSelect = new Date();
		referenceData = "";
		parameterNumberSelect = "";		
		listParameterNumber = new LinkedHashMap<String, String>();
		listStudentParameterGeneral = new ArrayList<ParameterEditUtil>();
		listPresentationParameter = new LinkedHashMap<StudentSpecificUtil, List<BasicParameterData>>();
		
	}

	public String returnAdmin() {
		cleanData();
		matrixSize = new Integer(0);
		listStudentSpecific = new ArrayList<StudentSpecificUtil>();
		return "teacher_admin";
	}
	
	public void msgFatal(String message){
	   	 facesMessage = new FacesMessage(FacesMessage.SEVERITY_FATAL,"Error", message);
	     faceContext.addMessage(null, facesMessage);
	}
   
    public void msgError(String message){
   	   facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",message);
       faceContext.addMessage(null, facesMessage);
    }
   
    public void msgInfo(String message){
	   facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", message);
       faceContext.addMessage(null, facesMessage);
    }
    
    public void msgFatalConnection(){
      	 facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", "Falló la "
      	 		+ "conexión a internet.");
           faceContext.addMessage(null, facesMessage);
      }
    public void msgFatalConnection2(){
    	
    	flash = faceContext.getExternalContext().getFlash();
     	facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,"Advertencia", "Se presentaron problemas con la conexión"
     	 		+ "a internet. Es probable que se presente el numero de cédula del estudiante "
     	 		+ "únicamente.");
        faceContext.addMessage(null, facesMessage);
        flash.setKeepMessages(true);
     }

    
	/*getters - setters*/
	




	public Integer getMatrixSize() {	
		return matrixSize;
	}

	public void setMatrixSize(Integer matrixSize) {
		this.matrixSize = matrixSize;
	}

	public List<TeacherModuleMatterUtil> getListTeacherModuleMatters() {
		return listTeacherModuleMatters;
	}
	public void setListTeacherModuleMatters(
			List<TeacherModuleMatterUtil> listTeacherModuleMatters) {
		this.listTeacherModuleMatters = listTeacherModuleMatters;
	}

	public Teacher getTeacherSession() {
		return teacherSession;
	}

	public void setTeacherSession(Teacher teacherSession) {
		this.teacherSession = teacherSession;
	}

	public String getTableTitle() {
		return tableTitle;
	}

	public void setTableTitle(String tableTitle) {
		this.tableTitle = tableTitle;
	}

	public List<StudentSpecificUtil> getListStudentSpecific() {
		return listStudentSpecific;
	}

	public void setListStudentSpecific(List<StudentSpecificUtil> listStudentSpecific) {
		this.listStudentSpecific = listStudentSpecific;
	}

	public Map<StudentSpecificUtil, List<BasicParameterData>> getListPresentationEvaluation() {
		return listPresentationParameter;
	}

	public void setListPresentationEvaluation(
			Map<StudentSpecificUtil, List<BasicParameterData>> listPresentationEvaluation) {
		this.listPresentationParameter = listPresentationEvaluation;
	}

	public Date getDateSelect() {
		return dateSelect;
	}

	public void setDateSelect(Date dateSelect) {
		this.dateSelect = dateSelect;
	}

	public StudentSpecificUtil getSelectedStudent() {
		return selectedStudent;
	}

	public void setSelectedStudent(StudentSpecificUtil selectedStudent) {
		this.selectedStudent = selectedStudent;
	}

	public Map.Entry<String, List<String>> getEntry() {
		return entry;
	}

	public void setEntry(Map.Entry<String, List<String>> entry) {
		this.entry = entry;
	}

	public String getReferenceData() {
		return referenceData;
	}

	public void setReferenceData(String referenceData) {
		this.referenceData = referenceData;
	}

	public int getVal() {
		return val;
	}

	public void setVal(int val) {
		this.val = val;
	}

	public String getOfferStr() {
		return offerStr;
	}


	public void setOfferStr(String offerStr) {
		this.offerStr = offerStr;
	}

	public String getCareerStr() {
		return careerStr;
	}

	public void setCareerStr(String careerStr) {
		this.careerStr = careerStr;
	}

	public String getModuleStr() {
		return moduleStr;
	}

	public void setModuleStr(String moduleStr) {
		this.moduleStr = moduleStr;
	}

	public String getParallelStr() {
		return parallelStr;
	}

	public void setParallelStr(String parallelStr) {
		this.parallelStr = parallelStr;
	}

	public String getMatterStr() {
		return matterStr;
	}

	public void setMatterStr(String matterStr) {
		this.matterStr = matterStr;
	}

	public String getParameterMatterSelect() {
		return parameterMatterSelect;
	}

	public void setParameterMatterSelect(String parameterMatterSelect) {
		this.parameterMatterSelect = parameterMatterSelect;
	}

	public String getParameterNumberSelect() {
		return parameterNumberSelect;
	}


	public void setParameterNumberSelect(String parameterNumberSelect) {
		this.parameterNumberSelect = parameterNumberSelect;
	}




	public void setListParameterNumber(Map<String, String> listParameterNumber) {
		this.listParameterNumber = listParameterNumber;
	}


	public List<ParameterEditUtil> getListStudentParameterGeneral() {
		return listStudentParameterGeneral;
	}


	public void setListStudentParameterGeneral(
			List<ParameterEditUtil> listStudentParameterGeneral) {
		this.listStudentParameterGeneral = listStudentParameterGeneral;
	}


	public List<ParameterDetailUtil> getListStudentParameterDetail() {
		return listStudentParameterDetail;
	}


	public void setListStudentParameterDetail(
			List<ParameterDetailUtil> listStudentParameterDetail) {
		this.listStudentParameterDetail = listStudentParameterDetail;
	}


	public Map<StudentSpecificUtil, List<BasicParameterData>> getListPresentationParameter() {
		return listPresentationParameter;
	}


	public void setListPresentationParameter(
			Map<StudentSpecificUtil, List<BasicParameterData>> listPresentationParameter) {
		this.listPresentationParameter = listPresentationParameter;
	}


	public String getGeneralVar() {
		return generalVar;
	}


	public void setGeneralVar(String generalVar) {
		this.generalVar = generalVar;
	}


	public String getOffer() {
		return offer;
	}


	public void setOffer(String offer) {
		this.offer = offer;
	}


	public String getCareer() {
		return career;
	}


	public void setCareer(String career) {
		this.career = career;
	}


	public String getModuleParallel() {
		return moduleParallel;
	}


	public void setModuleParallel(String moduleParallel) {
		this.moduleParallel = moduleParallel;
	}


	public Map<String, String> getListAcademicOffer() {
		loadOffers();
		return listAcademicOffer;
	}


	public void setListAcademicOffer(Map<String, String> listAcademicOffer) {
		this.listAcademicOffer = listAcademicOffer;
	}


	public Map<String, String> getListCareer() {
		loadCareers();
		return listCareer;
	}


	public void setListCareer(Map<String, String> listCareer) {
		this.listCareer = listCareer;
	}


	public Map<String, String> getListModulesParallel() {
		loadModulesParallel();
		return listModulesParallel;
	}


	public void setListModulesParallel(Map<String, String> listModulesParallel) {
		this.listModulesParallel = listModulesParallel;
	}


	public Date getStartOffer() {
		return startOffer;
	}


	public void setStartOffer(Date startOffer) {
		this.startOffer = startOffer;
	}


	public Date getEndOffer() {
		return endOffer;
	}


	public void setEndOffer(Date endOffer) {
		this.endOffer = endOffer;
	}


	public TabView getMessagesTab() {
		return messagesTab;
	}


	public void setMessagesTab(TabView messagesTab) {
		this.messagesTab = messagesTab;
	}


	public int getActiveTabIndex() {
		return activeTabIndex;
	}


	public void setActiveTabIndex(int activeTabIndex) {
		this.activeTabIndex = activeTabIndex;
	}


	public Double getEvaluationPerc() {
		return evaluationPerc;
	}


	public void setEvaluationPerc(Double evaluationPerc) {
		this.evaluationPerc = evaluationPerc;
	}
	public Double getLessonPerc() {
		return lessonPerc;
	}
	public void setLessonPerc(Double lessonPerc) {
		this.lessonPerc = lessonPerc;
	}
	public Double getClassWorkPerc() {
		return classWorkPerc;
	}
	public void setClassWorkPerc(Double classWorkPerc) {
		this.classWorkPerc = classWorkPerc;
	}
	public Double getExtraClassWorkPerc() {
		return extraClassWorkPerc;
	}
	public void setExtraClassWorkPerc(Double extraClassWorkPerc) {
		this.extraClassWorkPerc = extraClassWorkPerc;
	}
	public Double getParticipationPerc() {
		return participationPerc;
	}
	public void setParticipationPerc(Double participationPerc) {
		this.participationPerc = participationPerc;
	}
	public Double getLaboratoryPerc() {
		return laboratoryPerc;
	}
	public void setLaboratoryPerc(Double laboratoryPerc) {
		this.laboratoryPerc = laboratoryPerc;
	}
	public Integer getEvaluationNumber() {
		return evaluationNumber;
	}
	public void setEvaluationNumber(Integer evaluationNumber) {
		this.evaluationNumber = evaluationNumber;
	}
	public Integer getLessonNumber() {
		return lessonNumber;
	}
	public void setLessonNumber(Integer lessonNumber) {
		this.lessonNumber = lessonNumber;
	}
	public Integer getClassWorkNumber() {
		return classWorkNumber;
	}
	public void setClassWorkNumber(Integer classWorkNumber) {
		this.classWorkNumber = classWorkNumber;
	}
	public Integer getExtraClassWorkNumber() {
		return extraClassWorkNumber;
	}
	public void setExtraClassWorkNumber(Integer extraClassWorkNumber) {
		this.extraClassWorkNumber = extraClassWorkNumber;
	}
	public Integer getParticipationNumber() {
		return participationNumber;
	}
	public void setParticipationNumber(Integer participationNumber) {
		this.participationNumber = participationNumber;
	}
	public Integer getLaboratoryNumber() {
		return laboratoryNumber;
	}
	public void setLaboratoryNumber(Integer laboratoryNumber) {
		this.laboratoryNumber = laboratoryNumber;
	}
	public String getParamTitle() {
		return paramTitle;
	}
	public void setParamTitle(String paramTitle) {
		this.paramTitle = paramTitle;
	}
	public double getParamAverage() {
		return paramAverage;
	}
	public void setParamAverage(double paramAverage) {
		this.paramAverage = paramAverage;
	}
	public String getMsgNone() {
		return msgNone;
	}
	public void setMsgNone(String msgNone) {
		this.msgNone = msgNone;
	}
	public String getMsgEval() {
		return msgEval;
	}
	public void setMsgEval(String msgEval) {
		this.msgEval = msgEval;
	}
	public String getMsgLess() {
		return msgLess;
	}
	public void setMsgLess(String msgLess) {
		this.msgLess = msgLess;
	}
	public String getMsgCW() {
		return msgCW;
	}
	public void setMsgCW(String msgCW) {
		this.msgCW = msgCW;
	}
	public String getMsgECW() {
		return msgECW;
	}
	public void setMsgECW(String msgECW) {
		this.msgECW = msgECW;
	}
	public String getMsgPart() {
		return msgPart;
	}
	public void setMsgPart(String msgPart) {
		this.msgPart = msgPart;
	}
	public String getMsgLab() {
		return msgLab;
	}
	public void setMsgLab(String msgLab) {
		this.msgLab = msgLab;
	}



	public int getCheck() {
		return check;
	}



	public void setCheck(int check) {
		this.check = check;
	}
	
}