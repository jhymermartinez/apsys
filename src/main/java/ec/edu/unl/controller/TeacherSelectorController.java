package ec.edu.unl.controller;

import ec.edu.unl.DAO.AcademicDAO;
import ec.edu.unl.DAO.CareerDAO;

import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import ec.edu.unl.DAO.ConfigurationDAO;
import ec.edu.unl.DAO.InstitutionalFactorDAO;
import ec.edu.unl.DAO.MatterDAO;
import ec.edu.unl.DAO.MatterModuleDAO;
import ec.edu.unl.DAO.MatterOfferDAO;
import ec.edu.unl.DAO.ModuleDAO;
import ec.edu.unl.DAO.OfferDAO;
import ec.edu.unl.DAO.PersonalFactorDAO;
import ec.edu.unl.DAO.SocialFactorDAO;
import ec.edu.unl.DAO.StudentCareerDAO;
import ec.edu.unl.DAO.StudentDAO;
import ec.edu.unl.DAO.StudentMatterDAO;
import ec.edu.unl.DAO.StudentModuleDAO;
import ec.edu.unl.DAO.TeacherCareerDAO;
import ec.edu.unl.DAO.TeacherDAO;
import ec.edu.unl.DAO.TeacherMatterDAO;
import ec.edu.unl.DAO.TeacherModuleDAO;
import ec.edu.unl.entity.Career;
import ec.edu.unl.entity.Configuration;
import ec.edu.unl.entity.Matter;
import ec.edu.unl.entity.Module;
import ec.edu.unl.entity.Offer;
import ec.edu.unl.entity.Student;
import ec.edu.unl.entity.StudentCareer;
import ec.edu.unl.entity.StudentMatter;
import ec.edu.unl.entity.StudentModule;
import ec.edu.unl.entity.Teacher;
import ec.edu.unl.entity.TeacherCareer;
import ec.edu.unl.entity.TeacherMatter;
import ec.edu.unl.entity.TeacherModule;
import ec.edu.unl.operations.Conections;
import ec.edu.unl.operations.Operations;
import ec.edu.unl.utilities.academic.TeacherModuleMatterUtil;

public class TeacherSelectorController implements Serializable{


    @Autowired
    @Qualifier("student")
    private Student student;

    @Autowired
    @Qualifier("operationsImpl")
    private Operations operations;

    @Autowired
    @Qualifier("conectionImpl")
    private Conections conectionWS;

    @Autowired
    @Qualifier("student")
    private Student studentForSave;

    @Autowired
    @Qualifier("matter")
    private Matter matterForSave;

    @Autowired
    @Qualifier("teacher")
    private Teacher teacherForSave;

    @Autowired
    @Qualifier("career")
    private Career careerForSave;

    @Autowired
    @Qualifier("module")
    private Module moduleForSave;
    
    @Autowired
    @Qualifier("offer")
    private Offer offerForSave;
    

    // //// DAOS /////////
    @Autowired
    @Qualifier("studentDAOImpl")
    private StudentDAO studentDAO;

    @Autowired
    @Qualifier("matterDAOImpl")
    private MatterDAO matterDAO;

    @Autowired
    @Qualifier("moduleDAOImpl")
    private ModuleDAO moduleDAO;

    @Autowired
    @Qualifier("careerDAOImpl")
    private CareerDAO careerDAO;

    @Autowired
    @Qualifier("teacherDAOImpl")
    private TeacherDAO teacherDAO;

    @Autowired
    @Qualifier("academicDAOImpl")
    private AcademicDAO academicDAO;

    @Autowired
    @Qualifier("offerDAOImpl")
    private OfferDAO offerDAO;

    
    @Autowired
    @Qualifier("studentMatterDAOImpl")
    private StudentMatterDAO studentMatterDAO;

    @Autowired
    @Qualifier("studentModuleDAOImpl")
    private StudentModuleDAO studentModuleDAO;

    @Autowired
    @Qualifier("studentCareerDAOImpl")
    private StudentCareerDAO studentCareerDAO;

    @Autowired
    @Qualifier("teacherMatterDAOImpl")
    private TeacherMatterDAO teacherMatterDAO;

    @Autowired
    @Qualifier("teacherModuleDAOImpl")
    private TeacherModuleDAO teacherModuleDAO;

    @Autowired
    @Qualifier("teacherCareerDAOImpl")
    private TeacherCareerDAO teacherCareerDAO;

    @Autowired
    @Qualifier("matterOfferDAOImpl")
    private MatterOfferDAO matterOfferDAO;

    @Autowired
    @Qualifier("matterModuleDAOImpl")
    private MatterModuleDAO matterModuleDAO;

    @Autowired
    @Qualifier("configurationDAOImpl")
    private ConfigurationDAO configurationDAO;

    @Autowired
    @Qualifier("personalFactorDAOImpl")
    private PersonalFactorDAO personalFactorDAO;
    
    @Autowired
    @Qualifier("socialFactorDAOImpl")
    private SocialFactorDAO socialFactorDAO;
    
    @Autowired
    @Qualifier("institutionalFactorDAOImpl")
    private InstitutionalFactorDAO institutionalFactorDAO;
    
// //////////////
    @Autowired
    @Qualifier("studentCareer")
    private StudentCareer studentCareerForSave;

    @Autowired
    @Qualifier("studentMatter")
    private StudentMatter studentMatterForSave;

    @Autowired
    @Qualifier("studentModule")
    private StudentModule studentModuleForSave;

    @Autowired
    @Qualifier("teacherMatter")
    private TeacherMatter teacherMatterForSave;

    @Autowired
    @Qualifier("teacherModule")
    private TeacherModule teacherModuleForSave;

    @Autowired
    @Qualifier("teacherCareer")
    private TeacherCareer teacherCareerForSave;
    
    @Autowired
    @Qualifier("configuration")
    private Configuration configurationForSave;
    
    static HttpServletRequest httpServletRequest;
    static FacesContext faceContext;
    FacesMessage facesMessage;
    static Flash flash;

    /**
     * listas para presentar en los oneMenu
     */
    private Map<String, String> listPeriods;
    private Map<String, String> listAcademicOffer;
    private Map<String, String> listCareer;
    private Map<String, String> listCareerVerif;
    private Map<String, String> listModulesParallel;
    private Map<String, String> numeroModuloParalelo;
    private Map<String, String> listMatters;
    private Map<String, String> listDataMatters;
    private List<TeacherModuleMatterUtil> listTeacherModuleMatter;
    
    private Map<String,String> mapOfferDateStartEnd;

    /**
     * Datos academicos
     */
    private String periods;
    private String offer;
    private String careerCode;
    private String career;
    private String parallel;
    private String matter;
    private Double numHoursMatter;
    
	private Date startDateMatter;
	private Date endDateMatter;

    // /////////////
    private String[] vector;
    private String moduleIndv;
    private String parallelIndv;

    
    private String msgIncorrectData;
    private String msgIncompleteData;
    private String msgExistData;
    
    
    
    public void init(){
    	faceContext = FacesContext.getCurrentInstance();
		httpServletRequest = (HttpServletRequest) faceContext.getExternalContext().getRequest();
	     /**Mensajes de error*/
        msgIncorrectData = "La información seleccionada es incorrecta o aun no existe suficiente información en la base de datos del SGA";      
        msgIncompleteData = "La información es incompleta. No se han seleccionado todos los campos";
        msgExistData = "El registro ya existe y no puede volver a ser creado";
    }
       
    public boolean checkConnection(){
    	boolean result = true;
    	try {	
            URL url = new URL("http://ws.unl.edu.ec/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.connect();
            if (con.getResponseCode() == 200){
            	result = true;
            }
        } catch (Exception exception) {
        	result = false;
            
        }
    	
    	return result;
    	
    }
    
    /**
     * getter modificado
     */
    public Map<String, String> getListPeriods() {
    	
    	init();
    
        startPeriods();
        
        String atrPeriodosLectivos = conectionWS.conectionAcademicaWS().sgawsPeriodosLectivos();
        JSONArray jsonPeriodosLec = operations.processJSON(atrPeriodosLectivos);

        for (int i = 0; i < jsonPeriodosLec.size(); i++) {
            JSONArray jsonPeriodosLectivosIndv = operations.processJSON(jsonPeriodosLec.get(i)+"");
            listPeriods.put(jsonPeriodosLectivosIndv.get(1) + "",jsonPeriodosLectivosIndv.get(0) + "");
        }
        return listPeriods;
	}
    	
    

    public void loadListAcademicOffer() {
    	
    	init();

        
    	if(!checkConnection()){
    		msgFatalConnection();
    	}else{
    		
	    	
	    	if (!periods.equals("")) {
	        	
	        	startOffers();
	            
	        	String strJsonAcademicOffer = conectionWS.conectionAcademicaWS().sgawsOfertasAcademicas(periods);
	            JSONArray jsonAcademicOffer = operations.processJSON(strJsonAcademicOffer);
	            String temp = jsonAcademicOffer.get(0) + "";
	            
	            if (!temp.equalsIgnoreCase("_error")) {
	                for (int i = 0; i < jsonAcademicOffer.size(); i++) {
	                    JSONArray jsonAcademicOfferIndv = operations.processJSON(jsonAcademicOffer.get(i) + "");
	                    listAcademicOffer.put(jsonAcademicOfferIndv.get(1) + "",jsonAcademicOfferIndv.get(0) + "");   
	                    String dateStartEnd = jsonAcademicOfferIndv.get(2)+"#"+jsonAcademicOfferIndv.get(3);
	                    mapOfferDateStartEnd.put(jsonAcademicOfferIndv.get(0)+"", dateStartEnd);
	                }
	            } else {    
	            	msgFatal(msgIncorrectData);
	                cleanOffers();
	            }
	        } else {
	            cleanOffers();
	        }
    	}
    }

    public void loadListCareer() {
    	
    	init();

        
    	if(!checkConnection()){
    		msgFatalConnection();
    	}else{
	        
	    	if (!offer.equals("")) {
	    		
	    		startCarrers();
	
	            Teacher teacher = (Teacher) httpServletRequest.getSession().getAttribute("teacherSession");
	            String strCareer = conectionWS.conectionAcademicaWS().sgawsCargaHorariaDocente(offer,teacher.getIdentification_card());
	            JSONArray jsonCareer = operations.processJSON(strCareer);
	            String error = jsonCareer.get(0) + "";
	
	            if (!error.equalsIgnoreCase("_error")) {
	                JSONArray jsonCareerIndv = operations.processJSON(jsonCareer.get(jsonCareer.size() - 1) + "");
	                for (int i = 0; i < jsonCareerIndv.size(); i++) {
	                    JSONArray jsonCareersFinal = operations.processJSON(jsonCareerIndv.get(i) + "");
	                    listCareer.put(jsonCareersFinal.get(0) + "", i + "");
	                    listCareerVerif.put(i + "", jsonCareersFinal.get(0) + "");
	                }
	
	            } else {
	            	msgFatal(msgIncorrectData);
	                cleanCareers();
	            }
	
	        } else {
	        	cleanCareers();
	        }
    	}
    }

    public void loadModulesParallel() {
    	
    	init();

    	if(!checkConnection()){
    		msgFatalConnection();
    	}else{
    		
	    	
	    	if (!career.equals("")) {
	        	
	        	startModuleParallels();
	
	            /**
	             * procesar carreras para obtener su id
	             */
	            String strCompleteListCareer = conectionWS.conectionInstitucionalWS().sgawsDatosCarreras(offer);
	            JSONArray jsonCompleteListCareer = operations.processJSON(strCompleteListCareer);
	            careerCode = listCareerVerif.get(career);
	
	            for (int i = 0; i < jsonCompleteListCareer.size(); i++) {
	                JSONArray jsonCompleteListCareerIndv = operations.processJSON(jsonCompleteListCareer.get(i) + "");
	                if (careerCode.equalsIgnoreCase(jsonCompleteListCareerIndv.get(1) + "")) {
	                    careerCode = jsonCompleteListCareerIndv.get(0) + "";
	                    break;
	                }
	            }
	
	
	            String strModules = conectionWS.conectionInstitucionalWS().sgawsParalelosCarrera(offer, careerCode);
	            JSONArray jsonModules = operations.processJSON(strModules);
	            String error = jsonModules.get(0) + "";
	
	            if (!error.equalsIgnoreCase("_error")) {
	
	                JSONArray jsonModulesIndv = operations.processJSON(jsonModules.get(jsonModules.size() - 1) + "");
	
	                for (int i = 0; i < jsonModulesIndv.size(); i++) {
	                    JSONArray jsonModulesFinal = operations.processJSON(jsonModulesIndv.get(i)+"");
	                    listModulesParallel.put(
	                            "Modulo " + jsonModulesFinal.get(2) + " Paralelo "
	                            + jsonModulesFinal.get(3),
	                            jsonModulesFinal.get(0) + "#"
	                            + jsonModulesFinal.get(2) + "#"
	                            + jsonModulesFinal.get(3));
	
	                }
	            } else {
	            	msgFatal(msgIncorrectData);
	                cleanModuleParallels();
	            }
	        } else {
	        	cleanModuleParallels();
	        }
    	}
    }

    public void loadMatter() {
    	
    	init();
        
    	if(!checkConnection()){
    		msgFatalConnection();
    	}else{
    	
	    	if (!parallel.equals("")) {
	
	    		startMatters();
	
	            Teacher teacher = (Teacher) httpServletRequest.getSession().getAttribute("teacherSession");
	
	            /**
	             * carga horaria del docente
	             */
	            String strCareer = conectionWS.conectionAcademicaWS().sgawsCargaHorariaDocente(offer,teacher.getIdentification_card());
	            JSONArray jsonMatter = operations.processJSON(strCareer);
	            String error = jsonMatter.get(0) + "";
	            
	            if (!error.equalsIgnoreCase("_error")) {
	
	                /**
	                 * recuperar codigo, modulo y paralelo almacenado en la variable
	                 */
	                vector = parallel.split("#");
	                parallel = vector[0];
	                moduleIndv = vector[1];
	                parallelIndv = vector[2];
	
	                /**
	                 * El ultimo elemento del JSON arrayMatter previamente retornado
	                 */
	                JSONArray jsonMatterIndv = operations.processJSON(jsonMatter.get(jsonMatter.size() - 1) + "");
	
	                for (int i = 0; i < jsonMatterIndv.size(); i++) {
	                    JSONArray jsonMatterFinal = operations.processJSON(jsonMatterIndv.get(i) + "");
	
	                    if (moduleIndv.equalsIgnoreCase(jsonMatterFinal.get(3) + "") && parallelIndv.equalsIgnoreCase(jsonMatterFinal.get(4) + "")) {
	                        listMatters.put(jsonMatterFinal.get(1) + "",jsonMatterFinal.get(1) + "");
	                        listDataMatters.put(jsonMatterFinal.get(1) + "",jsonMatterFinal.get(2) + "");
	                    }
	                }
	
	                if (listMatters.isEmpty()) {
	                	msgFatal(msgIncorrectData);
	                	cleanMatters();
	                }
	            } else {
	            	msgFatal(msgIncorrectData);
	            	cleanMatters();
	            }
	        } else {
	        	cleanMatters();
	        }
    	}
    }

    public String saveAndRenderTeacherAdmin() {
    	init();
    	
    	if(!checkConnection()){
    		msgFatalConnection();
    		return "";
    	}
    	
    	
    	
    	
    	 
    	if(this.periods.equals("")||
    		this.offer.equals("")||
    		this.career.equals("")||
    		this.parallel.equals("")||
    		this.matter.equals("")){
    		
            msgError(msgIncompleteData); 
       
            return "";	
    	}
    	
    	

    	String [] vector = this.mapOfferDateStartEnd.get(this.offer).split("#");
   	 
   	 	Date start = operations.convertStringtoDate(vector[0]);
   	 	Date end = operations.convertStringtoDate(vector[1]);
   	 	
        Integer moduleNumber = Integer.parseInt(moduleIndv);

        boolean recuperateModule = false;
        boolean recuperateMatter = false;
        boolean recuperateCareer = false;
        boolean recuperateTeacher = false;
        boolean recuperateOffer = false;

        long idMatter = 0;
        long idModule = 0;
        long idCareer = 0;
        long idTeacher = 0;
        long idOffer = 0;
        long idDateDef = 0;
        
        long offerData;
        long careerData;
        long moduleData;
        long matterData;
        
        Teacher teacher = (Teacher) httpServletRequest.getSession().getAttribute("teacherSession");
        numHoursMatter = Double.parseDouble(listDataMatters.get(matter));
        String strStudents = conectionWS.conectionAcademicaWS().sgawsEstadoestudiantesParalelo(parallel);
        JSONArray jsonStudents = operations.processJSON(strStudents);
        JSONArray jsonStudentsIndv = operations.processJSON(jsonStudents.get(jsonStudents.size() - 1) + "");
        String teacherIC = teacher.getIdentification_card();
        
        /**recupera descripcion de la oferta seleccionada*/
        //String descriptionOffer = operations.returnPeriodsDescription(listPeriods,periods);
        String descriptionOffer = operations.returnElementMap(listAcademicOffer,offer);
        String careerName = listCareerVerif.get(career);
        long codeCareer = Long.valueOf(careerCode);
        
    	/**verificacion de datos existentes*/
    	int count = 0;
    	if(teacherModuleDAO.existRelationTeacherModule(teacherIC,moduleNumber,parallelIndv)){
    		count++;
    	}
    	if(teacherMatterDAO.existRelationTeacherMatter(teacherIC,matter,numHoursMatter)){
    		count++;
    	}
    	if(teacherCareerDAO.existRelationTeacherCareer(teacherIC,codeCareer,careerName)){
    		count++;
    	}
    	if(matterModuleDAO.existRelationMatterModule(matter,numHoursMatter,moduleNumber,parallelIndv)){
    		count++;
    	}
    	if(matterOfferDAO.existRelationMatterOffer(matter,numHoursMatter,descriptionOffer,start,end)){
    		count++;
    	}
        	
    	if (count == 5) {    
            msgError(msgExistData);
            return "";
        } else {

            /**
             * vefifica si existe el docente en base de datos
             */
            if (teacherDAO.existTeacher(teacher.getIdentification_card())) {
                /**
                 * recupera el id del docente
                 */
                idTeacher = teacherDAO.returnTeacherId(teacher.getIdentification_card());
                recuperateTeacher = true;

            } else {

                /**
                 * guarda profesor
                 */
                teacherForSave.setIdentification_card(teacher.getIdentification_card());
                teacherDAO.saveTeacher(teacherForSave);

                /**
                 * no se recupero el docente de la BD
                 */
                recuperateTeacher = false;
            }
            
            ////////////////// MODULOS ////////////////
            
            if (moduleDAO.existModule(moduleNumber, parallelIndv)) {

                idModule = moduleDAO.returnModuleId(moduleNumber,parallelIndv);
                /**
                 * si se recupero el docente se usan los id
                 */
                if (recuperateTeacher) {
                    academicDAO.relationUpdateModuleTeacher(idModule, idTeacher);

                    /**
                     * caso contrario el id del modulo y la entidad docente
                     * guardada anteriormente
                     */
                } else {
                    academicDAO.relationUpdateModuleTeacher(idModule, teacherForSave);
                }
                recuperateModule = true;
                moduleData = idModule;
                
            } else {
                /**
                 * crea y guarda modulo
                 */
                moduleForSave.setNumber(moduleNumber);
                moduleForSave.setParallel(parallelIndv);
                moduleDAO.saveModule(moduleForSave);

                /**
                 * relacion muchos a muchos entre docente y modulo
                 */
                if (recuperateTeacher) {
                    academicDAO.relationSaveModuleTeacher(moduleForSave, idTeacher);
                } else {
                    academicDAO.relationSaveModuleTeacher(moduleForSave, teacherForSave);
                }
                recuperateModule = false;
                moduleData = moduleForSave.getId();
            }
            
            ////////////////////////// MATERIAS /////////////////////////////

            if (matterDAO.existMatter(matter, numHoursMatter)) {

                /**
                 * id de materia
                 */
                idMatter = matterDAO.returnMatterId(matter,numHoursMatter);

                if (recuperateTeacher) {
                    /**
                     * si se recupero el docente de la BD usamos los id
                     * recuperados anteriormente
                     */
                    academicDAO.relationUpdateMatterTeacher(idMatter, idTeacher);
                } else {
                    /**
                     * caso contrario en id de materia y la entidad Teacher
                     * guardada anteriormente
                     */
                    academicDAO.relationUpdateMatterTeacher(idMatter, teacherForSave);
                }

                if (recuperateModule) {
                    academicDAO.relationUpdateMatterModule(idMatter, idModule);
                } else {
                    academicDAO.relationUpdateMatterModule(idMatter, moduleForSave);
                }

                recuperateMatter = true;
                matterData = idMatter;
                
            } else {

                matterForSave.setName(matter);
                matterForSave.setNum_hours(numHoursMatter);
                matterDAO.saveMatter(matterForSave);

                if (recuperateTeacher) {
                    /**
                     * si se recupero el docente entonces se usa el idTeacher
                     */
                    academicDAO.relationSaveMatterTeacher(matterForSave, idTeacher);
                } else {
                    /**
                     * caso contrario la entidad teacherForSave guardada
                     * anteriormente
                     */
                    academicDAO.relationSaveMatterTeacher(matterForSave, teacherForSave);
                }

                if (recuperateModule) {
                    academicDAO.relationSaveMatterModule(matterForSave, idModule);
                } else {
                    academicDAO.relationSaveMatterModule(matterForSave, moduleForSave);
                }

                recuperateMatter = false;
                matterData = matterForSave.getId();
            }

            
            ////////////////// CARRERAS /////////////////////
                      
            if (careerDAO.existCareer(careerName, codeCareer)) {
            	
                idCareer = careerDAO.returnCareerId(careerName, codeCareer);
                
                if (recuperateTeacher) {
                    academicDAO.relationUpdateCareerTeacher(idCareer, idTeacher);
                } else {
                    academicDAO.relationUpdateCareerTeacher(idCareer, teacherForSave);
                }
                recuperateCareer = true;
                careerData = idCareer;
                
            } else {

                /**
                 * crea y guarda carrera
                 */  
                careerForSave.setCode_career(Long.valueOf(careerCode));
                careerForSave.setName(listCareerVerif.get(career));
                careerDAO.saveCareer(careerForSave);

                if (recuperateTeacher) {
                    academicDAO.relationSaveCareerTeacher(careerForSave, idTeacher);
                } else {
                    academicDAO.relationSaveCareerTeacher(careerForSave, teacherForSave);
                }

                recuperateCareer = false;
                careerData = careerForSave.getId();
            }

            
            
            /////// OFERTAS ///////////
            
            if(offerDAO.existOffer(descriptionOffer,start,end)){
            	idOffer = offerDAO.returnOfferId(descriptionOffer,start,end);
            	
                if(recuperateMatter){
                	academicDAO.relationUpdateMatterOffer(idMatter,idOffer);
                }else{
                	academicDAO.relationUpdateMatterOffer(matterForSave,idOffer);
                }
            	recuperateOffer = true;
            	offerData = idOffer;
            }else{
            	
                offerForSave.setDescription(descriptionOffer);
                offerForSave.setStartDate(start);
                offerForSave.setEndDate(end);
                offerDAO.saveOffer(offerForSave);
                
                if(recuperateMatter){
                	academicDAO.relationSaveMatterOffer(idMatter,offerForSave);
                }else{
                	academicDAO.relationSaveMatterOffer(matterForSave,offerForSave);
                }
                
                recuperateOffer = true;
                offerData = offerForSave.getId();
                
            }
            
  
            for (int i = 0; i < jsonStudentsIndv.size(); i++) {
                JSONArray jsonStudentsFinal = operations.processJSON(jsonStudentsIndv.get(i) + "");
                String studentIC = jsonStudentsFinal.get(3)+ "";
				long idStudent=0;
                boolean recuperateStudent=false;
                
                if(studentDAO.existStudent(studentIC)){
                	idStudent = studentDAO.returnStudentId(studentIC);
                	recuperateStudent=true;
                }else{
                	/**
                     * crea y guarda estudiante
                     */
                	studentForSave.setIdentification_card(studentIC);
    				studentDAO.saveStudent(studentForSave);
                	recuperateStudent=false;
                }
                
                
                
                if(recuperateStudent){
               
                	///// ESTUDIANTE - CARRERA /////
                	
                	if(recuperateCareer){
                		academicDAO.relationSaveOrUpdateCareerStudent(idCareer,idStudent);
                		
                	}else{
                		academicDAO.relationSaveOrUpdateCareerStudent(careerForSave,idStudent);
                		
                	}
                	
                	///// ESTUDIANTE - MODULO /////
                	if(recuperateModule){
                		academicDAO.relationSaveOrUpdateModuleStudent(idModule,idStudent);
                		
                	}else{
                		academicDAO.relationSaveOrUpdateModuleStudent(moduleForSave,idStudent);
                		
                	}
                	
                	///// ESTUDIANTE - MATERIA /////	
                	if(recuperateMatter){
                		academicDAO.relationSaveOrUpdateMatterStudent(idMatter,idStudent);
                		
                	}else{
                		academicDAO.relationSaveOrUpdateMatterStudent(matterForSave,idStudent);
                		
                	}
                	
                	//guardar configuraciones adicionales 
                	//del estudiante
                	//predictionParameterDAO.generateDefaultPredictionParameter(offerData,careerData,moduleData,matterData,idStudent);
                	if(!personalFactorDAO.existPersonalFactorForThisStudentCareer(offerData,careerData, idStudent)){
                		personalFactorDAO.generateDefaultPersonalFactor(offerData,careerData,idStudent);
                		personalFactorDAO.generateDefaultPersonalFactor(idStudent);
                    	socialFactorDAO.generateDefaultSocialFactor(idStudent);
                    	institutionalFactorDAO.generateDefaultInstitutionalFactor(offerData,careerData,idStudent);
                	}
                	institutionalFactorDAO.generateDefaultInstitutionalFactor(offerData,careerData,moduleData,matterData,idStudent);
                	personalFactorDAO.generateDefaultPersonalFactor(offerData,careerData,moduleData,matterData,idStudent);
                	
                }else{
                	
                	///// ESTUDIANTE - CARRERA /////                	
                	if(recuperateCareer){
                		academicDAO.relationSaveOrUpdateCareerStudent(idCareer,studentForSave);  
                		
                	}else{
                		academicDAO.relationSaveOrUpdateCareerStudent(careerForSave,studentForSave);
                	}
                	
                	 ///// ESTUDIANTE - MODULO /////
                	if(recuperateModule){
                		academicDAO.relationSaveOrUpdateModuleStudent(idModule,studentForSave);
                	}else{
                		academicDAO.relationSaveOrUpdateModuleStudent(moduleForSave,studentForSave);
                	}
                	
                    ///// ESTUDIANTE - MATERIA /////	
                	if(recuperateMatter){
                		academicDAO.relationSaveOrUpdateMatterStudent(idMatter,studentForSave);
                	}else{
                		academicDAO.relationSaveOrUpdateMatterStudent(matterForSave,studentForSave);
                	}
                	
                	
                	//guardar configuraciones adicionales 
                	//del estudiante
                	//predictionParameterDAO.generateDefaultPredictionParameter(offerData,careerData,moduleData,matterData,studentForSave.getId());
                	//personalFactorDAO.generateDefaultPersonalFactor(offerData,careerData,moduleData,matterData,studentForSave.getId());
                	//socialFactorDAO.generateDefaultSocialFactor(studentForSave.getId());
                	//institutionalFactorDAO.generateDefaultInstitutionalFactor(offerData,careerData,moduleData,matterData,studentForSave.getId());
                	
                	if(!personalFactorDAO.existPersonalFactorForThisStudentCareer(offerData,careerData, studentForSave.getId())){
                		personalFactorDAO.generateDefaultPersonalFactor(offerData,careerData,studentForSave.getId());
                		personalFactorDAO.generateDefaultPersonalFactor(studentForSave.getId());
                    	socialFactorDAO.generateDefaultSocialFactor(studentForSave.getId());
                    	institutionalFactorDAO.generateDefaultInstitutionalFactor(offerData,careerData,studentForSave.getId());
                	}
                	institutionalFactorDAO.generateDefaultInstitutionalFactor(offerData,careerData,moduleData,matterData,studentForSave.getId());
                	personalFactorDAO.generateDefaultPersonalFactor(offerData,careerData,moduleData,matterData,studentForSave.getId());
                }
            }
            
            /**creacion de la configuracion de porcentajes*/
            
            long idTeacherNew = teacherDAO.returnTeacherId(teacherIC);
            configurationDAO.generateDefaultConfiguration(offerData,careerData,moduleData,matterData,idTeacherNew);
            
            
            matterForSave = new Matter();
            moduleForSave = new Module();
            teacherForSave = new Teacher();
            careerForSave = new Career();
            
        
            flash = faceContext.getExternalContext().getFlash();
            msgInfo("Datos Grabados correctamente");
            flash.setKeepMessages(true);

            return "teacher_admin";
        }

    }
    
    
    
    /**mensajes*/
    
   public void msgFatal(String message){
   	 facesMessage = new FacesMessage(FacesMessage.SEVERITY_FATAL,"Error", message);
        faceContext.addMessage(null, facesMessage);
   }
   
   public void msgFatalConnection(){
   	 facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", "Falló la "
   	 		+ "conexión a internet.");
        faceContext.addMessage(null, facesMessage);
   }
   
   public void msgError(String message){
   	facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",message);
       faceContext.addMessage(null, facesMessage);
   }
   
   public void msgInfo(String message){
	   facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", message);
       faceContext.addMessage(null, facesMessage);
   }
   /**limpiar variables en caso de error*/
   
   public void cleanOffers(){
       listCareer = new TreeMap<String, String>();
       listModulesParallel = new TreeMap<String, String>();
       listMatters = new TreeMap<String, String>();
       mapOfferDateStartEnd = new TreeMap<String, String>();
   	startDateMatter = new Date();
       endDateMatter = new Date();
       
       offer = "";
       career = "";
       parallel = "";
       matter = "";
   }
   
   public void cleanCareers(){
   	   listCareer = new TreeMap<String, String>();
          listModulesParallel = new TreeMap<String, String>();
          listMatters = new TreeMap<String, String>();
          startDateMatter = new Date();
          endDateMatter = new Date();
          parallel = "";
          matter = "";
   }
   
   public void cleanMatters(){
   	startDateMatter = new Date();
       endDateMatter = new Date();      
       listMatters = new TreeMap<String, String>();

   }
   
   public void cleanModuleParallels(){
       listModulesParallel = new TreeMap<String, String>();
       listMatters = new TreeMap<String, String>();
       startDateMatter = new Date();
       endDateMatter = new Date();
       matter = "";
   }
    
   /**limpiar variable al iniciar*/
   
   public void startPeriods(){
   	/**
        * listas para presentar en los oneMenu
        */
       listPeriods = new TreeMap<String, String>();
       listAcademicOffer = new TreeMap<String, String>();
       listCareer = new TreeMap<String, String>();
       listCareerVerif = new TreeMap<String, String>();
       listModulesParallel = new TreeMap<String, String>();
       numeroModuloParalelo = new TreeMap<String, String>();
       listMatters = new TreeMap<String, String>();
       startDateMatter = new Date();
       endDateMatter = new Date();
       
       mapOfferDateStartEnd = new TreeMap<String, String>();
       /**
        * Datos academicos
        */
       periods = "";
       offer = "";
       careerCode = "";
       career = "";
       parallel = "";
       matter = "";
   }
   
   public void startOffers(){
        listAcademicOffer = new TreeMap<String, String>();
        listCareer = new TreeMap<String, String>();
        listModulesParallel = new TreeMap<String, String>();
        listMatters = new TreeMap<String, String>();
        startDateMatter = new Date();
        endDateMatter = new Date();
        mapOfferDateStartEnd = new TreeMap<String, String>();
   
        offer = "";
        career = "";
        parallel = "";
        matter = "";
    }
    
   public void startCarrers(){
        listCareer = new TreeMap<String, String>();
        listModulesParallel = new TreeMap<String, String>();
        listMatters = new TreeMap<String, String>();
        startDateMatter = new Date();
        endDateMatter = new Date();
        parallel = "";
        matter = "";
    }
    
   public void startModuleParallels(){
        listModulesParallel = new TreeMap<String, String>();
        listMatters = new TreeMap<String, String>();
        startDateMatter = new Date();
        endDateMatter = new Date();
        matter = "";
    }
    
   public void startMatters(){
        listMatters = new TreeMap<String, String>();
        listDataMatters = new TreeMap<String, String>();
        startDateMatter = new Date();
        endDateMatter = new Date();
    }
   
   
   /**
     * *********************************************
     */
    // getters y setters
    public String getPeriods() {
        return periods;
    }
    public void setPeriods(String periods) {
        this.periods = periods;
    }
    public void setListPeriods(Map<String, String> listPeriods) {
        this.listPeriods = listPeriods;
    }
    public Map<String, String> getListAcademicOffer() {
        return listAcademicOffer;
    }
    public void setListAcademicOffer(Map<String, String> listAcademicOffer) {
        this.listAcademicOffer = listAcademicOffer;
    }
    public Map<String, String> getListCareer() {
        return listCareer;
    }
    public void setListCareer(Map<String, String> listCareer) {
        this.listCareer = listCareer;
    }
    public String getOffer() {
        return offer;
    }
    public void setOffer(String offer) {
        this.offer = offer;
    }
    public String getCareer() {
        return career;
    }
    public void setCareer(String career) {
        this.career = career;
    }
    public String getParallel() {
        return parallel;
    }
    public void setParallel(String parallel) {
        this.parallel = parallel;
    }
    public Map<String, String> getListModulesParallel() {
        return listModulesParallel;
    }
    public void setListModulesParallel(Map<String, String> listModulesParallel) {
        this.listModulesParallel = listModulesParallel;
    }
    public Map<String, String> getNumeroModuloParalelo() {
        return numeroModuloParalelo;
    }
    public void setNumeroModuloParalelo(Map<String, String> numeroModuloParalelo) {
        this.numeroModuloParalelo = numeroModuloParalelo;
    }
    public Map<String, String> getListMatters() {
        return listMatters;
    }
    public void setListMatters(Map<String, String> listMatters) {
        this.listMatters = listMatters;
    }
    public String getMatter() {
        return matter;
    }
    public void setMatter(String matter) {
        this.matter = matter;
    }
    public String getCareerTemp() {
        return careerCode;
    }
    public void setCareerTemp(String careerTemp) {
        this.careerCode = careerTemp;
    }
    public Map<String, String> getListDataMatters() {
        return listDataMatters;
    }
    public void setListDataMatters(Map<String, String> listDataMatters) {
        this.listDataMatters = listDataMatters;
    }
    public Double getNumHoursMatter() {
        return numHoursMatter;
    }
    public void setNumHoursMatter(Double numHoursMatter) {
        this.numHoursMatter = numHoursMatter;
    }
    public Student getStudent() {
        return student;
    }
    public void setStudent(Student student) {
        this.student = student;
    }
    public Map<String, String> getListCareerVerif() {
        return listCareerVerif;
    }
    public void setListCareerVerif(Map<String, String> listCareerVerif) {
        this.listCareerVerif = listCareerVerif;
    }
    public List<TeacherModuleMatterUtil> getListTeacherModuleMatter() {
        return listTeacherModuleMatter;
    }
    public void setListTeacherModuleMatter(
            List<TeacherModuleMatterUtil> listTeacherModuleMatter) {
        this.listTeacherModuleMatter = listTeacherModuleMatter;
    }
	public String[] getVector() {
		return vector;
	}
	public void setVector(String[] vector) {
		this.vector = vector;
	}
	public String getModuleIndv() {
		return moduleIndv;
	}
	public void setModuleIndv(String moduleIndv) {
		this.moduleIndv = moduleIndv;
	}
	public String getParallelIndv() {
		return parallelIndv;
	}
	public void setParallelIndv(String parallelIndv) {
		this.parallelIndv = parallelIndv;
	}
	public Date getStartDateMatter() {
		return startDateMatter;
	}
	public void setStartDateMatter(Date startDateMatter) {
		this.startDateMatter = startDateMatter;
	}
	public Date getEndDateMatter() {
		return endDateMatter;
	}
	public void setEndDateMatter(Date endDateMatter) {
		this.endDateMatter = endDateMatter;
	}


	public String getMsgIncorrectData() {
		return msgIncorrectData;
	}


	public void setMsgIncorrectData(String msgIncorrectData) {
		this.msgIncorrectData = msgIncorrectData;
	}


	public String getMsgIncompleteData() {
		return msgIncompleteData;
	}


	public void setMsgIncompleteData(String msgIncompleteData) {
		this.msgIncompleteData = msgIncompleteData;
	}


	public String getMsgExistData() {
		return msgExistData;
	}


	public void setMsgExistData(String msgExistData) {
		this.msgExistData = msgExistData;
	}
	
}
