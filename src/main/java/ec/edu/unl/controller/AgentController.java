package ec.edu.unl.controller;

import jade.util.leap.Properties;
import jade.wrapper.gateway.JadeGateway;

import java.io.Serializable;

public class AgentController implements Serializable{
	
    /**Levanta el agente con sus propiedades*/
    public void startGatewayAgent(){
    	Properties prop = new Properties();
    	prop.put("host", "localhost");
    	prop.put("port", 1099);

    	/**Localización del agente dentro del
        proyecto*/
        JadeGateway.init("ec.edu.unl.agents.agents.AcademicAgent",prop);
    }
    
    /**Fin de la ejecución del agente*/
    public void endGateWayAgent(){
    	JadeGateway.shutdown();
    }
    

    /**Método utilizado con el fin de pasar información al 
     * agente para que ejecute las operaciones correspondientes*/
    public void executeGateWayAgent(Object obj){
    		try {
				
    			JadeGateway.execute(obj);
			
    		} catch (Exception e) {
				e.printStackTrace();
			} 
    }
    
}
