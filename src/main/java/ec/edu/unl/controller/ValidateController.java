package ec.edu.unl.controller;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import ec.edu.unl.DAO.TeacherDAO;
import ec.edu.unl.entity.Student;
import ec.edu.unl.entity.Teacher;
import ec.edu.unl.operations.Conections;
import ec.edu.unl.operations.Operations;
import ec.edu.unl.utilities.login.Login;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

public class ValidateController implements Serializable{

	static HttpServletRequest httpServletRequest;
	static FacesContext faceContext;
	FacesMessage facesMessage;
	static Flash flash;

	private String userName;


	@Autowired
	@Qualifier("operationsImpl")
	private Operations operations;

	@Autowired
	@Qualifier("student")
	private Student student;

	@Autowired
	@Qualifier("teacher")
	private Teacher teacher;

	@Autowired
	@Qualifier("login")
	private Login login;

	@Autowired
	@Qualifier("conectionImpl")
	private Conections conectionWS;

	@Autowired
	@Qualifier("teacherDAOImpl")
	private TeacherDAO teacherDAO;
	
	@Autowired
	@Qualifier("agentController")
	private AgentController agentController;

	private Map<String, String> checker;
	
	int isStudent;

    public void init(){
    	faceContext = FacesContext.getCurrentInstance();
		httpServletRequest = (HttpServletRequest) faceContext.getExternalContext().getRequest();
    }	

	public String validate() {
		
		init();
		
		String route = "";
		checker = new TreeMap<String, String>();
		String identification_card = login.getIdentification_card();
		String password = login.getPassword();

		try {
			
			//String studentLogin = conectionWS.conectionValidacionWS().sgawsValidarEstudiante(identification_card, password);
			String teacherLogin = conectionWS.conectionValidacionWS().sgawsValidarDocente(identification_card, password);

			String studentLogin = operations.validarEstudiantePrueba(identification_card);	
//			String teacherLogin = operations.validarDocentePrueba(identification_card);
	
			checker.put("studentLogin", studentLogin);
			
			//////////aqui se cambio/////////////
			checker.put("teacherLogin", teacherLogin);
			//////////por esto////////////////
			
			//checker.put("teacherLogin", "true");
			///////////////////////////////////////

			if (checker.get("studentLogin").equals("true")) {
	
				
				String studentData = conectionWS.conectionPersonalWS().sgawsDatosEstudiante(identification_card);
				JSONArray array = operations.processJSON(studentData);
	
				/**
				 * se crea objeto student(estudiante)
				 *
				 */
				student.setIdentification_card(String.valueOf(array.get(0)));
				student.setFirst_name(String.valueOf(array.get(1)));
				student.setLast_name(String.valueOf(array.get(2)));
				

				/**
				 * se almacena en sesion el objeto
				 */
				flash = faceContext.getExternalContext().getFlash();
				httpServletRequest.getSession().setAttribute("studentSession",student);
				facesMessage = new FacesMessage("Acceso Correcto", "Bienvenido!");	
				faceContext.addMessage(null, facesMessage);
				flash.setKeepMessages(true);

				userName = httpServletRequest.getSession().getAttribute("studentSession").toString();
				isStudent = 1;
				
				route = "student_ok";
	
			} else {
				if (checker.get("teacherLogin").equals("true")) {
	
					//////// aqui se cambio esto /////////
					
					String teacherData = conectionWS.conectionPersonalWS().sgawsDatosDocente(login.getIdentification_card());
					JSONArray array = operations.processJSON(teacherData);
	
					teacher.setFirst_name(String.valueOf(array.get(0)));
					teacher.setLast_name(String.valueOf(array.get(1)));
					teacher.setIdentification_card(String.valueOf(array.get(2)));
					
					
					///////// por esto //////////////
	
					 /*
					teacher.setFirst_name("nombre 1");
					teacher.setLast_name("apellido 1");
					teacher.setIdentification_card("1104258460");
					*/
					/////////////////////////////////////////////
					
					
					/**
					 * se almacena en sesion el objeto
					 */
					httpServletRequest.getSession().setAttribute("teacherSession",teacher);
					userName = httpServletRequest.getSession().getAttribute("teacherSession")+"";
					
					/**no es estudiante*/
					isStudent = 0;
					
					/*** mensaje de bienvenida */
					flash = faceContext.getExternalContext().getFlash();
					facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "","Bienvenido " + userName);
					faceContext.addMessage(null, facesMessage);
					flash.setKeepMessages(true);
	
					/** verifica si el docente ya se encuentra en base de datos */
					Teacher teacherReturn = (Teacher) teacherDAO.findTeacherByIdentificactionCard(teacher.getIdentification_card());
	
					if (teacherReturn != null) {
						route = "teacher_admin";
	
					} else {
						route = "teacher_is_new";
					}
	
				} else {
				;
					flash = faceContext.getExternalContext().getFlash();
					facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", "Usuario o contraseña incorrecto");
					faceContext.addMessage(null, facesMessage);
					flash.setKeepMessages(true);
					
					route = "login";
				}
			}
		
		} catch (Exception ex) {

			facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error","Fallo la conexión con el SGA, por favor intentelo nuevamente");
			faceContext.addMessage(null, facesMessage);
			ex.printStackTrace();
			
			return "";
		}
		return route;
	}



	public String logout() {

		init();
		
		flash = faceContext.getExternalContext().getFlash();

		agentController.endGateWayAgent();
		
		httpServletRequest.getSession().invalidate();
		facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "","Session cerrada correctamente");
		faceContext.addMessage(null, facesMessage);
		flash.setKeepMessages(true);
		
		return "logout";
	}

	
	/**
	 * getters y setters
	 */

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Operations getOperations() {
		return operations;
	}
	public void setOperations(Operations operations) {
		this.operations = operations;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	public Teacher getTeacher() {
		return teacher;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	public Login getLogin() {
		return login;
	}
	public void setLogin(Login login) {
		this.login = login;
	}
	public Map<String, String> getChecker() {
		return checker;
	}
	public void setChecker(Map<String, String> checker) {
		this.checker = checker;
	}
	public int getIsStudent() {
		return isStudent;
	}
	public void setIsStudent(int isStudent) {
		this.isStudent = isStudent;
	}
}
