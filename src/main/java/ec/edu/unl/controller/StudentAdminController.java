package ec.edu.unl.controller;

import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SlideEndEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import ec.edu.unl.DAO.AcademicDAO;
import ec.edu.unl.DAO.ClassWorkDAO;
import ec.edu.unl.DAO.ConfigurationDAO;
import ec.edu.unl.DAO.EvaluationDAO;
import ec.edu.unl.DAO.ExtraClassWorkDAO;
import ec.edu.unl.DAO.InstitutionalFactorDAO;
import ec.edu.unl.DAO.LaboratoryDAO;
import ec.edu.unl.DAO.LessonDAO;
import ec.edu.unl.DAO.OfferDAO;
import ec.edu.unl.DAO.ParticipationDAO;
import ec.edu.unl.DAO.PersonalFactorDAO;
import ec.edu.unl.DAO.SocialFactorDAO;
import ec.edu.unl.DAO.StudentDAO;
import ec.edu.unl.agents.objects.Parameter;
import ec.edu.unl.entity.InstitutionalFactor;
import ec.edu.unl.entity.PersonalFactor;
import ec.edu.unl.entity.SocialFactor;
import ec.edu.unl.entity.Student;
import ec.edu.unl.entity._InstitutionalFactor;
import ec.edu.unl.entity._PersonalFactor;
import ec.edu.unl.entity.__PersonalFactor;
import ec.edu.unl.operations.Operations;
import ec.edu.unl.rest.RestClient;
import ec.edu.unl.utilities.evaluation.EvaluationUtil;
import ec.edu.unl.utilities.lesson.LessonUtil;
import ec.edu.unl.utilities.participation.ParticipationUtil;
import ec.edu.unl.utilities.academic.ConfigurationUtil;
import ec.edu.unl.utilities.academic.PredictionParameterUtil;
import ec.edu.unl.utilities.academic.ReferenceLink;
import ec.edu.unl.utilities.academic.StudentMatterUtil;
import ec.edu.unl.utilities.academic.TeacherUtil;
import ec.edu.unl.utilities.classWork.ClassWorkUtil;
import ec.edu.unl.utilities.extraClassWork.ExtraClassWorkUtil;
import ec.edu.unl.utilities.laboratory.LaboratoryUtil;

public class StudentAdminController implements Serializable{
	
	@Autowired
	@Qualifier("academicDAOImpl")
	private AcademicDAO academicDAO;
	
	@Autowired
    @Qualifier("offerDAOImpl")
    private OfferDAO offerDAO;
	
    @Autowired
    @Qualifier("configurationDAOImpl")
    private ConfigurationDAO configurationDAO;
	
    @Autowired
    @Qualifier("personalFactorDAOImpl")
    private PersonalFactorDAO personalFactorDAO;
    
    @Autowired
    @Qualifier("socialFactorDAOImpl")
    private SocialFactorDAO socialFactorDAO;
    
    @Autowired
    @Qualifier("institutionalFactorDAOImpl")
    private InstitutionalFactorDAO institutionalFactorDAO;
    
	@Autowired
	@Qualifier("operationsImpl")
	private Operations operations;
	
	@Autowired
	@Qualifier("agentController")
	private AgentController agentController;
	    
    @Autowired
    @Qualifier("studentDAOImpl")
    StudentDAO studentDAO;
	
    @Autowired
    @Qualifier("evaluationDAOImpl")
    EvaluationDAO evaluationDAO;

    @Autowired
    @Qualifier("lessonDAOImpl")
    LessonDAO lessonDAO;
    
    @Autowired
    @Qualifier("classWorkDAOImpl")
    ClassWorkDAO classWorkDAO;    
    
    @Autowired
    @Qualifier("extraClassWorkDAOImpl")
    ExtraClassWorkDAO extraClassWorkDAO;
    
    @Autowired
    @Qualifier("participationDAOImpl")
    ParticipationDAO participationDAO;

    @Autowired
    @Qualifier("laboratoryDAOImpl")
    LaboratoryDAO laboratoryDAO;
    
    @Autowired
    @Qualifier("restClient")
    RestClient restClient;
    
    @Autowired
    StudentSelectorController studentSelectorController;

    static HttpServletRequest httpServletRequest;
	static FacesContext faceContext;
	FacesMessage facesMessage;
	static Flash flash;
	
	long offerSelect;
	long careerSelect;
	long moduleSelect;
	long idStudent;
	long idMatter;
	
	List<StudentMatterUtil> listStudentMatter;
	
	List<EvaluationUtil> evaluations;
	List<LessonUtil> lessons;
	List<ClassWorkUtil> classWorks;
	List<ExtraClassWorkUtil> extraClassWorks;
	List<ParticipationUtil> participations;
	List<LaboratoryUtil> laboratories;
	
	private LineChartModel modelEvaluation;
	private LineChartModel modelLesson;
	private LineChartModel modelClassWork;
	private LineChartModel modelExtraClassWork;
	private LineChartModel modelParticipation;
	private LineChartModel modelLaboratory;
	
	private String messageEvaluationOK;
	private String messageEvaluationFATAL;
	
	private String messageLessonOK;
	private String messageLessonFATAL;
	
	private String messageClassWorkOK;
	private String messageClassWorkFATAL;
	
	private String messageExtraClassWorkOK;
	private String messageExtraClassWorkFATAL;
	
	private String messageParticipationOK;
	private String messageParticipationFATAL;
	
	private String messageLaboratoryOK;
	private String messageLaboratoryFATAL;
	
	private String messageLessonOne;
	private String messageEvaluationOne;
	private String messageClassWorkOne;
	private String messageExtraClassWorkOne;
	private String messageParticipationOne;
	private String messageLaboratoryOne;
	
	private String messageLessonEmpty;
	private String messageEvaluationEmpty;
	private String messageClassWorkEmpty;
	private String messageExtraClassWorkEmpty;
	private String messageParticipationEmpty;
	private String messageLaboratoryEmpty;

	private String messageLessonNone;
	private String messageEvaluationNone;
	private String messageClassWorkNone;
	private String messageExtraClassWorkNone;
	private String messageParticipationNone;
	private String messageLaboratoryNone;
	
	private String msgEvalLessPart = "";
	private String msgCwEcwLab = "";
	private String msgPart2="";
	private String msgFinal="";
	
	private String messageEvaluationComplete = "";
	private String messageLessonComplete = "";
	private String messageClassWorkComplete = "";
	private String messageExtraClassWorkComplete = "";
	private String messageParticipationComplete = "";
	private String messageLaboratoryComplete = "";
	
	private Date startDate;
	private Date endDate;
	
	private Parameter parameters;
	
	private int activeTabIndex;
	
	private List<ConfigurationUtil> listConf;
	
	private List<PredictionParameterUtil> listConfStudent;
	
	private String eam;
	private String leam;
	private String cwam;
	private String ecwam;
	private String pam;
	private String lam;
	
	private String eam2;
	private String leam2;
	private String cwam2;
	private String ecwam2;
	private String pam2;
	private String lam2;

	private String eam3;
	private String leam3;
	private String cwam3;
	private String ecwam3;
	private String pam3;
	private String lam3;
	
	private double evaluationPartialAverage;
	private double lessonPartialAverage;
	private double classWorkPartialAverage;
	private double extraClassWorkPartialAverage;
	private double participationPartialAverage;
	private double laboratoryPartialAverage;
	
	private double evaluationAverage;
	private double lessonAverage;
	private double classWorkAverage;
	private double extraClassWorkAverage;
	private double participationAverage;
	private double laboratoryAverage;
	
	private double totalAverage;
	
	private String matterName;
	

	private String suggestion1;
	//private String suggestion2;
	
	private String title2;
	
	private double averageReal;
	private double averagePred;
	
	/*clases para recuperar los factores
	 * para prediccion*/
	
	private PersonalFactor personalFactor;
	private _PersonalFactor _personalFactor;
	private __PersonalFactor __personalFactor;
	private SocialFactor socialFactor;
	private InstitutionalFactor institutionalFactor;
	private _InstitutionalFactor _institutionalFactor;
	
	private String titleButtonhelp;
	private String textHelp1;
	private String texthelp2;
	private String textLinkAdjustPrediction;
	
	/*indices activos*/
	private int activeTabIndexAccordion;
	private int activeTabIndexEvals;
	private int activeTabIndexLess;
	private int activeTabIndexCWs;
	private int activeTabIndexECWs;
	private int activeTabIndexParts;
	private int activeTabIndexLabs;
	private int suggestionActiveIndex;
	
	/*listas para presentar información 
	 * de ayuda*/
	
	private List<ReferenceLink> listReferenceBlog;
	private List<ReferenceLink> listReferenceVideo;
	private List<ReferenceLink> listReferenceNews;
	private List<ReferenceLink> listReferenceWebPage;
	private List<ReferenceLink> listReferenceTestIQ;

	private String param;
	
	public StudentAdminController(){
		
	}
	
	
	
    public void init(){
    	faceContext = FacesContext.getCurrentInstance();
		httpServletRequest = (HttpServletRequest) faceContext.getExternalContext().getRequest();
    }
	
	public void loadDataStudent(){
		
		init();
		Student stud = (Student) httpServletRequest.getSession().getAttribute("studentSession");	
		/**recupera id del estudiante*/
		idStudent = studentDAO.returnStudentId(stud.getIdentification_card());
	}
		
	public String renderStudentMain() {
		init();
		isLoadMatter =0;
		matterName = "";
		activeTabIndex =0;
		activeTabIndexAccordion = 0;
		activeTabIndexCWs = 0;
		activeTabIndexECWs = 0;
		activeTabIndexEvals = 0;
		activeTabIndexLabs = 0;
		activeTabIndexLess = 0;
		activeTabIndexParts = 0;
		
		/**recuperacion de parametros*/
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		
		try{		
			offerSelect = Long.parseLong(params.get("offerSelect"));
			careerSelect = Long.parseLong(params.get("careerSelect"));
			moduleSelect = Long.parseLong(params.get("moduleSelect"));
	
		}catch(java.lang.NumberFormatException ex){
			msgError("Faltan parametros por seleccionar");
			return "";
		}
			
		loadDataStudent();
		listStudentMatter = academicDAO.returnListStudentMatter(offerSelect,careerSelect,moduleSelect,idStudent);
		
		return "student_main";
	}
	
	public String viewGraph(){
		
		isLoadMatter = 1;
		
		evaluations = new ArrayList<EvaluationUtil>();
		lessons = new ArrayList<LessonUtil>();
		classWorks = new ArrayList<ClassWorkUtil>();
		extraClassWorks = new ArrayList<ExtraClassWorkUtil>();
		participations = new ArrayList<ParticipationUtil>();
		laboratories = new ArrayList<LaboratoryUtil>();
		
		messageEvaluationOK = "";
		messageEvaluationFATAL = "";
		
		messageLessonOK = "";
		messageLessonFATAL= "";
		
		messageClassWorkOK= "";
		messageClassWorkFATAL= "";
		
		messageExtraClassWorkOK= "";
		messageExtraClassWorkFATAL= "";
		
		messageParticipationOK= "";
		messageParticipationFATAL= "";
		
		messageLaboratoryOK= "";
		messageLaboratoryFATAL= "";
		
		
		messageLessonOne="";
		messageEvaluationOne="";
		messageClassWorkOne="";
		messageExtraClassWorkOne="";
		messageParticipationOne="";
		messageLaboratoryOne="";
		
		messageLessonEmpty="";
		messageEvaluationEmpty="";
		messageClassWorkEmpty="";
		messageExtraClassWorkEmpty="";
		messageParticipationEmpty="";
		messageLaboratoryEmpty="";

		messageLessonNone="";
		messageEvaluationNone="";
		messageClassWorkNone="";
		messageExtraClassWorkNone="";
		messageParticipationNone="";
		messageLaboratoryNone="";
		
		
		
		/*recuperacion de parametros*/
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		
		idMatter = Long.parseLong(params.get("matterSelect"));
		matterName = params.get("matterName");
		
		/*actualiza datos estudiante*/
		loadDataStudent();
		
		/*actualiza listas de calificaciones*/
		loadListQualificationsStudents();
		
		//operaciones con agente inteligente
		startAgentOperations();
		
		/*Crea el modelo en base a los datos 
		 * que se reciban como parámetro*/
		createModel(parameters,evaluations,lessons,classWorks,extraClassWorks,participations,laboratories,offerSelect,careerSelect,moduleSelect,idMatter,matterName,idStudent);
		
		return "student_view_graph";
		
	}

	public void loadListQualificationsStudents() {
		
		evaluations = evaluationDAO.returnListStudentEvaluations(offerSelect,careerSelect,moduleSelect, idMatter, idStudent);
		lessons =  lessonDAO.returnListStudentLessons(offerSelect,careerSelect,moduleSelect, idMatter, idStudent);
		classWorks =  classWorkDAO.returnListStudentClassWorks(offerSelect,careerSelect,moduleSelect, idMatter, idStudent);
		extraClassWorks = extraClassWorkDAO.returnListStudentExtraClassWorks(offerSelect,careerSelect,moduleSelect, idMatter, idStudent);
		participations = participationDAO.returnListStudentParticipations(offerSelect,careerSelect,moduleSelect, idMatter, idStudent);	
		laboratories =  laboratoryDAO.returnListStudentLaboratories(offerSelect,careerSelect,moduleSelect, idMatter, idStudent);
			
		
	}

	public void startAgentOperations(){
		/*recuperacion y formateo de la fecha de
		 * inicio - fin de la oferta */
		startDate = offerDAO.returnOfferById(offerSelect).getStartDate();
		endDate = offerDAO.returnOfferById(offerSelect).getEndDate();
		
		/*se recupera el docente de la materia*/
		TeacherUtil teacherData = studentDAO.returnTeacherByMatter(offerSelect, careerSelect, moduleSelect, idMatter, idStudent);
		listConf = configurationDAO.returnListConfigurations(offerSelect, careerSelect, moduleSelect, idMatter, teacherData.getIdTeacher());
		
		/*se recupera de la BD los factores para
		 * prediccion con agente inteligente */
		
		loadDataFactorsForPrediction();
	
		
		//crea los promedios para el estudiante
		calculateAverageStudent(evaluations,lessons,classWorks,extraClassWorks,participations,laboratories);
		
		//para que no exista alteraciones en la lista original
		List<EvaluationUtil> evalAgent = new ArrayList<EvaluationUtil>();
		evalAgent.addAll(evaluations);
		List<LessonUtil> lessAgent = new ArrayList<LessonUtil>();
		lessAgent.addAll(lessons);
		List<ClassWorkUtil> cWAgent = new ArrayList<ClassWorkUtil>();
		cWAgent.addAll(classWorks);
		List<ExtraClassWorkUtil> eCWAgent = new ArrayList<ExtraClassWorkUtil>();
		eCWAgent.addAll(extraClassWorks);
		List<ParticipationUtil> partAgent = new ArrayList<ParticipationUtil>();
		partAgent.addAll(participations);
		List<LaboratoryUtil> labAgent = new ArrayList<LaboratoryUtil>();
		labAgent.addAll(laboratories);
	
		Date startOferAgent = startDate; 
		Date endOfferAgent = endDate;
		
		/*prepara loa datos a pasar al agente*/
		parameters = new Parameter();
		parameters.setEvaluations(evalAgent);
		parameters.setLessons(lessAgent);
		parameters.setClassWorks(cWAgent);
		parameters.setExtraClassWorks(eCWAgent);
		parameters.setParticipations(partAgent);
		parameters.setLaboratories(labAgent);
		
		/*fecha de inicio - fin de la oferta*/
		parameters.setStartOffer(startOferAgent);
		parameters.setEndOffer(endOfferAgent);
		
		/*configuraciones docente*/
		parameters.setListConfigurations(listConf);	
		
		/*factores configurados por el estudiante*/
		parameters.setPersonalFactor(personalFactor);
		parameters.set_personalFactor(_personalFactor);
		parameters.set__personalFactor(__personalFactor);
		parameters.setSocialFactor(socialFactor);
		parameters.setInstitutionalFactor(institutionalFactor);
		parameters.set_institutionalFactor(_institutionalFactor);
		
		/*inicio de operaciones con 
		 * el agente al mismo se le pasa un objeto,
		 * lo procesa y retorna un objeto */
		agentController.startGatewayAgent();
		agentController.executeGateWayAgent(parameters);
		
	}
	
	public void loadDataFactorsForPrediction(){
		personalFactor = personalFactorDAO.returnPersonalFactor(offerSelect, careerSelect, moduleSelect, idMatter, idStudent);
		_personalFactor = personalFactorDAO.returnPersonalFactor(offerSelect, careerSelect, idStudent);
		__personalFactor = personalFactorDAO.returnPersonalFactor(idStudent);
		
		socialFactor = socialFactorDAO.returnSocialFactor(idStudent);
		institutionalFactor = institutionalFactorDAO.returnInstitutionalFactor(offerSelect, careerSelect, moduleSelect, idMatter, idStudent);
		_institutionalFactor= institutionalFactorDAO.returnInstitutionalFactor(offerSelect, careerSelect, idStudent);
		
	}
	
	public String redirectViewDataFactorEdit(){
		loadSelectedSuggesionTest();
		activeTabIndex = 0;
		loadDataFactorsForPrediction();
		return "student_view_factor_edit";
	}
	
	public void cancelOperation(){
		loadDataFactorsForPrediction();
		
	}
	
	public String saveNewFactorsForPrediction(){
		init();
		
		
		/*guarda los datos actualizados*/
		personalFactorDAO.updateAllNewPersonalFactor(offerSelect, careerSelect, moduleSelect, idMatter, idStudent, personalFactor,_personalFactor,__personalFactor);
		socialFactorDAO.updateAllNewSocialFactor(idStudent, socialFactor);
		institutionalFactorDAO.updateAllNewInstitutionalFactor(offerSelect, careerSelect, moduleSelect, idMatter, idStudent, institutionalFactor,_institutionalFactor);
		
		
		//actualiza informacion de graficas
		loadListQualificationsStudents();
		startAgentOperations();
		createModel(parameters,evaluations,lessons,classWorks,extraClassWorks,participations,laboratories,offerSelect,careerSelect,moduleSelect,idMatter,matterName,idStudent);
		
		msgInfo("Información actualizada correctamente");
		return "";
	}
	
	public void onTabChange(TabChangeEvent event) {
		
	    init();
		String idTab = event.getTab().getId();
		if(idTab.equals("tabPersonalFactor")){
			//generalVar = "evaluation";
			//loadParameters("evaluation");
			TabView tv = (TabView) event.getComponent();
		    this.setActiveTabIndex(tv.getActiveIndex());	
		}
		if(idTab.equals("tabSocialFactor")){
			
			TabView tv = (TabView) event.getComponent();
		
		    this.setActiveTabIndex(tv.getActiveIndex());
		   
		}
		if(idTab.equals("tabInstitutionalFactor")){

			TabView tv = (TabView) event.getComponent();
		    this.setActiveTabIndex(tv.getActiveIndex());
			
		}
		
		
		msgInfo("Se ha seleccionado "+ event.getTab().getTitle());
	}
	
	public void calculateAverageStudent(List<EvaluationUtil> evaluations,
			List<LessonUtil> lessons, List<ClassWorkUtil> classWorks,
			List<ExtraClassWorkUtil> extraClassWorks,
			List<ParticipationUtil> participations,
			List<LaboratoryUtil> laboratories) {
		
		eam = "";
		leam = "";
		cwam = "";
		ecwam = "";
		pam = "";
		lam = "";
				
		eam2 = "";
		leam2 = "";
		cwam2 = "";
		ecwam2 = "";
		pam2 = "";
		lam2 = "";
		
		eam3 = "";
		leam3 = "";
		cwam3 = "";
		ecwam3 = "";
		pam3 = "";
		lam3 = "";
		
		evaluationPartialAverage =0.0;
		
		lessonPartialAverage =0.0;
		classWorkPartialAverage = 0.0;
		extraClassWorkPartialAverage =0.0;
		participationPartialAverage =0.0;
		laboratoryPartialAverage =0.0;
		
		
		String text1 = "Con un peso de ";
		String text2 = " a la nota final. ";
		String msg1 = "¡No se califica este parámetro!";
		String msg2 = " - Aun no se han subido calificaciones -";
		String msg3 = "Restan por calificar ";
		
		if(!evaluations.isEmpty()){
			
			int count =0;
			double average =0.0;
			double val = listConf.get(0).getPercentage();
			int num = listConf.get(0).getNumberDefault();
			for(EvaluationUtil eu: evaluations){
				average = average + eu.getQualificationEval(); 
				count ++;
			}
			
			evaluationPartialAverage = average / count;
			evaluationAverage =  evaluationPartialAverage * val;
			
			eam = text1 + (val*100)+" %" + text2;
			eam3 = msg3 + (num-count) + " evaluaciones";
		
		}else{
			double val = listConf.get(0).getPercentage();
			eam = text1 + (val*100)+" %" + text2;
			eam2 = msg2;
		}
		 
		
		if(!lessons.isEmpty()){
			
			int count =0;
			double average =0.0;
			double val = listConf.get(1).getPercentage();
			int num = listConf.get(1).getNumberDefault();
			
			for(LessonUtil eu: lessons){
				average = average + eu.getQualificationLesson(); 
				count ++;
			}
			lessonPartialAverage = average / count;
			lessonAverage = lessonPartialAverage * val;
			
			double res = operations.roundNumber((val*100), 2);
			leam = text1 + res+" %" + text2;
			leam3 = msg3 + (num-count) + " lecciones";
		}else{
			double val = listConf.get(1).getPercentage();
			if(val == 0.00){
				leam2 = msg1;
			}else{
				double res = operations.roundNumber((val*100), 2);
				leam = text1 + res +" %" + text2;
				leam2 = msg2;
			}
		}		
		
		
		if(!classWorks.isEmpty()){
			int count =0;
			double average =0.0;
			double val = listConf.get(2).getPercentage();
			int num = listConf.get(2).getNumberDefault();
			for(ClassWorkUtil eu: classWorks){
				average = average + eu.getQualificationClassWork(); 
				count ++;
			}
			classWorkPartialAverage = average / count;
			classWorkAverage = classWorkPartialAverage * val;
			
			double res = operations.roundNumber((val*100), 2);
			cwam = text1 + res+" %" + text2;
			cwam3 = msg3 + (num-count) + " trabajos autónomos";
		}else{
			double val = listConf.get(2).getPercentage();
			if(val == 0.00){
				cwam2 = msg1;
			}else{
				double res = operations.roundNumber((val*100), 2);
				cwam = text1 + res +" %" + text2;
				cwam2 = msg2;
			}
		}
		
		if(!extraClassWorks.isEmpty()){
			int count =0;
			double average =0.0;
			double val = listConf.get(3).getPercentage();
			int num = listConf.get(3).getNumberDefault();
			for(ExtraClassWorkUtil eu: extraClassWorks){
				average = average + eu.getQualificationExtraClassWork(); 
				count ++;
			}
			extraClassWorkPartialAverage = average / count;
			extraClassWorkAverage = extraClassWorkPartialAverage * val;
			double res = operations.roundNumber((val*100), 2);
			ecwam = text1 + res+" %" + text2;
			ecwam3 = msg3 + (num-count) + " deberes - tareas";
			
		}else{
			double val = listConf.get(3).getPercentage();
			if(val == 0.00){
				ecwam2 = msg1;
			}else{
				double res = operations.roundNumber((val*100), 2);
				ecwam = text1 + res+" %" + text2;
				ecwam2 = msg2;
			}
		}		
		
		
		if(!participations.isEmpty()){
			int count =0;
			double average =0.0;
			double val = listConf.get(4).getPercentage();
			int num = listConf.get(4).getNumberDefault();
			for(ParticipationUtil eu: participations){
				average = average + eu.getQualificationParticipation(); 
				count ++;
			}
			participationPartialAverage = average / count;
			participationAverage = participationPartialAverage * val;
			
			double res = operations.roundNumber((val*100), 2);
			pam = text1 + res+" %" + text2;
			pam3 = msg3 + (num-count) + " participaciones";
		}else{
			double val = listConf.get(4).getPercentage();
			if(val == 0.00){
				pam2 = msg1;
			}else{
				double res = operations.roundNumber((val*100), 2);
				pam = text1 + res+" %" + text2;
				pam2 = msg2;
			}
		}
		
		
		
		if(!laboratories.isEmpty()){
			int count =0;
			double average =0.0;
			double val = listConf.get(5).getPercentage();
			int num = listConf.get(5).getNumberDefault();
			for(LaboratoryUtil eu: laboratories){
				average = average + eu.getQualificationLaboratory(); 
				count ++;
			}
			laboratoryPartialAverage = average / count;
			laboratoryAverage = laboratoryPartialAverage * val;
			double res = operations.roundNumber((val*100), 2);
			lam = text1 + res +" %" + text2;
			lam3 = msg3 + (num-count) + " laboratorios - talleres";
		}else{
			double val = listConf.get(5).getPercentage();
			if(val == 0.00){
				lam2 = msg1;
			}else{
				double res = operations.roundNumber((val*100), 2);
				lam = text1 + res+" %" + text2;
				lam2 = msg2;
				
			}
		}
		
		
		totalAverage = evaluationAverage +
				lessonAverage +
				classWorkAverage +
				extraClassWorkAverage +
				participationAverage +
				laboratoryAverage;
		
	}

	public void createModel(Parameter parametersNew,
			List<EvaluationUtil> evaluations,
			List<LessonUtil> lessons,
			List<ClassWorkUtil> classWorks,
			List<ExtraClassWorkUtil> extraClassWorks,
			List<ParticipationUtil> participations,
			List<LaboratoryUtil> laboratories,
			long idOffer,
			long idCareer,
			long idModule, 
			long idMatter,
			String matterName, 
			long idStudent){
		
		//se reinicia el indice para
		//las tabs
		activeTabIndexAccordion =0;
		activeTabIndexEvals = 0;
		
		modelEvaluation = new LineChartModel();
		modelLesson =  new LineChartModel();
		modelClassWork =  new LineChartModel();
		modelExtraClassWork = new LineChartModel();
		modelParticipation = new LineChartModel();
		modelLaboratory =  new LineChartModel();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		String msg1 = "Su promedio en ";
		String msg2 = " no alcanza el mínimo necesario (70%) "
				+ "a pesar de obtener 10 (100%) en "
				+ "los aportes restantes, "
				+ "por favor comuníquese con el docente responsable.";
		
		String msgComp=" no alcanzó el mínimo necesario (70%), "
				+ "por favor comuníquese con el docente responsable.";
		
		String msg3= "El docente especificó únicamente ";	
		String msg4 = " a calificar por lo que no se "
				+ "pueden generar más notas predictivas.";
		
		String msg5 = "No se tomará en cuenta ";
		String msg6 = " en el proceso de calificación, "
				+ "en caso de inconformidad comunicarse "
				+ "con el docente responsable de la materia.";
				
		String msg7 = "Hasta la fecha no se ha "
				+ "ingresado ninguna calificación de ";
		String msg8 = " al sistema, una vez que se ingresen "
				+ "datos las gráficas serán "
				+ "presentadas automáticamente.";
		
	
		String label1="Calificación real";
		String label2="Predicción";
		
		msgEvalLessPart = "El sistema predice que la siguiente calificación para la próxima ";
		msgCwEcwLab = "El sistema predice que la siguiente calificación para el próximo ";
		msgPart2=", podría aproximarse a ";
		msgFinal=" tomando como base sus calificaciones actuales y los "
				+ "factores que intervienen en el rendimiento académico.";
		
		String msgComplete = "Se ha culminado el proceso de calificacion para ";
		
		String lineColorPrediction = "4BB2C5";
		
		messageEvaluationComplete = "";
		messageLessonComplete = "";
		messageClassWorkComplete = "";
		messageExtraClassWorkComplete = "";
		messageParticipationComplete = "";
		messageLaboratoryComplete = "";
		
		
		messageEvaluationFATAL = "";
		messageLessonFATAL = "";
		messageClassWorkFATAL = "";
		messageExtraClassWorkFATAL="";
		messageParticipationFATAL = "";
		messageLaboratoryFATAL = "";
		
		messageEvaluationOne = "";
		messageLessonOne = "";
		messageClassWorkOne = "";
		messageExtraClassWorkOne="";
		messageParticipationOne = "";
		messageLaboratoryOne = "";
		
		
		
		
		if(!evaluations.isEmpty()){
			
			int numberEvaluations = parametersNew.getListConfigurations().get(0).getNumberDefault();
			
			/*grafica de calificaciones reales*/
			LineChartSeries evaluationChart = new LineChartSeries();
			evaluationChart.setLabel(label1);
			
			for(EvaluationUtil data:evaluations){
				String dateEval = format.format(data.getDateEvaluation());
				evaluationChart.set(dateEval, data.getQualificationEval());			
			}
			if(numberEvaluations > 1){
				LineChartSeries evaluationPrediction = new LineChartSeries();
				evaluationPrediction.setLabel(label2);
					for(EvaluationUtil data:parameters.getEvaluations()){
						String dateEval = format.format(data.getDateEvaluation());
						evaluationPrediction.set(dateEval, data.getQualificationEval());
					}
					modelEvaluation.addSeries(evaluationPrediction);
					modelEvaluation.setSeriesColors(lineColorPrediction+",006400");
				
					if(!parameters.getLowEval()){
						
						if(evaluations.size() == parameters.getEvaluations().size()){
							messageEvaluationFATAL = msg1+ "EVALUACIONES"+msgComp;
						}else{
							messageEvaluationFATAL = msg1+ "EVALUACIONES"+msg2;
						}
						
					}
					
					
					if(evaluations.size() == parameters.getEvaluations().size()){
						messageEvaluationComplete = msgComplete+"EVALUACIONES";
						
					}

				}else{
					messageEvaluationOne = msg3+"una evaluación"+msg4;
				}
			modelEvaluation.addSeries(evaluationChart);
			modelEvaluation.setTitle("EVALUACIONES");
			this.generateYAxisData(modelEvaluation,matterName);
			this.generateXAxisData(modelEvaluation,idOffer,format);
		}else{
			messageEvaluationEmpty = msg7 + "evaluación" + msg8;

		}
		
		
		
//////////////////////////////////////////////////////////////////		
		
		if(!lessons.isEmpty()){
			int numberLessons = parametersNew.getListConfigurations().get(1).getNumberDefault();
			LineChartSeries lessonChart = new LineChartSeries();
			lessonChart.setLabel(label1);
			for(LessonUtil data:lessons){
				String dateLess = format.format(data.getDateLesson());
				lessonChart.set(dateLess, data.getQualificationLesson());
			}
		
			if(numberLessons > 1){
				LineChartSeries lessonPrediction = new LineChartSeries();
				lessonPrediction.setLabel(label2);
					for(LessonUtil data:parameters.getLessons()){
						String dateLess = format.format(data.getDateLesson());
						lessonPrediction.set(dateLess, data.getQualificationLesson());
					}
					modelLesson.addSeries(lessonPrediction);
					modelLesson.setSeriesColors(lineColorPrediction+",FF8C00");	
					if(!parameters.getLowLess()){
						if(lessons.size() == parameters.getLessons().size()){
							messageLessonFATAL = msg1+ "LECCIONES"+msgComp;
						}else{
							messageLessonFATAL = msg1+ "LECCIONES"+msg2;
						}
						
					}
					
					if(lessons.size() == parameters.getLessons().size()){
						messageLessonComplete = msgComplete+"LECCIONES";
					}						
			}else{
				messageLessonOne = msg3+"una lección"+msg4;
			}
			modelLesson.addSeries(lessonChart);
			modelLesson.setTitle("LECCIONES");
			this.generateYAxisData(modelLesson,matterName);
			this.generateXAxisData(modelLesson,idOffer,format);
	
		}else{
			int numberLessons = parametersNew.getListConfigurations().get(1).getNumberDefault();
			
			if(numberLessons == 0){
				messageLessonEmpty =msg5 + "lección"+ msg6;
			}else{
				messageLessonEmpty = msg7 + "lección" + msg8;
			}
		}
		
		
////////////////////////////////////////////////////////////////////

		if(!classWorks.isEmpty()){
			int numberClassWorks = parametersNew.getListConfigurations().get(2).getNumberDefault();
			LineChartSeries classWorkChart = new LineChartSeries();
			classWorkChart.setLabel(label1);
			for(ClassWorkUtil data:classWorks){
				String dateCW = format.format(data.getDateClassWork());
				classWorkChart.set(dateCW, data.getQualificationClassWork());
			}
				if(numberClassWorks > 1){
					LineChartSeries classWorkPrediction = new LineChartSeries();
					classWorkPrediction.setLabel(label2);
						for(ClassWorkUtil data:parameters.getClassWorks()){
							String dateCW = format.format(data.getDateClassWork());
							classWorkPrediction.set(dateCW, data.getQualificationClassWork());
							
						}
						modelClassWork.addSeries(classWorkPrediction);
						modelClassWork.setSeriesColors(lineColorPrediction+",bf00ff");
						if(!parameters.getLowCW()){
							
							if(classWorks.size() == parameters.getClassWorks().size()){
								messageClassWorkFATAL = msg1+ "TRABAJOS AUTÓNOMOS"+msgComp;
							}else{
								messageClassWorkFATAL = msg1+ "TRABAJOS AUTÓNOMOS"+msg2;
							}
							
						}
						
						if(classWorks.size() == parameters.getClassWorks().size()){
							messageClassWorkComplete = msgComplete+"TRABAJOS AUTÓNOMOS";
						}
				}else{
					messageClassWorkOne = msg3 + "un trabajo autónomo" + msg4;
				}
				
			
			modelClassWork.addSeries(classWorkChart);
			modelClassWork.setTitle("TRABAJOS AUTÓNOMOS");
			this.generateYAxisData(modelClassWork,matterName);
			this.generateXAxisData(modelClassWork,idOffer,format);
			
		}else{
			int numberClassWorks = parametersNew.getListConfigurations().get(2).getNumberDefault();
			
			if(numberClassWorks == 0){
				messageClassWorkNone =msg5 + "trabajos autónomos"+ msg6;
			}else{
				messageClassWorkEmpty = msg7 + "trabajo autónomo" + msg8;
			}
		}
		
		
///////////////////////////////////////////////////////////////////////////////
		
		if(!extraClassWorks.isEmpty()){
			int numberExtraClassWorks = parametersNew.getListConfigurations().get(3).getNumberDefault();
			LineChartSeries extraClassWorkChart = new LineChartSeries();
			extraClassWorkChart.setLabel(label1);
			for(ExtraClassWorkUtil data:extraClassWorks){
				String dateECW = format.format(data.getDateExtraClassWork());
				extraClassWorkChart.set(dateECW, data.getQualificationExtraClassWork());
			}
			
			if(numberExtraClassWorks > 1){
				LineChartSeries extraClassWorkPrediction = new LineChartSeries();
				extraClassWorkPrediction.setLabel(label2);
					for(ExtraClassWorkUtil data:parameters.getExtraClassWorks()){
						String dateECW = format.format(data.getDateExtraClassWork());
						extraClassWorkPrediction.set(dateECW, data.getQualificationExtraClassWork());
					}
					modelExtraClassWork.addSeries(extraClassWorkPrediction);
					modelExtraClassWork.setSeriesColors(lineColorPrediction+",666600");
					if(!parameters.getLowECW()){
						if(extraClassWorks.size() == parameters.getExtraClassWorks().size()){
							messageExtraClassWorkFATAL = msg1+ "DEBERES - TAREAS"+msgComp;
						}else{
							messageExtraClassWorkFATAL = msg1+ "DEBERES - TAREAS"+msg2;
						}
						
					}
					
					if(classWorks.size() == parameters.getExtraClassWorks().size()){
						messageExtraClassWorkComplete = msgComplete+"DEBERES - TAREAS";
					}
			}else{
				messageExtraClassWorkOne = msg3 + "una tarea o deber" + msg4;
			}
			
			modelExtraClassWork.addSeries(extraClassWorkChart);
			modelExtraClassWork.setTitle("DEBERES - TAREAS");
			this.generateYAxisData(modelExtraClassWork,matterName);
			this.generateXAxisData(modelExtraClassWork,idOffer,format);
		}else{
			int numberExtraClassWorks = parametersNew.getListConfigurations().get(3).getNumberDefault();
			if(numberExtraClassWorks == 0){
				messageExtraClassWorkNone = msg5 + "deberes o tareas" + msg6;
			}else{
				messageExtraClassWorkEmpty = msg7 + "deber o tarea" + msg8;
			}
		}
		
		
////////////////////////////////////////////////////////////////////////////////
		
		if(!participations.isEmpty()){
			int numberParticipations = parametersNew.getListConfigurations().get(4).getNumberDefault();
			LineChartSeries participationChart = new LineChartSeries();
			participationChart.setLabel(label1);	
			for(ParticipationUtil data:participations){
				String dateParti = format.format(data.getDateParticipation());
				participationChart.set(dateParti, data.getQualificationParticipation());
			}
			
			if(numberParticipations > 1){
				LineChartSeries participationPrediction = new LineChartSeries();
				participationPrediction.setLabel(label2);
				for(ParticipationUtil data:parameters.getParticipations()){
					String datePart = format.format(data.getDateParticipation());
					participationPrediction.set(datePart, data.getQualificationParticipation());
				
				}

				modelParticipation.addSeries(participationPrediction);
				modelParticipation.setSeriesColors(lineColorPrediction+",666666");
			
				if(!parameters.getLowPart()){
					
					if(participations.size() == parameters.getParticipations().size()){
						messageParticipationFATAL = msg1+ "PARTICIPACIONES EN CLASE"+msgComp;
					}else{
						messageParticipationFATAL = msg1+ "PARTICIPACIONES EN CLASE"+msg2;
					}
					
				}
				
				if(participations.size() == parameters.getParticipations().size()){
					messageParticipationComplete = msgComplete+"PARTICIPACIONES EN CLASE";
				}
					
			}else{
				messageParticipationOne = msg3 + "una participación" + msg4;		
			}
			
			modelParticipation.addSeries(participationChart);
			modelParticipation.setTitle("PARTICIPACIONES EN CLASE");
			this.generateYAxisData(modelParticipation,matterName);
			this.generateXAxisData(modelParticipation,idOffer,format);
		}else{
			int numberParticipations = parametersNew.getListConfigurations().get(4).getNumberDefault();
			
			if(numberParticipations == 0){
				messageParticipationNone = msg5 + "participaciones" + msg6;
			}else{
				messageParticipationEmpty = msg7 + "participación en clase" + msg8;
			}
			
		}
		
////////////////////////////////////////////////////////////////////
		if(!laboratories.isEmpty()){
			int numberLaboratories = parametersNew.getListConfigurations().get(5).getNumberDefault();
			LineChartSeries laboratoryChart = new LineChartSeries();
			laboratoryChart.setLabel(label1);
			for(LaboratoryUtil data:laboratories){
				String dateLab = format.format(data.getDateLaboratory());
				laboratoryChart.set(dateLab, data.getQualificationLaboratory());
			}
			if(numberLaboratories > 1){
				LineChartSeries laboratoryPrediction = new LineChartSeries();
				laboratoryPrediction.setLabel(label2);
				for(LaboratoryUtil data:parameters.getLaboratories()){
					String datePart = format.format(data.getDateLaboratory());
					laboratoryPrediction.set(datePart, data.getQualificationLaboratory());
					
				}
				
				modelLaboratory.addSeries(laboratoryPrediction);
				modelLaboratory.setSeriesColors(lineColorPrediction+",CC0000");
				
				if(!parameters.getLowLab()){
					
					if((laboratories.size()*2) == parameters.getLaboratories().size()){
						messageLaboratoryFATAL = msg1+ "LABORATORIOS - TALLERES"+msgComp;
					}else{
						messageLaboratoryFATAL = msg1+ "LABORATORIOS - TALLERES"+msg2;
					}
					
				}
				
				if((laboratories.size()*2) == parameters.getLaboratories().size()){
					messageLaboratoryComplete = msgComplete+"LABORATORIOS - TALLERES";
				}
			}else{
				messageLaboratoryOne = msg3 + "un laboratorio o taller" + msg4;
				
			}
			
			modelLaboratory.addSeries(laboratoryChart);
			modelLaboratory.setTitle("LABORATORIOS - TALLERES");
			this.generateYAxisData(modelLaboratory,matterName);
			this.generateXAxisData(modelLaboratory,idOffer,format);
		}else{
			int numberLaboratories = parametersNew.getListConfigurations().get(5).getNumberDefault();
			if(numberLaboratories == 0){
				messageLaboratoryNone = msg5 + "laboratorios o talleres" + msg6;
				
			}else{
				messageLaboratoryEmpty = msg7 + "laboratorio o taller" + msg8;
			}
		}
		
	}
	
	public String proccessDialogReal(String param) {
		
		double cumSum =0.0;
		averageReal =0.0;
		int count =0;
		
		if(param.equals("evaluation")){
			for(EvaluationUtil data:evaluations){
				cumSum = cumSum + data.getQualificationEval();
				count++;
			}
			averageReal = cumSum / count;
			title2 = "evaluaciones";
		}
		
		if(param.equals("lesson")){
			for(LessonUtil data:lessons){
				cumSum = cumSum + data.getQualificationLesson();
				count++;
			}
			averageReal = cumSum / count;
			title2 = "lecciones";
		}	
		
		if(param.equals("classWork")){	
			for(ClassWorkUtil data:classWorks){
				cumSum = cumSum + data.getQualificationClassWork();
				count++;
			}
			averageReal = cumSum / count;
			title2 = "trabajos autónomos";
		}
		
		if(param.equals("extraClassWork")){	
			for(ExtraClassWorkUtil data:extraClassWorks){
				cumSum = cumSum + data.getQualificationExtraClassWork();
				count++;
			}
			averageReal = cumSum / count;
			title2 = "deberes - tareas";
		}
		
		if(param.equals("participation")){
			for(ParticipationUtil data:participations){
				cumSum = cumSum + data.getQualificationParticipation();
				count++;
			}
			averageReal = cumSum / count;
			title2 = "participaciones clase";
		}
		
		if(param.equals("laboratory")){	
			for(LaboratoryUtil data:laboratories){
				cumSum = cumSum + data.getQualificationLaboratory();
				count++;
			}
			averageReal = cumSum / count;
			title2 = "laboratorios - talleres";
		}

		
		String result="";
		
		if((averageReal <= 10.00) && (averageReal >= 9.00)) {
			result = "El estudiante domina los aprendizajes requeridos.";
		}
		if((averageReal < 9.00) && (averageReal >= 7)) {
			result = "El estudiante alcanza los aprendizajes requeridos.";
		}		
		if((averageReal < 7.00) && (averageReal >= 4.01)) {
			result = "El estudiante está próximo a alcanzar los aprendizajes requeridos.";
		}
		if((averageReal <= 4.00) && (averageReal >= 0)) {
			result = "El estudiante no alcanza los aprendizajes requeridos.";
		}
		
		return result;
	}
	
	public void viewDialogCustomized(TabChangeEvent tab) {

    	
    	suggestion1 = "";
    	//suggestion2 = "";
    	
    	
    	init();
    	/**recuperacion de parametros*/
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		param = params.get("idParamSuggestion");
    	
		suggestion1 = proccessDialogReal(param);
		//suggestion2 = proccessDialogPrediction(param);

    }

    private void generateXAxisData(LineChartModel model, long idOffer,SimpleDateFormat format) {
		
		/**extiende 30 dias en la fecha final para
		 * presentar correctamente la leyenda*/
		Date newEndDate = operations.addDate(offerDAO.returnOfferById(idOffer).getEndDate(),30);
		
		String startDateStr = format.format(startDate);
		String endDateStr = format.format(newEndDate);
		
		SimpleDateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
		Date dateTemp = offerDAO.returnOfferById(idOffer).getEndDate();
		String offerData = "Del "+format2.format(startDate)+" al "+format2.format(dateTemp);
		
		DateAxis axisDate = new DateAxis("OFERTA ACADÉMICA ("+offerData+")");
		axisDate.setTickAngle(-20);
		axisDate.setMin(startDateStr);
		axisDate.setMax(endDateStr);
		axisDate.setTickFormat("%d-%m-%Y");
		
		model.setLegendPosition("e");
		model.getAxes().put(AxisType.X, axisDate);
	}

	private void generateYAxisData(LineChartModel model,
				String matterName) {
		
		model.getAxis(AxisType.Y).setLabel("PROGRESO ACADÉMICO");
		model.getAxis(AxisType.Y).setMax(10.0);
		model.getAxis(AxisType.Y).setMin(0.0);
		model.getAxis(AxisType.Y).setTickFormat("%.2f");
			
		}

	
	/*INTERACCION CON EVENTOS AJAX*/
	
	/*competencia cognitiva*/
    public void onSlideCognitiveCompetence(SlideEndEvent event) {
        _personalFactor.setCognitiveCompetence(event.getValue());
    } 
    
    /*condiciones cognitivas*/
    public void onSlideCognitiveConditions(SlideEndEvent event) {
    	_personalFactor.setCognitiveConditions(event.getValue());
    }
    
    /*asistencia a clases*/
    public void onSlideClassesAssistance(SlideEndEvent event){
    	personalFactor.setClassesAssistance(event.getValue());
    }
    /*inteligencia*/
    public void onSlideIntelligence(SlideEndEvent event){
    	__personalFactor.setIntelligence(event.getValue());
    }
    
    /*preparación en colegio*/
    public void onSlideAcademicTrainingSchool(SlideEndEvent event){
    	__personalFactor.setAcademicTrainingSchool(event.getValue());
    }
    
    /*IFac Complejidad carrera*/
    public void onSlideComplexityCareer(SlideEndEvent event){
    	institutionalFactor.setComplexityMatter(event.getValue());
    }
    
    /*IFac estado institucion*/
    public void onSlideInstitutionalStatus(SlideEndEvent event){
    	_institutionalFactor.setInstitutionalStatus(event.getValue());
    }
       
    /*para todos los select One button*/
    public void onSelectOneButtonRefresh(){
    	
    }
	
    /*para los spinners*/
    public void onSpinnerRefresh(){

    }
	
	
	/*REDIRECCIONES*/
	public String redirectStudentMain(){
		return "student_main";
	}

	public String redirectGraph(){
		suggestionActiveIndex = 0;
		return "student_view_graph";
	}
	
	public void loadSelectedSuggesionTest(){
		
		suggestionActiveIndex = 4;
		//String route = redirectSuggestion();
		redirectSuggestionNew();
		//return route;
	}
	
	public String redirectSuggestionNew(){
		listReferenceBlog = new ArrayList<ReferenceLink>();
		listReferenceVideo = new ArrayList<ReferenceLink>();
		listReferenceNews = new ArrayList<ReferenceLink>();
		listReferenceWebPage = new ArrayList<ReferenceLink>();
		listReferenceTestIQ = new ArrayList<ReferenceLink>();
		
		
		JSONArray array = operations.processJSON(restClient.execute());
		
		for(int i=0; i<array.size();i++){
			JSONObject jsonObject = operations.processJSONObject(array.get(i)+"");
			ReferenceLink rl = new ReferenceLink();
			rl.setCategory(jsonObject.get("category")+"");
			rl.setDescription(jsonObject.get("description")+"");
			rl.setLocation(jsonObject.get("location")+"");
			rl.setName(jsonObject.get("name")+"");
			
			if(rl.getCategory().equals("video")){
				listReferenceVideo.add(rl);
			}
			if(rl.getCategory().equals("blog")){
				listReferenceBlog.add(rl);
			}
			if(rl.getCategory().equals("news")){
				listReferenceNews.add(rl);
			}
			if(rl.getCategory().equals("web page")){
				listReferenceWebPage.add(rl);
			}
			
			
			if(rl.getCategory().equals("test")){
				listReferenceTestIQ.add(rl);
			}
			
		}
		
		
		return "suggestion";
	}
	
	public String redirectSuggestion(){
		suggestionActiveIndex = 0;
		String route = redirectSuggestionNew();
		return route;
	}
	
	public String redirectStudentSelectData(){
		studentSelectorController.clearData();
		return "student_select_data";
	}
	
	public String atras(){
		return "student_main";
	}
	
	public String redirectDetailReg(){
		return "student_datil_reg";
	}
	
	private int isLoadMatter;
	
	
	
	
	
	/*MENSAJES*/
	
	public int getIsLoadMatter() {
		return isLoadMatter;
	}

	public void setIsLoadMatter(int isLoadMatter) {
		this.isLoadMatter = isLoadMatter;
	}

	public void msgFatal(String message){
	   	 facesMessage = new FacesMessage(FacesMessage.SEVERITY_FATAL,"Error", message);
	     faceContext.addMessage(null, facesMessage);
	}
  
	public void msgError(String message){
  	   	facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",message);
  	   	faceContext.addMessage(null, facesMessage);
	}
  
	public void msgInfo(String message){
		facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", message);
		faceContext.addMessage(null, facesMessage);
	}

	
	/*getters - setters*/
   
	public AcademicDAO getAcademicDAO() {
		return academicDAO;
	}
	public void setAcademicDAO(AcademicDAO academicDAO) {
		this.academicDAO = academicDAO;
	}
	public Operations getOperations() {
		return operations;
	}
	public void setOperations(Operations operations) {
		this.operations = operations;
	}
	public long getOfferSelect() {
		return offerSelect;
	}
	public void setOfferSelect(long offerSelect) {
		this.offerSelect = offerSelect;
	}
	public long getCareerSelect() {
		return careerSelect;
	}
	public void setCareerSelect(long careerSelect) {
		this.careerSelect = careerSelect;
	}
	public long getModuleSelect() {
		return moduleSelect;
	}
	public void setModuleSelect(long moduleSelect) {
		this.moduleSelect = moduleSelect;
	}
	public long getIdStudent() {
		return idStudent;
	}
	public void setIdStudent(long idStudent) {
		this.idStudent = idStudent;
	}
	public List<StudentMatterUtil> getListStudentMatter() {
		return listStudentMatter;
	}
	public void setListStudentMatter(List<StudentMatterUtil> listStudentMatter) {
		this.listStudentMatter = listStudentMatter;
	}
	public long getIdMatter() {
		return idMatter;
	}
	public void setIdMatter(long idMatter) {
		this.idMatter = idMatter;
	}
	public String getMatterName() {
		return matterName;
	}
	public void setMatterName(String matterName) {
		this.matterName = matterName;
	}
	public List<EvaluationUtil> getEvaluations() {
		return evaluations;
	}
	public void setEvaluations(List<EvaluationUtil> evaluations) {
		this.evaluations = evaluations;
	}
	public List<LessonUtil> getLessons() {
		return lessons;
	}
	public void setLessons(List<LessonUtil> lessons) {
		this.lessons = lessons;
	}
	public List<ClassWorkUtil> getClassWorks() {
		return classWorks;
	}
	public void setClassWorks(List<ClassWorkUtil> classWorks) {
		this.classWorks = classWorks;
	}
	public List<ExtraClassWorkUtil> getExtraClassWorks() {
		return extraClassWorks;
	}
	public void setExtraClassWorks(List<ExtraClassWorkUtil> extraClassWorks) {
		this.extraClassWorks = extraClassWorks;
	}
	public List<ParticipationUtil> getParticipations() {
		return participations;
	}
	public void setParticipations(List<ParticipationUtil> participations) {
		this.participations = participations;
	}
	public List<LaboratoryUtil> getLaboratories() {
		return laboratories;
	}
	public void setLaboratories(List<LaboratoryUtil> laboratories) {
		this.laboratories = laboratories;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public LineChartModel getModelEvaluation() {
		return modelEvaluation;
	}
	public void setModelEvaluation(LineChartModel modelEvaluation) {
		this.modelEvaluation = modelEvaluation;
	}
	public LineChartModel getModelLesson() {
		return modelLesson;
	}
	public void setModelLesson(LineChartModel modelLesson) {
		this.modelLesson = modelLesson;
	}
	public LineChartModel getModelClassWork() {
		return modelClassWork;
	}
	public void setModelClassWork(LineChartModel modelClassWork) {
		this.modelClassWork = modelClassWork;
	}
	public LineChartModel getModelExtraClassWork() {
		return modelExtraClassWork;
	}
	public void setModelExtraClassWork(LineChartModel modelExtraClassWork) {
		this.modelExtraClassWork = modelExtraClassWork;
	}
	public LineChartModel getModelParticipation() {
		return modelParticipation;
	}
	public void setModelParticipation(LineChartModel modelParticipation) {
		this.modelParticipation = modelParticipation;
	}
	public LineChartModel getModelLaboratory() {
		return modelLaboratory;
	}
	public void setModelLaboratory(LineChartModel modelLaboratory) {
		this.modelLaboratory = modelLaboratory;
	}
	public String getMessageEvaluationOK() {
		return messageEvaluationOK;
	}
	public void setMessageEvaluationOK(String messageEvaluationOK) {
		this.messageEvaluationOK = messageEvaluationOK;
	}
	public String getMessageEvaluationFATAL() {
		return messageEvaluationFATAL;
	}
	public void setMessageEvaluationFATAL(String messageEvaluationFATAL) {
		this.messageEvaluationFATAL = messageEvaluationFATAL;
	}
	public Parameter getParameters() {
		return parameters;
	}
	public void setParameters(Parameter parameters) {
		this.parameters = parameters;
	}
	public String getMessageLessonOK() {
		return messageLessonOK;
	}
	public void setMessageLessonOK(String messageLessonOK) {
		this.messageLessonOK = messageLessonOK;
	}
	public String getMessageLessonFATAL() {
		return messageLessonFATAL;
	}
	public void setMessageLessonFATAL(String messageLessonFATAL) {
		this.messageLessonFATAL = messageLessonFATAL;
	}
	public String getMessageClassWorkOK() {
		return messageClassWorkOK;
	}
	public void setMessageClassWorkOK(String messageClassWorkOK) {
		this.messageClassWorkOK = messageClassWorkOK;
	}
	public String getMessageClassWorkFATAL() {
		return messageClassWorkFATAL;
	}
	public void setMessageClassWorkFATAL(String messageClassWorkFATAL) {
		this.messageClassWorkFATAL = messageClassWorkFATAL;
	}
	public String getMessageExtraClassWorkOK() {
		return messageExtraClassWorkOK;
	}
	public void setMessageExtraClassWorkOK(String messageExtraClassWorkOK) {
		this.messageExtraClassWorkOK = messageExtraClassWorkOK;
	}
	public String getMessageExtraClassWorkFATAL() {
		return messageExtraClassWorkFATAL;
	}
	public void setMessageExtraClassWorkFATAL(String messageExtraClassWorkFATAL) {
		this.messageExtraClassWorkFATAL = messageExtraClassWorkFATAL;
	}
	public String getMessageParticipationOK() {
		return messageParticipationOK;
	}
	public void setMessageParticipationOK(String messageParticipationOK) {
		this.messageParticipationOK = messageParticipationOK;
	}
	public String getMessageParticipationFATAL() {
		return messageParticipationFATAL;
	}
	public void setMessageParticipationFATAL(String messageParticipationFATAL) {
		this.messageParticipationFATAL = messageParticipationFATAL;
	}
	public String getMessageLaboratoryOK() {
		return messageLaboratoryOK;
	}
	public void setMessageLaboratoryOK(String messageLaboratoryOK) {
		this.messageLaboratoryOK = messageLaboratoryOK;
	}
	public String getMessageLaboratoryFATAL() {
		return messageLaboratoryFATAL;
	}
	public void setMessageLaboratoryFATAL(String messageLaboratoryFATAL) {
		this.messageLaboratoryFATAL = messageLaboratoryFATAL;
	}
	public String getMessageLessonEmpty() {
		return messageLessonEmpty;
	}
	public void setMessageLessonEmpty(String messageLessonEmpty) {
		this.messageLessonEmpty = messageLessonEmpty;
	}
	public String getMessageEvaluationEmpty() {
		return messageEvaluationEmpty;
	}
	public void setMessageEvaluationEmpty(String messageEvaluationEmpty) {
		this.messageEvaluationEmpty = messageEvaluationEmpty;
	}
	public String getMessageLessonOne() {
		return messageLessonOne;
	}
	public void setMessageLessonOne(String messageLessonOne) {
		this.messageLessonOne = messageLessonOne;
	}
	public String getMessageEvaluationOne() {
		return messageEvaluationOne;
	}
	public void setMessageEvaluationOne(String messageEvaluationOne) {
		this.messageEvaluationOne = messageEvaluationOne;
	}
	public String getMessageClassWorkOne() {
		return messageClassWorkOne;
	}
	public void setMessageClassWorkOne(String messageClassWorkOne) {
		this.messageClassWorkOne = messageClassWorkOne;
	}
	public String getMessageExtraClassWorkOne() {
		return messageExtraClassWorkOne;
	}
	public void setMessageExtraClassWorkOne(String messageExtraClassWorkOne) {
		this.messageExtraClassWorkOne = messageExtraClassWorkOne;
	}
	public String getMessageParticipationOne() {
		return messageParticipationOne;
	}
	public void setMessageParticipationOne(String messageParticipationOne) {
		this.messageParticipationOne = messageParticipationOne;
	}
	public String getMessageLaboratoryOne() {
		return messageLaboratoryOne;
	}
	public void setMessageLaboratoryOne(String messageLaboratoryOne) {
		this.messageLaboratoryOne = messageLaboratoryOne;
	}
	public String getMessageClassWorkEmpty() {
		return messageClassWorkEmpty;
	}
	public void setMessageClassWorkEmpty(String messageClassWorkEmpty) {
		this.messageClassWorkEmpty = messageClassWorkEmpty;
	}
	public String getMessageExtraClassWorkEmpty() {
		return messageExtraClassWorkEmpty;
	}
	public void setMessageExtraClassWorkEmpty(String messageExtraClassWorkEmpty) {
		this.messageExtraClassWorkEmpty = messageExtraClassWorkEmpty;
	}
	public String getMessageParticipationEmpty() {
		return messageParticipationEmpty;
	}
	public void setMessageParticipationEmpty(String messageParticipationEmpty) {
		this.messageParticipationEmpty = messageParticipationEmpty;
	}
	public String getMessageLaboratoryEmpty() {
		return messageLaboratoryEmpty;
	}
	public void setMessageLaboratoryEmpty(String messageLaboratoryEmpty) {
		this.messageLaboratoryEmpty = messageLaboratoryEmpty;
	}
	public String getMessageLessonNone() {
		return messageLessonNone;
	}
	public void setMessageLessonNone(String messageLessonNone) {
		this.messageLessonNone = messageLessonNone;
	}
	public String getMessageEvaluationNone() {
		return messageEvaluationNone;
	}
	public void setMessageEvaluationNone(String messageEvaluationNone) {
		this.messageEvaluationNone = messageEvaluationNone;
	}
	public String getMessageClassWorkNone() {
		return messageClassWorkNone;
	}
	public void setMessageClassWorkNone(String messageClassWorkNone) {
		this.messageClassWorkNone = messageClassWorkNone;
	}
	public String getMessageExtraClassWorkNone() {
		return messageExtraClassWorkNone;
	}
	public void setMessageExtraClassWorkNone(String messageExtraClassWorkNone) {
		this.messageExtraClassWorkNone = messageExtraClassWorkNone;
	}
	public String getMessageParticipationNone() {
		return messageParticipationNone;
	}
	public void setMessageParticipationNone(String messageParticipationNone) {
		this.messageParticipationNone = messageParticipationNone;
	}
	public String getMessageLaboratoryNone() {
		return messageLaboratoryNone;
	}
	public void setMessageLaboratoryNone(String messageLaboratoryNone) {
		this.messageLaboratoryNone = messageLaboratoryNone;
	}
	public List<ConfigurationUtil> getListConf() {
		return listConf;
	}
	public void setListConf(List<ConfigurationUtil> listConf) {
		this.listConf = listConf;
	}
	public String getEam() {
		return eam;
	}
	public void setEam(String eam) {
		this.eam = eam;
	}
	public double getEvaluationAverage() {
		return evaluationAverage;
	}
	public void setEvaluationAverage(double evaluationAverage) {
		this.evaluationAverage = evaluationAverage;
	}
	public String getLeam() {
		return leam;
	}
	public void setLeam(String leam) {
		this.leam = leam;
	}
	public String getCwam() {
		return cwam;
	}
	public void setCwam(String cwam) {
		this.cwam = cwam;
	}
	public String getEcwam() {
		return ecwam;
	}
	public void setEcwam(String ecwam) {
		this.ecwam = ecwam;
	}
	public String getPam() {
		return pam;
	}
	public void setPam(String pam) {
		this.pam = pam;
	}
	public String getLam() {
		return lam;
	}
	public void setLam(String lam) {
		this.lam = lam;
	}
	public double getLessonAverage() {
		return lessonAverage;
	}
	public void setLessonAverage(double lessonAverage) {
		this.lessonAverage = lessonAverage;
	}
	public double getClassWorkAverage() {
		return classWorkAverage;
	}
	public void setClassWorkAverage(double classWorkAverage) {
		this.classWorkAverage = classWorkAverage;
	}
	public double getExtraClassWorkAverage() {
		return extraClassWorkAverage;
	}

	public void setExtraClassWorkAverage(double extraClassWorkAverage) {
		this.extraClassWorkAverage = extraClassWorkAverage;
	}
	public double getParticipationAverage() {
		return participationAverage;
	}
	public void setParticipationAverage(double participationAverage) {
		this.participationAverage = participationAverage;
	}
	public double getLaboratoryAverage() {
		return laboratoryAverage;
	}
	public void setLaboratoryAverage(double laboratoryAverage) {
		this.laboratoryAverage = laboratoryAverage;
	}
	public String getEam2() {
		return eam2;
	}
	public void setEam2(String eam2) {
		this.eam2 = eam2;
	}
	public String getLeam2() {
		return leam2;
	}
	public void setLeam2(String leam2) {
		this.leam2 = leam2;
	}
	public String getCwam2() {
		return cwam2;
	}
	public void setCwam2(String cwam2) {
		this.cwam2 = cwam2;
	}
	public String getEcwam2() {
		return ecwam2;
	}
	public void setEcwam2(String ecwam2) {
		this.ecwam2 = ecwam2;
	}
	public String getPam2() {
		return pam2;
	}
	public void setPam2(String pam2) {
		this.pam2 = pam2;
	}
	public String getLam2() {
		return lam2;
	}
	public void setLam2(String lam2) {
		this.lam2 = lam2;
	}
	public String getEam3() {
		return eam3;
	}
	public void setEam3(String eam3) {
		this.eam3 = eam3;
	}
	public String getLeam3() {
		return leam3;
	}
	public void setLeam3(String leam3) {
		this.leam3 = leam3;
	}
	public String getCwam3() {
		return cwam3;
	}
	public void setCwam3(String cwam3) {
		this.cwam3 = cwam3;
	}
	public String getEcwam3() {
		return ecwam3;
	}
	public void setEcwam3(String ecwam3) {
		this.ecwam3 = ecwam3;
	}
	public String getPam3() {
		return pam3;
	}
	public void setPam3(String pam3) {
		this.pam3 = pam3;
	}
	public String getLam3() {
		return lam3;
	}
	public void setLam3(String lam3) {
		this.lam3 = lam3;
	}
	public double getTotalAverage() {
		return totalAverage;
	}
	public void setTotalAverage(double totalAverage) {
		this.totalAverage = totalAverage;
	}
	public double getEvaluationPartialAverage() {
		return evaluationPartialAverage;
	}
	public void setEvaluationPartialAverage(double evaluationPartialAverage) {
		this.evaluationPartialAverage = evaluationPartialAverage;
	}
	public double getLessonPartialAverage() {
		return lessonPartialAverage;
	}
	public void setLessonPartialAverage(double lessonPartialAverage) {
		this.lessonPartialAverage = lessonPartialAverage;
	}
	public double getClassWorkPartialAverage() {
		return classWorkPartialAverage;
	}
	public void setClassWorkPartialAverage(double classWorkPartialAverage) {
		this.classWorkPartialAverage = classWorkPartialAverage;
	}
	public double getExtraClassWorkPartialAverage() {
		return extraClassWorkPartialAverage;
	}
	public void setExtraClassWorkPartialAverage(double extraClassWorkPartialAverage) {
		this.extraClassWorkPartialAverage = extraClassWorkPartialAverage;
	}
	public double getParticipationPartialAverage() {
		return participationPartialAverage;
	}
	public void setParticipationPartialAverage(double participationPartialAverage) {
		this.participationPartialAverage = participationPartialAverage;
	}
	public double getLaboratoryPartialAverage() {
		return laboratoryPartialAverage;
	}

	public void setLaboratoryPartialAverage(double laboratoryPartialAverage) {
		this.laboratoryPartialAverage = laboratoryPartialAverage;
	}

	public String getSuggestion1() {
		return suggestion1;
	}

	public void setSuggestion1(String suggestion1) {
		this.suggestion1 = suggestion1;
	}

	/*
	public String getSuggestion2() {
		return suggestion2;
	}

	public void setSuggestion2(String suggestion2) {
		this.suggestion2 = suggestion2;
	}
*/
	public String getTitle2() {
		return title2;
	}

	public void setTitle2(String title2) {
		this.title2 = title2;
	}

	public double getAverageReal() {
		return averageReal;
	}

	public void setAverageReal(double averageReal) {
		this.averageReal = averageReal;
	}

	public double getAveragePred() {
		return averagePred;
	}

	public void setAveragePred(double averagePred) {
		this.averagePred = averagePred;
	}

	public String getMsgEvalLessPart() {
		return msgEvalLessPart;
	}

	public void setMsgEvalLessPart(String msgEvalLessPart) {
		this.msgEvalLessPart = msgEvalLessPart;
	}

	public String getMsgCwEcwLab() {
		return msgCwEcwLab;
	}

	public void setMsgCwEcwLab(String msgCwEcwLab) {
		this.msgCwEcwLab = msgCwEcwLab;
	}

	public String getMsgPart2() {
		return msgPart2;
	}

	public void setMsgPart2(String msgPart2) {
		this.msgPart2 = msgPart2;
	}

	public String getMsgFinal() {
		return msgFinal;
	}

	public void setMsgFinal(String msgFinal) {
		this.msgFinal = msgFinal;
	}


	public List<ReferenceLink> getListReferenceBlog() {
		return listReferenceBlog;
	}

	public void setListReferenceBlog(List<ReferenceLink> listReferenceBlog) {
		this.listReferenceBlog = listReferenceBlog;
	}

	public List<ReferenceLink> getListReferenceVideo() {
		return listReferenceVideo;
	}

	public void setListReferenceVideo(List<ReferenceLink> listReferenceVideo) {
		this.listReferenceVideo = listReferenceVideo;
	}

	public List<ReferenceLink> getListReferenceNews() {
		return listReferenceNews;
	}

	public void setListReferenceNews(List<ReferenceLink> listReferenceNews) {
		this.listReferenceNews = listReferenceNews;
	}

	public List<ReferenceLink> getListReferenceWebPage() {
		return listReferenceWebPage;
	}

	public void setListReferenceWebPage(List<ReferenceLink> listReferenceWebPage) {
		this.listReferenceWebPage = listReferenceWebPage;
	}

	public String getMessageEvaluationComplete() {
		return messageEvaluationComplete;
	}

	public void setMessageEvaluationComplete(String messageEvaluationComplete) {
		this.messageEvaluationComplete = messageEvaluationComplete;
	}

	public String getMessageLessonComplete() {
		return messageLessonComplete;
	}

	public void setMessageLessonComplete(String messageLessonComplete) {
		this.messageLessonComplete = messageLessonComplete;
	}

	public String getMessageClassWorkComplete() {
		return messageClassWorkComplete;
	}

	public void setMessageClassWorkComplete(String messageClassWorkComplete) {
		this.messageClassWorkComplete = messageClassWorkComplete;
	}

	public String getMessageExtraClassWorkComplete() {
		return messageExtraClassWorkComplete;
	}

	public void setMessageExtraClassWorkComplete(
			String messageExtraClassWorkComplete) {
		this.messageExtraClassWorkComplete = messageExtraClassWorkComplete;
	}

	public String getMessageParticipationComplete() {
		return messageParticipationComplete;
	}

	public void setMessageParticipationComplete(String messageParticipationComplete) {
		this.messageParticipationComplete = messageParticipationComplete;
	}

	public String getMessageLaboratoryComplete() {
		return messageLaboratoryComplete;
	}

	public void setMessageLaboratoryComplete(String messageLaboratoryComplete) {
		this.messageLaboratoryComplete = messageLaboratoryComplete;
	}

	public List<PredictionParameterUtil> getListConfStudent() {
		return listConfStudent;
	}

	public void setListConfStudent(List<PredictionParameterUtil> listConfStudent) {
		this.listConfStudent = listConfStudent;
	}



	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public PersonalFactor getPersonalFactor() {
		return personalFactor;
	}

	public void setPersonalFactor(PersonalFactor personalFactor) {
		this.personalFactor = personalFactor;
	}

	public SocialFactor getSocialFactor() {
		return socialFactor;
	}

	public void setSocialFactor(SocialFactor socialFactor) {
		this.socialFactor = socialFactor;
	}

	public InstitutionalFactor getInstitutionalFactor() {
		return institutionalFactor;
	}

	public void setInstitutionalFactor(InstitutionalFactor institutionalFactor) {
		this.institutionalFactor = institutionalFactor;
	}

	public int getActiveTabIndex() {
		return activeTabIndex;
	}

	public void setActiveTabIndex(int activeTabIndex) {
		this.activeTabIndex = activeTabIndex;
	}

	public String getTitleButtonhelp() {
		/*boton de ayuda que aparece
		 * junto al text*/
		titleButtonhelp = "¿Más información sobre este parámetro?";
		return titleButtonhelp;
	}

	public void setTitleButtonhelp(String titleButtonhelp) {
		this.titleButtonhelp = titleButtonhelp;
	}

	public String getTextHelp1() {
		textHelp1 = "* La información proporcionada no será publicada bajo ningún motivo.";
		return textHelp1;
	}

	public void setTextHelp1(String textHelp1) {
		this.textHelp1 = textHelp1;
	}

	public String getTexthelp2() {
		texthelp2 = "* En caso de necesitar más información haz clic sobre el texto de cualquier parámetro";
		return texthelp2;
	}

	public void setTexthelp2(String texthelp2) {
		this.texthelp2 = texthelp2;
	}

	public String getTextLinkAdjustPrediction() {
		textLinkAdjustPrediction = "Ajustar los parámetros que influyen en esta predicción";
		return textLinkAdjustPrediction;
	}

	public void setTextLinkAdjustPrediction(String textLinkAdjustPrediction) {
		this.textLinkAdjustPrediction = textLinkAdjustPrediction;
	}
	
	public int getActiveTabIndexAccordion() {
		return activeTabIndexAccordion;
	}

	public void setActiveTabIndexAccordion(int activeTabIndexAccordion) {
		this.activeTabIndexAccordion = activeTabIndexAccordion;
	}

	public int getActiveTabIndexLess() {
		return activeTabIndexLess;
	}

	public void setActiveTabIndexLess(int activeTabIndexLess) {
		this.activeTabIndexLess = activeTabIndexLess;
	}

	public int getActiveTabIndexCWs() {
		return activeTabIndexCWs;
	}

	public void setActiveTabIndexCWs(int activeTabIndexCWs) {
		this.activeTabIndexCWs = activeTabIndexCWs;
	}

	public int getActiveTabIndexECWs() {
		return activeTabIndexECWs;
	}

	public void setActiveTabIndexECWs(int activeTabIndexECWs) {
		this.activeTabIndexECWs = activeTabIndexECWs;
	}

	public int getActiveTabIndexParts() {
		return activeTabIndexParts;
	}

	public void setActiveTabIndexParts(int activeTabIndexParts) {
		this.activeTabIndexParts = activeTabIndexParts;
	}

	public int getActiveTabIndexLabs() {
		return activeTabIndexLabs;
	}

	public void setActiveTabIndexLabs(int activeTabIndexLabs) {
		this.activeTabIndexLabs = activeTabIndexLabs;
	}

	public int getActiveTabIndexEvals() {
		return activeTabIndexEvals;
	}

	public void setActiveTabIndexEvals(int activeTabIndexEvals) {
		this.activeTabIndexEvals = activeTabIndexEvals;
	}

	public int getSuggestionActiveIndex() {
		return suggestionActiveIndex;
	}

	public void setSuggestionActiveIndex(int suggestionActiveIndex) {
		this.suggestionActiveIndex = suggestionActiveIndex;
	}

	public List<ReferenceLink> getListReferenceTestIQ() {
		return listReferenceTestIQ;
	}

	public void setListReferenceTestIQ(List<ReferenceLink> listReferenceTestIQ) {
		this.listReferenceTestIQ = listReferenceTestIQ;
	}

	public _PersonalFactor get_personalFactor() {
		return _personalFactor;
	}

	public void set_personalFactor(_PersonalFactor _personalFactor) {
		this._personalFactor = _personalFactor;
	}

	public __PersonalFactor get__personalFactor() {
		return __personalFactor;
	}

	public void set__personalFactor(__PersonalFactor __personalFactor) {
		this.__personalFactor = __personalFactor;
	}

	public _InstitutionalFactor get_institutionalFactor() {
		return _institutionalFactor;
	}

	public void set_institutionalFactor(_InstitutionalFactor _institutionalFactor) {
		this._institutionalFactor = _institutionalFactor;
	}

	
	



	
	
	/*
	public String proccessDialogPrediction(String param) {
		double cumSum =0.0;
		averagePred =0.0;
		int count =0;
		String result="";
		
		title2 = "";
		
		if(param.equals("evaluation")){
			
			for(EvaluationUtil data:parameters.getEvaluations()){
				cumSum = cumSum + data.getQualificationEval();
				count++;
			}
			averagePred = cumSum / count;
			title2 = "evaluaciones";
		}
		
		if(param.equals("lesson")){
			
			for(LessonUtil data:parameters.getLessons()){
				cumSum = cumSum + data.getQualificationLesson();
				count++;
			}
			averagePred = cumSum / count;
			title2 = "lecciones";
		}
		
		if(param.equals("classWork")){
			
			for(ClassWorkUtil data:parameters.getClassWorks()){
				cumSum = cumSum + data.getQualificationClassWork();
				count++;
			}
			averagePred = cumSum / count;
			title2 = "trabajos autónomos";
		}
		
		if(param.equals("extraClassWork")){
			
			for(ExtraClassWorkUtil data:parameters.getExtraClassWorks()){
				cumSum = cumSum + data.getQualificationExtraClassWork();
				count++;
			}
			averagePred = cumSum / count;
			title2 = "deberes - tareas";
		}
		
		if(param.equals("participation")){
			
			for(ParticipationUtil data:parameters.getParticipations()){
				cumSum = cumSum + data.getQualificationParticipation();
				count++;
			}
			averagePred = cumSum / count;
			title2 = "participaciones";
		}
		
		if(param.equals("laboratory")){
			
			for(LaboratoryUtil data:parameters.getLaboratories()){
				cumSum = cumSum + data.getQualificationLaboratory();
				count++;
			}
			averagePred = cumSum / count;
			title2 = "laboratorios - talleres";
		}
		
		if((averagePred <= 10.00) && (averagePred >= 9.00)) {
			result = "Domina los aprendizajes requeridos.";
		}
		if((averagePred < 9.00) && (averagePred >= 7)) {
			result = "Alcanza los aprendizajes requeridos.";
		}		
		if((averagePred < 7.00) && (averagePred >= 4.01)) {
			result = "Está próximo a alcanzar los aprendizajes requeridos.";
		}
		if((averagePred <= 4.00) && (averagePred >= 0)) {
			result = "No alcanza los aprendizajes requeridos.";
		}
		
		return result;
	}
*/
	
	/*
    public void viewDialogFatal(){
    	suggestion1 = "";
    	suggestion2 = "";
    	
    	init();
    	//recuperacion de parametros
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String param = params.get("idParamSuggestion");
		
		
		String msj = "Se recomienda mejorar el promedio en el resto de parametros calificables para elevar su promedio general.";
		
		if(param.equals("evaluation")){
			suggestion1 = proccessDialogReal(param);
			title2 = "evaluaciones";
		}
		
		if(param.equals("lesson")){
			suggestion1 = proccessDialogReal(param);
			title2 = "lecciones";
		}	
		
		if(param.equals("classWork")){	
			suggestion1 = proccessDialogReal(param);
			title2 = "trabajos autónomos";
		}
		
		if(param.equals("extraClassWork")){	
			suggestion1 = proccessDialogReal(param);
			title2 = "deberes - tareas";
		}
		
		if(param.equals("participation")){
			suggestion1 = proccessDialogReal(param);
			title2 = "participaciones clase";
		}
		
		if(param.equals("laboratory")){	
			suggestion1 = proccessDialogReal(param);
			title2 = "laboratorios - talleres";
		}
    	
    	
    	suggestion2 = msj;
    	
    }
	*/
	
}
