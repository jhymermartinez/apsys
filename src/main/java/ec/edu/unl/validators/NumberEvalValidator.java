package ec.edu.unl.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class NumberEvalValidator implements Validator{

	
	public void validate(FacesContext arg0, UIComponent arg1, Object obj)
			throws ValidatorException {
		
		
		Integer num = (Integer) obj;
		
		
		if(num < 1){
			  throw new ValidatorException(new FacesMessage(
	            		FacesMessage.SEVERITY_ERROR, "Error", "La cantidad de evaluaciones no puede ser menor a 1"));
	        
		}
		
	}

}
