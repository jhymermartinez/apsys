package ec.edu.unl.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class DecimalValidator implements Validator{

	
	public void validate(FacesContext arg0, UIComponent arg1, Object obj)
			throws ValidatorException {
		
		
		Double num = (Double) obj;
		String a = num+"";
	    String b = a.replace(".", "#");
	    String[] c = b.split("#");
	    
	    
		
		if(c[1].length()>2){
			  throw new ValidatorException(new FacesMessage(
	            		FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar como máximo 2 decimales"));
	        
		}
		
		if(num < 0){
			  throw new ValidatorException(new FacesMessage(
	            		FacesMessage.SEVERITY_ERROR, "Error", "No se permite ingresar números negativos"));
	        
		}
		
	}

}
