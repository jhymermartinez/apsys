package ec.edu.unl.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class RangeValidatorNew implements Validator{

	
	public void validate(FacesContext arg0, UIComponent arg1, Object obj)
			throws ValidatorException {
		
			

			
			double valor =0.0;
			
			try {
				valor = Double.valueOf(obj+"");
			} catch (Exception e) {
				 throw new ValidatorException(new FacesMessage(
		            		FacesMessage.SEVERITY_ERROR, "Error", "Se debe ingresar valores numéricos. Ejem: 7.5"));
			}
	
			if (!((valor >= 0.0) & (valor <= 10.0))) {
	            throw new ValidatorException(new FacesMessage(
	            		FacesMessage.SEVERITY_ERROR, "Error", "El valor debe estar entre 0.0 y 10.0. Corrija la cantidad caso contrario se almacenara el valor anterior"));
	        
	        }
	

		
	}

}
