package ec.edu.unl.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class PercentageValidator implements Validator{

	
	public void validate(FacesContext arg0, UIComponent arg1, Object obj)
			throws ValidatorException {
		
			

			
		

	        double valor = (Double) obj;
	        
	        if (!((valor >= 0.0) & (valor <= 1.0))) {
	            throw new ValidatorException(new FacesMessage(
	            		FacesMessage.SEVERITY_ERROR, "Error", "El valor debe estar entre 0.0 y 1.0 "));
	        
	        }
		
		
		
	}

}
